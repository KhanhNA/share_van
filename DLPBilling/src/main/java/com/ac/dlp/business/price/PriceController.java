package com.ac.dlp.business.price;

import com.ac.dlp.business.price.cache.CommonCache;
import com.ac.dlp.config.DroolsBeanFactory;
import com.ac.dlp.entity.*;
import org.kie.api.runtime.KieSession;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/app/v1")
public class PriceController {
    public static final String GLOBAL_BILL_DETAIL = "Bill_detail_Holder";

    @PostMapping("/price/calc")
//    @PreAuthorize("hasAuthority('/price/calc')")
    public ResponseEntity<List<BillLadingDetail>> createBanner(@RequestBody List<BillLadingDetail> requests) {
//        PriceDrools.fireDrools(requests);
        DistanceCompute distanceCompute = CommonCache.getCacheHubPrice("DP000001", "HB000003");
        KieSession kieSession = new DroolsBeanFactory().getKieSession();
//        kieSession.setGlobal(GLOBAL_BILL_DETAIL, requests);
        for (BillLadingDetail detail : requests) {
            detail.index1 = 0;
            double extra_price = 0.0;
            for (AreaDistance areaDistance : detail.getAreaDistances()) {
                areaDistance.setParent(detail);
                kieSession.insert(areaDistance);
            }
        }
        kieSession.fireAllRules();
        return ResponseEntity.ok(requests);
    }

    @PostMapping("/price/calc/bill")
    public ResponseEntity<BillLading> createBannerBill(@RequestBody BillLading requests) {
        DistanceCompute distanceCompute = CommonCache.getCacheHubPrice("DP000001", "HB000003");
        KieSession kieSession = new DroolsBeanFactory().getKieSession();
        requests.setTotal_amount(0.0);
        for (BillLadingDetail detail : requests.getArrBillLadingDetail()) {
            detail.index1 = 0;
            detail.setPrice(0.0);
            System.out.println(detail.getZone_area_id());
            for (AreaDistance areaDistance : detail.getAreaDistances()) {
                areaDistance.setParent(detail);
                kieSession.insert(areaDistance);
            }
            kieSession.insert(detail);
        }
        kieSession.insert(requests);
        kieSession.fireAllRules();
        return ResponseEntity.ok(requests);
    }
}