package com.ac.dlp.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "sharevan_product_type")
@Data
public class ProductType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "name_seq")
    private String name_seq;

    @Column(name = "status")
    private String status;

    @Column(name = "extra_price")
    private Double extra_price;

    @Column(name = "type")
    private String type;
}
