package com.ac.dlp.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "sharevan_bill_order_package")
@Data
@NoArgsConstructor()
public class OrderPackage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "extra_cost")
    private Double extraCost;

    @Column(name = "extra_amount_percent")
    private Double extraAmountPercent;

    @Column(name = "type")
    private String type;

    @Column(name = "status")
    private String status;

    @Column(name = "description")
    private String description;


}
