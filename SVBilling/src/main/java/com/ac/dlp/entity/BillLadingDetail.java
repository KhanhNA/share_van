package com.ac.dlp.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "sharevan_bill_lading_detail")
@Data
@NoArgsConstructor()
public class BillLadingDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name_seq")
    private String nameSeq;
    @Column(name = "bill_Lading_Id")
    private Integer billLadingId;
    @Column(name = "total_Weight")
    private Double totalWeight;
    @Column(name = "warehouse_Id")
    private Integer warehouseId;
    @Column(name = "warehouse_Type")
    private String warehouseType;

    @Column(name = "from_Bill_Lading_Detail_Id")
    private Integer fromBillLadingDetailId;
    @Column(name = "description")
    private String description;

    @Column(name = "expected_From_Time")
    private java.sql.Timestamp expectedFromTime;
    @Column(name = "expected_To_Time")
    private java.sql.Timestamp expectedToTime;

    @Column(name = "status")
    private String status;
    @Column(name = "approved_Type")
    private String approvedType;

    @Column(name = "from_Warehouse_Id")
    private Integer fromWarehouseId;
    @Column(name = "status_Order")
    private String statusOrder;

    @Column(name = "name")
    private String name;

}
