package com.ac.dlp.tasks;

import com.ac.dlp.service.RoutingPlanDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class RoutingPlanDaySync {

    @Autowired
    private RoutingPlanDayService routingPlanDayService;

//    @Scheduled(cron = "${scheduled.tasks.sync}", initialDelay = 0)
//    @Scheduled(initialDelay = 0, fixedDelay = 10000)
    public void run() {
        System.out.println("hello");
        try {
            routingPlanDayService.getBillLading();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
