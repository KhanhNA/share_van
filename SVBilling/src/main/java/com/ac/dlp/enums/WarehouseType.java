package com.ac.dlp.enums;

public enum WarehouseType {
    PICKUP("0"), DELIVERY("1");
    String value;
    WarehouseType(String v){
        this.value = v;
    }
    public String getValue(){
        return value;
    }
    public static WarehouseType of(String v) throws Exception {
        if(PICKUP.getValue().equals(v)){
            return PICKUP;
        }else if (DELIVERY.getValue().equals(v)){
            return DELIVERY;
        }
        throw new Exception("WrongValue");
    }

}
