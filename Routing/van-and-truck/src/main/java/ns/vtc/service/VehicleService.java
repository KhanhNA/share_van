package ns.vtc.service;
import ns.utils.DateUtils;
import ns.utils.JPAUtility;
import ns.vtc.constants.Constants;
import ns.vtc.entity.*;
import ns.vtc.enums.*;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
public class VehicleService {
    private static VehicleService instance;

    public static VehicleService getInstance() {
        if (instance == null) {
            instance = new VehicleService();
        }
        return instance;
    }
    public List<FleetVehicle> getVehicles(String datePlan){
        HashMap<String,Object> params = new HashMap<>();
        params.put("status", StatusType.RUNNING.getValue());
        params.put("datePlan",datePlan);
        //   System.out.println("dateksnsnjsdn: " + datePlan);
        // params.put("statusRouting",StatusRouting.NOT_CONFIRM.getValue());
        return JPAUtility.getInstance().jpaFindAll("select fv  " +
                "     from FleetVehicle fv " +
                "     join FleetVehicleState fvs on fvs.id = fv.stateId " +
                "     where fv.active = true and fvs.code  in ('AVAILABLE') " +
                "     and fv.capacity > 0 and fv.capacity is not null and fv.companyId = 1 and fv.activeType = 'fleet' " +
                "     AND  EXISTS (select 1 " +
                "           from FleetVehicleAssignationLog al " +
                "           where al.vehicleId = fv.id " +
                "          and al.status = :status " +
                "          and al.driverStatus = '1' " +
                "          and al.startDate = to_date(:datePlan,'dd/mm/yyyy')) " +
                "     AND NOT EXISTS (select 1  " +
                "           from RoutingPlanDay rpd " +
                "           where rpd.vehicleId = fv.id " +
                "           and rpd.status in ('0','5') " +
                "           and rpd.datePlan = to_date(:datePlan,'dd/mm/yyyy'))", params);
    }
    public List<FleetVehicle> getListVehicleRouting(String datePlan){
        HashMap<String,Object> param = new HashMap<>();
        param.put("datePlan",datePlan);
        param.put("shipType",ShipType.IN_ZONE.getType());
        param.put("status", StatusRouting.NOT_CONFIRM.getValue());
        // param.put("shipType","0"); //inzone
        //  param.put("status","0"); //routing chua duoc thuc hien di tuyen
        return JPAUtility.getInstance().jpaFindAll
                ("select DISTINCT fv from FleetVehicle fv " +
                        " join RoutingPlanDay  rpd on fv.id = rpd.vehicleId " +
                        "where rpd.datePlan = to_date(:datePlan,'dd/mm/yyyy')\n" +
                        "and rpd.vehicleId is not null \n" +
                        "and rpd.driverId is null \n" +
                        "and rpd.shipType = :shipType \n" +
                        "and rpd.status = :status",param);
    }
    public HashMap<Integer, Integer>getHisDriverByVehicle(int vehicleId, String datePlan){
        HashMap<Integer,Integer> lstHisDriver = new HashMap<>();
        List<Object> param = new ArrayList<>();
        HashMap<String,String> columns = new HashMap<>();
//        param.put("datePlan",datePlan);
//        param.put("vehicleId",vehicleId);
        param.add(ShipType.IN_ZONE.getType());
        param.add(datePlan);
        param.add(vehicleId);
        try {
            ResultSet re = JPAUtility.getInstance().getJdbcResultSet
                    ("select tmp.driver_id, count(tmp.date_plan) dem from \n" +
                            "(select DISTINCT driver_id, vehicle_id, date_plan from sharevan_routing_plan_day \n" +
                            " where ship_type = ? and date_plan < to_date(?,'dd-mm-yyyy') " +
                            "and driver_id is not null and vehicle_id = ?) tmp\n" +
                            " group by tmp.driver_id",param,columns);
            while (re.next()){
                //  HashMap<Integer,Integer> data = new HashMap<>();
                lstHisDriver.put(re.getInt("driver_id"),re.getInt("dem"));
                //   lstHisDriver.add(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstHisDriver;
    }
    public List<FleetVehicle> getListVehicleRoutingSO(String datePlan){
        HashMap<String,Object> param = new HashMap<>();
        param.put("datePlan",datePlan);
        param.put("shipType",ShipType.IN_ZONE.getType());
        param.put("status",StatusRouting.DRAFT.getValue());
        // param.put("shipType","0"); //inzone
        //  param.put("status","0"); //routing chua duoc thuc hien di tuyen
        return JPAUtility.getInstance().jpaFindAll
                ("select DISTINCT fv from FleetVehicle fv " +
                        " join RoutingPlanDay  rpd on fv.id = rpd.vehicleId " +
                        "where rpd.datePlan = to_date(:datePlan,'dd/mm/yyyy')\n" +
                        "and rpd.vehicleId is not null \n" +
                        "and rpd.driverId is null \n" +
                        "and rpd.shipType = :shipType \n" +
                        "and rpd.status = :status" +
                        " and rpd.soType = true",param);
    }
    public FleetVehicleState getVehicleStateByCode(String vehicleState){
        HashMap<String,Object> param = new HashMap<>();
        param.put("code",vehicleState);
        return JPAUtility.getInstance().jpaFindOne(" FROM FleetVehicleState where code = :code",param);
    }
    public void updateStateVehicle (long vehicleId, int stateId) throws Exception {
        List<Object> param = new ArrayList<>();
        param.add(stateId);
        param.add(vehicleId);
        JPAUtility.getInstance().executeJdbcUpdate("update fleet_vehicle set state_id = ? where id = ?", param);
    }
    public List<FleetVehicle> getVehiclesTest(String datePlan){
        HashMap<String,Object> params = new HashMap<>();
        params.put("status", StatusType.RUNNING.getValue());
        params.put("datePlan",datePlan);
        // params.put("statusRouting",StatusRouting.NOT_CONFIRM.getValue());
        return JPAUtility.getInstance().jpaFindAll("select fv  " +
                "     from FleetVehicle fv " +
                "     join FleetVehicleState fvs on fvs.id = fv.stateId " +
                "     where fv.active = true and fvs.code not in ('MAINTENANCE','DOWNGRADED') " +
                "     and fv.capacity > 0 and fv.capacity is not null and fv.companyId = 1 and fv.activeType = 'fleet' " +
                "     AND NOT EXISTS (select 1 " +
                "           from FleetVehicleAssignationLog al " +
                "           where al.vehicleId = fv.id " +
                "          and al.status = :status " +
                "          and al.driverStatus = '1' " +
                "          and al.startDate = to_date(:datePlan,'dd/mm/yyyy')) " +
                "     AND NOT EXISTS (select 1  " +
                "           from RoutingPlanDay rpd " +
                "           where rpd.vehicleId = fv.id " +
                "           and rpd.status in ('0','5') " +
                "           and rpd.datePlan = to_date(:datePlan,'dd/mm/yyyy'))", params);
    }
}
