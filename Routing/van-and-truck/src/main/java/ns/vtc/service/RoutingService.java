package ns.vtc.service;
import ns.utils.DateUtils;
import ns.utils.JPAUtility;
import ns.vtc.constants.Constants;
import ns.vtc.entity.*;
import ns.vtc.enums.*;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
public class RoutingService {
    private static RoutingService instance;

    public static RoutingService getInstance() {
        if (instance == null) {
            instance = new RoutingService();
        }
        return instance;
    }
    public List<RoutingPlanDay> getRoutingPlanDay(Integer solutionDayId, String groupCode, LocalDate planDate){

        HashMap<String, Object> params = new HashMap<>();
//        params.put("groupCode", groupCode);
        //     params.put("solutionDayId", solutionDayId);
        params.put("datePlan", DateUtils.toString(planDate, DateUtils.DD_MM_YYYY));
        params.put("status",StatusRouting.NOT_CONFIRM.getValue());
        params.put("shipType",ShipType.IN_ZONE.getType());
        params.put("soType",false);
        System.out.println("date_plan: " + DateUtils.toString(planDate, DateUtils.DD_MM_YYYY));
        return JPAUtility.getInstance()
                .jpaFindAll("from RoutingPlanDay where  datePlan = TO_DATE(:datePlan,'dd/mm/yyyy') and status= :status and shipType= :shipType and driverId is null and vehicleId is null and soType = :soType ", params);
        //  .jpaFindAll("from RoutingPlanDay where solutionDayId = :solutionDayId and datePlan = TO_DATE(:datePlan,'dd/mm/yyyy') and status= :status and shipType= :shipType and driverId is null and vehicleId is null ", params);
    }
    public List<RoutingInfo> getInfoRouting(Integer solutionDayId, String groupCode, LocalDate planDate){
        List<RoutingInfo> lstResult = new ArrayList<>();
        List<Object> params = new ArrayList<>();
        //        params.put("groupCode", groupCode);
        params.add(solutionDayId);
        params.add(DateUtils.toString(planDate, DateUtils.DD_MM_YYYY));
        params.add(StatusRouting.NOT_CONFIRM.getValue());
        params.add(ShipType.IN_ZONE.getType());
        System.out.println("date_plan: " + DateUtils.toString(planDate, DateUtils.DD_MM_YYYY));

        try {
            ResultSet re =  JPAUtility.getInstance()
                    .getJdbcResultSet("select rpd.id, c.point, bop.type from sharevan_routing_plan_day rpd\n" +
                            "left join res_company c on c.id =rpd.company_id\n" +
                            "left join sharevan_bill_lading_detail bld on bld.id = rpd.bill_lading_detail_id\n" +
                            "left join sharevan_bill_lading bl on bl.id = bld.bill_lading_id\n" +
                            "left join sharevan_bill_order_package bop on bop.id = bl.order_package_id " +
                            "where solutionDayId = ? and datePlan = TO_DATE(?,'dd/mm/yyyy') and status=? and shipType=?", params, new HashMap());
            while (re.next()){
                RoutingInfo routing = new RoutingInfo();
                routing.setRoutingPlanDayId(re.getInt("id"));
                routing.setPoint(re.getInt("point"));
                routing.setOrderType(re.getString("type"));
                lstResult.add(routing);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstResult;
    }
    public void  updateRoutingPlan(Long solId, String insertInfo) throws Exception{

        String sqlUpdate = "INSERT INTO sharevan_routing_plan_day (id, vehicle_id, next_id, previous_id, write_date, write_uid, status,expected_from_time, expected_to_time, order_number,capacity_vehicle, driver_id,trouble_type ) VALUES " + insertInfo +
                " ON CONFLICT (id) DO UPDATE set vehicle_id = EXCLUDED.vehicle_id, " +
                "                         next_id = EXCLUDED.next_id, previous_id = EXCLUDED.previous_id," +
                "                         write_date = current_date, write_uid = 2," +
                "                         expected_from_time = EXCLUDED.expected_from_time," +
                "                         expected_to_time = EXCLUDED.expected_to_time," +
                "                         order_number = EXCLUDED.order_number," +
                "                         capacity_vehicle = EXCLUDED.capacity_vehicle," +
                "                         driver_id = EXCLUDED.driver_id," +
                "                         trouble_type = EXCLUDED.trouble_type"
                ;

        JPAUtility.getInstance().executeJdbcUpdate(sqlUpdate, null);

    }
    public void addDriverForRouting(String datePlan, long vehicleId, long driverId, String type) throws Exception {
        String sql = "update sharevan_routing_plan_day set driver_id = ?\n" +
                "where date_plan = to_date(?,'dd/mm/yyyy')\n" +
                "and ship_type = ?\n" +
                "and status = ?\n" +
                "and vehicle_id = ? "+
                "and so_type = ?";
        List<Object> params = new ArrayList<>();
        params.add(driverId);
        params.add(datePlan);
        params.add(ShipType.IN_ZONE.getType());
        if(type.equals(BillType.SO.getValue())){
            params.add(StatusRouting.DRAFT.getValue());
            params.add(vehicleId);
            params.add(true);
        }
        else if(type.equals(BillType.DLP.getValue())){
            params.add(StatusRouting.NOT_CONFIRM.getValue());
            params.add(vehicleId);
            params.add(false);
        }

        JPAUtility.getInstance().executeJdbcUpdate(sql, params);
    }
    /*     method for phase Mtore customer      */
    public List<RoutingPlanDay> getListRoutingSO( String planDate){

        HashMap<String, Object> params = new HashMap<>();
//        params.put("groupCode", groupCode);
        // params.put("solutionDayId", solutionDayId);
        params.put("datePlan", planDate);
        params.put("status",StatusRouting.DRAFT.getValue());
        params.put("shipType",ShipType.IN_ZONE.getType());
        params.put("soType",true);
        //    System.out.println("date_plan: " + planDate);
        return JPAUtility.getInstance()
                .jpaFindAll("from RoutingPlanDay where  datePlan = TO_DATE(:datePlan,'dd/mm/yyyy') and status=:status and shipType=:shipType and soType= :soType", params);
    }
}
