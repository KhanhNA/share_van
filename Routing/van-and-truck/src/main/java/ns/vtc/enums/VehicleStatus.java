package ns.vtc.enums;

public enum VehicleStatus {
    AVAILABLE("AVAILABLE"),
    ORDERED("ORDERED"),
    DOWNGRADED("DOWNGRADED"),
    SHIPPING("SHIPPING"),
    MAINTENANCE("MAINTENANCE"),
    CONFIRMTAKE("CONFIRMTAKE"),
    CONFIRMRETURN("CONFIRMRETURN");
    private String value;

    VehicleStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public static VehicleStatus of(String value) throws Exception {
        if(AVAILABLE.getValue().equals(value)){
            return AVAILABLE;
        }
        if(ORDERED.getValue().equals(value)){
            return ORDERED;
        }
        if(DOWNGRADED.getValue().equals(value)){
            return DOWNGRADED;
        }
        if(SHIPPING.getValue().equals(value)){
            return SHIPPING;
        }
        if(MAINTENANCE.getValue().equals(value)){
            return MAINTENANCE;
        }
        if(CONFIRMTAKE.getValue().equals(value)){
            return CONFIRMTAKE;
        }
        if(CONFIRMRETURN.getValue().equals(value)){
            return CONFIRMRETURN;
        }
        throw new Exception ("Wrong value!");
    }
}
