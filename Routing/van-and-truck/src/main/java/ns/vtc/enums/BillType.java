package ns.vtc.enums;

public enum BillType {
    DLP("DLP"),
    SO("SO");
    private String value;

    BillType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public static BillType of(String value) throws Exception {
        if(DLP.getValue().equals(value)){
            return DLP;
        }
        if(SO.getValue().equals(value)){
            return SO;
        }
        throw new Exception ("Wrong value!");
    }
}
