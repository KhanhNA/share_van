package ns.vtc.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="fleet_vehicle_status")
@Getter
@Setter
public class FleetVehicleStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code_name")
    private String codeName;

    @Column(name = "vehicle_id")
    private Long vehicleId;

    @Column(name = "driver_id")
    private Long driverId;

    @Column(name = "assignation_log_id")
    private Long assignationLogId;

    @Column(name = "company_id")
    private Integer companyId;

    @Column(name = "toogle")
    private Boolean toogle;

    @Column(name = "status_domain")
    private Boolean statusDomain;

    @Column(name = "status")
    private String status;

    @Column(name = "date_start")
    private LocalDateTime dateStart;

    @Column(name = "date_end")
    private LocalDateTime dateEnd;

    @Column(name = "create_Uid")
    private Integer createUid;

    @Column(name = "create_Date")
    private LocalDateTime createDate;

    @Column(name = "write_Uid")
    private Integer writeUid;

    @Column(name = "write_Date")
    private LocalDateTime writeDate;
}
