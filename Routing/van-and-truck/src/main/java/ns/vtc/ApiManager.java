package ns.vtc;

import ns.vtc.constants.Constants;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

public class ApiManager {
    public static  void callApiSocket(String data, String url) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = new URI(url);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(data, headers);
        ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);
        System.out.println(result.getStatusCode());
    }
    public static void  callNotificationOdoo(String lstDriverIds, String type){
//        if(mapDriver.size() == 0)
//            return;
//        String lst = "";
//        for(Long i : mapDriver.keySet()){
//            lst += mapDriver.get(i) + ",";
//        }
//        System.out.println(lst);
//        lst = lst.substring(0,lst.length()-1);
        String lst = "[" + lstDriverIds + "]";
        String json = "{\n" +
                "\"jsonrpc\": \"2.0\",\n" +
                "\"params\": {\n" +
                "        \"list_driver\": " + lst +",\n" +
                "        \"secret_key\":\"" + Constants.SECRET_KEY +"\"\n" +

                "}\n" +
                "}";
        try {
            if(type.equals("NOTIFICATION_DRIVER")){
                ApiManager.callApiSocket(json, Constants.API_NOTIFICATION_DRIVER);
            }
            else if(type.equals("NOTIFICATION_OUTDATE_LICENSE")){
                ApiManager.callApiSocket(json, Constants.API_NOTIFICATION_OUTDATE_LICENSE);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
