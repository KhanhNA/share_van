package maps;

import ns.vtc.entity.LocationData;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import vehiclerouting.customize.entity.ShipmentStandstill;
import vehiclerouting.customize.types.DistanceInfo;
import vehiclerouting.domain.DataGoogleApi;
import vehiclerouting.domain.location.Location;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GoogleDistanceAPI {


    public static final String API_GOOGLE_MAP_DESTINATION = "AIzaSyCmzEKuZOtAuDR5-iHmJvLScbSolUJEhBk";
    private static RestTemplate restTemplate = null;

    private static void initRest() {

        if (restTemplate != null) {
            return;
        }
        restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.APPLICATION_JSON));
        messageConverters.add(converter);
        restTemplate.setMessageConverters(messageConverters);

    }
    public static LocationData getDistance(ShipmentStandstill from, ShipmentStandstill to) {
        Location f = from.getLocation();
        Location t = to.getLocation();
        return getDistance(true, f.getLatitude(), f.getLongitude(),
                t.getLatitude(), t.getLongitude());
    }
    public static LocationData getDistance(Boolean shoudBeStandard, Double fLatitude, Double fLongitude, Double tLatitude, Double tLongitude) {

        fLatitude = shoudBeStandard ? LocationData.standardLocation(fLatitude) : fLatitude;
        fLongitude = shoudBeStandard ? LocationData.standardLocation(fLongitude) : fLongitude;
        tLatitude = shoudBeStandard ? LocationData.standardLocation(tLatitude) : tLatitude;
        tLongitude = shoudBeStandard ? LocationData.standardLocation(tLongitude) : tLongitude;

        final String requestUrl = "https://maps.googleapis.com/maps/api/directions/json?" +
                "mode=driving&"
                + "transit_routing_preference=less_driving&"
                + "origin=" + fLatitude + "," + fLongitude + "&"
                + "destination=" + tLatitude + "," + tLongitude + "&"
                + "key=" + API_GOOGLE_MAP_DESTINATION;
        initRest();
        GoogleDistance result = restTemplate.getForObject(requestUrl, GoogleDistance.class);
        DataGoogleApi data = getDistanceInfo(result);
        DistanceInfo<Double, Double> distanceInfo = data.getDistanceInfo();
        System.out.println(result);
        LocationData locationData = new LocationData(fLatitude, fLongitude, tLatitude, tLongitude,
                                        distanceInfo.getMinute(), distanceInfo.getCost(),data.getStartAddress(),data.getEndAddress());
        return  locationData;
    }

    public static DataGoogleApi getDistanceInfo(GoogleDistance googleDistance){
        Double minutes = 0d, cost = 0d;
        String startAddress = "", endAddress="";
        if(googleDistance == null || googleDistance.getRoutes() == null){
            return null;
        }
        for(Route route: googleDistance.getRoutes()){
            for (Leg leg: route.getLegs()){
                minutes += leg.getDuration().getValue()/60;
                cost += leg.getDistance().getValue();
                startAddress = leg.getStart_address();
                endAddress = leg.getEnd_address();
            }
        }
        DataGoogleApi data = new DataGoogleApi();
        data.setDistanceInfo( DistanceInfo.of(minutes, cost));
        data.setStartAddress(startAddress);
        data.setEndAddress(endAddress);
        return data;
    }


    public static void main(String[] argv) {
        LocationData data = GoogleDistanceAPI.getDistance(true, 21.040, 105.767, 21.030, 105.814);
//        GoogleDistanceAPI.getDistance(21.04027, 105.76763, 21.03027, 105.81392);
        System.out.println(Math.round(21.04027 * 1000d) / 1000d);
    }
}
