package drivingschedule.app;

import common.constants.Constansts;
import drivingschedule.domain.DrivingSchedule;
import drivingschedule.domain.DrivingScheduleSolution;
import ns.utils.DateUtils;
import ns.vtc.ApiManager;
import ns.vtc.entity.FleetDriver;
import ns.vtc.entity.FleetVehicleAssignationLog;
import ns.vtc.entity.FleetVehicleStatus;
import ns.vtc.enums.BillType;
import ns.vtc.enums.StatusType;
import org.optaplanner.core.api.score.ScoreManager;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.impl.score.director.InnerScoreDirector;
import vehiclerouting.customize.StaticData;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

public class DrivingScheduleSolver {
    private static volatile Solver<DrivingScheduleSolution> solver;
    private String solutionName = null;
    private static InnerScoreDirector<DrivingScheduleSolution> guiScoreDirector;
    private static ScoreManager<DrivingScheduleSolution, HardSoftLongScore> scoreManager;
    public static void main(String[] args) throws Exception {
        SolverFactory<DrivingScheduleSolution> solverFactory = SolverFactory.createFromXmlResource(
                "vehiclerouting/solver/drivingScheduleSolverConfig.xml");
        solver = solverFactory.buildSolver();
//        scoreManager = ScoreManager.create(solverFactory);
//        guiScoreDirector =
//                ((DefaultSolverFactory<DrivingScheduleSolution>) solverFactory).getScoreDirectorFactory().buildScoreDirector();
   //     LocalDateTime ldt = LocalDateTime.of(2021,01,07,0,0);

        LocalDateTime ldt = LocalDateTime.now();
        //check va gui thong bao nhung thang sap het han bang lai xe
        getDriverOutDateAndSendNotifycation(DateUtils.toString(ldt,DateUtils.DD_MM_YYYY));
        String type = BillType.DLP.getValue();
        DrivingScheduleSolution problem = StaticData.loadProblemForDrivingScheduleTest(DateUtils.toString(ldt, DateUtils.DD_MM_YYYY));
        DrivingScheduleSolution result = solver.solve(problem);

        if(result != null && result.getScore() != null && result.getScore().getHardScore() == 0){
            printResult(result,DateUtils.toString(ldt, DateUtils.DD_MM_YYYY),type);
            createDriverCalendar(result,ldt);
        }
        else{
            System.out.println("không có phương án tối ưu");
        }

//        DrivingScheduleSolution problem = StaticData.loadProblemForDrivingSchedule(DateUtils.toString(ldt, DateUtils.DD_MM_YYYY),type);
////        guiScoreDirector.setWorkingSolution(problem);
//        DrivingScheduleSolution result = solver.solve(problem);
//        if(result != null){
//            printResult(result,DateUtils.toString(ldt, DateUtils.DD_MM_YYYY),type);
//            createDriverCalendar(result,ldt);
//        }

        //solver.solve()
    }
    public static void printResult(DrivingScheduleSolution result, String datePlan, String type){
        List<DrivingSchedule> lstSchedule = result.getLstSchedule();
        HashMap<Long,Long> mapDriver = new HashMap<>();
        for(DrivingSchedule schedule: lstSchedule){
            if(schedule.getDriver() != null)
                System.out.println("driver: " + schedule.getDriver().getName());
            if(schedule.getVehicle() != null)
                System.out.println("vehicle: " + schedule.getVehicle().getName());
            System.out.println("--------------------------------");

            if(schedule.getDriver() != null && schedule.getVehicle() != null){
                try {

//                    StaticData.setDriverForRouting(datePlan,schedule.getDriver().getId(),schedule.getVehicle().getId(),type);
//                    mapDriver.put(schedule.getVehicle().getId(),schedule.getDriver().getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        //ApiManager.callNotificationOdoo(mapDriver);
        System.out.println("=============================");
    }
    public static void createDriverCalendar(DrivingScheduleSolution solver, LocalDateTime datePlan) throws Exception {
        List<DrivingSchedule> lstSchedule = solver.getLstSchedule();
        StaticData.updateOldAssignationLog(DateUtils.toString(datePlan.plusDays(-1), DateUtils.DD_MM_YYYY));
        for(DrivingSchedule schedule: lstSchedule){
            FleetVehicleAssignationLog calendar = new FleetVehicleAssignationLog();
            calendar.setVehicleId(schedule.getVehicle().getId());
            calendar.setDriverId(schedule.getDriver().getId());
            calendar.setStatus("running");
            calendar.setCompanyId(schedule.getVehicle().getCompanyId());
            calendar.setStartDate(LocalDateTime.parse(DateUtils.toString(datePlan, DateUtils.DD_MM_YYYY) + " 00:00:00", DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            calendar.setEndDate(LocalDateTime.parse(DateUtils.toString(datePlan.plusDays(1), DateUtils.DD_MM_YYYY + " 00:00:00"), DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            calendar.setDriverStatus("1");
            calendar.setCreateDate(datePlan);
            StaticData.createDriverCalendar(calendar);
            calendar = StaticData.getScheduleByAssignLog(calendar);
            String code = Constansts.scheduleCode + calendar.getId();
            calendar.setAssignationLogCode(code);
            StaticData.createDriverCalendar(calendar);

            //Bo sung them fleet_vehicle_status
            FleetVehicleStatus vehicleStatus = new FleetVehicleStatus();
            vehicleStatus.setVehicleId(calendar.getVehicleId());
            vehicleStatus.setDriverId(calendar.getDriverId());
            vehicleStatus.setCompanyId(calendar.getCompanyId());
            vehicleStatus.setStatusDomain(true);
            vehicleStatus.setToogle(true);
            vehicleStatus.setStatus(StatusType.DELETED.getValue());
            vehicleStatus.setAssignationLogId(calendar.getId());
            vehicleStatus.setDateStart(calendar.getStartDate());
            vehicleStatus.setDateEnd(calendar.getEndDate());
            StaticData.createFleetVehicleStatus(vehicleStatus);
        }

    }
    public static void getDriverOutDateAndSendNotifycation(String datePlan){
        String ids = "";
        List<FleetDriver> lstDriver = StaticData.getListDriverOutdateLicense(datePlan);
        if(lstDriver != null && !lstDriver.isEmpty()){
            for( FleetDriver fd : lstDriver){
                ids += ids + ",";
            }
            ids = ids.substring(0,ids.length()-1);
            ApiManager.callNotificationOdoo(ids,"NOTIFICATION_OUTDATE_LICENSE");
        }


    }
}
