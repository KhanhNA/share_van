package drivingschedule.solver.domain;

import drivingschedule.domain.Driver;

import java.util.Collections;
import java.util.Comparator;

import static java.util.Comparator.comparing;

public class DriverStrengthComparator  implements Comparator<Driver> {

    private static final Comparator<Driver> COMPARATOR =
            Collections.reverseOrder(Comparator.comparingInt(Driver::getNumTrip))
            .thenComparing(Collections.reverseOrder(comparing(Driver::getPoint)))// Descending (but this is debatable)
            .thenComparingLong(Driver::getId);
    @Override
    public int compare(Driver o1, Driver o2) {
        return COMPARATOR.compare(o1, o2);
    }
}
