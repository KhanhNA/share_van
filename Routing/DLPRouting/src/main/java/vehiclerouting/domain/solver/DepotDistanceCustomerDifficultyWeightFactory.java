/*
 * Copyright 2014 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vehiclerouting.domain.solver;


import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionSorterWeightFactory;
import vehiclerouting.customize.entity.ShipmentPot;
import vehiclerouting.domain.Depot;
import vehiclerouting.domain.VehicleRoutingSolutionV2;

import java.util.Comparator;

import static java.util.Comparator.comparingDouble;
import static java.util.Comparator.comparingLong;

/**
 * On large datasets, the constructed solution looks like a Matryoshka doll.
 */
public class DepotDistanceCustomerDifficultyWeightFactory
        implements SelectionSorterWeightFactory<VehicleRoutingSolutionV2, ShipmentPot> {

    @Override
    public DepotDistanceCustomerDifficultyWeight createSorterWeight(VehicleRoutingSolutionV2 vehicleRoutingSolution,
            ShipmentPot shipmentPot) {
        Depot depot = vehicleRoutingSolution.getDepotList().get(0);
        return new DepotDistanceCustomerDifficultyWeight(shipmentPot,
                shipmentPot.getLocation().getDistanceTo(depot.getLocation())
                        + depot.getLocation().getDistanceTo(shipmentPot.getLocation()));
    }

    public static class DepotDistanceCustomerDifficultyWeight
            implements Comparable<DepotDistanceCustomerDifficultyWeight> {

        private static final Comparator<DepotDistanceCustomerDifficultyWeight> COMPARATOR =
                // Ascending (further from the depot are more difficult)
                comparingDouble((DepotDistanceCustomerDifficultyWeight weight) -> weight.depotRoundTripDistance)
                        .thenComparingDouble(weight -> weight.shipmentPot.getDemand())
                        .thenComparingDouble(weight -> weight.shipmentPot.getLocation().getLatitude())
                        .thenComparingDouble(weight -> weight.shipmentPot.getLocation().getLongitude())
                        .thenComparing(weight -> weight.shipmentPot, comparingLong(ShipmentPot::getId));

        private final ShipmentPot shipmentPot;
        private final Double depotRoundTripDistance;

        public DepotDistanceCustomerDifficultyWeight(ShipmentPot shipmentPot,
                Double depotRoundTripDistance) {
            this.shipmentPot = shipmentPot;
            this.depotRoundTripDistance = depotRoundTripDistance;
        }

        @Override
        public int compareTo(DepotDistanceCustomerDifficultyWeight other) {
            return COMPARATOR.compare(this, other);
        }
    }
}
