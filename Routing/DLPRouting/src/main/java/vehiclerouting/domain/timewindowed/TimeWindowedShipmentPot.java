/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vehiclerouting.domain.timewindowed;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import ns.utils.DateUtils;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.CustomShadowVariable;
import org.optaplanner.core.api.domain.variable.PlanningVariableReference;
import vehiclerouting.customize.entity.ShipmentPot;
import vehiclerouting.customize.types.ShipType;
import vehiclerouting.domain.location.Location;
import vehiclerouting.domain.timewindowed.solver.ArrivalTimeUpdatingVariableListener;

import java.time.LocalDateTime;

@PlanningEntity
@XStreamAlias("VrpTimeWindowedCustomer")
public class TimeWindowedShipmentPot extends ShipmentPot {

    // Times are multiplied by 1000 to avoid floating point arithmetic rounding errors
    private Double readyTime;
    private Double dueTime;
    private Long serviceDuration;

    // Shadow variable
    private Double arrivalTime;


    public TimeWindowedShipmentPot() {
    }

    public TimeWindowedShipmentPot(Integer id, Location location, Double demand, LocalDateTime readyTime, LocalDateTime dueTime,
                                   long serviceDuration, String customerCode, Integer depotId, Integer warehouseId,
                                   String prevId, String nextId,
                                   ShipType warehouseType,
                                   Integer fromShipmentPotId,
                                   Integer numReceive,
                                   String date_plan,
                                   Boolean soType) {
        super(id, location, demand, customerCode, depotId, warehouseId, warehouseType,fromShipmentPotId,numReceive,date_plan, soType);
        //ZoneOffset offset = OffsetDateTime.now().getOffset();
        this.readyTime = readyTime == null? 0: new Double(DateUtils.toSeconds(readyTime));
        this.dueTime = dueTime == null? 0: new Double(DateUtils.toSeconds(dueTime));
        this.serviceDuration = serviceDuration;
        this.prevId = prevId;
        this.nextId = nextId;

//        this.shipType = ShipType.of(warehouseType);
    }


    /**
     * @return a positive number, the time multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public Double getReadyTime() {
        return readyTime;
    }

    public void setReadyTime(Double readyTime) {
        this.readyTime = readyTime;
    }

    /**
     * @return a positive number, the time multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public Double getDueTime() {
        return dueTime;
    }

    public void setDueTime(Double dueTime) {
        this.dueTime = dueTime;
    }

    /**
     * @return a positive number, the time multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public long getServiceDuration() {
        return serviceDuration;
    }

    public void setServiceDuration(long serviceDuration) {
        this.serviceDuration = serviceDuration;
    }

    /**
     * @return a positive number, the time multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    @CustomShadowVariable(variableListenerClass = ArrivalTimeUpdatingVariableListener.class,
            // Arguable, to adhere to API specs (although this works), nextShipmentPot should also be a source,
            // because this shadow must be triggered after nextShipmentPot (but there is no need to be triggered by nextShipmentPot)
            sources = {@PlanningVariableReference(variableName = "previousStandstill")})
    public Double getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Double arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************

    /**
     * @return a positive number, the time multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public Double getDepartureTime() {
        if (arrivalTime == null) {
            return null;
        }
        return Math.max(arrivalTime, readyTime) + serviceDuration;
    }

    public boolean isArrivalBeforeReadyTime() {
        return arrivalTime != null
                && arrivalTime < readyTime;
    }

    public boolean isArrivalAfterDueTime() {
        return arrivalTime != null
                && dueTime < arrivalTime;
    }

    @Override
    public TimeWindowedShipmentPot getNextShipmentPot() {
        return (TimeWindowedShipmentPot) super.getNextShipmentPot();
    }

    /**
     * @return a positive number, the time multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public Double getTimeWindowGapTo(TimeWindowedShipmentPot other) {
        // dueTime doesn't account for serviceDuration
        Double latestDepartureTime = dueTime + serviceDuration;
        Double otherLatestDepartureTime = other.getDueTime() + other.getServiceDuration();
        if (latestDepartureTime < other.getReadyTime()) {
            return other.getReadyTime() - latestDepartureTime;
        }
        if (otherLatestDepartureTime < readyTime) {
            return readyTime - otherLatestDepartureTime;
        }
        return 0d;
    }

    @Override
    public String getInfo() {
        return "<html>{" +
                "Customer code: " + this.customerCode +
                "<br/> Demand: " + this.demand +
                "<br/> Ready time:" + DateUtils.toString(this.readyTime.longValue(), null) +
                "<br/> DueTime:" + DateUtils.toString(dueTime.longValue(), null) +
                "<br/> Service duration:" + serviceDuration +
                "<br/> Arrive time:" + (arrivalTime == null? "": DateUtils.toString(arrivalTime.longValue(), null)) +
                "} </html> ";
    }
}
