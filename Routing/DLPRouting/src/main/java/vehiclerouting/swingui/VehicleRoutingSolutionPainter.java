/*
 * Copyright 2020 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vehiclerouting.swingui;

import common.swingui.latitudelongitude.LatitudeLongitudeTranslator;
import ns.utils.DateUtils;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;

import org.optaplanner.swing.impl.TangoColorFactory;
import vehiclerouting.customize.entity.ShipmentPot;
import vehiclerouting.domain.Depot;
import vehiclerouting.domain.VehicleRoutingSolutionV2;
import vehiclerouting.domain.VehicleV2;
import vehiclerouting.domain.location.AirLocation;
import vehiclerouting.domain.location.DistanceType;
import vehiclerouting.domain.location.Location;
import vehiclerouting.domain.timewindowed.TimeWindowedDepot;
import vehiclerouting.domain.timewindowed.TimeWindowedShipmentPot;
import vehiclerouting.domain.timewindowed.TimeWindowedVehicleRoutingSolution;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;

public class VehicleRoutingSolutionPainter {

    private static final int TEXT_SIZE = 12;
    private static final int TIME_WINDOW_DIAMETER = 26;
    private static final NumberFormat NUMBER_FORMAT = new DecimalFormat("#,##0.00");

    private static final String IMAGE_PATH_PREFIX = "/vehiclerouting/swingui/";

    private ImageIcon depotImageIcon;
    private ImageIcon[] vehicleImageIcons;

    private BufferedImage canvas = null;
    private LatitudeLongitudeTranslator translator = null;
    private Double minimumTimeWindowTime = null;
    private Double maximumTimeWindowTime = null;

    public VehicleRoutingSolutionPainter() {
        depotImageIcon = new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "depot.png"));
        vehicleImageIcons = new ImageIcon[] {
                new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "vehicleChameleon.png")),
                new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "vehicleButter.png")),
                new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "vehicleSkyBlue.png")),
                new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "vehicleChocolate.png")),
                new ImageIcon(getClass().getResource(IMAGE_PATH_PREFIX + "vehiclePlum.png")),
        };
        int sequenceSize = TangoColorFactory.SEQUENCE_1.size();
        if (vehicleImageIcons.length != sequenceSize) {
            throw new IllegalStateException("The vehicleImageIcons length (" + vehicleImageIcons.length
                    + ") should be equal to the TangoColorFactory.SEQUENCE length (" + sequenceSize + ").");
        }
    }

    public BufferedImage getCanvas() {
        return canvas;
    }

    public LatitudeLongitudeTranslator getTranslator() {
        return translator;
    }

    public void reset(VehicleRoutingSolutionV2 solution, Dimension size, ImageObserver imageObserver) {
        translator = new LatitudeLongitudeTranslator();
        for (Location location : solution.getLocationList()) {
            translator.addCoordinates(location.getLatitude(), location.getLongitude());
        }
        determineMinimumAndMaximumTimeWindowTime(solution);

        double width = size.getWidth();
        double height = size.getHeight();
        translator.prepareFor(width, height - 10 - TEXT_SIZE);

        Graphics2D g = createCanvas(width, height);
        g.setFont(g.getFont().deriveFont((float) TEXT_SIZE));
        g.setStroke(TangoColorFactory.NORMAL_STROKE);
        DecimalFormat df = new DecimalFormat("###.##");
        int readyHourDegree, dueHourDegree;
        for (ShipmentPot shipmentPot : solution.getShipmentPotList()) {
            Location location = shipmentPot.getLocation();
            int x = translator.translateLongitudeToX(location.getLongitude());
            int y = translator.translateLatitudeToY(location.getLatitude());
            g.setColor(TangoColorFactory.ALUMINIUM_4);
            g.fillRect(x - 1, y - 1, 3, 3);
            String demandString = df.format(shipmentPot.getDemand()); //Double.toString(shipmentPot.getDemand());
            g.drawString(demandString, x - (g.getFontMetrics().stringWidth(demandString) / 2), y - TEXT_SIZE / 2);
            if (shipmentPot instanceof TimeWindowedShipmentPot) {
                TimeWindowedShipmentPot windowedShipmentPot = (TimeWindowedShipmentPot) shipmentPot;
                g.setColor(TangoColorFactory.ALUMINIUM_3);
                int circleX = x - (TIME_WINDOW_DIAMETER / 2);
                int circleY = y + 5;
                g.drawOval(circleX, circleY, TIME_WINDOW_DIAMETER, TIME_WINDOW_DIAMETER);

                LocalDateTime readyTime = DateUtils.toDate(windowedShipmentPot.getReadyTime().longValue());
                readyHourDegree =calcDegreeByHour(readyTime.getHour(), readyTime.getMinute(), readyTime.getSecond());
                LocalDateTime dueTime = DateUtils.toDate(windowedShipmentPot.getDueTime().longValue());
                dueHourDegree =calcDegreeByHour(dueTime.getHour(), dueTime.getMinute(), dueTime.getSecond());

                g.fillArc(circleX, circleY, TIME_WINDOW_DIAMETER, TIME_WINDOW_DIAMETER,
                        readyHourDegree,  -Math.abs( (dueTime.getHour() - readyTime.getHour())*30 - 5)
                );

//                g.fillArc(circleX, circleY, TIME_WINDOW_DIAMETER, TIME_WINDOW_DIAMETER,
//                        90 - calculateTimeWindowDegree(windowedShipmentPot.getReadyTime()),
//                        calculateTimeWindowDegree(windowedShipmentPot.getReadyTime())
//                                - calculateTimeWindowDegree(windowedShipmentPot.getDueTime()));
                if (windowedShipmentPot.getArrivalTime() != null) {
                    if (windowedShipmentPot.isArrivalAfterDueTime()) {
                        g.setColor(TangoColorFactory.SCARLET_2);
                    } else if (windowedShipmentPot.isArrivalBeforeReadyTime()) {
                        g.setColor(TangoColorFactory.ORANGE_2);
                    } else {
                        g.setColor(TangoColorFactory.ALUMINIUM_6);
                    }
                    g.setStroke(TangoColorFactory.THICK_STROKE);
                    int circleCenterY = y + 5 + TIME_WINDOW_DIAMETER / 2;


                    int angle = calcDegreeLine(windowedShipmentPot.getArrivalTime().longValue());
                    g.drawLine(x, circleCenterY,
                            x + (int) (Math.sin(Math.toRadians(angle)) * (TIME_WINDOW_DIAMETER / 2 + 3)),
                            circleCenterY - (int) (Math.cos(Math.toRadians(angle)) * (TIME_WINDOW_DIAMETER / 2 + 3)));
                    String arrTime = "" + DateUtils.toDate(windowedShipmentPot.getArrivalTime().longValue()).getHour();
                    x = x + (int) (Math.sin(Math.toRadians(angle)) * (TIME_WINDOW_DIAMETER / 2 + 3));
                    y = circleCenterY - (int) (Math.cos(Math.toRadians(angle)) * (TIME_WINDOW_DIAMETER / 2 + 3));
                    g.drawString(arrTime, x - (g.getFontMetrics().stringWidth(arrTime) / 2), y - TEXT_SIZE / 2);
                    g.setStroke(TangoColorFactory.NORMAL_STROKE);
                }
            }
        }
        g.setColor(TangoColorFactory.BUTTER_1);
        for (Depot depot : solution.getDepotList()) {
            int x = translator.translateLongitudeToX(depot.getLocation().getLongitude());
            int y = translator.translateLatitudeToY(depot.getLocation().getLatitude());
            g.fillRect(x - 2, y - 2, 5, 5);
            g.drawImage(depotImageIcon.getImage(),
                    x - depotImageIcon.getIconWidth() / 2, y - 2 - depotImageIcon.getIconHeight(), imageObserver);
        }
        int colorIndex = 0;
        // TODO Too many nested for loops
        for (VehicleV2 vehicle : solution.getVehicleList()) {
            g.setColor(TangoColorFactory.SEQUENCE_2.get(colorIndex));
            ShipmentPot vehicleInfoCustomer = null;
            Double longestNonDepotDistance = -1d;
            int load = 0;
            for (ShipmentPot shipmentPot : solution.getShipmentPotList()) {
                if (shipmentPot.getPreviousStandstill() != null && shipmentPot.getVehicle() == vehicle) {
                    load += shipmentPot.getDemand();
                    Location previousLocation = shipmentPot.getPreviousStandstill().getLocation();
                    Location location = shipmentPot.getLocation();
                    translator.drawRoute(g, previousLocation.getLongitude(), previousLocation.getLatitude(),
                            location.getLongitude(), location.getLatitude(),
                            location instanceof AirLocation, false);
                    // Determine where to draw the vehicle info
                    Double distance = shipmentPot.getDistanceFromPreviousStandstill();
                    if (shipmentPot.getPreviousStandstill() instanceof ShipmentPot) {
                        if (longestNonDepotDistance < distance) {
                            longestNonDepotDistance = distance;
                            vehicleInfoCustomer = shipmentPot;
                        }
                    } else if (vehicleInfoCustomer == null) {
                        // If there is only 1 shipmentPot in this chain, draw it on a line to the Depot anyway
                        vehicleInfoCustomer = shipmentPot;
                    }
                    // Line back to the vehicle depot
                    if (shipmentPot.getNextShipmentPot() == null) {
                        Location vehicleLocation = vehicle.getLocation();
                        translator.drawRoute(g, location.getLongitude(), location.getLatitude(),
                                vehicleLocation.getLongitude(), vehicleLocation.getLatitude(),
                                location instanceof AirLocation, true);
                    }
                }
            }
//             Draw vehicle info
            if (vehicleInfoCustomer != null) {
                if (load > vehicle.getCapacity()) {
                    g.setColor(TangoColorFactory.SCARLET_2);
                }
                Location previousLocation = vehicleInfoCustomer.getPreviousStandstill().getLocation();
                Location location = vehicleInfoCustomer.getLocation();
                double longitude = (previousLocation.getLongitude() + location.getLongitude()) / 2.0;
                int x = translator.translateLongitudeToX(longitude);
                double latitude = (previousLocation.getLatitude() + location.getLatitude()) / 2.0;
                int y = translator.translateLatitudeToY(latitude);
                boolean ascending = (previousLocation.getLongitude() < location.getLongitude())
                        ^ (previousLocation.getLatitude() < location.getLatitude());

                ImageIcon vehicleImageIcon = vehicleImageIcons[colorIndex];
                int vehicleInfoHeight = vehicleImageIcon.getIconHeight() + 2 + TEXT_SIZE;
                g.drawImage(vehicleImageIcon.getImage(),
                        x + 1, (ascending ? y - vehicleInfoHeight - 1 : y + 1), imageObserver);
                g.drawString(load + " / " + vehicle.getCapacity(),
                        x + 1, (ascending ? y - 1 : y + vehicleInfoHeight + 1));
            }
            colorIndex = (colorIndex + 1) % TangoColorFactory.SEQUENCE_2.size();
        }

        // Legend
        g.setColor(TangoColorFactory.ALUMINIUM_3);
        g.fillRect(5, (int) height - 12 - TEXT_SIZE - (TEXT_SIZE / 2), 5, 5);
        g.drawString("Depot", 15, (int) height - 10 - TEXT_SIZE);
        String vehiclesSizeString = solution.getVehicleList().size() + " vehicles";
        g.drawString(vehiclesSizeString,
                ((int) width - g.getFontMetrics().stringWidth(vehiclesSizeString)) / 2, (int) height - 10 - TEXT_SIZE);
        g.setColor(TangoColorFactory.ALUMINIUM_4);
        g.fillRect(6, (int) height - 6 - (TEXT_SIZE / 2), 3, 3);
        g.drawString((solution instanceof TimeWindowedVehicleRoutingSolution)
                ? "Customer: demand, time window and arrival time"
                : "Customer: demand", 15, (int) height - 5);
        String customersSizeString = solution.getShipmentPotList().size() + " customers";
        g.drawString(customersSizeString,
                ((int) width - g.getFontMetrics().stringWidth(customersSizeString)) / 2, (int) height - 5);
        if (solution.getDistanceType() == DistanceType.AIR_DISTANCE) {
            String clickString = "Right click anywhere on the map to add a customer.";
            g.drawString(clickString, (int) width - 5 - g.getFontMetrics().stringWidth(clickString), (int) height - 5);
        }
        // Show soft score
        g.setColor(TangoColorFactory.ORANGE_3);
        HardSoftLongScore score = solution.getScore();
        if (score != null) {
            String distanceString;
            if (!score.isFeasible()) {
                distanceString = "Not feasible";
            } else {
                distanceString = solution.getDistanceString(NUMBER_FORMAT);
            }
            g.setFont(g.getFont().deriveFont(Font.BOLD, (float) TEXT_SIZE * 2));
            g.drawString(distanceString,
                    (int) width - g.getFontMetrics().stringWidth(distanceString) - 10, (int) height - 10 - TEXT_SIZE);
        }
    }

    private void determineMinimumAndMaximumTimeWindowTime(VehicleRoutingSolutionV2 solution) {
        minimumTimeWindowTime = Double.MAX_VALUE;
        maximumTimeWindowTime = Double.MIN_VALUE;
        for (Depot depot : solution.getDepotList()) {
            if (depot instanceof TimeWindowedDepot) {
                TimeWindowedDepot timeWindowedDepot = (TimeWindowedDepot) depot;
                Double readyTime = timeWindowedDepot.getReadyTime();
                if (readyTime < minimumTimeWindowTime) {
                    minimumTimeWindowTime = readyTime;
                }
                Double dueTime = timeWindowedDepot.getDueTime();
                if (dueTime > maximumTimeWindowTime) {
                    maximumTimeWindowTime = dueTime;
                }
            }
        }
        for (ShipmentPot shipmentPot : solution.getShipmentPotList()) {
            if (shipmentPot instanceof TimeWindowedShipmentPot) {
                TimeWindowedShipmentPot timeWindowedCustomer = (TimeWindowedShipmentPot) shipmentPot;
                Double readyTime = timeWindowedCustomer.getReadyTime();
                if (readyTime < minimumTimeWindowTime) {
                    minimumTimeWindowTime = readyTime;
                }
                Double dueTime = timeWindowedCustomer.getDueTime();
                if (dueTime > maximumTimeWindowTime) {
                    maximumTimeWindowTime = dueTime;
                }
            }
        }
    }

    private int calculateTimeWindowDegree(Double timeWindowTime) {
        return (int) (360L * (timeWindowTime - minimumTimeWindowTime) / (maximumTimeWindowTime - minimumTimeWindowTime));
    }

    public Graphics2D createCanvas(double width, double height) {
        int canvasWidth = (int) Math.ceil(width) + 1;
        int canvasHeight = (int) Math.ceil(height) + 1;
        canvas = new BufferedImage(canvasWidth, canvasHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = canvas.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, canvasWidth, canvasHeight);
        return g;
    }
    private int calcDegreeByHour(int h, int m, int s){
        h = h + (m + s/60) /60;
        h = h <= 12 ? h + 12 : h;
//        return ((360 / 12) * (hour + 1)) - 180;

//        if (h <0 || m < 0 || h >12 || m > 60)
//            System.out.println("Wrong input");
//        if (h == 12)
//            h = 0;
//        if (m == 60)
//            m = 0;
//        int hour_angle = (int)(0.5 * (h*60 + m));
//        int minute_angle = (int)(6*m);
//        int angle = Math.abs(hour_angle - minute_angle);
//        angle = Math.min(360-angle, angle);
//        return angle;
//        return 360 - ((h-21)*30 + 180);

//        int second = h * 24*60*60 + m * 60 + s;
//        return 180 - (second-21* 24*60*60)/(240*12) ;
        return h >= 15 ? (360-(h-15)*30) : (15 - h)*30;
    }

    private int calcDegreeLine(long time){
        LocalDateTime dateTime = DateUtils.toDate(time);
        return calcDegreeLine(dateTime.getHour(), 0, 0);
    }
    private int calcDegreeLine(int h, int m, int s){
        h = h <= 12 ? h : h - 12;
        return h * 30;
    }
}
