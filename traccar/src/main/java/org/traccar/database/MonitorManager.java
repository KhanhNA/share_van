/*
 * Copyright 2015 - 2018 Anton Tananaev (anton@traccar.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.traccar.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traccar.Context;
import org.traccar.model.*;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MonitorManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(MonitorManager.class);

    private final Map<Long, Set<Long>> deviceUsers = new HashMap<>();
    private final Map<Long, Set<Long>> userDevices = new HashMap<>();

    public MonitorManager() {

    }

    public Set<Long> getAllDeviceUsers(long deviceId) {
        if (!deviceUsers.containsKey(deviceId)) {
            deviceUsers.put(deviceId, new HashSet<>());
        }
        return deviceUsers.get(deviceId);
    }
    public void remove(long userId) {
        Set<Long> deviceIds = userDevices.get(userId);

        if(deviceIds != null) {
            for(long deviceId: deviceIds) {
                remove(userId, deviceId);
            }
        }

    }

    public void remove(long userId, long deviceId) {

        Set<Long> userIds = deviceUsers.get(deviceId);
        if(userIds != null) {
            userIds.remove(userId);
        }

    }

    public void addMonitor(long userId, long deviceId) {
        addData(userDevices, userId, deviceId);
        addData(deviceUsers, deviceId, userId);
    }

    public void addData(Map<Long, Set<Long>> map, long key, long value) {

        Set<Long> values = map.get(key);

        if(values == null) {
            values = new HashSet<>();
            map.put(key, values);

        }
        values.add(value);

    }



}
