package com.ac.repository;

import com.ac.entity.ConfigParameter;
import com.ac.entity.LocationData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ConfigParameterRepository extends JpaRepository<ConfigParameter, Integer>,
                                                    JpaSpecificationExecutor<ConfigParameter> {
    ConfigParameter findByKey(String key);
}
