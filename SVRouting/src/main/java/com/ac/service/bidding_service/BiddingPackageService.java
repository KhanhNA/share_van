package com.ac.service.bidding_service;

import com.ac.Constants;
import com.ac.api.ApiManager;
import com.ac.entity.bidding_entity.SharevanBiddingPackage;
import com.ac.entity.bidding_entity.SharevanDepot;
import com.ac.enums.BiddingOrderStatus;
import com.ac.enums.BiddingOrderType;
import com.ac.enums.BiddingPackageStatus;
import com.ac.model.SocketResult;
import com.ac.repository.SharevanBiddingPackageRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Transient;
import javax.transaction.Transactional;
@Service
public class BiddingPackageService {
    @Autowired
    protected SharevanBiddingPackageRepository biddingPackageRepository;

    @PersistenceContext
    protected EntityManager entityManager;
    final  String urlSocket = "http://demo.nextsolutions.com.vn:3001/pushAll";

    private void changeJsonAndCallSocket(List<SharevanBiddingPackage> lstBiddingPackage, String actionType) throws URISyntaxException {
        //if(lstBiddingPackage != null && !lstBiddingPackage.isEmpty()){
        SocketResult sm = new SocketResult();
        sm.setActionType(actionType);
        sm.setLstBiddingPackages((ArrayList<SharevanBiddingPackage>) lstBiddingPackage);
        String json = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create().toJson(sm);
        json = "{\"data\": {"  + " \"result\": {" +
                "        \"records\": [" +
                json +
                "]" +
                "}}}";
        System.out.println(json);
        ApiManager.callApiSocket(json, urlSocket);
        //  }

    }
    private SharevanBiddingPackage editBiddingPackage(SharevanBiddingPackage bp){
        //change price
        int max_count = bp.getMax_count();
        if(max_count > 0){

            Float price = bp.getPrice() + bp.getPrice_level_change();
            bp.setMax_count((max_count-1));
            bp.setPrice(price);
            Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now(ZoneOffset.UTC));
            bp.setChange_price_time(timestamp);
        }
        else{
            bp.setStatus(BiddingPackageStatus.OVERTIME_BIDDING.getValue());
        }
        return bp;
    }
    @Transactional
    protected void changePriceBiddingPackage(List<SharevanBiddingPackage> lstBidding) throws SQLException {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        for(SharevanBiddingPackage i: lstBidding){
            String sql = "update sharevan_bidding_package \n" +
                    "set max_count = ?,\n" +
                    "change_price_time = ?,\n" +
                    "price = ?\n" +
                    "where id = ?";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setInt(1,i.getMax_count());
            st.setTimestamp(2,i.getChange_price_time());
            st.setFloat(3,i.getPrice());
            st.setInt(4,i.getId());
            st.executeUpdate();

        }
        cnn.commit();

    }
    @Transactional
    protected void resetBiddingPackage(List<SharevanBiddingPackage> lstBidding) throws SQLException {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        for(SharevanBiddingPackage i: lstBidding){
            String sql = "update sharevan_bidding_package \n" +
                    "set status = ?,\n" +
                    "confirm_time = ? ," +
                    "max_confirm_time = ? ," +
                    "bidding_order_id = ? " +
                    "where id = ?";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setString(1, BiddingPackageStatus.NOT_BIDDING.getValue());
            st.setTimestamp(2,null);
            st.setTimestamp(3,null);
            st.setTimestamp(4,null);
            st.setInt(5,i.getId());
            st.executeUpdate();

        }
        cnn.commit();

    }
    @Transactional
    protected void destroyBiddingOrder(List<Integer> lstOrder) throws SQLException {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        for(Integer i: lstOrder){
            String sql = "update sharevan_bidding_order \n" +
                    "set type = ?\n" +
                    "where id = ?";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setString(1, BiddingOrderType.CANCEL.getValue());
            st.setInt(2,i);
            st.executeUpdate();
        }
        cnn.commit();

    }
    @Transactional
    protected void destroyBiddingPackage(List<SharevanBiddingPackage> lstBidding) throws SQLException {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        for(SharevanBiddingPackage i: lstBidding){
            String sql = "update sharevan_bidding_package \n" +
                    "set status = ?\n" +
                    "where id = ?";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setString(1,BiddingPackageStatus.OVERTIME_BIDDING.getValue());
            st.setInt(2,i.getId());
            st.executeUpdate();
        }
        cnn.commit();

    }
    @Transactional
    public void getListBiddingPackageForBidding() throws SQLException, URISyntaxException {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        String sql = "select id,sbp.bidding_order_id," +
                "sbp.bidding_package_number," +
                "sbp.status," +
                "to_char(sbp.confirm_time,'yyyy-MM-dd HH:mm:ss') confirm_time," +
                "to_char(sbp.release_time,'yyyy-MM-dd HH:mm:ss') release_time," +
                "to_char(sbp.bidding_time,'yyyy-MM-dd HH:mm:ss') bidding_time," +
                "sbp.max_count," +
                "sbp.from_depot_id," +
                "sbp.to_depot_id," +
                "sbp.total_weight," +
                "sbp.distance," +
                "sbp.from_latitude," +
                "sbp.from_longitude," +
                "sbp.to_latitude," +
                "sbp.to_longitude," +
                "to_char(sbp.from_receive_time,'yyyy-MM-dd HH:mm:ss') from_receive_time ," +
                "to_char(sbp.from_return_time,'yyyy-MM-dd HH:mm:ss') from_return_time," +
                "to_char(sbp.to_receive_time,'yyyy-MM-dd HH:mm:ss') to_receive_time," +
                "to_char(sbp.to_return_time,'yyyy-MM-dd HH:mm:ss') to_return_time," +
                "sbp.price_origin," +
                "sbp.price," +
                "sbp.countdown_time," +
                "sbp.price_time_change," +
                "sbp.price_level_change from sharevan_bidding_package sbp " +
                "where " +
                " (to_char(sbp.bidding_time,'yyyy-mm-dd HH24:MI') = to_char(now() at time zone 'UTC','yyyy-mm-dd HH24:MI') " +
                "  or ((to_char(sbp.release_time,'yyyy-mm-dd HH24:MI') = to_char(now() at time zone 'UTC','yyyy-mm-dd HH24:MI') and sbp.release_time != sbp.bidding_time )))" +
                "and sbp.status = ? ";
        PreparedStatement st = cnn.prepareStatement(sql);
        st.setString(1, BiddingPackageStatus.NOT_BIDDING.getValue());
        ResultSet re = st.executeQuery();

        if(re != null){
            List<SharevanBiddingPackage> lstBiddingPackage = new ArrayList<>();
            if (re.next()){
                changeJsonAndCallSocket(lstBiddingPackage, Constants.BIDDING_TIME);
            }

        }
        else{
            return;
        }
    }

    @Transactional
    public void getListBiddingChangePrice() throws SQLException {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        String sql = "select sbp.id,sbp.bidding_order_id," +
                "sbp.bidding_package_number," +
                "sbp.status," +
                "to_char(sbp.confirm_time,'yyyy-MM-dd HH:mm:ss') confirm_time," +
                "sbp.release_time release_time," +
                "to_char(sbp.bidding_time,'yyyy-MM-dd HH:mm:ss') bidding_time," +
                "sbp.max_count," +
                "sbp.from_depot_id," +
                "sbp.to_depot_id," +
                "sbp.total_weight," +
                "sbp.distance," +
                "sbp.from_latitude," +
                "sbp.from_longitude," +
                "sbp.to_latitude," +
                "sbp.to_longitude," +
                "to_char(sbp.from_receive_time,'yyyy-MM-dd HH:mm:ss') from_receive_time ," +
                "to_char(sbp.from_return_time,'yyyy-MM-dd HH:mm:ss') from_return_time," +
                "to_char(sbp.to_receive_time,'yyyy-MM-dd HH:mm:ss') to_receive_time," +
                "to_char(sbp.to_return_time,'yyyy-MM-dd HH:mm:ss') to_return_time," +
                "sbp.price_origin," +
                "sbp.price," +
                "sbp.countdown_time," +
                "sbp.price_time_change," +
                "fdepot.address fadress," +
                "tdepot.address tadress," +
                "sbp.price_level_change , " +
                "(select count(id) from sharevan_cargo sc where sc.bidding_package_id = sbp.id) cargo " +
                " from sharevan_bidding_package sbp " +
                "left join sharevan_bidding_order bo on sbp.bidding_order_id = bo.id\n" +
                "join sharevan_depot fdepot on sbp.from_depot_id = fdepot.id\n" +
                "join sharevan_depot tdepot on sbp.to_depot_id = tdepot.id\n" +
                "where sbp.status = ? " +
                "and sbp.max_count >= 0 " +
                "and to_char((sbp.change_price_time + (sbp.price_time_change * interval '1 minute' )) ,'yyyy-mm-dd HH24:MI') = to_char(now() at time zone 'UTC','yyyy-mm-dd HH24:MI')";
        PreparedStatement ps = cnn.prepareStatement(sql);
        ps.setString(1, BiddingPackageStatus.NOT_BIDDING.getValue());
        ResultSet re = ps.executeQuery();
        if(re != null){
            List<SharevanBiddingPackage> lstBiddingPackageChangePrice = new ArrayList<>();
            List<SharevanBiddingPackage> lstBiddingPackageDestroy = new ArrayList<>();
            List<SharevanBiddingPackage> lstBiddingPackage = new ArrayList<>();
            while (re.next()){
                SharevanBiddingPackage bp = new SharevanBiddingPackage();
                SharevanDepot fromDepot = new SharevanDepot();
                SharevanDepot toDepot = new SharevanDepot();
                bp.setId(re.getInt("id"));
                System.out.println("bidding_order_id: "+re.getInt("bidding_order_id"));
                bp.setBidding_order_id(re.getInt("bidding_order_id"));
                bp.setBidding_package_number(re.getString("bidding_package_number"));
                bp.setStatus(re.getString("status"));
                System.out.println("confirm_time: "+re.getTimestamp("confirm_time"));
                bp.setConfirm_time(re.getTimestamp("confirm_time"));
                bp.setRelease_time(re.getTimestamp("release_time"));
                bp.setBidding_time(re.getTimestamp("bidding_time"));
                System.out.println("timestamp: " + re.getTimestamp("release_time"));
                System.out.println("get time: " + bp.getRelease_time());
                bp.setMax_count(re.getInt("max_count"));
                fromDepot.setId(re.getInt("from_depot_id"));
                fromDepot.setAddress(re.getString("fadress"));
                toDepot.setId(re.getInt("to_depot_id"));
                toDepot.setAddress(re.getString("tadress"));
//                bp.setFromDepotId(re.getInt("from_depot_id"));
//                bp.setToDepotId(re.getInt("to_depot_id"));
                bp.setTotal_weight(re.getFloat("total_weight"));
                bp.setDistance(re.getFloat("distance"));
                bp.setFrom_latitude(re.getFloat("from_latitude"));
                bp.setFrom_longitude(re.getFloat("from_longitude"));
                bp.setTo_latitude(re.getFloat("to_latitude"));
                bp.setTo_longitude(re.getFloat("to_longitude"));
                bp.setFrom_receive_time(re.getTimestamp("from_receive_time"));
                bp.setFrom_return_time(re.getTimestamp("from_return_time"));
                bp.setTo_receive_time(re.getTimestamp("to_receive_time"));
                bp.setTo_return_time(re.getTimestamp("to_return_time"));
                bp.setPrice_origin(re.getFloat("price_origin"));
                bp.setPrice(re.getFloat("price"));
                bp.setCountdown_time(re.getInt("countdown_time"));
                bp.setPrice_time_change(re.getInt("price_time_change"));
                bp.setPrice_level_change(re.getFloat("price_level_change"));
                bp.setFrom_depot(fromDepot);
                bp.setTo_depot(toDepot);
                bp.setTotal_cargo(re.getInt("cargo"));
                editBiddingPackage(bp);
                lstBiddingPackage.add(bp);
                if(re.getInt("max_count") > 0){
                    lstBiddingPackageChangePrice.add(bp);
                }
                else{
                    lstBiddingPackageDestroy.add(bp);
                }

            }

            try {
                if(!lstBiddingPackageChangePrice.isEmpty()){
                    changePriceBiddingPackage(lstBiddingPackageChangePrice);
                }
                if(!lstBiddingPackageDestroy.isEmpty()){
                    destroyBiddingPackage(lstBiddingPackageDestroy);
                }
                if(!lstBiddingPackageChangePrice.isEmpty() || !lstBiddingPackageDestroy.isEmpty()){
                    if(lstBiddingPackage != null && !lstBiddingPackage.isEmpty()){
                        changeJsonAndCallSocket(lstBiddingPackage, Constants.CHANGE_PRICE);
                    }

                }

            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    @Transactional
    public void checkOutdateBidding() throws SQLException, URISyntaxException {
        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        String sql = "select sbp.id,sbp.bidding_order_id," +
                "sbp.bidding_package_number," +
                "sbp.status," +
                "to_char(sbp.confirm_time,'yyyy-MM-dd HH:mm:ss') confirm_time," +
                "to_char(sbp.release_time,'yyyy-MM-dd HH:mm:ss') release_time," +
                "to_char(sbp.bidding_time,'yyyy-MM-dd HH:mm:ss') bidding_time," +
                "sbp.max_count," +
                "sbp.from_depot_id," +
                "sbp.to_depot_id," +
                "sbp.total_weight," +
                "sbp.distance," +
                "sbp.from_latitude," +
                "sbp.from_longitude," +
                "sbp.to_latitude," +
                "sbp.to_longitude," +
                "to_char(sbp.from_receive_time,'yyyy-MM-dd HH:mm:ss') from_receive_time ," +
                "to_char(sbp.from_return_time,'yyyy-MM-dd HH:mm:ss') from_return_time," +
                "to_char(sbp.to_receive_time,'yyyy-MM-dd HH:mm:ss') to_receive_time," +
                "to_char(sbp.to_return_time,'yyyy-MM-dd HH:mm:ss') to_return_time," +
                "sbp.price_origin," +
                "sbp.price," +
                "sbp.countdown_time," +
                "sbp.price_time_change," +
                "fdepot.address fadress," +
                "tdepot.address tadress," +
                "sbp.price_level_change , " +
                "(select count(id) from sharevan_cargo sc where sc.bidding_package_id = sbp.id) cargo " +
                " from sharevan_bidding_package sbp  " +
                "join sharevan_bidding_order bo on sbp.bidding_order_id = bo.id\n" +
                "join sharevan_depot fdepot on sbp.from_depot_id = fdepot.id\n" +
                "join sharevan_depot tdepot on sbp.to_depot_id = tdepot.id\n" +
                "where sbp.status = ? \n" +
                "and bo.status = ? \n" +
                "and bo.type = ? \n" +
                "and to_char(sbp.max_confirm_time,'yyyy-mm-dd HH24:MI') = to_char(now() at time zone 'UTC','yyyy-mm-dd HH24:MI')";
        PreparedStatement ps = cnn.prepareStatement(sql);
        ps.setString(1,BiddingPackageStatus.WAITING_ACCEPT.getValue());
        ps.setString(2, BiddingOrderStatus.NOT_CONFIRM.getValue());
        ps.setString(3,BiddingOrderType.NOT_APPROVE.getValue());
        ResultSet re = ps.executeQuery();
        if(re != null){
            List<SharevanBiddingPackage> lstBidding = new ArrayList<>();
            List<Integer> lstOrder = new ArrayList<>();
            while (re.next()){
                SharevanBiddingPackage bp = new SharevanBiddingPackage();
                SharevanDepot fromDepot = new SharevanDepot();
                SharevanDepot toDepot = new SharevanDepot();
                bp.setId(re.getInt("id"));
                System.out.println("bidding_order_id: "+re.getInt("bidding_order_id"));
                bp.setBidding_order_id(re.getInt("bidding_order_id"));
                bp.setBidding_package_number(re.getString("bidding_package_number"));
                bp.setStatus(re.getString("status"));
                System.out.println("confirm_time: "+re.getTimestamp("confirm_time"));
                bp.setConfirm_time(re.getTimestamp("confirm_time"));
                bp.setRelease_time(re.getTimestamp("release_time"));
                bp.setBidding_time(re.getTimestamp("bidding_time"));
                bp.setMax_count(re.getInt("max_count"));
                fromDepot.setId(re.getInt("from_depot_id"));
                fromDepot.setAddress(re.getString("fadress"));
                toDepot.setId(re.getInt("to_depot_id"));
                toDepot.setAddress(re.getString("tadress"));
//                bp.setFromDepotId(re.getInt("from_depot_id"));
//                bp.setToDepotId(re.getInt("to_depot_id"));
                bp.setTotal_weight(re.getFloat("total_weight"));
                bp.setDistance(re.getFloat("distance"));
                bp.setFrom_latitude(re.getFloat("from_latitude"));
                bp.setFrom_longitude(re.getFloat("from_longitude"));
                bp.setTo_latitude(re.getFloat("to_latitude"));
                bp.setTo_longitude(re.getFloat("to_longitude"));
                bp.setFrom_receive_time(re.getTimestamp("from_receive_time"));
                bp.setFrom_return_time(re.getTimestamp("from_return_time"));
                bp.setTo_receive_time(re.getTimestamp("to_receive_time"));
                bp.setTo_return_time(re.getTimestamp("to_return_time"));
                bp.setPrice_origin(re.getFloat("price_origin"));
                bp.setPrice(re.getFloat("price"));
                bp.setCountdown_time(re.getInt("countdown_time"));
                bp.setPrice_time_change(re.getInt("price_time_change"));
                bp.setPrice_level_change(re.getFloat("price_level_change"));
                bp.setFrom_depot(fromDepot);
                bp.setTo_depot(toDepot);
                bp.setTotal_cargo(re.getInt("cargo"));
                if(re.getInt("bidding_order_id") > 0){
                    lstOrder.add(bp.getBidding_order_id());
                }
                bp.setStatus(BiddingPackageStatus.NOT_BIDDING.getValue());
                bp.setConfirm_time(null);
                bp.setMax_confirm_time(null);
                bp.setBidding_order_id(null);
                lstBidding.add(bp);
            }

            if(!lstOrder.isEmpty()){
                destroyBiddingOrder(lstOrder);
            }

            System.out.println("size: " + lstBidding.size() );
            if( !lstBidding.isEmpty()) {
                resetBiddingPackage(lstBidding);
                System.out.println("thomvu");
                changeJsonAndCallSocket(lstBidding, Constants.OVERTIME_BIDDING);
            }
        }
    }
    @Transactional
    public void getTimeSlotChangeBidding() throws SQLException {
        Connection con =  entityManager.unwrap(SessionImpl.class).connection();
        String sql = "select id from sharevan_bidding_package sbp " +
                "where to_char(sbp.bidding_time,'yyyy-mm-dd HH24:00:00') = to_char(now() at time zone 'UTC','yyyy-mm-dd HH24:00:00')";
        PreparedStatement ps = con.prepareStatement(sql);
        ResultSet re = ps.executeQuery();
        if(re.next()){
            try {
                changeJsonAndCallSocket(new ArrayList<SharevanBiddingPackage>(),Constants.BIDDING_TIME);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }
}
