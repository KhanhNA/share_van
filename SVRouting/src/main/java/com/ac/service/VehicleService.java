package com.ac.service;

import com.ac.entity.FleetDriver;
import com.ac.entity.FleetVehicle;
import com.ac.entity.SharevanBillPackageRoutingPlan;
import com.ac.enums.ShipType;
import com.ac.enums.StatusRouting;
import com.ac.enums.StatusType;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Service
public class VehicleService {
    @PersistenceContext
    protected EntityManager entityManager;

    public List<FleetVehicle> getListAvailableVehicle(String datePlan){
        Query query  = entityManager.createQuery
                ("select fv  \n" +
                        "                     from FleetVehicle fv \n" +
                        "                             join FleetVehicleState fvs on fvs.id = fv.stateId \n" +
                        "                             where fv.active = true and fvs.code  in ('AVAILABLE') \n" +
                        "                             and fv.capacity > 0 and fv.capacity is not null and fv.companyId = 1 \n" +
                        "                            AND  EXISTS (select 1 \n" +
                        "                                  from FleetVehicleAssignationLog al \n" +
                        "                                   where al.vehicleId = fv.id \n" +
                        "                                  and al.status = :status \n" +
                        "                                  and al.driverStatus = '1' \n" +
                        "                                  and al.startDate = to_date(:datePlan,'dd/mm/yyyy')) \n" +
                        "                             AND NOT EXISTS (select 1  \n" +
                        "                                   from RoutingPlanDay rpd \n" +
                        "                                   where rpd.vehicleId = fv.id \n" +
                        "                                   and rpd.status in (:status1,:status2) \n" +
                        "                                   and rpd.datePlan = to_date(:datePlan,'dd/mm/yyyy')) ", FleetVehicle.class);


        query.setParameter("status1", StatusRouting.NOT_CONFIRM.getValue());
        query.setParameter("status2",StatusRouting.DRAFT.getValue());
        query.setParameter("status",StatusType.RUNNING.getValue());

      //  query.setParameter("shipType", ShipType.IN_ZONE.getValue());
        query.setParameter("datePlan",datePlan);
        List<FleetVehicle> result = query.getResultList();
        if(result != null && !result.isEmpty()){
            return result;
        }
        return null;
    }
    public FleetDriver getDriverByAssignationLog(String datePlan, Integer vehicleId){
        Query query = entityManager.createQuery("select fd from FleetVehicleAssignationLog l\n" +
                " join FleetDriver fd on fd.id = l.driverId " +
                "where l.vehicleId = :vehicleId\n" +
                //   " and driverId = :driverId\n" +
                " and l.startDate = to_date(:startDate,'dd/mm/yyyy')\n" +
                " and giveCarBack is  null \n" +
                " and l.status = :status \n" +
                " and l.driverStatus = '1' order by fd.id desc ", FleetDriver.class);
        query.setParameter("vehicleId", vehicleId);
        query.setParameter("startDate", datePlan);
        query.setParameter("status",StatusType.RUNNING.getValue());
        List<FleetDriver> lstDriver = query.getResultList();
        if(lstDriver != null &&  !lstDriver.isEmpty()){
            return lstDriver.get(0);
        }
        return null;
    }



}
