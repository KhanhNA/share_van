package com.ac.controller;

import com.ac.entity.RoutingPlanDay;
import com.ac.model.RoutingClaim;
import com.ac.repository.LocationDataRepository;
import com.ac.service.RoutingPlanDayService;
import ns.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/routing")
public class RoutingController {
    @Autowired
    LocationDataRepository dataRepository;
    @Autowired
    RoutingPlanDayService routingPlanDayService;
    @PostMapping("/assign_routing_sos")
    public ResponseEntity<Object> assignRoutingSOS(@RequestBody List<RoutingPlanDay> entity){
        LocalDateTime ldt = LocalDateTime.now();
        System.out.println("sixdmdkcmdkm: " + entity.size());
        for(RoutingPlanDay d: entity){
            System.out.println(d);
        }
        List<RoutingPlanDay> data = routingPlanDayService.assignRoutingSOS(entity, DateUtils.toString(ldt,DateUtils.DD_MM_YYYY));
        for(RoutingPlanDay d: data){
            System.out.println(data);
        }
        if(data == null || data.isEmpty()){
            return new ResponseEntity<>(data, HttpStatus.NO_CONTENT);
        }
        return  new ResponseEntity<>(data, HttpStatus.OK);
    }
    @PostMapping("/assign_routing/nocustomer")
    public ResponseEntity<Object> assignRoutingNotCustomer(@RequestBody RoutingClaim claim){
        Integer[] lstIdRouting = claim.getLstIdRouting();
        String type = claim.getType();
        System.out.println(type);

        LocalDateTime ldt = LocalDateTime.now();
        if(lstIdRouting != null && lstIdRouting.length > 0 && type != null){
            List<Integer> lstIdNewRouting = routingPlanDayService.assignRoutingNoCustomer(lstIdRouting,type,DateUtils.toString(ldt, DateUtils.DD_MM_YYYY));
            return  new ResponseEntity<>(lstIdNewRouting, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<RoutingPlanDay>(), HttpStatus.NO_CONTENT);
    }

}
