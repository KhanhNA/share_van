package com.ac.entity;

import com.ac.Constants;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * bill package routing when export
 */
@Entity
@Getter
@Setter
@Table(name = "sharevan_routing_plan_day_service")
public class SharevanRoutingPlanDayService implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    /**
     * quantity by export
     */
    @Column(name = "billading_detail_id")
    private Integer billLadingDetailId;

    /**
     * length of plan pacel
     */
    @Column(name = "routing_plan_day_id")
    private Integer routingPlanDayId;

    /**
     * width of plan pacel
     */
    @Column(name = "service_id")
    private Integer serviceId;

    /**
     * height of plan pacel
     */
    @Column(name = "assign_partner_id")
    private Integer assignPartnerId;

    /**
     * total_weight of plan pacel
     */
//    @Column(name = "name")
//    private String name;

    /**
     * capacity of plan pacel
     */
    @Column(name="type")
    private String type;

    @Column(name = "create_uid")
    private Integer createUid;

    /**
     * Created on
     */
    @Column(name = "create_date")
    private Timestamp createDate;

    /**
     * Last Updated by
     */
    @Column(name = "write_uid")
    private Integer writeUid;

    /**
     * Last Updated on
     */
    @Column(name = "write_date")
    private Timestamp writeDate;

    public SharevanRoutingPlanDayService() {
    }

    public SharevanRoutingPlanDayService(SharevanRoutingPlanDayService other) {
       // this.id = other.id;
        this.billLadingDetailId = other.billLadingDetailId;
        this.routingPlanDayId = other.routingPlanDayId;
        this.serviceId = other.serviceId;
        this.assignPartnerId = other.assignPartnerId;
        this.type = other.type;
        this.createUid = other.createUid;
        this.createDate = Constants.getNowUTC();
        this.writeUid = other.writeUid;
        this.writeDate = Constants.getNowUTC();
    }
}