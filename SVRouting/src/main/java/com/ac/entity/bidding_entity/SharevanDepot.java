package com.ac.entity.bidding_entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * depot
 */
@Table(name = "sharevan_depot")
@Getter
@Setter
@Entity
public class SharevanDepot implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    /**
     * Depot Name
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * Depot Code
     */
    @Column(name = "depot_code")
    private String depot_code;

    /**
     * Address
     */
    @Column(name = "address")
    private String address;

    /**
     * Street
     */
    @Column(name = "street")
    private String street;

    /**
     * Street2
     */
    @Column(name = "street2")
    private String street2;

    /**
     * City
     */
    @Column(name = "city_name")
    private String city_name;

    /**
     * District
     */
    @Column(name = "district", nullable = false)
    private Integer district;

    /**
     * Ward
     */
    @Column(name = "ward")
    private Integer ward;

    /**
     * Zip
     */
    @Column(name = "zip")
    private String zip;

    /**
     * Province
     */
    @Column(name = "state_id", nullable = false)
    private Integer state_id;

    /**
     * Country
     */
    @Column(name = "country_id", nullable = false)
    private Integer country_id;

    /**
     * Geo Latitude
     */
    @Column(name = "latitude")
    private BigDecimal latitude;

    /**
     * Geo Longitude
     */
    @Column(name = "longitude")
    private BigDecimal longitude;

    /**
     * Phone
     */
    @Column(name = "phone")
    private String phone;

    /**
     * Zone
     */
    @Column(name = "zone_id")
    private Integer zone_id;

    /**
     * Area
     */
    @Column(name = "area_id")
    private Integer area_id;

    /**
     * Company
     */
    @Column(name = "company_id", nullable = false)
    private Integer company_id;

    /**
     * Customer
     */
    @Column(name = "customer_id", nullable = false)
    private Integer customer_id;

    /**
     * Status
     */
    @Column(name = "status")
    private String status;

    /**
     * Depot Reference
     */
    @Column(name = "name_seq", nullable = false)
    private String name_seq;

    /**
     * Created by
     */
    @Column(name = "create_uid")
    private Integer create_uid;

    /**
     * Created on
     */
    @Column(name = "create_date")
    private String create_date;

    /**
     * Last Updated by
     */
    @Column(name = "write_uid")
    private Integer write_uid;

    /**
     * Last Updated on
     */
    @Column(name = "write_date")
    private String write_date;

    /**
     * Open time
     */
    @Column(name = "open_time")
    private BigDecimal open_time;

    /**
     * Closing time
     */
    @Column(name = "closing_time")
    private BigDecimal closing_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepot_code() {
        return depot_code;
    }

    public void setDepot_code(String depot_code) {
        this.depot_code = depot_code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public Integer getDistrict() {
        return district;
    }

    public void setDistrict(Integer district) {
        this.district = district;
    }

    public Integer getWard() {
        return ward;
    }

    public void setWard(Integer ward) {
        this.ward = ward;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public Integer getState_id() {
        return state_id;
    }

    public void setState_id(Integer state_id) {
        this.state_id = state_id;
    }

    public Integer getCountry_id() {
        return country_id;
    }

    public void setCountry_id(Integer country_id) {
        this.country_id = country_id;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getZone_id() {
        return zone_id;
    }

    public void setZone_id(Integer zone_id) {
        this.zone_id = zone_id;
    }

    public Integer getArea_id() {
        return area_id;
    }

    public void setArea_id(Integer area_id) {
        this.area_id = area_id;
    }

    public Integer getCompany_id() {
        return company_id;
    }

    public void setCompany_id(Integer company_id) {
        this.company_id = company_id;
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName_seq() {
        return name_seq;
    }

    public void setName_seq(String name_seq) {
        this.name_seq = name_seq;
    }

    public Integer getCreate_uid() {
        return create_uid;
    }

    public void setCreate_uid(Integer create_uid) {
        this.create_uid = create_uid;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public Integer getWrite_uid() {
        return write_uid;
    }

    public void setWrite_uid(Integer write_uid) {
        this.write_uid = write_uid;
    }

    public String getWrite_date() {
        return write_date;
    }

    public void setWrite_date(String write_date) {
        this.write_date = write_date;
    }

    public BigDecimal getOpen_time() {
        return open_time;
    }

    public void setOpen_time(BigDecimal open_time) {
        this.open_time = open_time;
    }

    public BigDecimal getClosing_time() {
        return closing_time;
    }

    public void setClosing_time(BigDecimal closing_time) {
        this.closing_time = closing_time;
    }
}