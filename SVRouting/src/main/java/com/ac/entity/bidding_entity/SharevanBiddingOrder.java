package com.ac.entity.bidding_entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Bidding order
 */
@Entity
@Getter
@Setter
@Table(name = "sharevan_bidding_order")
public class SharevanBiddingOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    /**
     * Company
     */
    @Column(name = "company_id", nullable = false)
    private Integer companyId;

    /**
     * Bidding order number
     */
    @Column(name = "bidding_order_number", nullable = false)
    private String biddingOrderNumber;

    /**
     * From Depot
     */
    @Column(name = "from_depot_id", nullable = false)
    private Integer fromDepotId;

    /**
     * To Depot
     */
    @Column(name = "to_depot_id", nullable = false)
    private Integer toDepotId;

    /**
     * Total weight
     */
    @Column(name = "total_weight", nullable = false)
    private Float totalWeight;

    /**
     * Total cargo
     */
    @Column(name = "total_cargo", nullable = false)
    private Integer totalCargo;

    /**
     * Price
     */
    @Column(name = "price", nullable = false)
    private Float price;

    /**
     * Distance
     */
    @Column(name = "distance", nullable = false)
    private Float distance;

    /**
     * Type
     */
    @Column(name = "type", nullable = false)
    private String type;

    /**
     * Status
     */
    @Column(name = "status", nullable = false)
    private String status;

    /**
     * Cargo
     */
    @Column(name = "cargo_id")
    private Integer cargoId;

    /**
     * Default Description
     */
    @Column(name = "note")
    private String note;

    /**
     * Bidding order received
     */
//    @Column(name = "bidding_order_receive_id")
//    private Integer biddingOrderReceiveId;
//
//    /**
//     * Bidding order return
//     */
//    @Column(name = "bidding_order_return_id")
//    private Integer biddingOrderReturnId;

    /**
     * Created by
     */
    @Column(name = "create_uid")
    private Integer createUid;

    /**
     * Created on
     */
    @Column(name = "create_date")
    private java.sql.Timestamp createDate;

    /**
     * Last Updated by
     */
    @Column(name = "write_uid")
    private Integer writeUid;

    /**
     * Last Updated on
     */
    @Column(name = "write_date")
    private java.sql.Timestamp writeDate;

    /**
     * Bidding package
     */
    @Column(name = "bidding_package_id")
    private Integer biddingPackageId;

    /**
     * Pick up from time
     */
    @Column(name = "from_receive_time")
    private java.sql.Timestamp fromReceiveTime;

    /**
     * Pick up to time
     */
    @Column(name = "to_receive_time")
    private java.sql.Timestamp toReceiveTime;

    /**
     * Drop from time
     */
    @Column(name = "from_return_time")
    private java.sql.Timestamp fromReturnTime;

    /**
     * Drop to time
     */
    @Column(name = "to_return_time")
    private java.sql.Timestamp toReturnTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getBiddingOrderNumber() {
        return biddingOrderNumber;
    }

    public void setBiddingOrderNumber(String biddingOrderNumber) {
        this.biddingOrderNumber = biddingOrderNumber;
    }

    public Integer getFromDepotId() {
        return fromDepotId;
    }

    public void setFromDepotId(Integer fromDepotId) {
        this.fromDepotId = fromDepotId;
    }

    public Integer getToDepotId() {
        return toDepotId;
    }

    public void setToDepotId(Integer toDepotId) {
        this.toDepotId = toDepotId;
    }

    public Float getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Float totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Integer getTotalCargo() {
        return totalCargo;
    }

    public void setTotalCargo(Integer totalCargo) {
        this.totalCargo = totalCargo;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCargoId() {
        return cargoId;
    }

    public void setCargoId(Integer cargoId) {
        this.cargoId = cargoId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getCreateUid() {
        return createUid;
    }

    public void setCreateUid(Integer createUid) {
        this.createUid = createUid;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Integer getWriteUid() {
        return writeUid;
    }

    public void setWriteUid(Integer writeUid) {
        this.writeUid = writeUid;
    }

    public Timestamp getWriteDate() {
        return writeDate;
    }

    public void setWriteDate(Timestamp writeDate) {
        this.writeDate = writeDate;
    }

    public Integer getBiddingPackageId() {
        return biddingPackageId;
    }

    public void setBiddingPackageId(Integer biddingPackageId) {
        this.biddingPackageId = biddingPackageId;
    }

    public Timestamp getFromReceiveTime() {
        return fromReceiveTime;
    }

    public void setFromReceiveTime(Timestamp fromReceiveTime) {
        this.fromReceiveTime = fromReceiveTime;
    }

    public Timestamp getToReceiveTime() {
        return toReceiveTime;
    }

    public void setToReceiveTime(Timestamp toReceiveTime) {
        this.toReceiveTime = toReceiveTime;
    }

    public Timestamp getFromReturnTime() {
        return fromReturnTime;
    }

    public void setFromReturnTime(Timestamp fromReturnTime) {
        this.fromReturnTime = fromReturnTime;
    }

    public Timestamp getToReturnTime() {
        return toReturnTime;
    }

    public void setToReturnTime(Timestamp toReturnTime) {
        this.toReturnTime = toReturnTime;
    }
}