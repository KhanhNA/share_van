package com.ac.entity.bidding_entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

import java.sql.Timestamp;

/**
 * cargo
 */
@Getter
@Setter
@Entity
@Table(name = "sharevan_cargo")
public class SharevanCargo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    /**
     * cargo code, HN-HCM : HNTHCM01 or HN_HCM_01
     */
    @Column(name = "cargo_number")
    private String cargoNumber;

    /**
     * From Depot
     */
    @Column(name = "from_depot_id", nullable = false)
    private Integer fromDepotId;

    /**
     * To Depot
     */
    @Column(name = "to_depot_id", nullable = false)
    private Integer toDepotId;

    /**
     * Distance
     */
    @Column(name = "distance", nullable = false)
    private Float distance;

    /**
     * Size standard
     */
    @Column(name = "size_id", nullable = false)
    private Integer sizeId;

    /**
     * Created by
     */
    @Column(name = "create_uid")
    private Integer createUid;

    /**
     * Created on
     */
    @Column(name = "create_date")
    private java.sql.Timestamp createDate;

    /**
     * Last Updated by
     */
    @Column(name = "write_uid")
    private Integer writeUid;

    /**
     * Last Updated on
     */
    @Column(name = "write_date")
    private java.sql.Timestamp writeDate;

    /**
     * Weight
     */
    @Column(name = "weight", nullable = false)
    private Float weight;

    /**
     * Default Description
     */
    @Column(name = "description")
    private String description;

    /**
     * Price
     */
    @Column(name = "price")
    private Float price;

    /**
     * From latitude
     */
    @Column(name = "from_latitude")
    private Float fromLatitude;

    /**
     * To latitude
     */
    @Column(name = "to_latitude")
    private Float toLatitude;

    /**
     * Bidding package
     */
    @Column(name = "bidding_package_id")
    private Integer biddingPackageId;

    /**
     * From longitude
     */
    @Column(name = "from_longitude")
    private Float fromLongitude;

    /**
     * To longitude
     */
    @Column(name = "to_longitude")
    private Float toLongitude;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCargoNumber() {
        return cargoNumber;
    }

    public void setCargoNumber(String cargoNumber) {
        this.cargoNumber = cargoNumber;
    }

    public Integer getFromDepotId() {
        return fromDepotId;
    }

    public void setFromDepotId(Integer fromDepotId) {
        this.fromDepotId = fromDepotId;
    }

    public Integer getToDepotId() {
        return toDepotId;
    }

    public void setToDepotId(Integer toDepotId) {
        this.toDepotId = toDepotId;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public Integer getCreateUid() {
        return createUid;
    }

    public void setCreateUid(Integer createUid) {
        this.createUid = createUid;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Integer getWriteUid() {
        return writeUid;
    }

    public void setWriteUid(Integer writeUid) {
        this.writeUid = writeUid;
    }

    public Timestamp getWriteDate() {
        return writeDate;
    }

    public void setWriteDate(Timestamp writeDate) {
        this.writeDate = writeDate;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getFromLatitude() {
        return fromLatitude;
    }

    public void setFromLatitude(Float fromLatitude) {
        this.fromLatitude = fromLatitude;
    }

    public Float getToLatitude() {
        return toLatitude;
    }

    public void setToLatitude(Float toLatitude) {
        this.toLatitude = toLatitude;
    }

    public Integer getBiddingPackageId() {
        return biddingPackageId;
    }

    public void setBiddingPackageId(Integer biddingPackageId) {
        this.biddingPackageId = biddingPackageId;
    }

    public Float getFromLongitude() {
        return fromLongitude;
    }

    public void setFromLongitude(Float fromLongitude) {
        this.fromLongitude = fromLongitude;
    }

    public Float getToLongitude() {
        return toLongitude;
    }

    public void setToLongitude(Float toLongitude) {
        this.toLongitude = toLongitude;
    }
}