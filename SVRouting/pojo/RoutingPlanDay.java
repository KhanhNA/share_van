package com.sample;


public class SharevanRoutingPlanDay {

  private long id;
  private String routingPlanDayCode;
  private java.sql.Date datePlan;
  private long vehicleId;
  private long driverId;
  private String latitude;
  private String longitude;
  private long orderNumber;
  private String status;
  private double capacityExpected;
  private java.sql.Timestamp expectedFromTime;
  private java.sql.Timestamp expectedToTime;
  private java.sql.Timestamp actualTime;
  private long warehouseId;
  private long zoneAreaId;
  private String warehouseType;
  private long stockManId;
  private long createUid;
  private java.sql.Timestamp createDate;
  private long writeUid;
  private java.sql.Timestamp writeDate;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getRoutingPlanDayCode() {
    return routingPlanDayCode;
  }

  public void setRoutingPlanDayCode(String routingPlanDayCode) {
    this.routingPlanDayCode = routingPlanDayCode;
  }


  public java.sql.Date getDatePlan() {
    return datePlan;
  }

  public void setDatePlan(java.sql.Date datePlan) {
    this.datePlan = datePlan;
  }


  public long getVehicleId() {
    return vehicleId;
  }

  public void setVehicleId(long vehicleId) {
    this.vehicleId = vehicleId;
  }


  public long getDriverId() {
    return driverId;
  }

  public void setDriverId(long driverId) {
    this.driverId = driverId;
  }


  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }


  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }


  public long getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(long orderNumber) {
    this.orderNumber = orderNumber;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public double getCapacityExpected() {
    return capacityExpected;
  }

  public void setCapacityExpected(double capacityExpected) {
    this.capacityExpected = capacityExpected;
  }


  public java.sql.Timestamp getExpectedFromTime() {
    return expectedFromTime;
  }

  public void setExpectedFromTime(java.sql.Timestamp expectedFromTime) {
    this.expectedFromTime = expectedFromTime;
  }


  public java.sql.Timestamp getExpectedToTime() {
    return expectedToTime;
  }

  public void setExpectedToTime(java.sql.Timestamp expectedToTime) {
    this.expectedToTime = expectedToTime;
  }


  public java.sql.Timestamp getActualTime() {
    return actualTime;
  }

  public void setActualTime(java.sql.Timestamp actualTime) {
    this.actualTime = actualTime;
  }


  public long getWarehouseId() {
    return warehouseId;
  }

  public void setWarehouseId(long warehouseId) {
    this.warehouseId = warehouseId;
  }


  public long getZoneAreaId() {
    return zoneAreaId;
  }

  public void setZoneAreaId(long zoneAreaId) {
    this.zoneAreaId = zoneAreaId;
  }


  public String getWarehouseType() {
    return warehouseType;
  }

  public void setWarehouseType(String warehouseType) {
    this.warehouseType = warehouseType;
  }


  public long getStockManId() {
    return stockManId;
  }

  public void setStockManId(long stockManId) {
    this.stockManId = stockManId;
  }


  public long getCreateUid() {
    return createUid;
  }

  public void setCreateUid(long createUid) {
    this.createUid = createUid;
  }


  public java.sql.Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(java.sql.Timestamp createDate) {
    this.createDate = createDate;
  }


  public long getWriteUid() {
    return writeUid;
  }

  public void setWriteUid(long writeUid) {
    this.writeUid = writeUid;
  }


  public java.sql.Timestamp getWriteDate() {
    return writeDate;
  }

  public void setWriteDate(java.sql.Timestamp writeDate) {
    this.writeDate = writeDate;
  }

}
