import {FormGroup} from '@angular/forms';

export class CustomerModel {
  id: number;
  code: string;
  name: string;
  address: string;
  phone: string;
  status: boolean;
  vip: boolean;

  constructor(form: FormGroup | number) {
    if (form instanceof FormGroup) {
      if (form.get('id')) {
        this.id = form.get('id').value;
      }
      if (form.get('code')) {
        this.code = form.get('code').value;
      }
      if (form.get('name')) {
        this.name = form.get('name').value;
      }
      if (form.get('address')) {
        this.address = form.get('address').value;
      }
      if (form.get('phone')) {
        this.phone = form.get('phone').value;
      }
      if (form.get('status')) {
        this.status = form.get('status').value === 1 ? true : false;
      }
      if (form.get('vip')) {
        this.vip = form.get('vip').value === 1 ? true : false;
      }
    } else {
      this.id = form;
    }
  }
}
