// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  BASE_URL: 'http://localhost:8001',
  BASE_AUTHORIZATION_URL: 'http://192.168.1.69:9999',
  CLIENT_ID: 'ShareVan',
  CLIENT_SECRET: '1qazXSW@3edcVFR$5tgbNHY^7ujm<KI*9ol.?:P)',
  PAGE_SIZE: 10,
  PAGE_SIZE_OPTIONS: [5, 10, 20, 30, 50, 100],
  DEFAULT_LANGUAGE: 'vi',
  DEFAULT_THEME: 'default',
  AUTHORITIES: [] as string[],
  LANGS: [{id: 1, code: 'vi', name: 'Việt Nam', logo: 'assets/Flags/vi.ico'},
    {id: 2, code: 'en', name: 'English', logo: 'assets/Flags/en.ico'},
    {id: 3, code: 'my', name: 'Myamar', logo: 'assets/Flags/vi.ico'}],
  CURR_LANG: 'vi',
  API_DATE_FORMAT: 'yyyy-MM-dd HH:mm:ss.SSS\'Z\'',
  DIS_DATE_FORMAT: 'dd/MM/yyyy',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
