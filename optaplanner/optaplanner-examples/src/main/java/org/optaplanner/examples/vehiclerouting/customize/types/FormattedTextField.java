package org.optaplanner.examples.vehiclerouting.customize.types;

import javax.swing.*;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.text.DecimalFormat;

public class FormattedTextField extends JPanel {
    private JFormattedTextField inputField;
    private final Dimension lblSize = new Dimension(60, 20);
    private JLabel label;

//    public FormattedTextField(String label, Object defaultValue,  Formater format) {
//        this(label, defaultValue, (DefaultFormatter)null);
//    }

    public FormattedTextField(String label, Object defaultValue, DefaultFormatter format) {
        super();
        setLayout(new FlowLayout(FlowLayout.LEFT, 6, 0));
        if(format == null){
            this.inputField = new JFormattedTextField();
        }else {
            this.inputField = new JFormattedTextField(format);
        }

        this.inputField.setValue(defaultValue);
        this.label = new JLabel(label);
        this.label.setLabelFor(this.inputField);
        this.add(this.label);
        this.add(this.inputField);


        this.label.setMinimumSize(lblSize);
        this.label.setPreferredSize(lblSize);
        this.label.setMaximumSize(lblSize);
//        this.inputField.setPreferredSize(new Dimension(100, 60));
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                System.out.println("Resized to " + e.getComponent().getSize());
                Dimension d = e.getComponent().getSize();
//                FormattedTextField.this.inputField.setMinimumSize(new Dimension(d.width- lblSize.width - 10, lblSize.height));
                FormattedTextField.this.inputField.setMinimumSize(new Dimension(d.width - 100, 30));
                FormattedTextField.this.inputField.setPreferredSize(new Dimension(d.width - 100, 30));
                FormattedTextField.this.inputField.setMaximumSize(new Dimension(d.width - 100, 30));
//                FormattedTextField.this.inputField.setMaximumSize(new Dimension(d.width - lblSize.width - 10, lblSize.height));
            }

        });
    }

    public String getText() {
        return this.inputField.getText();
    }

    public Object getValue() {
        return this.inputField.getValue();
    }

    public DefaultFormatter doubleFormat() {
        DecimalFormat format = new DecimalFormat("##,####");
        format.setGroupingUsed(true);
        NumberFormatter formatter = new NumberFormatter(format);
        formatter.setValueClass(Integer.class);
        formatter.setMaximum(Integer.MAX_VALUE);
        formatter.setAllowsInvalid(false);

        return formatter;
    }




//    private Format getFomat(Object obj){
//        if(obj instanceof Number){
//            return NumberFormat.getNumberInstance();
//        }
//        return null;
//    }

}
