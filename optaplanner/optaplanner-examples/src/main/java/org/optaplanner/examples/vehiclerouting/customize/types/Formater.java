package org.optaplanner.examples.vehiclerouting.customize.types;

import ns.utils.DateUtils;

import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.MaskFormatter;
import javax.swing.text.NumberFormatter;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class Formater {
    public static final DefaultFormatter LONGITUDE = longitudeFormat();
    public static final DefaultFormatter LATITUDE = latitudeFormat();
    public static final DefaultFormatter DATE = dateFormat();
    public static final DefaultFormatter LONG = numberFormat(Long.class);
    public static final DefaultFormatter INTEGER = numberFormat(Integer.class);


    public static DefaultFormatter latitudeFormat() {
        return maskFormat("##.######", '0', true);

    }

    public static DefaultFormatter dateFormat(){
        DateFormat displayFormat = new SimpleDateFormat(DateUtils.DD_MM_YYYY_HH_MM_SS);
        DateFormatter displayFormatter = new DateFormatter(displayFormat);

        return displayFormatter;
    }
    public static DefaultFormatter longitudeFormat() {
        return maskFormat("##.######", '0', true);
    }


    public static DefaultFormatter maskFormat(String masker, char placeholder, boolean overwriteMode){
        try {
            MaskFormatter maskFormatter = new MaskFormatter(masker);

            maskFormatter.setPlaceholderCharacter(placeholder);
//            maskFormatter.setOverwriteMode(overwriteMode);
            return maskFormatter;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static DefaultFormatter numberFormat(Class cls) {
        NumberFormat format = NumberFormat.getInstance();
        NumberFormatter formatter = new NumberFormatter(format);
        formatter.setValueClass(cls);
//        formatter.setMinimum(0);
//        formatter.setMaximum(Integer.MAX_VALUE);
        formatter.setAllowsInvalid(false);
        // If you want the value to be committed on each keystroke instead of focus lost
//        formatter.setCommitsOnValidEdit(true);
        return formatter;
    }

}
