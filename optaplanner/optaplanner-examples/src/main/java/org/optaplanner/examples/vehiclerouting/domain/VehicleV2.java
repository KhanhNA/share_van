/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.vehiclerouting.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.examples.vehiclerouting.customize.entity.ShipmentPot;
import org.optaplanner.examples.vehiclerouting.customize.entity.ShipmentStandstill;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;

@XStreamAlias("VrpVehicle")
@Getter
@Setter
public class VehicleV2 extends AbstractPersistable implements ShipmentStandstill {

    protected Long capacity;
    protected Depot depot;
    protected Double vehicleCost;
    // Shadow variables
    protected ShipmentPot nextShipmentPot;

    protected String name;

    public VehicleV2() {
    }

    public VehicleV2(String name, long id, Long capacity, Depot depot, Double vehicleCost) {
        super(id);
        this.capacity = capacity;
        this.depot = depot;
        this.vehicleCost = vehicleCost;
        this.name = name;
    }



    @Override
    public ShipmentPot getNextShipmentPot() {
        return nextShipmentPot;
    }

    @Override
    public void setNextShipmentPot(ShipmentPot nextShipmentPot) {
        this.nextShipmentPot = nextShipmentPot;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************

    @Override
    public VehicleV2 getVehicle() {
        return this;
    }

    @Override
    public Location getLocation() {
        return depot.getLocation();
    }

    @Override
    public String getCode() {
        return "D" + depot.getId();
    }

    @Override
    public String getCodeFromId() {
        return "D" + depot.getId();
    }

    /**
     * @param standstill never null
     * @return a positive number, the distance multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public long getDistanceTo(Standstill standstill) {
        return depot.getDistanceTo(standstill);
    }

    @Override
    public String toString() {
        Location location = getLocation();
        if (location.getName() == null) {
            return super.toString();
        }
        return location.getName() + "/" + super.toString();
    }
    @Override
    public String getInfo(){
        return this.toString();
    }
}
