/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.vehiclerouting.swingui;

import org.optaplanner.examples.common.swingui.latitudelongitude.LatitudeLongitudeTranslator;
import org.optaplanner.examples.vehiclerouting.customize.entity.ShipmentPot;
import org.optaplanner.examples.vehiclerouting.customize.entity.ShipmentStandstill;
import org.optaplanner.examples.vehiclerouting.customize.panel.DepotPanel;
import org.optaplanner.examples.vehiclerouting.customize.panel.DistancePanel;
import org.optaplanner.examples.vehiclerouting.customize.panel.ShipmentPanel;
import org.optaplanner.examples.vehiclerouting.customize.types.ObjCRUD;
import org.optaplanner.examples.vehiclerouting.customize.types.RefObject;
import org.optaplanner.examples.vehiclerouting.domain.Depot;
import org.optaplanner.examples.vehiclerouting.domain.VehicleRoutingSolutionV2;
import org.optaplanner.examples.vehiclerouting.domain.VehicleV2;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.List;

public class VehicleRoutingWorldPanel extends JPanel {

    private final VehicleRoutingPanel vehicleRoutingPanel;

    private VehicleRoutingSolutionPainter solutionPainter = new VehicleRoutingSolutionPainter();

    //    PopUpMenu myPopupMenu;
    public VehicleRoutingWorldPanel(VehicleRoutingPanel vehicleRoutingPanel) {
        this.vehicleRoutingPanel = vehicleRoutingPanel;
        ToolTipManager.sharedInstance().registerComponent(this);
        solutionPainter = new VehicleRoutingSolutionPainter();
//        myPopupMenu = new PopUpMenu(this);
//        setComponentPopupMenu(myPopupMenu);
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                // TODO Not thread-safe during solving
                VehicleRoutingSolutionV2 solution = VehicleRoutingWorldPanel.this.vehicleRoutingPanel.getSolution();
                if (solution != null) {
                    resetPanel(solution);
                }
            }
        });
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {

                if (e.getButton() == MouseEvent.BUTTON2 || e.getButton() == MouseEvent.BUTTON3) {
                    LatitudeLongitudeTranslator translator = solutionPainter.getTranslator();
                    if (translator != null) {
                        double longitude = translator.translateXToLongitude(e.getX());
                        double latitude = translator.translateYToLatitude(e.getY());
//                        VehicleRoutingWorldPanel.this.vehicleRoutingPanel.insertLocationAndCustomer(longitude, latitude);

//                        myPopupMenu.setLatitude(latitude);
//                        myPopupMenu.setLongitude(longitude);
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {

                                try {
//                                    VehicleRoutingSolutionV2 solution = VehicleRoutingWorldPanel.this.vehicleRoutingPanel.getSolution();
                                    ObjCRUD objFocus = getFocus(VehicleRoutingWorldPanel.this, latitude, longitude);
                                    if (objFocus != null) {
                                        objFocus.createAndShowGUI();
                                    }
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            }
                        });

                    }
                }
            }


        });
        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                LatitudeLongitudeTranslator translator = solutionPainter.getTranslator();
                if (translator != null) {
                    double longitude = translator.translateXToLongitude(e.getX());
                    double latitude = translator.translateYToLatitude(e.getY());
                    setSPToolTip(latitude, longitude);
                }
            }
        });
    }

    private void doPop(MouseEvent e) {
        PopupMenu menu = new PopupMenu();
        menu.show(e.getComponent(), e.getX(), e.getY());
    }

    private ObjCRUD getFocus(VehicleRoutingWorldPanel vehiclePanel, double latitude, double longitude) throws Exception {
        VehicleRoutingSolutionV2 solution = vehiclePanel.vehicleRoutingPanel.getSolution();
        List shipmentPotList = solution.getShipmentPotList();

        RefObject objFocus;
        ObjCRUD objCRUD;
        objFocus = getDisplayObj(shipmentPotList, latitude, longitude);
        if (objFocus != null) {
            objCRUD = new ShipmentPanel(vehiclePanel, objFocus, false);
            return objCRUD;
        }

        List depotList = solution.getDepotList();
        objFocus = getDisplayObj(depotList, latitude, longitude);

        if (objFocus != null) {
            objCRUD = new DepotPanel(vehiclePanel, objFocus, false);
            return objCRUD;
        }
        objFocus = getDistanceObj(shipmentPotList, latitude, longitude);

        if (objFocus != null) {
            objCRUD = new DistancePanel(vehiclePanel, objFocus, false);
            return objCRUD;
        }
        return null;
    }


    private RefObject getDisplayObj(List<RefObject> objs, double latitude, double longitude) {
        Location location;

        for (RefObject sp : objs) {

            location = sp.getBaseLocation();
            if ((location.getLatitude() - 5 < latitude) && (location.getLatitude() + 5 > latitude)
                    && (location.getLongitude() - 5 < longitude) && (location.getLongitude() + 5 > longitude)) {
                return sp;

            }
        }
        return null;
    }

    private RefObject getDistanceObj(List<ShipmentPot> objs, double latitude, double longitude) {
        Location location;
        LatitudeLongitudeTranslator translator = solutionPainter.getTranslator();
        if (translator == null) {
            return null;
        }
        double min = 100;
        ShipmentPot minSp = null;
        for (ShipmentPot sp : objs) {
            ShipmentStandstill previous = sp.getPreviousStandstill();


            if(!(longitude > previous.getLocation().getLongitude() && latitude > previous.getLocation().getLatitude()
                && longitude < sp.getLocation().getLongitude() && latitude < sp.getLocation().getLatitude())
                ||(!(longitude < previous.getLocation().getLongitude() && latitude < previous.getLocation().getLatitude()
                    && longitude > sp.getLocation().getLongitude() && latitude > sp.getLocation().getLatitude()))){ //x0>x1, y0>y1, x0<x2, y0<y2
                continue;
            }

            double d = linearEquations(previous.getLocation().getLongitude(), previous.getLocation().getLatitude(),
                    sp.getLocation().getLongitude(),sp.getLocation().getLatitude(),
                    longitude, latitude);
            if (min > d) {
                min = d;
                minSp = sp;
            }
        }
        if(min < 10){
            return minSp;
        }
        return null;
    }

    public void setSPToolTip(double latitude, double longitude) {
        VehicleRoutingSolutionV2 solution = VehicleRoutingWorldPanel.this.vehicleRoutingPanel.getSolution();
        List<ShipmentPot> shipmentPotList = solution.getShipmentPotList();


        ShipmentStandstill shipmentStandstill = null;
        Depot depot = null;
        String displayInfo = null;

        displayInfo = getDisplayInfo(shipmentPotList, latitude, longitude);
        if (displayInfo == null) {
            List<Depot> depotList = solution.getDepotList();
            displayInfo = getDisplayInfo(depotList, latitude, longitude);
        }
        if (displayInfo == null) {
            List<VehicleV2> vehicles = solution.getVehicleList();
            displayInfo = getDisplayInfo(vehicles, latitude, longitude);
        }

        if (displayInfo != null) {
            VehicleRoutingWorldPanel.this.setToolTipText(displayInfo);
        }


    }

    private String getDisplayInfo(List objs, double latitude, double longitude) {
        Location location;

        for (Object sp : objs) {

            location = (sp instanceof Depot) ? ((Depot) sp).getLocation() : ((ShipmentStandstill) sp).getLocation();
            location = (sp instanceof Depot) ? ((Depot) sp).getLocation() : ((ShipmentStandstill) sp).getLocation();

            if ((location.getLatitude() - 5 < latitude) && (location.getLatitude() + 5 > latitude)
                    && (location.getLongitude() - 5 < longitude) && (location.getLongitude() + 5 > longitude)) {
                return (sp instanceof Depot) ? ((Depot) sp).toString() : ((ShipmentStandstill) sp).getInfo();

            }
        }
        return null;
    }

    public void resetPanel(VehicleRoutingSolutionV2 solution) {
        solutionPainter.reset(solution, getSize(), this);
        repaint();
    }

    public void updatePanel(VehicleRoutingSolutionV2 solution) {
        resetPanel(solution);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        BufferedImage canvas = solutionPainter.getCanvas();
        if (canvas != null) {
            g.drawImage(canvas, 0, 0, this);
        }
    }

    public VehicleRoutingPanel getVehicleRoutingPanel() {
        return vehicleRoutingPanel;
    }

    public VehicleRoutingSolutionPainter getSolutionPainter() {
        return solutionPainter;
    }

    private double linearEquations(double x1, double y1, double x2, double y2, double x0, double y0) {

        double a, b, c, d;
        if (x1 == x2) {
            d = x1 - x0;
        } else if (y1 == y2) {

            d = y1 - y0;
        } else {
            a = 1 / (x2 - x1);
            b = 1 / (y1 - y2);
            c = y1 / (y2 - y1) - x1 / (x2 - x1);
            d = (a * x0 + b * y0 + c) / Math.sqrt(a * a + b * b);
        }
        return Math.abs(d);
    }

}
