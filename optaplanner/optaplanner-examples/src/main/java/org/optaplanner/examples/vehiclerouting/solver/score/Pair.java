package org.optaplanner.examples.vehiclerouting.solver.score;

import org.apache.commons.lang3.builder.CompareToBuilder;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

public class Pair<L, R> implements Map.Entry<L, R>, Comparable<Pair<L, R>>, Serializable {
    private static final Pair NULL = of((Object)null, (Object)null);
    private static final long serialVersionUID = 4954918890077093841L;
    public L left;
    public R right;

    public static <L, R> Pair<L, R> nullPair() {
        return NULL;
    }

    public static <L, R> Pair<L, R> of(L left, R right) {
        return new Pair(left, right);
    }

    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public void set(L left, R right){
        this.left = left;
        this.right = right;
    }
    public void set(Pair<L, R> pair){
        this.left = pair.getLeft();
        this.right = pair.getRight();
    }
    public L getLeft() {
        return this.left;
    }

    public R getRight() {
        return this.right;
    }

    @Override
    public L getKey() {
        return getLeft();
    }

    @Override
    public R getValue() {
        return getRight();
    }

    public R setValue(R value) {
        throw new UnsupportedOperationException();
    }




    public Pair() {
    }


    public int compareTo(Pair<L, R> other) {
        return (new CompareToBuilder()).append(this.getLeft(), other.getLeft()).append(this.getRight(), other.getRight()).toComparison();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (!(obj instanceof Map.Entry)) {
            return false;
        } else {
            Map.Entry<?, ?> other = (Map.Entry)obj;
            return Objects.equals(this.getKey(), other.getKey()) && Objects.equals(this.getValue(), other.getValue());
        }
    }

    public int hashCode() {
        return (this.getKey() == null ? 0 : this.getKey().hashCode()) ^ (this.getValue() == null ? 0 : this.getValue().hashCode());
    }

    public String toString() {
        return "(" + this.getLeft() + ',' + this.getRight() + ')';
    }

    public String toString(String format) {
        return String.format(format, this.getLeft(), this.getRight());
    }
}
