package org.optaplanner.examples.vehiclerouting.customize.types;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public interface ObjCRUD {
    final HashMap<Keys, JComponent> objMap = new HashMap<>();
    public default void createAndShowGUI() throws Exception {
        //Create and set up the window.
        JFrame frame = new JFrame("GridBagLayoutDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Set up the content pane.
        Container pane = frame.getContentPane();

//        JPanel panel = addComponentsToPane(pane, getPanel());
        pane.add("North", (Container)this);
        addAction(frame);
        //Display the window.
        frame.pack();
        frame.repaint();
        frame.setVisible(true);
    }

    default ObjCRUD addCell(Keys key, JComponent cell, GridBagConstraints c) {
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        ((Container)this).add(cell, c);
        int gridx = (c.gridx + 1);
        c.gridx = gridx % 2;
        c.gridy = gridx > 1 ? c.gridy + 1 : c.gridy;
        objMap.put(key, cell);
        return this;
    }

    default ObjCRUD addAction(JFrame frame){
        JPanel panel =new JPanel();
        JButton btnOk = new JButton("OK");
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                actionOk(frame, e);
            }
        });

        JButton btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Cancel");
                actionCancel(frame, e);
            }
        });
        Container pane = frame.getContentPane();
        panel.add(btnOk );
        panel.add(btnCancel );
        pane.add("South", panel);
        return this;
    }
    void actionOk(JFrame frame, ActionEvent e);
    default void actionCancel(JFrame frame, ActionEvent e){
        frame.setVisible(false);
        frame.dispose();
    }


//     JPanel addComponentsToPane(Container pane, RefObject objFocus) throws Exception;
}
