package ns.vtc.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalTime;


@Entity
@Table(name="Parking_Point")
@Data
public class ParkingPoint {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  private Integer id;
  @Column(name="address")
  private String address;
  @Column(name="latitude")
  private Double latitude;
  @Column(name="longitude")
  private Double longitude;
  @Column(name="day_ready_Time")
  private Long dayReadyTime;

  @Column(name="day_due_Time")
  private Long dayDueTime;

//  @Transient
//  private Location location;

  public ParkingPoint(Integer id, String address, Double latitude, Double longitude) {
    this.id = id;
    this.address = address;
    this.latitude = latitude;
    this.longitude = longitude;
//    this.location = new Location(address, latitude, longitude);
  }
  public ParkingPoint(){
    super();
  }
}
