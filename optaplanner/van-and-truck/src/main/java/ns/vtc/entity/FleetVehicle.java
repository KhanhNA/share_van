package ns.vtc.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="fleet_vehicle")
@Data
public class FleetVehicle {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  private Integer id;

  @Column(name="name")
  private String name;

  @Column(name="active")
  private String active;

  @Column(name="company_Id")
  private Integer companyId;


  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "parking_point_id", insertable = false, updatable = false)
  private ParkingPoint parkingPoint;
//  private Integer parkingPointId;

  @Column(name="license_Plate")
  private String licensePlate;
  @Column(name="vin_Sn")
  private String vinSn;
//  private Integer modelId;
//  private Integer brandId;
//  private String locationLog;
//  private java.sql.Date nextAssignationDate;
//  private java.sql.Date acquisitionDate;
//  private java.sql.Date firstContractDate;
//  private long color;
//  private Integer stateId;
//  private String location;
//  private long seats;
//  private String modelYear;
//  private long doors;
//  private String odometerUnit;
//  private String transmission;
//  private String fuelType;
//  private long horsepower;
//  private double horsepowerTax;
//  private long power;
//  private double co2;
//  private double carValue;
//  private double netCarValue;
//  private double residualValue;
//  private String planToChangeCar;
@Column(name="latitude")
  private Double latitude;
  @Column(name="longitude")
  private Double longitude;
//  private String warrantyName1;
//  private java.sql.Date warrantyDate1;
//  private double warrantyMeter1;
//  private String warrantyName2;
//  private java.sql.Date warrantyDate2;
//  private double warrantyMeter2;
//  private String vehicleInspection;
//  private java.sql.Date inspectionDueDate;
//  private double availableCapacity;
//  private String vehicleRegistration;
//  private String description;
//  private String createUser;
//  private java.sql.Date updateDate;
//  private String updateUser;
@Column(name="capacity")
  private Long capacity;
  @Column(name="cost")
  private Double cost;
  @Column(name="cost_Center")
  private Long costCenter;
//  private long maintenanceTemplateId;
//  private double axle;
//  private double tireFrontSize;
//  private double tireFrontPressure;
//  private double tireRearSize;
//  private double tireRearPressure;
//  private double bodyLength;
//  private double bodyWidth;
//  private double height;
//  private long vehicleType;
//  private double engineSize;
//  private long createUid;
//  private java.sql.Timestamp createDate;
//  private long writeUid;
//  private java.sql.Timestamp writeDate;
//  private long messageMainAttachmentId;
//  private long tonageId;


}
