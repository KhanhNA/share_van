package ns.vtc.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="location_data")
@Data
public class LocationData {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  private Integer id;

  @Column(name="code")
  private String code;

  @Column(name="from_latitude")
  private Double fromLatitude;

  @Column(name="from_longtitude")
  private Double fromLongtitude;


  @Column(name="to_latitude")
  private Double toLatitude;

  @Column(name="to_longitude")
  private Double toLongitude;


  @Column(name="minutes")
  private Double minutes;

  @Column(name="cost")
  private Double cost;

  public LocationData() {
  }

  public LocationData(Double fromLatitude, Double fromLongtitude, Double toLatitude, Double toLongitude, Double minutes, Double cost) {
//    this.code = "FLAT" + fromLatitude + "-FLON" + fromLongtitude + "TLAT" + toLatitude + "-TLON" + toLongitude;
    this.code = LocationData.getLocationCode(fromLatitude, fromLongtitude, toLatitude, toLongitude);
    this.fromLatitude = fromLatitude;
    this.fromLongtitude = fromLongtitude;
    this.toLatitude = toLatitude;
    this.toLongitude = toLongitude;
    this.minutes = minutes;
    this.cost = cost;
  }

  public static Double standardLocation(Double d){
    return Math.round(d * 1000d) / 1000d;
  }
  public static String getLocationCode(Double fLat, Double fLong, Double tLat, Double tLong){

    fLat = standardLocation(fLat);
    fLong = standardLocation(fLong);
    tLat = standardLocation(tLat);
    tLong = standardLocation(tLong);
    return  "FLAT" + fLat + "-FLON" + fLong +
            "TLAT" + tLat + "-TLON" + tLong;
  }
}
