/**
 * Odoo, Open Source Management Solution
 * Copyright (C) 2012-today Odoo SA (<http:www.odoo.com>)
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>
 * <p/>
 * Created on 21/4/15 4:09 PM
 */
package com.ns.odoolib_retrofit.model;

import android.os.Bundle;

import lombok.Getter;
import lombok.Setter;

/**
 * Stores odoo version information
 * <p/>
 * 8.saas~6     : [8,'saas~6',0,'final',0],
 * 9.0alpha1    : [9,0,0,'alpha',1],
 * 8.0          : [8,0,0,'final',0]
 */
@Getter
@Setter
public class OdooVersion {
    public static final String TAG = OdooVersion.class.getSimpleName();
    private String serverSerie, serverVersion, versionType, versionRelease;
    private int versionNumber, versionTypeNumber;
    private Boolean isEnterprise = false;

    private String server_serie, server_version, version_type, version_release, server_version_info[];

    public Bundle getAsBundle() {
        Bundle version = new Bundle();
        version.putString("server_serie", getServerSerie());
        version.putString("server_version", getServerVersion());
        version.putString("version_type", getVersionType());
        version.putString("version_release", getVersionRelease());
        version.putInt("version_number", getVersionNumber());
        version.putInt("version_type_number", getVersionTypeNumber());
        return version;
    }


    public int getVersionNumber() {
//        return versionNumber;
        return 13;
    }

    public void setVersionNumber(int versionNumber) {
        this.versionNumber = versionNumber;
    }

    @Override
    public String toString() {
        return "OdooVersion{" +
                "serverSerie='" + serverSerie + '\'' +
                ", serverVersion='" + serverVersion + '\'' +
                ", versionType='" + versionType + '\'' +
                ", versionRelease='" + versionRelease + '\'' +
                ", versionNumber=" + versionNumber +
                ", versionTypeNumber=" + versionTypeNumber +
                ", isEnterprise=" + isEnterprise +
                '}';
    }
}
