package com.ns.res;

import android.content.Context;

import com.ns.odoo.core.orm.OModel;
import com.ns.odoo.core.orm.fields.OColumn;
import com.ns.odoo.core.orm.types.OVarchar;
import com.ns.odoo.core.support.OUser;

public class ResCountryState extends OModel {

    OColumn name = new OColumn("Name", OVarchar.class);
    OColumn code = new OColumn("Code", OVarchar.class);
    OColumn country_id = new OColumn("Country", ResCountry.class, OColumn.RelationType.ManyToOne);

    public ResCountryState(Context context, OUser user) {
        super(context, "res.country.state", user);
    }
}
