package com.ns.odootest.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoutingPlanDayDetail {
    String routing_plan_detail_code;
    Integer quantity;
    String service_name;
    String insurance_name;
    String type;
}
