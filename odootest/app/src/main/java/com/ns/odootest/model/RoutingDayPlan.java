package com.ns.odootest.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoutingDayPlan {
    String routing_plan_day_code;
    String license_plate;
    String color;
    String warehouse_name;
    String driver_name;
    String vehicle_name;
    List<RoutingPlanDayDetail> routing_plan_day_details = new ArrayList<>();
}
