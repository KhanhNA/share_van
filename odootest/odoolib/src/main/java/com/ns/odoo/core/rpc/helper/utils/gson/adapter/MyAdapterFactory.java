package com.ns.odoo.core.rpc.helper.utils.gson.adapter;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.MalformedJsonException;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class MyAdapterFactory<E> implements TypeAdapterFactory {
    @Override
    public <T> TypeAdapter<T> create(final Gson gson, final TypeToken<T> typeToken) {

//        if ( !ArrayList.class.isAssignableFrom(typeToken.getRawType()) ) {
//            return null;
//
//        }
        // Resolving the list parameter type
        final Type elementType = resolveTypeArgument(typeToken.getType());
        @SuppressWarnings("unchecked")
        final TypeAdapter<E> elementTypeAdapter = (TypeAdapter<E>) gson.getAdapter(TypeToken.get(elementType));
        // Note that the always-list type adapter is made null-safe, so we don't have to check nulls ourselves
        @SuppressWarnings("unchecked")
        final TypeAdapter<T> alwaysListTypeAdapter = (TypeAdapter<T>) new ArrayAdapter<>(typeToken, elementTypeAdapter).nullSafe();
        return alwaysListTypeAdapter;
    }

    private static Type resolveTypeArgument(final Type type) {
        // The given type is not parameterized?
        if ( !(type instanceof ParameterizedType) ) {
            // No, raw
            return Object.class;
        }
        final ParameterizedType parameterizedType = (ParameterizedType) type;
        return parameterizedType.getActualTypeArguments()[0];
    }

    private static final class ArrayAdapter<E>
            extends TypeAdapter<ArrayList<E>> {

        private final TypeAdapter<E> elementTypeAdapter;
        private Class<ArrayList> mClazz;
        private ArrayAdapter(TypeToken typeToken, final TypeAdapter<E> elementTypeAdapter) {
            this.elementTypeAdapter = elementTypeAdapter;
            this.mClazz = typeToken.getRawType();
//                if(typeToken.getRawType() == OdooRelType.class){
//                    mClazz = new OdooRelType<>();
//                }else {
//                    this.mClazz = new ArrayList<>();
//                }

        }

        @Override
        public void write(final JsonWriter out, final ArrayList<E> list) throws IOException{
//            Gson gson = new Gson();
//            String json = gson.toJson(list);
//            out.jsonValue(json);
            throw new IOException("not implement");
        }

        @Override
        public ArrayList<E> read(final JsonReader in)
                throws IOException {
            // This is where we detect the list "type"
            final ArrayList<E> list;
            try {

                list = mClazz.newInstance();
            } catch (Exception e) {
                throw new AssertionError("Unexpected clazz: " + mClazz);
            }
            final JsonToken token = in.peek();
            switch (token) {
                case BEGIN_ARRAY:
                    // If it's a regular list, just consume [, <all elements>, and ]
                    in.beginArray();
                    while (in.hasNext()) {
                        list.add(elementTypeAdapter.read(in));
                    }
                    in.endArray();
                    break;
                case BEGIN_OBJECT:
                case STRING:
                case NUMBER:
                case BOOLEAN:
                    // An object or a primitive? Just add the current value to the result list
                    list.add(elementTypeAdapter.read(in));
                    break;
                case NULL:
                    throw new AssertionError("Must never happen: check if the type adapter configured with .nullSafe()");
                case NAME:
                case END_ARRAY:
                case END_OBJECT:
                case END_DOCUMENT:
                    throw new MalformedJsonException("Unexpected token: " + token);
                default:
                    throw new AssertionError("Must never happen: " + token);
            }
            return list;
        }
    }
}
