package com.ns.odoo.core.support;

import java.util.Calendar;
import java.util.Date;

public class OdooDate extends Date {
    private static Calendar calendar = Calendar.getInstance();
    static {

    }
    public static void removeTime(Calendar c){
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
    }
    public OdooDate() {
        calendar.setTime(new Date());
        removeTime(calendar);
        setTime(calendar.getTimeInMillis());
    }

    public OdooDate(long date) {
        super();
        calendar.setTimeInMillis(date);
        removeTime(calendar);
        setTime(calendar.getTimeInMillis());
    }
}
