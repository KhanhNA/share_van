/**
 * Odoo, Open Source Management Solution
 * Copyright (C) 2012-today Odoo SA (<http:www.odoo.com>)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>
 *
 * Created on 21/4/15 5:51 PM
 */
package com.ns.odoo.core.rpc.helper.utils.gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OdooErrorDetail {
    private String name, debug, message;
    private String[] arguments;
    private String exception_type;

    @Override
    public String toString() {
        String debugNew = debug == null?"": debug.replaceAll("\"","\'")
                .replaceAll("\r\n","\n")
                .replaceAll("\n", " ")
                .replaceAll("\\\\", "/");

        return "{" +
                "\"name\":\"" + name + '\"' +
                ", \"debug\":\"" + debugNew + '\"' +
                ", \"message\":\"" + message + '\"' +
                ", \"arguments\":\"" + Arrays.toString(arguments).replaceAll("\"","\'") + '\"' +
                ", \"exception_type\":\"" + exception_type + '\"' +

                '}';
    }
}
