package com.ns.odoo.wrapper;
/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ns.odoo.core.rpc.helper.utils.gson.DeserializeOnly;
import com.ns.odoo.core.rpc.helper.utils.gson.OdooResponseDto;
import com.ns.odoo.core.rpc.helper.utils.gson.adapter.Adapter;
import com.ns.odoo.core.rpc.helper.utils.gson.adapter.ArrayAdapterFactory;
import com.ns.odoo.core.rpc.listenersV2.IOdooResponse;
import com.ns.odoo.core.support.OdooDate;
import com.ns.odoo.core.support.OdooDateTime;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OdooRequest<Result> extends Request<Result> {
    /**
     * Default charset for JSON request.
     */
    protected static final String PROTOCOL_CHARSET = "utf-8";

    /**
     * Content type for request.
     */
    private static final String PROTOCOL_CONTENT_TYPE =
            String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    /**
     * Lock to guard mListener as it is cleared on cancel() and read on delivery.
     */
    private final Object mLock = new Object();
    private Type mClazz;
    private Gson mGson;
    public static String sessionCokie;


    @Nullable
    @GuardedBy("mLock")
    private Listener<Result> mListener;

    @Nullable
    @GuardedBy("mLock")
    private Response.ErrorListener mErrorListener;

    @Nullable
    private final String mRequestBody;

    @Nullable
    @GuardedBy("mLock")
    IOdooResponse mOdooResponse;


    public OdooRequest(
            int method,
            String url,
            @Nullable String requestBody,
            Type clazz,
            Listener<Result> listener,
            @Nullable ErrorListener errorListener) {
        super(method, url, errorListener);

        mListener = listener;
        mErrorListener = errorListener;

        mRequestBody = requestBody;
        mClazz = clazz;
        mGson = new Gson();
        mOdooResponse = null;
    }

    public OdooRequest(
            String url,
            @Nullable String requestBody,
            Class<Result> clazz,
            Listener<Result> listener,
            @Nullable ErrorListener errorListener
    ) {
        this(requestBody == null ? Method.GET : Method.POST, url, requestBody, clazz, listener, errorListener);

    }

    public OdooRequest(
            String url,
            @Nullable String requestBody,
            Type clazz,
            IOdooResponse odooResponse
    ) {
        this(requestBody == null ? Method.GET : Method.POST, url, requestBody, clazz, null, null);
        mOdooResponse = odooResponse;
    }

    @Override
    public void cancel() {
        super.cancel();
        synchronized (mLock) {
            mListener = null;
        }
    }


    @Override
    protected void deliverResponse(Result response) {
        Response.Listener<Result> listener;
        IOdooResponse odooResponse;
        synchronized (mLock) {
            listener = mListener;
        }

        if (listener != null) {
            listener.onResponse(response);
            return;
        }


        synchronized (mLock) {
            odooResponse = mOdooResponse;
        }
        if (mOdooResponse != null) {
            mOdooResponse.onResponse(response, null);
        }
    }

    @Override
    public void deliverError(VolleyError error) {
        Response.ErrorListener listener;
        synchronized (mLock) {
            listener = mErrorListener;
        }
        if (listener != null) {
            listener.onErrorResponse(error);
            return;
        }
        IOdooResponse odooResponse;
        synchronized (mLock) {
            odooResponse = mOdooResponse;
        }
        if (mOdooResponse != null) {
            mOdooResponse.onResponse(null, error);
        }
    }


    @Override
    protected Response<Result> parseNetworkResponse(NetworkResponse response) {
        try {

            sessionCokie = response.headers.get("Set-Cookie");

            String jsonString =
                    new String(
                            response.data,
                            HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            OdooResponseDto<Result> data;

            Type type = TypeToken.getParameterized(OdooResponseDto.class, mClazz).getType();

            GsonBuilder builder = new GsonBuilder()
                    .registerTypeAdapter(String.class, Adapter.STRING)
                    .registerTypeAdapter(Number.class, Adapter.NUMBER)
                    .registerTypeAdapter(Integer.class, Adapter.INTEGER)
                    .registerTypeAdapter(Long.class, Adapter.LONG)
                    .registerTypeAdapter(Float.class, Adapter.FLOAT)
                    .registerTypeAdapter(Double.class, Adapter.DOUBLE)
                    .registerTypeAdapter(OdooDate.class, Adapter.DATE)
                    .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
//                    .registerTypeAdapter(ArrayList.class, new ArrayDeserializer<>())




                    .registerTypeAdapterFactory(new ArrayAdapterFactory())

//                    .registerTypeAdapter(type, new MyDeserializer<OdooResponseDto>())
                    ;
//
            builder.addSerializationExclusionStrategy(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(final FieldAttributes f) {
                    return f.getAnnotation(DeserializeOnly.class) != null;
                }

                @Override
                public boolean shouldSkipClass(final Class<?> clazz) {
                    return false;
                }
            });
            data = builder.create().fromJson(jsonString, type);
//            data = mGson.fromJson(new OJsonReader(new StringReader(jsonString)), type);
            if(data.error != null){
                return Response.error(new VolleyError(data.error.toString()));
            }
            Result ret = data == null? null: data.result;
            return Response.success(ret, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (Exception je) {
            return Response.error(new ParseError(je));
        }
    }


    @Override
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @Override
    public byte[] getBody() {
        try {
            return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf(
                    "Unsupported Encoding while trying to get the bytes of %s using %s",
                    mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Content-Type", "application/json");
        if (sessionCokie != null) {
            params.put("Cookie", sessionCokie);
        }
        return params;
    }


}
