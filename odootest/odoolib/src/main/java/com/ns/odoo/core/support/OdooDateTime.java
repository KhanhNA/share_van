package com.ns.odoo.core.support;

import java.util.Date;

public class OdooDateTime extends Date {
    public OdooDateTime(long date) {
        super(date);
    }

    public OdooDateTime() {
        super();
    }
}
