package com.ns.odoo.core.rpc.helper.utils.gson;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class MySerializer<T> implements JsonSerializer<T> {

    @Override
    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
//        if(typeOfSrc instanceof ParameterizedType && ((ParameterizedType) typeOfSrc).getRawType() == OdooRelType.class){
        if(typeOfSrc == OdooRelType.class){
            OdooRelType odooRelType = (OdooRelType)src;
            if(odooRelType == null || odooRelType.size() < 1){
                return null;
            }
            Long id = Long.parseLong((String)odooRelType.get(0));
            return new JsonPrimitive(id);
        }else{
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(src, typeOfSrc);
            return element;
        }
    }
}
