package ac.dlp.locationService.model;

import ac.dlp.locationService.service.RabbitMQConsumer;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "Fleet_Vehicle_Location_Log")
@Data
public class FleetVehicleLocationLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "vehicle_Id")
    private Integer vehicleId;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "assign_Date_Time")
    private LocalDateTime assignDateTime;


    @Column(name = "status")
    private String status;

    @Column(name = "create_Uid")
    private Integer createUid;

    @Column(name = "create_Date")
    private java.sql.Timestamp createDate;

    @Column(name = "write_Uid")
    private Integer writeUid;

    @Column(name = "write_Date")
    private java.sql.Timestamp writeDate;

    private transient String assignDateTimeStr;

    public void setLog(LocationLog log){
        this.latitude = log.latitude;
        this.vehicleId = log.vehicle_id;
        this.longitude = log.longitude;
        assignDateTimeStr = log.assign_date_time;
        if(log.assign_date_time != null) {
            this.assignDateTime = LocalDateTime.parse(log.assign_date_time, RabbitMQConsumer.dtFormater);
        }
        this.createUid = 2;
        this.status = "running";


    }



}
