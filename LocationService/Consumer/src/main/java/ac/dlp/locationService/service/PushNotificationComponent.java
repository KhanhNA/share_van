package ac.dlp.locationService.service;

import ac.dlp.locationService.service.firebase.PushNotificationService;
import ac.dlp.locationService.model.NotificationVehicle;
import ac.dlp.locationService.model.PushNotificationEvent;
import ac.dlp.locationService.model.firebase.NotificationAction;
import ac.dlp.locationService.model.firebase.PushNotificationRequest;
import lombok.RequiredArgsConstructor;
import ns.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class PushNotificationComponent {
    //    private final CommonService commonService;
    private final PushNotificationService pushNotificationService;
    //    private final NotificationMerchantService notificationService;
    private final MessageSource messageSource;
    //    private final OrderV2Service orderService;
    private static final Logger log = LogManager.getLogger(PushNotificationComponent.class);

    @Async
    @EventListener
    @Transactional
    public void pushNotificationOrderEventListener(PushNotificationEvent pushNotificationEvent) {
        try {

//            String sql = "select * from token_fb_merchant where merchant_id =:merchantId";
            String lstToken = pushNotificationEvent.getVehicle().tokens;
            if(lstToken == null){
                return;
            }
            String[] arrToken = lstToken.split(",");
                    //{"exhFPCPcQAqrAYII4wXlba:APA91bE3xFlEs1YJb8yRpIQIGfPnwCzmsE-nGBcrOyMDVRTOg4RkP3i1MEhS35AcoKVRWdN5L_pX3lwXZYGNuc30r8YyTg2dvZIz2BK2nj3ejSgxNpxwH5bltVSnylLWkOUgclGOyUWS"};//commonService.findAll(null, TokenFbMerchant.class, sql, pushNotificationEvent.getOrder().getParentMerchantId());
            if (arrToken != null && arrToken.length > 0) {
                NotificationVehicle notificationVehicle = new NotificationVehicle(pushNotificationEvent.getVehicle(), pushNotificationEvent.getLstTracing());
//                notificationVehicle.setTitle("location update");
//                notificationVehicle.setMessage("tesst body");
//                notificationVehicle.setBody("tesst body");
//                notificationVehicle.setTypeId(NotificationAction.L2_ORDER.getValue());
//                notificationVehicle.setActionId(pushNotificationEvent.getVehicle().getId());
//                notificationVehicle.setClick_action("com.ts.sharevandriver.TARGET_ROUTING_PLAN_DAY");
//                    notificationVehicle.setOrderPackingName(pushNotificationEvent.getOrder().calculatePackings.get(0).getProductName());
//                    notificationVehicle.setOrderQuantity(pushNotificationEvent.getOrder().calculatePackings.get(0).getOrderQuantity());
//                    notificationVehicle.setOrderPackingUrl(pushNotificationEvent.getOrder().calculatePackings.get(0).getPackingUrl());
//                notificationVehicle.setMerchantName(pushNotificationEvent.getFullName());
//                notificationVehicle.setMerchantNumberPhone(pushNotificationEvent.getMobilePhone());
//                    notificationVehicle.setOrderCode(pushNotificationEvent.getOrder().orderNo);
//                notificationVehicle.setIsSeen(0);
                for (String token : arrToken) {
                    notificationVehicle.setToken(token);
//                    notificationVehicle.setMerchantReceiverId(pushNotificationEvent.getVehicle().getId());
                    // Push notification to device
                    pushNotificationService.sendPushNotificationToToken(Utils.getData(notificationVehicle, PushNotificationRequest.class));
                }
                // Save notification
//                    notificationService.save(notificationVehicle);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


}
