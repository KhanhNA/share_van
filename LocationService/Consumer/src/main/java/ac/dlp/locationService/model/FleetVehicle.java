package ac.dlp.locationService.model;

import lombok.Data;
import org.redisson.api.annotation.REntity;
import org.redisson.api.annotation.RId;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@REntity
@Entity
@Table(name = "Fleet_Vehicle")
@Data
public class FleetVehicle {

    @RId
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "company_Id")
    private Integer companyId;

    @Column(name = "parking_Point_Id")
    private Integer parkingPointId;

    @Column(name = "license_Plate")
    private String licensePlate;

    @Column(name = "vin_Sn")
    private String vinSn;

    @Column(name = "model_Id")
    private Integer modelId;

    @Column(name = "brand_Id")
    private Integer brandId;

    @Column(name = "location_Log")
    private String locationLog;


    @Column(name = "color")
    private Integer color;


    @Column(name = "seats")
    private Integer seats;

    @Column(name = "model_Year")
    private String modelYear;

    @Column(name = "doors")
    private Integer doors;

    @Column(name = "odometer_Unit")
    private String odometerUnit;

    @Column(name = "transmission")
    private String transmission;

    @Column(name = "fuel_Type")
    private String fuelType;

    @Column(name = "horsepower")
    private Integer horsepower;


    @Column(name = "power")
    private Integer power;

    @Column(name = "co2")
    private Double co2;


    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;


    @Column(name = "last_update_location_time")
    private LocalDateTime lastUpdateLocationTime;


    @Column(name = "available_Capacity")
    private Double availableCapacity;

    @Column(name = "capacity")
    private Double capacity;

    @Column(name = "cost")
    private Double cost;

    @Column(name = "cost_Center")
    private Integer costCenter;


    @Column(name = "body_Length")
    private Double bodyLength;

    @Column(name = "body_Width")
    private Double bodyWidth;

    @Column(name = "height")
    private Double height;

    @Column(name = "vehicle_Type")
    private Integer vehicleType;

    @Column(name = "engine_Size")
    private Double engineSize;

    @Transient
    public String tokens;


//    public transient List<LocLatLng> lstTracing;

//    @Transient
//    public List<String> tokens1;


    public FleetVehicle() {
    }

    public FleetVehicle(Integer vehicleId) {
        super();
        this.id = vehicleId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

//  private long maintenanceTemplateId;
//  private double axle;
//  private double tireFrontSize;
//  private double tireFrontPressure;
//  private double tireRearSize;
//  private double tireRearPressure;
    //  private String vehicleRegistration;
//  private String description;
//  private String createUser;
//  private java.sql.Date updateDate;
//  private String updateUser;

    //    @Column(name = "id")
//    private double horsepowerTax;

//    @Column(name = "id")
//    private double carValue;

    //    @Column(name = "id")
//    private double netCarValue;
//  private double residualValue;
//  private String planToChangeCar;
    //  private String warrantyName1;
//  private java.sql.Date warrantyDate1;
//  private double warrantyMeter1;
//  private String warrantyName2;
//  private java.sql.Date warrantyDate2;
//  private double warrantyMeter2;
//  private String vehicleInspection;
//  private java.sql.Date inspectionDueDate;
    //    @Column(name = "id")
//    private long stateId;

//    @Column(name = "id")
//    private String location;
    //    @Column(name = "next_Assignation_Date")
//    private java.sql.Date nextAssignationDate;

//    @Column(name = "acquisition_Date")
//    private java.sql.Date acquisitionDate;

//    @Column(name = "id")
//    private java.sql.Date firstContractDate;

    //  private long createUid;
//  private java.sql.Timestamp createDate;
//  private long writeUid;
//  private java.sql.Timestamp writeDate;
//  private long messageMainAttachmentId;
//  private long tonageId;
//    public void addToken(String token) {
//        if (tokens1 == null) {
//            tokens1 = new ArrayList<>();
//        }
//
//        tokens1.add(token);
//    }

//    public void removeToken(String token) {
//        if (tokens1 == null || tokens1.size() == 0) {
//            return;
//        }
//        tokens1.remove(token);
//    }

    public void addToken(String token) {
        if(tokens == null ){
            tokens = token + ",";
            return;
        }
        token = token + ",";
        if(tokens.contains(token)){
            return;
        }
        tokens = tokens + token;

    }
}
