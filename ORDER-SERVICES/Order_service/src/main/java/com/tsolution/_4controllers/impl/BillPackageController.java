package com.tsolution._4controllers.impl;

import com.tsolution._1entities.BillPackage;

import com.tsolution._3services.BillPackageService;
import com.tsolution._4controllers.BaseController;
import com.tsolution._4controllers.IBaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/bill-packets")
public class BillPackageController extends BaseController implements IBaseController<BillPackage> {

    @Autowired

    private BillPackageService billPackageService;

    @Override
    @GetMapping("/all")
    @PreAuthorize("hasAuthority('get/bill-packets/all')")
    public ResponseEntity<Object> findAll() {
        return this.billPackageService.findAll();

    }

    @Override
    @GetMapping("/{id}")
    //@PreAuthorize("hasAuthority('get/bill-packets/{id}')")
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return this.findById(id);
    }

    @Override
    public ResponseEntity<Object> create(List<BillPackage> entity) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> update(Long id, Optional<BillPackage> source) throws BusinessException {
        return null;
    }
}
