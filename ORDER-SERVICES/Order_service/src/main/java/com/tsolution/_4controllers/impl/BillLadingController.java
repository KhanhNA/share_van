package com.tsolution._4controllers.impl;

import com.tsolution._1entities.BillLading;
import com.tsolution._3services.BillLadingService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/bill-ladings")
public class BillLadingController extends BaseController {

    @Autowired
    private BillLadingService billLadingService;

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('get/bill-ladings/all')")
    public ResponseEntity<Object> findAll() {
        return this.billLadingService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/bill-ladings/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.billLadingService.findById(id);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('get/bill-ladings')")
    public Page<BillLading> find(@RequestParam(required = true ,value = "customerId") Long customerId,
                                 @RequestParam(required = false,value = "billLadingCode") String billLadingCode,
                                 @RequestParam(required = false,value = "fDate") @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime fDate,
                                 @RequestParam(required = false,value = "tDate") @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime tDate,
                                 @RequestParam(required = false,value = "status") Integer status,
                                 @RequestParam(required = false,value = "billCycalType") Integer billCycalType,
                                 @RequestParam("pageNumber") Integer pageNumber,
                                 @RequestParam("pageSize") Integer pageSize) throws BusinessException {

        return  this.billLadingService.find(billLadingCode,customerId,status,fDate,tDate,billCycalType,pageNumber,pageSize);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('post/bill-ladings')")
    public ResponseEntity<Object> create(@RequestHeader("Accept-Language") String acceptLanguage,@RequestBody List<BillLading> entity) throws BusinessException, ParseException {
        return this.billLadingService.create( acceptLanguage,entity.get(0));
    }


    @PostMapping("/caculateMoney")
    @PreAuthorize("hasAuthority('post/bill-ladings/caculateMoney')")
    public ResponseEntity<Object> caculateMoneyPreCustomerAcceptOrder(@RequestHeader("Accept-Language") String acceptLanguage,@RequestBody List<BillLading> entity) throws BusinessException, ParseException {
        return this.billLadingService.caculateMoneyPreCustomerAcceptOrder( acceptLanguage,entity.get(0));
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('patch/bill-ladings/{id}')")
    public ResponseEntity<Object> update(@RequestHeader("Accept-Language") String acceptLanguage,@PathVariable(name = "id", required = true) Long id,@RequestBody (required = true) List<BillLading> source) throws BusinessException, ParseException {
        return this.billLadingService.update(acceptLanguage,id, source.get(0));
    }

    @PostMapping ("/entity")
    @PreAuthorize("hasAuthority('post/bill-ladings/entity')")
    public ResponseEntity<Object> findByEntity(@RequestBody Optional<BillLading> source) {
        return this.billLadingService.findAll();
    }

    @PatchMapping("/{id}/cancel")
    @PreAuthorize("hasAuthority('patch/bill-ladings/{id}/cancel')")
    public ResponseEntity<Object> cancel(@PathVariable("id") Long id) throws BusinessException {
        return this.billLadingService.cancel(id);
    }
}