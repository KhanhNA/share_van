package com.tsolution._4controllers.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import com.tsolution._1entities.Staff;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tsolution._1entities.Warehouse;
import com.tsolution._3services.WarehouseService;
import com.tsolution._4controllers.BaseController;
import com.tsolution._4controllers.IBaseController;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/warehouse")
public class WarehouseController extends BaseController implements IBaseController<Warehouse> {

	@Autowired
	private WarehouseService warehouseService;

	@Override
	@GetMapping("/all")
	@PreAuthorize("hasAuthority('get/warehouse/all')")
	@ApiOperation(value = "Get all warehouse status = 1",
			notes = "warehouse status = 1",
			response = Warehouse.class,
			responseContainer = "List")
	public ResponseEntity<Object> findAll() {
		return this.warehouseService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('get/warehouse/{id}')")
	public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
		return this.warehouseService.findById(id);
	}



//	@GetMapping("/{id}/customer-rents")
//	//@PreAuthorize("hasAuthority('get/warehouses/{id}/customer-rents')")
//	public ResponseEntity<Object> findCustomerRentWarehouse(@PathVariable("id") Long id,
//			@RequestParam(required = true) Integer pageNumber, @RequestParam(required = true) Integer pageSize)
//			throws BusinessException {
//		return this.warehouseService.findCustomerRentWarehouse(id, pageNumber, pageSize);
//	}

	@GetMapping
	@PreAuthorize("hasAuthority('get/warehouse')")
	public ResponseEntity<Object> find(@RequestParam(required = false) Double currentLat,
			@RequestParam(required = false) Double currentLng, @RequestParam(required = false) Double maxDistance,
			@RequestParam(required = false) String code, @RequestParam(required = false) String name,
			@RequestParam(required = false) String description, @RequestParam(required = false) String address,
			@RequestParam(required = false) Integer status, @RequestParam(required = true) Integer pageNumber,
			@RequestParam(required = true) Integer pageSize) throws BusinessException {
		Warehouse warehouse = new Warehouse();
		warehouse.setCode(code);
		warehouse.setName(name);
		warehouse.setDescription(description);
		warehouse.setAddress(address);
		warehouse.setLat(currentLat == null ? null : new BigDecimal(currentLat));
		warehouse.setLng(currentLng == null ? null : new BigDecimal(currentLng));
		//warehouse.setMaxDistance(maxDistance);
		warehouse.setStatus(status);
		return this.warehouseService.find(warehouse, pageNumber, pageSize);
	}

	@Override
	@PostMapping
	@PreAuthorize("hasAuthority('post/warehouse')")
	public ResponseEntity<Object> create(@RequestBody List<Warehouse> entity) throws BusinessException {
		return this.warehouseService.create(entity);
	}

	@Override
	@PatchMapping("/{id}")
	@PreAuthorize("hasAuthority('patch/warehouse/{id}')")
	public ResponseEntity<Object> update(@PathVariable("id") Long id,
			@RequestBody(required = true) Optional<Warehouse> source) throws BusinessException {
		if (source.isPresent()) {
			return this.warehouseService.update(id, source.get());
		} else {
			throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
		}
	}



	@PatchMapping("/{id}/active")
	@PreAuthorize("hasAuthority('patch/warehouse/{id}/active')")
	public ResponseEntity<Object> active(@PathVariable(name = "id", required = true) Long id
			) throws BusinessException {
//		if (source.isPresent()) {
//			return this.warehouseService.active(id);
//		} else {
//			throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
//		}
		return this.warehouseService.active(id);
	}

	@PatchMapping("/{id}/deactive")
	@PreAuthorize("hasAuthority('patch/warehouse/{id}/deactive')")
	public ResponseEntity<Object> deactive(@PathVariable("id") Long id) throws BusinessException {
		return this.warehouseService.deactive(id);
	}

	@GetMapping("/{id}/warehouse_cus")
	@PreAuthorize("hasAuthority('get/warehouse/{id}/warehouse_cus')")
	public ResponseEntity<Object> getWarehouseByCus(@PathVariable("id") Long id) throws BusinessException {
		return this.warehouseService.getListWarehouseByCustomer(id);
	}

	@GetMapping("/warehouseByCus")
	// @PreAuthorize("hasAuthority('get/warehouseByCus')")
	public ResponseEntity<Object> getWarehouseByTokenCus() throws BusinessException {
		return this.warehouseService.getListWarehouseByCustomerToken();
	}

	@GetMapping("/warehouse_product_type")
	@PreAuthorize("hasAuthority('get/warehouse/{id}/warehouse_product_type')")
	public ResponseEntity<Object> getProductType() throws BusinessException {
		return this.warehouseService.getListProductType();
	}

	@GetMapping("/findListWareHouse")
	public ResponseEntity<Object> findListWareHouse(
									   @RequestParam(required = true,value = "customerId") Integer customerId,
									   @RequestParam(required = false,value = "code") String code,
								       @RequestParam(required = false,value = "name") String name,
									   @RequestParam(required = false,value = "status") Integer status,
									   @RequestParam("pageNumber") Integer pageNumber,
									   @RequestParam("pageSize") Integer pageSize) throws BusinessException {
		return this.warehouseService.findListWareHouse(customerId,code,name,status,pageNumber,pageSize);
	}
}
