package com.tsolution._4controllers.impl;

import com.tsolution._1entities.Rating;
import com.tsolution._3services.RatingService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/rating")
public class RatingController extends BaseController {

    @Autowired
    private RatingService ratingService;

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('get/rating/all')")
    public ResponseEntity<Object> findAll() {
        return this.ratingService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/rating/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.ratingService.findById(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('post/rating')")
    public ResponseEntity<Object> create(@RequestHeader("Authorization") String authorization,
                                         @RequestHeader("Accept-Language") String acceptLanguage,
                                         @RequestBody Rating rating)
            throws BusinessException {
        return this.ratingService.create(acceptLanguage, rating);
    }


}
