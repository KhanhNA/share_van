package com.tsolution._4controllers.impl;

import com.tsolution._1entities.BillService;
import com.tsolution._3services.BillServiceService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/bill-services")
public class BillServiceController extends BaseController {

    @Autowired
    private BillServiceService serviceService;

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('get/bill-services/all')")
    public ResponseEntity<Object> findAll() {
        return this.serviceService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/bill-services/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") long id) throws BusinessException {
        return this.serviceService.findById(id);
    }

    @GetMapping("/serviceActive")
    public ResponseEntity<Object> findServiceByActive() throws BusinessException {
        return this.serviceService.findByActive();
    }

    @GetMapping("/getByType/{type}")
    @PreAuthorize("hasAuthority('get/bill-services/getByType/{type}')")
    public ResponseEntity<Object> findServiceByType(@PathVariable("type") int type) {
        return this.serviceService.findServiceByType(type);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('post/bill-services')")
    public ResponseEntity<Object> create(
            @RequestBody BillService entity) throws BusinessException {
        return this.serviceService.create(entity);
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('patch/bill-services')")
    public ResponseEntity<Object> updateService(
            @RequestBody BillService entity) throws BusinessException {
        return this.serviceService.update(entity);
    }
}
