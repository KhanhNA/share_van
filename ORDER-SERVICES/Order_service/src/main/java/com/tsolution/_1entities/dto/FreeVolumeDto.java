package com.tsolution._1entities.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class FreeVolumeDto implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 5029986825144186097L;

	@Id
	private Long id;

	@JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
	@JsonSerialize(using = SerializeDateHandler.class)
	@JsonView(JsonEntityViewer.Human.Summary.class)
	private LocalDateTime date;

	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long warehouseId;

	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String warehouseCode;

	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long parcelId;

	@JsonView(JsonEntityViewer.Human.Summary.class)
	private String parcelCode;

	@JsonView(JsonEntityViewer.Human.Summary.class)
	private Long freeVolume;

	public LocalDateTime getDate() {
		return this.date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Long getWarehouseId() {
		return this.warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getWarehouseCode() {
		return this.warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public Long getParcelId() {
		return this.parcelId;
	}

	public void setParcelId(Long parcelId) {
		this.parcelId = parcelId;
	}

	public String getParcelCode() {
		return this.parcelCode;
	}

	public void setParcelCode(String parcelCode) {
		this.parcelCode = parcelCode;
	}

	public Long getFreeVolume() {
		return this.freeVolume;
	}

	public void setFreeVolume(Long freeVolume) {
		this.freeVolume = freeVolume;
	}

}
