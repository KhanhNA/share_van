package com.tsolution._4controllers.impl;


import com.tsolution._1entities.DayOff;
import com.tsolution._3services.DayOffService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/dayoff")
public class DayOffController {

    @Autowired
    private DayOffService dayOffService;

    @GetMapping
    public ResponseEntity<Object> find(@RequestParam(required = false) Long staffId,
                                       @RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime fdate,
                                       @RequestParam(required = false) @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDateTime tdate,
                                       @RequestParam(required = false) Integer spec,
                                       @RequestParam(required = false) Integer status
    ) throws BusinessException {
        return this.dayOffService.find(staffId,fdate,tdate,spec,status);
    }

    @PostMapping
    public ResponseEntity<Object> create(@RequestHeader("Accept-Language") String acceptLanguage,
                                         @RequestBody DayOff entity
                                         ) throws BusinessException {
        System.out.println(entity);
        return this.dayOffService.create(acceptLanguage,entity);

    }

    @PatchMapping("/{id}")
    public ResponseEntity<Object> update(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody DayOff entity,
            @PathVariable("id") Long id)
            throws BusinessException {
        return this.dayOffService.update(acceptLanguage, id, entity);
    }

    @GetMapping("/all")
    public ResponseEntity<Object> findValueAll() throws BusinessException {
        return this.dayOffService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.dayOffService.findById(id);
    }

}
