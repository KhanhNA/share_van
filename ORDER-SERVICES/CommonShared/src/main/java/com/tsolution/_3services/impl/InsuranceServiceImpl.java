

package com.tsolution._3services.impl;

import java.util.Optional;

import com.tsolution._1entities.Vendor;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution.utils.MathUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tsolution._1entities.Insurance;
import com.tsolution._2repositories.InsuranceRepository;
import com.tsolution._3services.InsuranceService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InsuranceServiceImpl extends BaseServiceImpl implements InsuranceService {

	@Autowired
	private InsuranceRepository insuranceRepository;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(String acceptLanguage,Insurance entities) throws BusinessException {
		if (StringUtils.isNullOrEmpty(entities.getInsuranceName())) {
			throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
		}
		this.validInsurance( entities );



		Optional<Insurance> oInsurance;
		if (entities.getInsuranceCode() != null) {
			String insuCode = entities.getInsuranceCode();
			oInsurance = this.insuranceRepository.findByInsuranceCode(insuCode);
			if (oInsurance.isPresent()) {
				throw new BusinessException(this.translator.toLocaleByFormatString("insurance.code.already.exists",
						entities.getInsuranceCode()));
			}
		}

		Long count = this.insuranceRepository.getOneResult("INS");
		String insuranceCode;
		if (count != null) {
			insuranceCode = "INS" + MathUtils.fixLengthString(++count, 4);
			boolean checkCustomer = false;
			while (!checkCustomer) {
				oInsurance = this.insuranceRepository.getInsuranceByCodeByCode(insuranceCode);
				if(oInsurance.isPresent()){
					insuranceCode = "INS" + MathUtils.fixLengthString(++count, 4);
				}else {
					checkCustomer=true;
				}
			}
		} else {
			insuranceCode = "INS" + MathUtils.fixLengthString(count++, 4);
		}

		entities.setInsuranceCode( insuranceCode );
		entities.setStatus(1);
		entities.setFromDate(this.getSysdate());
		entities = this.insuranceRepository.save(entities);
		return new ResponseEntity<>(entities, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(String acceptLanguage,Long insuranceId, Insurance entities) throws BusinessException {
		Optional<Insurance> insurance = this.insuranceRepository.findById(insuranceId);

		if (entities == null) {
			throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
		}

		if (insuranceId == null || insurance.get().getId() == null || !(insuranceId == insurance.get().getId())) {
			throw new BusinessException(this.translator.toLocale("insurance.id.err"));
		}
		Optional<Insurance> optionalInsuranceGroup = this.insuranceRepository.findById(insuranceId);
		if (!optionalInsuranceGroup.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("insurance.not.exists", insuranceId));
		}

		if (entities.getFromDate() == null) {
			throw new BusinessException(this.translator.toLocale("insurance.input.missing.fromdate"));
		}

		if (entities.getStatus() == null) {
			throw new BusinessException(this.translator.toLocale("insurance.input.missing.status"));
		}

		this.validInsurance( entities );

		insurance.get().setInsuranceName(entities.getInsuranceName());
		insurance.get().setStatus(entities.getStatus());
		insurance.get().setDeductible(entities.getDeductible());
		insurance.get().setNote(entities.getNote());
		insurance.get().setFromDate(entities.getFromDate());
		insurance.get().setToDate(entities.getToDate());
		insurance.get().setVendor(entities.getVendor());

		entities = this.insuranceRepository.save(insurance.get());
		return new ResponseEntity<>(entities, HttpStatus.OK);

	}

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.insuranceRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findAvaiable() {
		return new ResponseEntity<>(this.insuranceRepository.findAvaiable(StatusType.RUNNING.getValue()),HttpStatus.OK);
	}

	private void validInsurance(Insurance entities) throws BusinessException {

		if (StringUtils.isNullOrEmpty(entities.getInsuranceName())) {
			throw new BusinessException(this.translator.toLocale("insurance.input.missing.name"));
		}

		if (entities.getVendor() == null) {
			throw new BusinessException(this.translator.toLocale("insurance.input.missing.vendor"));
		}

		if (entities.getDeductible() == null) {
			throw new BusinessException(this.translator.toLocale("insurance.input.missing.deductible"));
		}

		if (entities.getToDate() == null) {
			throw new BusinessException(this.translator.toLocale("insurance.input.missing.todate"));
		}

		//check fromdate todate
		if (entities.getFromDate().isAfter( entities.getToDate() )) {
			throw new BusinessException( this.translator.toLocaleByFormatString( "insurance.todate.is.bigger.than.fromdate" ) );
		}
		Long idVender =  entities.getVendor().getId();
		Optional<Vendor> vendor = this.vendorRepository.findById( idVender );

		if(!vendor.isPresent()){
			throw new BusinessException( this.translator.toLocaleByFormatString( "insurance.vender.does.not.exist" ) );
		}

		if(entities.getFromDate().equals(entities.getToDate())){
			throw new BusinessException( this.translator.toLocaleByFormatString( "insurance.from.date.and.todate.cannot.overlap" ) );
		}
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<Insurance> oInsurance = this.insuranceRepository.findById(id);
		if (!oInsurance.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("insurance.id.not.exists"));
		}
		return new ResponseEntity<>(oInsurance, HttpStatus.OK);
	}

}
