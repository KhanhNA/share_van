package com.tsolution._3services.impl;

import com.tsolution._1entities.*;
import com.tsolution._1entities.dto.BillServiceDto;
import com.tsolution._1entities.enums.*;
import com.tsolution._2repositories.*;
import com.tsolution._3services.BillLadingService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.DateUtil;
import com.tsolution.utils.MathUtils;
import com.tsolution.utils.StringUtils;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scala.App;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;

@Service
public class BillLadingServiceImpl extends BaseServiceImpl implements BillLadingService {

    private static final String BILL_LADING_ID_NOT_EXISITS = "bill.lading.id.is.not.exist";
    private static final String BILL_CYCLE_ID_NOT_EXISITS = "bill.cycle.id.is.not.exist";
    private static final String PARAM_BILL_LADING = "TS"; // Phần đầu của các mã code
    private static final String PARAM_BILL_LADING_DETAIL = "NS";
    private static final String PARAM_BILL_PACKAGE = "XS";
    private static LocalDate now = LocalDate.now();

    final float volumetricWeightContrain = 333;
    @Autowired
    private BillLadingRepository billLadingRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private BillLadingRepositoryCustom billLadingRepositoryCustom;

    @Autowired
    private InsuranceRepository insuranceRepository;

    @Autowired
    private BillCycleRepository billCycleRepository;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private BillPackageRepository billPackageRepository;

    @Autowired
    private BillLadingDetailRepository billLadingDetailRepository;

    @Autowired
    private BillServiceRepository serviceRepository;

    @Autowired
    private WarehouseDistanceRepositoryCustom warehouseDistanceRepositoryCustom;

    @Override
    public ResponseEntity<Object> findAll() {
        return new ResponseEntity<>(this.billLadingRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<BillLading> optionalBillLading = this.billLadingRepository.findById(id);
        if (!optionalBillLading.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString(BillLadingServiceImpl.BILL_LADING_ID_NOT_EXISITS, id));
        }
        return new ResponseEntity<>(optionalBillLading.get(), HttpStatus.OK);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ResponseEntity<Object> create(String acceptLanguage, BillLading source) throws BusinessException, ParseException {
        List<BillLading> billLadingList = new ArrayList<>();
        List<BillLadingDetail> billLadingDetailList = new ArrayList<>();
        List<BillPackage> billPackageList = new ArrayList<>();
        if (source.getId() != null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.can.not.exists.id"));
        }
        this.validBillLading(source);
        BillLading result = this.addOrEditBilllading(source);
        billLadingDetailList = this.getListBillLadingDetail(source);
        result.setStatus(BillLadingStatus.NEW.getValue());
        BillCycle billCycle = this.billCycleRepository.save(result.getBillCycle());
        result.setBillCycle(billCycle);
        BillLading resultFinal = this.billLadingRepository.save(result);
        billLadingDetailList.forEach(billLadingDetail -> billLadingDetail.setBillLadingId(resultFinal.getId()));

        /* Luu danh sach Bill_Lading_detail , sau do tim bill_lading_detail_id va gan vao from_bill_lading_id cho cac kho nhan hang */
        billLadingDetailList = this.billLadingDetailRepository.saveAll(billLadingDetailList);
        long fromBillLadingId = 0L;
        for (BillLadingDetail billLadingDetail : billLadingDetailList) {
            List<Long> billServiceIds = new ArrayList<>();
            Set<BillService> billServices = new HashSet<BillService>();

            if (billLadingDetail.getWarehouseType() == WarehouseType.UP.getValue()) {
                fromBillLadingId = billLadingDetail.getId();
            }

            if (billLadingDetail.getBillServiceDtoList() != null) {
                if (!billLadingDetail.getBillServiceDtoList().isEmpty()) {
                    List<BillServiceDto> billServiceDtoList = billLadingDetail.getBillServiceDtoList();
                    for (BillServiceDto billServiceDto : billServiceDtoList) {
                        Optional<BillService> oBillService = this.serviceRepository.findById(billServiceDto.getBillServiceId());
                        this.validBillService(oBillService.get());
                        billServices.add(oBillService.get());
                    }
                    billLadingDetail.setServices(billServices);
                }
            }
        }
        final long fromBillLadingFinal = fromBillLadingId;


        billLadingDetailList.stream()
                .filter(billLadingDetail -> billLadingDetail.getWarehouseType() == WarehouseType.DOWN.getValue())
                .forEach(billLadingDetail1 -> billLadingDetail1.setFromBillLadingDetailId(fromBillLadingFinal));


        for (BillLadingDetail billLadingDetail : billLadingDetailList) {
            billPackageList = billLadingDetail.getListBillPackages();
            for (BillPackage billPackage : billPackageList) {
                if (billPackage.getApparamValue() == null) {
                    throw new BusinessException(this.translator.toLocale("bill.package.missing.input.ap.param.value"));
                }
                billPackage.setBillLadingDetailId(billLadingDetail.getId());
            }
            billPackageList = this.billPackageRepository.saveAll(billPackageList);
            billLadingDetail.setListBillPackages(billPackageList);
        }
        billLadingDetailList = this.billLadingDetailRepository.saveAll(billLadingDetailList);
        resultFinal.setBillLadingDetails(billLadingDetailList);
        billLadingList.add(resultFinal);
        return new ResponseEntity<>(billLadingList, HttpStatus.OK);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> update(String acceptLanguage, Long id, BillLading source) throws BusinessException, ParseException {
        List<BillLadingDetail> billLadingDetailList = new ArrayList<>();
        List<BillPackage> billPackageList = new ArrayList<>();
        if ((id == null) || (source.getId() == null)) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.id"));
        }
        if (!source.getId().equals(id)) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.id"));
        }
        this.validBillLading(source);
        Optional<BillLading> optionalBillLading = this.billLadingRepository.findById(source.getId());
        if (!optionalBillLading.isPresent()) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.id"));
        }
        BillLading result = this.addOrEditBilllading(source);
        billLadingDetailList = this.getListBillLadingDetail(source);
        result.setStatus(BillLadingStatus.NEW.getValue());
        final BillCycle billCycle = this.billCycleRepository.save(result.getBillCycle());
        result.setBillCycle(billCycle);
        final BillLading resultFinal = this.billLadingRepository.save(result);
        billLadingDetailList.forEach(billLadingDetail -> billLadingDetail.setBillLadingId(resultFinal.getId()));

        /* Luu danh sach Bill_Lading_detail , sau do tim bill_lading_detail_id va gan vao from_bill_lading_id cho cac kho nhan hang */
        billLadingDetailList = this.billLadingDetailRepository.saveAll(billLadingDetailList);
        long fromBillLadingId = 0L;

        for (BillLadingDetail billLadingDetail : billLadingDetailList) {
            if (billLadingDetail.getBillServiceDtoList() != null && !(billLadingDetail.getBillServiceDtoList().isEmpty())) {
                List<BillServiceDto> billServiceDtoList = billLadingDetail.getBillServiceDtoList();
                Set<BillService> billServices = new HashSet<BillService>();
                for (BillServiceDto billServiceDto : billServiceDtoList) {
                    Optional<BillService> billService = this.serviceRepository.findById(billServiceDto.getBillServiceId());
                    if (!billService.isPresent()) {
                        throw new BusinessException(this.translator.toLocale("common.input.missing.bill.details.service"));
                    }
                    billServices.add(billService.get());
                }
                billLadingDetail.setServices(billServices);
            }

        }

        final long finalFromBillLadingId = fromBillLadingId;

        billLadingDetailList.stream()
                .filter(billLadingDetail -> billLadingDetail.getWarehouseType() == WarehouseType.DOWN.getValue())
                .forEach(billLadingDetail1 -> billLadingDetail1.setFromBillLadingDetailId(finalFromBillLadingId));

        for (BillLadingDetail billLadingDetail : billLadingDetailList) {
            billPackageList = billLadingDetail.getListBillPackages();
            for (BillPackage billPackage : billPackageList) {
                billPackage.setBillLadingDetailId(billLadingDetail.getId());
            }
            billPackageList = this.billPackageRepository.saveAll(billPackageList);
            billLadingDetail.setListBillPackages(billPackageList);
        }
        billLadingDetailList = this.billLadingDetailRepository.saveAll(billLadingDetailList);
        resultFinal.setBillLadingDetails(billLadingDetailList);
        return new ResponseEntity<>(resultFinal, HttpStatus.OK);


    }


    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> cancel(Long id) throws BusinessException {
        Optional<BillLading> optionalBillLading = this.billLadingRepository.findById(id);
        if (!optionalBillLading.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString(BillLadingServiceImpl.BILL_LADING_ID_NOT_EXISITS, id));
        }
        BillLading billLading = optionalBillLading.get();
        if (!BillLadingStatus.NEW.getValue().equals(billLading.getStatus())) {
            throw new BusinessException(this.translator.toLocale("bill.lading.deactive.only.status.new"));
        }
        billLading.setStatus(BillLadingStatus.CANCEL.getValue());
        billLading = this.billLadingRepository.save(billLading);
        return new ResponseEntity<>(billLading, HttpStatus.OK);
    }

    public ResponseEntity<Object> caculateMoneyPreCustomerAcceptOrder(String acceptLanguage, BillLading billLading) throws BusinessException {
        if (billLading == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.can.not.be.null"));
        }
        this.validBillLading(billLading);
        float totalWeight = 0;
        float totalDistanceDelivery = 0;
        float totalDeliveryTime = 0;
        float totalMoney = 0.0f;
        LocalTime localTime;
        if (billLading.getCustomer().getId() == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.missing.input.customer.id"));
        }
        List<WarehouseDistance> warehouseDistanceList = this.warehouseDistanceRepositoryCustom.findListWarehouseDistanceByCustomerId(billLading.getCustomer().getId());
        if (warehouseDistanceList.size() == 0) {
            throw new BusinessException(this.translator.toLocale("bill.lading.missing.input.customer.id"));
        }
        List<BillLadingDetail> billLadingDetailList = billLading.getBillLadingDetails();


        for (BillLadingDetail billLadingDetail : billLadingDetailList) {
            if (billLadingDetail.getWarehouseType() == WarehouseType.UP.getValue()) {
                continue;
            }
            for (WarehouseDistance warehouseDistance : warehouseDistanceList) {
                if (warehouseDistance.getFromWarehouseId() == billLadingDetail.getFromWarehouseId() && warehouseDistance.getToWarehouseId() == billLadingDetail.getWarehouse().getId()) {
                    totalDistanceDelivery += warehouseDistance.getDistance();
                    localTime = warehouseDistance.getEstimatedTime();
                    totalDeliveryTime += this.convertLocalTimeToFloat(localTime);
                }
            }
            totalWeight += this.caculateTotalWeight(billLadingDetail);
        }

        totalMoney = (totalDeliveryTime * totalWeight * totalDistanceDelivery) + ((float) 0.1 * (totalDeliveryTime * totalWeight * totalDistanceDelivery));
        BigDecimal totalMoneyTempoary = new BigDecimal(Float.toString(totalMoney));
        billLading.setTotalAmount(totalMoneyTempoary);
        return new ResponseEntity<>(billLading, HttpStatus.OK);
    }

    @Override
    public Page<BillLading> find(String billLadingCode, Long customerId, Integer status, LocalDateTime fDate, LocalDateTime tDate,Integer billCycleType, Integer pageNumber, Integer pageSize) throws BusinessException {
        return this.billLadingRepositoryCustom.findByCustomerIdAndDate(billLadingCode, status, customerId, fDate, tDate,billCycleType, PageRequest.of(pageNumber - 1, pageSize));
    }


    private List<BillLadingDetail> getListBillLadingDetail(BillLading billLading) throws BusinessException {
        if ((billLading.getBillLadingDetails() == null) || billLading.getBillLadingDetails().isEmpty()) {
            throw new BusinessException(this.translator.toLocale("common.input.missing.bill.details"));
        }
        List<BillLadingDetail> billLadingDetailList = new ArrayList<>();
        for (BillLadingDetail billLadingDetail : billLading.getBillLadingDetails()) {
            BillLadingDetail billDT = new BillLadingDetail();
            this.validBillLadingDetail(billLadingDetail, billLading);
            Optional<Warehouse> optionalWarehouse = this.warehouseRepository.findById(billLadingDetail.getWarehouse().getId());
            if (!optionalWarehouse.isPresent()) {
                throw new BusinessException(this.translator.toLocale("common.input.missing.bill.details.warehouse"));
            }
            Warehouse warehouse = optionalWarehouse.get();
            if (!WarehouseStatus.ACTIVED.getValue().equals(warehouse.getStatus())) {
                throw new BusinessException(this.translator.toLocale("bill.lading.detail.input.invalid.warehouse"));
            }
            billDT.setWarehouse(warehouse);
            Set<BillService> serviceSet = billLadingDetail.getServices();
            for (BillService service : serviceSet) {
                Optional<BillService> optionalBillService = this.serviceRepository.findById(service.getId());
                if (!optionalBillService.isPresent()) {
                    throw new BusinessException(this.translator.toLocale("bill.lading.detail.input.invalid.service"));
                }
            }
            //serviceSet.clear();
            billDT.setFromWarehouseId(billLadingDetail.getFromWarehouseId());
            billDT.setServices(null);
            billDT.setBillServiceDtoList(billLadingDetail.getBillServiceDtoList());
            billDT.setWarehouseType(billLadingDetail.getWarehouseType());
            billDT.setListBillPackages(billLadingDetail.getListBillPackages());
            billDT.setApprovedType(billLadingDetail.getApprovedType());
            billDT.setDescription(billLadingDetail.getDescription());
            billDT.setExpectedFromTime(billLadingDetail.getExpectedFromTime());
            billDT.setExpectedToTime(billLadingDetail.getExpectedToTime());
            billDT.setStatus(billLadingDetail.getStatus());
            billDT.setStatusOrder(billLadingDetail.getStatusOrder());
            billDT.setTotalWeight(billLadingDetail.getTotalWeight());
            billLadingDetailList.add(billDT);
        }

        return billLadingDetailList;
    }


    private BillLading addOrEditBilllading(BillLading billLading) throws BusinessException, ParseException {
        BillLading result;
        if (billLading.getId() == null) {
            result = new BillLading();
            result.setStatus(BillLadingStatus.NEW.getValue());
        } else {
            Optional<BillLading> optionalBillLading = this.billLadingRepository.findById(billLading.getId());
            if (!optionalBillLading.isPresent()) {
                throw new BusinessException(this.translator.toLocaleByFormatString(
                        BillLadingServiceImpl.BILL_LADING_ID_NOT_EXISITS, billLading.getId()));
            }
            result = optionalBillLading.get();

            long oldCustomerId =  result.getCustomer().getId();
            long updateCustomerId = billLading.getCustomer().getId();

            if(oldCustomerId !=  updateCustomerId){
                throw new BusinessException(this.translator.toLocale("customer.id.can.not.changed"));
            }

            Optional<BillCycle> oBillCycle = this.billCycleRepository.findById(result.getBillCycle().getId());
            if (!oBillCycle.isPresent()) {
                throw new BusinessException(this.translator.toLocaleByFormatString(
                        BillLadingServiceImpl.BILL_CYCLE_ID_NOT_EXISITS, result.getBillCycle().getId()));
            }
            this.billCycleRepository.delete(oBillCycle.get());
            List<BillLadingDetail> billLadingDetailList = result.getBillLadingDetails();
            if (billLadingDetailList == null) {
                throw new BusinessException(this.translator.toLocale("common.input.missing.bill.lading.detail"));
            }
            for (BillLadingDetail billLadingDetail : billLadingDetailList) {
                billLadingDetail.getServices().clear();
                this.validBillLadingDetail(billLadingDetail, billLading);
                List<BillPackage> billPackageList = billLadingDetail.getListBillPackages();

                if (billPackageList == null) {
                    throw new BusinessException(this.translator.toLocale("common.input.missing.list.package"));
                }
                this.billPackageRepository.deleteAll(billPackageList);

            }
            this.billLadingDetailRepository.deleteAll(result.getBillLadingDetails());

            if (!BillLadingStatus.NEW.getValue().equals(result.getStatus())) {
                throw new BusinessException(this.translator.toLocale("bill.lading.can.not.edit.without.new.status"));
            }

        }

        Optional<Customer> optionalCustomer = this.customerRepository.findById(billLading.getCustomer().getId());
        if (!optionalCustomer.isPresent()) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.customer"));
        }
        result.setCustomer(optionalCustomer.get());

        Optional<Insurance> optionalInsurance = this.insuranceRepository.findById(billLading.getInsurance().getId());
        if (!optionalInsurance.isPresent()) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.insurance"));
        }
        result.setInsurance(optionalInsurance.get());

        BillCycle billCycle = billLading.getBillCycle();
        if (billCycle == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.cycle"));
        }

        this.vaildBillCycle(billCycle);
        result.setBillCycle(billCycle);

        List<BillLadingDetail> billLadingDetailList = billLading.getBillLadingDetails();
        if (billLadingDetailList == null) {
            throw new BusinessException(this.translator.toLocale("common.input.missing.bill.lading.detail"));
        }
        for (BillLadingDetail billLadingDetail : billLadingDetailList) {
            this.validBillLadingDetail(billLadingDetail, billLading);
            List<BillPackage> billPackageList = billLadingDetail.getListBillPackages();
            if (billPackageList == null) {
                throw new BusinessException(this.translator.toLocale("common.input.missing.list.package"));
            }
        }
        result.setBillLadingDetails(null);
        String code = this.autoGenerateOrderCode("TS");
        result.setBillLadingCode(this.autoGenerateOrderCode("TS"));
        result.setPromotionCode(billLading.getPromotionCode());
        result.setReleaseType(billLading.getReleaseType());
        result.setSurcharge(billLading.getSurcharge());
        result.setTolls(billLading.getTolls());
        result.setTotalAmount(billLading.getTotalAmount());
        result.setTotalPacel(billLading.getTotalPacel());
        result.setTotalVolumn(billLading.getTotalVolumn());
        result.setTotalPayment(billLading.getTotalPayment());
        result.setTotalWeight(billLading.getTotalWeight());
        result.setVat(billLading.getVat());
        return result;
    }

    private List<BillLadingDetail> getBillLadingDetails(BillLading source) throws BusinessException {
        if ((source.getBillLadingDetails() == null) || source.getBillLadingDetails().isEmpty()) {
            throw new BusinessException(this.translator.toLocale("common.input.missing.details"));
        }
        return null;
    }

    private List<BillPackage> getListBillPackages(BillLadingDetail source) throws BusinessException {
        if ((source.getListBillPackages() == null) || source.getListBillPackages().isEmpty()) {
            throw new BusinessException(this.translator.toLocale("common.input.missing.details"));
        }
        return null;
    }


    @Override
    public ResponseEntity<Object> findByEntity(BillLading entities) throws BusinessException {
        return new ResponseEntity<>(this.billLadingRepositoryCustom.findByEntity(entities), HttpStatus.OK);
    }

    private void validBillLading(BillLading billLading) throws BusinessException {

        int totalParcel = 0;

        if (billLading.getTotalWeight() == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.total.weight"));
        }
        if (billLading.getTotalAmount() == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.total.amount"));
        }
        if (billLading.getTotalPayment() == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.total.payment"));
        }
        if (billLading.getTotalVolumn() == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.total.volumn"));
        }
        if (billLading.getVat() == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.vat"));
        }
        if (billLading.getCustomer() != null) {
            if (billLading.getCustomer().getId() == null) {
                throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.customer.id"));
            }
        } else {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.customer"));
        }

        if (billLading.getInsurance() != null) {
            if (billLading.getInsurance().getId() == null) {
                throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.insurance.id"));
            }
        } else {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.insurance"));
        }


        Optional<Customer> oCustomer = this.customerRepository.findById(billLading.getCustomer().getId());
        if (!oCustomer.isPresent()) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.customer.id"));
        }
        Customer customer = oCustomer.get();
        List<BillLadingDetail> lstBillLadingDetail = billLading.getBillLadingDetails();
        List<Warehouse> lstWarehouses = this.warehouseRepository.getListWarehouseByCustomer(customer.getId());
        if(lstWarehouses == null){
            throw new BusinessException(this.translator.toLocale("customer.do.not.have.warehouse"));
        }

        Boolean checkWarehouseCustomer = false;
        for (BillLadingDetail billLadingDetail : lstBillLadingDetail) {
            Optional<Warehouse> optionalWarehouse = this.warehouseRepository.findById(billLadingDetail.getWarehouse().getId());
            if (!optionalWarehouse.isPresent()) {
                throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.lading.details.warehouse.id"));
            }

            checkWarehouseCustomer  = lstWarehouses.contains(optionalWarehouse.get());
            System.out.println("Check thu phat"+ checkWarehouseCustomer);
            if(checkWarehouseCustomer == false){
                throw new BusinessException(this.translator.toLocale("bill.lading.detail.missing.input.warehouse"));
            }

            List<BillPackage> lstPackage = billLadingDetail.getListBillPackages();
            for (BillPackage BillPackage : lstPackage) {
                if (BillPackage == null) {
                    throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.lading.details.list.package"));
                }
                totalParcel += BillPackage.getQuantityParcel();
            }

        }

        if (totalParcel == 0 || totalParcel != billLading.getTotalPacel()) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.lading.details.list.package.equals.quantity.parcel.not.same"));
        }

    }

    private void validBillLadingDetail(BillLadingDetail billLadingDetail, BillLading billLading) throws BusinessException {
        if (billLadingDetail == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.lading.detail"));
        }
        if (billLading == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.can.not.null"));
        }
        if (billLadingDetail.getListBillPackages() == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.detail.input.missing.list.package"));
        }

        if (billLadingDetail.getWarehouse() == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.detail.input.missing.warehouse"));
        }
        LocalTime expectedFromTime = null;
        LocalTime expectedToTime = null;

        if (billLadingDetail.getExpectedFromTime() == null || billLadingDetail.getExpectedFromTime() == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.detail.input.missing.expected.time"));
        } else {
            expectedFromTime = billLadingDetail.getExpectedFromTime();
            expectedToTime = billLadingDetail.getExpectedToTime();
            int check = expectedToTime.compareTo(expectedFromTime);
                if (check  < 0) {
                throw new BusinessException(this.translator.toLocale("expected.from.time.must.less.than.expected.to.time"));
            }
        }


        if (billLading.getBillCycle().getCycleType() == BillCycleType.DeliveryImmediately.getValue()) {

            long paddingTime = 30; // thoi gian cong them 30 phut de he thong tim xe va sap xep
            LocalTime localDateTime = LocalTime.now().plusMinutes(paddingTime);

            int t = expectedFromTime.compareTo(localDateTime);
            if (t < 0) {
                throw new BusinessException(this.translator.toLocale("the.desired.time.must.be.30.minutes.away.from.the.current.time"));

            }

        }

    }


    private void validBillPackage(BillPackage billPackage) throws BusinessException {
        if (billPackage == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.package"));
        }
    }

    private void validBillService(BillService billService) throws BusinessException {
        if (billService == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.service"));

        }
    }

    private void vaildBillCycle(BillCycle billCycle) throws BusinessException, ParseException {
        LocalDate startDate = billCycle.getStartDate().toLocalDate();
        LocalDate endDate = billCycle.getEndDate().toLocalDate();

        boolean startDateIsAfter = startDate.isAfter(now);
        boolean endDateIsAfter = endDate.isAfter(now);
        boolean checkRangeDate = startDate.isBefore(endDate);

        if(startDateIsAfter == false){
            throw new BusinessException(this.translator.toLocale("start.date.must.after.current.date"));
        }
        if(endDateIsAfter == false){
            throw new BusinessException(this.translator.toLocale("end.date.must.after.current.date"));
        }

        if(checkRangeDate == false){
            throw new BusinessException(this.translator.toLocale("start.date.must.before.end.date"));
        }


        if (BillCycleType.MONTH.getValue() == billCycle.getCycleType()) {
            Duration diff = Duration.between(startDate.atStartOfDay(), endDate.atStartOfDay());
            long diffDays = diff.toDays();
            if(diffDays < 30){


                throw new BusinessException(this.translator.toLocale("time.period.from.day.to.day.must.be.greater.than.30.days"));

            }

            if (!StringUtils.isNullOrEmpty(billCycle.getDeliveryDate())) {
                String regex = "^[0-9]{2}[/][0-9]{2}[/][2][0]{1}[2-9]{1}[0-9]{1}";
                this.validFromDateToDatePlusOne(billCycle.getStartDate(), billCycle.getEndDate(), true, this.getSysdate());
                String[] time = billCycle.getDeliveryDate().split(",");
                this.validDayofMonth(regex, time, billCycle);
            } else {
                throw new BusinessException(this.translator.toLocale("bill.cycle.delivery.date.can.not.be.null"));
            }

        }
        if (!billCycle.getCycleType().equals(BillCycleType.MONTH.getValue()) && billCycle.getCycleType() != BillCycleType.WEEK.getValue()
                && billCycle.getCycleType() != BillCycleType.SCHEDULE.getValue() && billCycle.getCycleType() != BillCycleType.DeliveryImmediately.getValue()) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.cycle.type"));
        }

        if (billCycle.getCycleType() == BillCycleType.MONTH.getValue() && (billCycle.getDeliveryDate() == null || billCycle.getMonday() != null
                || billCycle.getThursday() != null || billCycle.getWednesday() != null || billCycle.getTuesday() != null || billCycle.getFriday() != null
                || billCycle.getSaturday() != null || billCycle.getSunday() != null || billCycle.getWeek1() != null || billCycle.getWeek2() != null
                || billCycle.getWeek3() != null || billCycle.getWeek4() != null)) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.cycle.type"));
        }

        if (billCycle.getCycleType() == BillCycleType.WEEK.getValue() && (billCycle.getDeliveryDate() != null ||
                (billCycle.getMonday() == null && billCycle.getTuesday() == null && billCycle.getWednesday() == null && billCycle.getThursday() == null
                        && billCycle.getFriday() == null && billCycle.getSaturday() == null && billCycle.getSunday() == null
                        && billCycle.getWeek1() == null && billCycle.getWeek2() == null && billCycle.getWeek3() == null && billCycle.getWeek4() == null))) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.cycle.type"));
        }

        if (billCycle.getCycleType() == BillCycleType.WEEK.getValue() && billCycle.getMonday() == null && billCycle.getThursday() == null && billCycle.getWednesday() == null && billCycle.getTuesday() == null && billCycle.getFriday() == null
                && billCycle.getSaturday() == null && billCycle.getSunday() == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.input.day.in.week"));
        }

        if (billCycle.getCycleType() == BillCycleType.WEEK.getValue() && billCycle.getWeek1() == null && billCycle.getWeek2() == null && billCycle.getWeek3() == null && billCycle.getWeek4() == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.input.week.in.month"));

        }

        if (billCycle.getCycleType() == BillCycleType.WEEK.getValue()) {
            if ((billCycle.getSunday() != null && billCycle.getSunday() != 1) || (billCycle.getThursday() != null && billCycle.getThursday() != 1) ||
                    (billCycle.getWednesday() != null && billCycle.getWednesday() != 1) || (billCycle.getTuesday() != null && billCycle.getTuesday() != 1) ||
                    (billCycle.getFriday() != null && billCycle.getFriday() != 1) || (billCycle.getSaturday() != null && billCycle.getSaturday() != 1) ||
                    (billCycle.getMonday() != null && billCycle.getMonday() != 1)) {
                throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.value.day"));

            }

            if ((billCycle.getWeek1() != null && billCycle.getWeek1() != 1) || (billCycle.getWeek2() != null && billCycle.getWeek2() != 1) ||
                    (billCycle.getWeek3() != null && billCycle.getWeek3() != 1) || (billCycle.getWeek4() != null && billCycle.getWeek4() != 1)) {
                throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.value.week"));

            }


        }


        if (billCycle.getCycleType() == BillCycleType.SCHEDULE.getValue()) {
            if (billCycle.getDeliveryDate() != null) {
                throw new BusinessException(this.translator.toLocale("delivery.date.must.null"));
            }
            if (billCycle.getWeek1() != null || billCycle.getWeek2() != null || billCycle.getWeek3() != null || billCycle.getWeek4() != null) {
                throw new BusinessException(this.translator.toLocale("week.1,2,3,4.must.null"));
            }
            if (billCycle.getMonday() != null || billCycle.getTuesday() != null || billCycle.getWednesday() != null || billCycle.getThursday() != null
                    || billCycle.getFriday() != null || billCycle.getSaturday() != null || billCycle.getSunday() != null) {
                throw new BusinessException(this.translator.toLocale("days.in.week.must.null"));
            }

        }


    }

    private void validDayofMonth(String regex, String[] time, BillCycle billCycle) throws BusinessException, ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        LocalDate startDate = billCycle.getStartDate().toLocalDate();
        LocalDate endDate = billCycle.getEndDate().toLocalDate();

        boolean startDateIsAfter = startDate.isAfter(now);
        boolean endDateIsAfter = endDate.isAfter(now);
        boolean checkRangeDate = startDate.isBefore(endDate);

        if(startDateIsAfter == false){
            throw new BusinessException(this.translator.toLocale("start.date.must.after.current.date"));
        }
        if(endDateIsAfter == false){
            throw new BusinessException(this.translator.toLocale("end.date.must.after.current.date"));
        }

        if(checkRangeDate == false){
            throw new BusinessException(this.translator.toLocale("start.date.must.before.end.date"));
        }

        for (int i = 0; i < time.length; i++) {
            boolean check = Pattern.matches(regex, time[i]);
            if (check == false) {
                throw new BusinessException(this.translator.toLocale("bill.cycle.delivery.date.must.formated.dd/mm/yyyy"));
            }
            String d = time[i];
            LocalDate localDate = LocalDate.parse(d, formatter);
            if (localDate.isBefore(startDate)) {
                throw new BusinessException(this.translator.toLocale("delivery.date.must.after.or.equal.start.date"));
            }
            if (localDate.isAfter(endDate)) {
                throw new BusinessException(this.translator.toLocale("delivery.date.must.before.or.equal.end.date"));
            }
            String[] str = time[i].split("/");
            String day = str[0];
            String year = str[2];
            String deliveryDate = "" + time[i];
            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(deliveryDate.trim());
            Date lastDayInMonth = DateUtil.getLastDateInMonth(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(lastDayInMonth);
            int lastDay = cal.get(Calendar.DAY_OF_MONTH);
            int deliveryDay = Integer.valueOf(day);

            if (deliveryDay > lastDay) {
                throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.cycle.delivery.date"));
            }
        }
    }


    private void validCustomer(Customer customer) throws BusinessException {
        if (customer == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.customer"));

        }
        Optional<Customer> oCustomer = this.customerRepository.findById(customer.getId());
        if (!oCustomer.isPresent()) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.customer"));
        }

    }

    private void validAppParam(AppParam appParam) throws BusinessException {
        Optional<AppParam> optionalAppParam = this.appParamRepository.findById(appParam.getId());
        if (!optionalAppParam.isPresent()) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.app.param.id"));
        }
    }

    private void validInsurance(Insurance insurance) throws BusinessException {
        if (insurance == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.insurance"));
        }
        Optional<Insurance> oInsurance = this.insuranceRepository.findById(insurance.getId());
        if (!oInsurance.isPresent()) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.insurance"));
        }

    }

    public Float caculateTotalWeight(BillLadingDetail billLadingDetail) throws BusinessException {
        if (billLadingDetail == null) {
            throw new BusinessException(this.translator.toLocale("bill.lading.input.missing.bill.lading.detail"));
        }
        float grossWeight = 0;
        float volumetricWeight = 0;


        List<BillPackage> billPackageList = billLadingDetail.getListBillPackages();
        for (BillPackage billPackage : billPackageList) {
            grossWeight += billPackage.getNetWeight();
            volumetricWeight += ((billPackage.getLength() * billPackage.getWidth() * billPackage.getHeight() * billPackage.getQuantityParcel()) * volumetricWeightContrain);
        }
        if (volumetricWeightContrain >= grossWeight) {
            return volumetricWeightContrain;
        }
        return grossWeight;
    }


    private Float convertLocalTimeToFloat(LocalTime localTime) {
        Float time;
        int hour = localTime.getHour();
        int minute = localTime.getMinute();
        time = (float) ((float) hour + (float) minute / 60);
        return time;
    }

    private String autoGenerateOrderCode(String param) {
        BigDecimal bkey = this.billLadingRepository.getOneResult(param);
        String orderCode = null;
        if (bkey != null) {
            long key = bkey.longValue();
            orderCode = String.valueOf(param)
                    + MathUtils.fixLengthString(++key, 4);
            Optional<BillLading> oBillLading = this.billLadingRepository.getBillLadingByCode(orderCode);
            BillLading checkBillLading = null;
            if (oBillLading.isPresent()) {
                checkBillLading = oBillLading.get();
            }
            while (checkBillLading != null) {
                orderCode = String.valueOf(param)
                        + MathUtils.fixLengthString(++key, 4);
                oBillLading = this.billLadingRepository.getBillLadingByCode(orderCode);
                if (oBillLading.isPresent()) {
                    checkBillLading = oBillLading.get();
                } else {
                    checkBillLading = null;
                }

            }

            return orderCode;
        } else {
            long key = 0;
            orderCode = String.valueOf(param)
                    + MathUtils.fixLengthString(++key, 4);
            return orderCode;
        }
    }
}

