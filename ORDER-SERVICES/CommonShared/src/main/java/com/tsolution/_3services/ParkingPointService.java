package com.tsolution._3services;

import com.tsolution._1entities.ParkingPoint;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface ParkingPointService {
    ResponseEntity<Object> findAll();

    ResponseEntity<Object> findById(Long id) ;

    ResponseEntity<Object> findByToken() throws BusinessException;

    ResponseEntity<Object> findByFleetId(Long id) throws BusinessException;

    ResponseEntity<Object> find(ParkingPoint searchData, Integer pageNumber, Integer pageSize) throws BusinessException;

    ResponseEntity<Object> create(ParkingPoint entity) throws BusinessException;

    ResponseEntity<Object> update(ParkingPoint entity)
            throws BusinessException;
    ResponseEntity<Object> delete(Long id)throws BusinessException;
}
