package com.tsolution._3services.impl;

import com.tsolution._1entities.ObjAttachment;
import com.tsolution._1entities.enums.OBJECT_ATTACHMENT;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution._2repositories.ObjAttachmentRepository;
import com.tsolution._3services.ObjAttachmentService;
import com.tsolution.excetions.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class ObjAttachmentServiceImpl extends BaseServiceImpl implements ObjAttachmentService {


    @Autowired
    ObjAttachmentRepository objAttachmentRepository;
    @Autowired
    FileStorageService fileStorageService;

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return new ResponseEntity<>(this.objAttachmentRepository.findById(id),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findByObjId(Long id, int attType) {
        return new ResponseEntity<>(this.objAttachmentRepository.findByObjId(id, attType, OBJECT_ATTACHMENT.VAN.getValue()),
                HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<Object> create(ObjAttachment entity) throws BusinessException {
        if (entity == null || entity.getId() != null || StringUtils.isEmpty(entity.getAttachUrl())
                || StringUtils.isEmpty(entity.getAttachName())
                || entity.getObjId() == null || entity.getObjId() == 0) {
            return new ResponseEntity<>(this.translator.toLocale("common.input.info.invalid"),
                    HttpStatus.OK);
        }
        boolean check = false;
        for (OBJECT_ATTACHMENT type : OBJECT_ATTACHMENT.values()) {
            if (type.getValue().equals(entity.getAttachName())) {
                check = true;
                break;
            }
        }
        if (!check) {
            return new ResponseEntity<>(this.translator.toLocale("common.input.info.invalid"),
                    HttpStatus.OK);
        }
        entity.setStatus(1);
        ObjAttachment entities1 = this.objAttachmentRepository.save(entity);
        return new ResponseEntity<>(entities1, HttpStatus.OK);
    }


    @Override
    @Transactional
    public ResponseEntity<Object> update(List<ObjAttachment> listObj) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> delete(ObjAttachment entity) throws BusinessException {
        if (entity == null || entity.getId() == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        } else {
            if (this.objAttachmentRepository.existsById(entity.getId())) {
                entity.setStatus(StatusType.STOPPED.getValue());
                this.objAttachmentRepository.save(entity);
            }
            return new ResponseEntity<>(entity, HttpStatus.OK);
        }
    }

    @Transactional
    @Override
    public boolean saveAttachment(Long id, Collection<ObjAttachment> attachments, int type, String attachName) {
        try {
            if (attachments != null && !attachments.isEmpty()) {
                Collection<ObjAttachment> saveList = new ArrayList<>();
                int order = 1;
                for (ObjAttachment obj : attachments) {
                    if (StringUtils.isEmpty(obj.getAttachUrl())) continue;
                    obj.setObjId(id);
                    obj.setAttachType(type);
                    obj.setStatus(StatusType.RUNNING.getValue());
                    obj.setAttachName(attachName);
                    obj.setOrd(order++);
                    saveList.add(obj);
                }
                this.objAttachmentRepository.saveAll(saveList);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Transactional
    public boolean deleteAttachment(Long id, Collection<ObjAttachment> attachments) {
        try {
            if (attachments != null && !attachments.isEmpty()) {
                for (ObjAttachment obj : attachments) {
                    Optional<ObjAttachment> oAtt = this.objAttachmentRepository.findActiveById(obj.getId());
                    if (oAtt.isPresent()) {
                        ObjAttachment att = oAtt.get();
                        if (att.getObjId().equals(id)) {
                            att.setStatus(StatusType.STOPPED.getValue());
                            this.objAttachmentRepository.save(att);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
