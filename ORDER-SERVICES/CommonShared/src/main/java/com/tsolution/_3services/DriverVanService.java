package com.tsolution._3services;

import com.tsolution._1entities.DriverVan;
import com.tsolution._3services.impl.BaseServiceImpl;
import com.tsolution.excetions.BusinessException;

import java.time.LocalDateTime;
import java.util.List;

public interface DriverVanService extends BaseService<DriverVan> {
    List<DriverVan> getVanForDriverByDate(Long driverId, LocalDateTime dateCheck) throws BusinessException;
}
