package com.tsolution._3services;

import com.google.firebase.messaging.FirebaseMessagingException;
import com.tsolution._1entities.dto.PushNotificationRequest;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface PushNotificationService {

    void sendPushNotificationWeb(PushNotificationRequest request) throws BusinessException;

//    void sendPushNotification(PushNotificationRequest request) throws BusinessException;

    void sendPushNotificationWeb() throws BusinessException;

    void sendPushNotificationWithoutData(PushNotificationRequest request) throws BusinessException;

    void sendPushNotificationToToken(PushNotificationRequest request) throws BusinessException;

    ResponseEntity<Object> sendAppNotification(PushNotificationRequest pushNotificationRequest) throws BusinessException;

    ResponseEntity<Object> sendMulticastAndHandleErrorsWeb(PushNotificationRequest pushNotificationRequest) throws BusinessException, FirebaseMessagingException;

    ResponseEntity<Object> sendAllAppNotification(PushNotificationRequest pushNotificationRequest) throws BusinessException;

}
