package com.tsolution._3services.impl;

import com.tsolution._1entities.*;
import com.tsolution._2repositories.ParkingPointRepository;
import com.tsolution._2repositories.VanRepository;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.CommonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class VanExcelService extends BaseExcelService<Van> {
    private static final Logger log = LogManager.getLogger(VanExcelService.class);
    @Autowired
    ParkingPointRepository parkingPointRepository;
    @Autowired
    private VanRepository vanRepository;

    @Override
    public Resource exportTemplate() throws Exception {
        Map<String, Object> dynamicValues = searchValidateValue();
        Map<String, Long> parkingPoint = (Map<String, Long>) dynamicValues.get("parkingPoint");
        Map<String, Long> listBrand = (Map<String, Long>) dynamicValues.get("brandName");
        Map<String, Integer> fuelType = (Map<String, Integer>) dynamicValues.get("fuelType");
        Map<String, Integer> statusCar = (Map<String, Integer>) dynamicValues.get("statusCar");
        Map<String, Integer> statusAvailable = (Map<String, Integer>) dynamicValues.get("statusAvailable");
        Map<String, Long> vanType = (Map<String, Long>) dynamicValues.get("vanType");
        List<List<Object>> exportBeans = new ArrayList<>();

        exportBeans.add(new ArrayList<>(fuelType.keySet()));
        exportBeans.add(new ArrayList<>(vanType.keySet()));
        exportBeans.add(new ArrayList<>(listBrand.keySet()));
        exportBeans.add(new ArrayList<>(statusCar.keySet()));
        exportBeans.add(new ArrayList<>(statusAvailable.keySet()));
        exportBeans.add(new ArrayList<>(parkingPoint.keySet()));
        return exportExcelDataTemplate(exportBeans,
                ExcelTemplate.VAN_EXCEL_ADD, null);
    }

    @Override
    protected Resource exportExcelDataTemplate(List<List<Object>> lstData, String tempFileName,
                                               Map<String, Object> fileHeaderParams) throws Exception {
        int dataSize = lstData != null ? lstData.size() : 0;
        if (dataSize > 0) {
            try {
                Map<String, Object> params = new HashMap<>();
                params.put("fuelType", lstData.get(0));
                params.put("vanType", lstData.get(1));
                params.put("brandName", lstData.get(2));
                params.put("statusCar", lstData.get(3));
                params.put("statusAvailable", lstData.get(4));
                params.put("parkingPoint", lstData.get(5));
                return updateExcelFile(params, tempFileName);
            } catch (Exception e) {
                log.error(e);
                throw new BusinessException(e.getMessage());
            }
        }
        throw new BusinessException("common.error");
    }


    @Override
    protected Van readRow(Row row, Map<String, Object> dynamicValues) throws BusinessException {
        Van temp = new Van();
        DataFormatter format = new DataFormatter();
        Map<String, Long> listParkingPoint = (Map<String, Long>) dynamicValues.get("parkingPoint");
        Map<String, Long> listBrand = (Map<String, Long>) dynamicValues.get("brandName");
        Map<String, Integer> fuelType = (Map<String, Integer>) dynamicValues.get("fuelType");
        Map<String, Integer> statusCar = (Map<String, Integer>) dynamicValues.get("statusCar");
        Map<String, Integer> statusAvailable = (Map<String, Integer>) dynamicValues.get("statusAvailable");
        Map<String, Long> vanType = (Map<String, Long>) dynamicValues.get("vanType");
        try {
            // Nếu cột bắt buộc thì return null, không thì bỏ qua
            int col = 1;
            if (row.getCell(col) != null && !row.getCell(col).toString().isEmpty())// Tên xe
                temp.setName(row.getCell(col++).getStringCellValue());
            else col++;
            if (row.getCell(col) != null && !row.getCell(col).toString().isEmpty())// Biển số xe
                temp.setLicencePlate(row.getCell(col++).getStringCellValue());
            else return null;
            if (row.getCell(col) != null && !row.getCell(col).toString().isEmpty())// Đăng kí xe
                temp.setVehicleRegistration(row.getCell(col++).getStringCellValue());
            else return null;
            if (row.getCell(col) != null && !row.getCell(col).toString().isEmpty())// Năm sản xuất
                temp.setYear(Integer.parseInt(format.formatCellValue(row.getCell(col++))));
            if (row.getCell(col) != null && !row.getCell(col).toString().isEmpty())// Chi ơhí
                temp.setCost(row.getCell(col++).getNumericCellValue());
            else return null;
            if (row.getCell(col) != null && !row.getCell(col).toString().isEmpty())// Sức chứa
                temp.setCapacity(row.getCell(col++).getNumericCellValue());
            else col++;
            // Loại nhiên liệu
            if (row.getCell(col) != null && fuelType.get((row.getCell(col).getStringCellValue())) != null){
                temp.setFuelTypeId(fuelType.get((row.getCell(col++).getStringCellValue())));
            }else col++;
            if (row.getCell(col) != null && !row.getCell(col).toString().isEmpty())// Lái xe
                temp.setDriver(row.getCell(col++).getStringCellValue());
            else col++;
            if (row.getCell(col) != null && vanType.get(row.getCell(col).getStringCellValue()) != null)// Loại xe
                temp.setVanTypeId(vanType.get(row.getCell(col++).getStringCellValue()));
            else col++;
            // Nhãn hiệu xe
            if (row.getCell(col) != null && listBrand.get(row.getCell(col).getStringCellValue()) != null){
                temp.setAppParamValueId(listBrand.get(row.getCell(col++).getStringCellValue()));
            } else return null;
            if (row.getCell(col) != null && !row.getCell(col).toString().isEmpty()){
                temp.setVehicleTonnage(row.getCell(col++).getNumericCellValue());
            }
            else col++;
            if (row.getCell(col) != null && !row.getCell(col).toString().isEmpty())
                temp.setVanInspection(row.getCell(col++).getStringCellValue());
            else col++;
            if (row.getCell(col) != null && !row.getCell(col).toString().isEmpty())
                temp.setInspectionDueDate(CommonUtil.
                        convertToLocalDateTimeViaMilisecond(row.getCell(col++).getDateCellValue()));
            else col++;
            if (row.getCell(col) != null && statusCar.get(row.getCell(col).getStringCellValue()) != null){
                temp.setStatusCar(statusCar.get(row.getCell(col++).getStringCellValue()));
            }else col++;

            if (row.getCell(col) != null && statusAvailable.get(row.getCell(col).getStringCellValue()) != null){
                temp.setStatusAvailable(statusAvailable.get(row.getCell(col++).getStringCellValue()));
            }else col++;
            if (row.getCell(col) != null && listParkingPoint.get(row.getCell(col).getStringCellValue()) != null){
                temp.setParkingPointId(listParkingPoint.get(row.getCell(col).getStringCellValue()));
            }
            else return null;
            temp.setLocation(row.getCell(col).getStringCellValue());
        } catch (Exception e) {
            log.error(e);
            throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
        }
        return temp;
    }

    @Override
    protected void persistAll(List<Van> listData) {
//        System.out.println(listData.toString());//for demo
        vanRepository.saveAll(listData);//for real
    }

    @Override
    protected Map<String, Object> searchValidateValue() throws BusinessException {
        Map<String, Object> result = new HashMap<>();
        // Setup data for validation
        Optional<Staff> currentStaff = this.getCurrentStaff();
        List<ParkingPoint> listParkingPoint = new ArrayList<>();
        if (currentStaff.isPresent() && currentStaff.get().getFleetId() != null) {
            listParkingPoint = this.parkingPointRepository.findByFleetId(currentStaff.get().getFleetId());
        }
        Map<String, Long> mapParkingPoint = new HashMap<>();
        for (ParkingPoint pp : listParkingPoint) {
            mapParkingPoint.put(pp.getName(), pp.getId());
        }
        List<ApparamValue> listBrand = apparamValueRepository.findByGroupCode("BRAND_GROUP");
        Map<String, Long> mapBrand = new HashMap<>();
        for (ApparamValue pp : listBrand) {
            mapBrand.put(pp.getParamName(), pp.getId());
        }
        Map<String, Integer> fuelType;
        Map<String, Integer> statusCar;
        Map<String, Integer> statusAvailable;
        Map<String, Long> vanType;
        fuelType = new HashMap<>();
        fuelType.put(this.translator.toLocale("van.dropdown.fuelType.gasoline"), 1);
        fuelType.put(this.translator.toLocale("van.dropdown.fuelType.diesel"), 2);
        statusCar = new HashMap<>();
        statusCar.put(this.translator.toLocale("common.status.activated"), 1);
        statusCar.put(this.translator.toLocale("common.status.deactivated"), 0);
        statusCar.put(this.translator.toLocale("common.status.maintenance"), 2);
        vanType = new HashMap<>();
        vanType.put(this.translator.toLocale("van.dropdown.vanType.regular"), 1L);
        vanType.put(this.translator.toLocale("van.dropdown.vanType.refrigerated"), 2L);
        statusAvailable = new HashMap<>();
        statusAvailable.put(translator.toLocale("common.statusAvailable.available"), 1);
        statusAvailable.put(translator.toLocale("common.statusAvailable.unavailable"), 0);
        result.put("parkingPoint", mapParkingPoint);
        result.put("brandName", mapBrand);
        result.put("fuelType", fuelType);
        result.put("statusCar", statusCar);
        result.put("statusAvailable", statusAvailable);
        result.put("vanType", vanType);
        return result;
    }
}
