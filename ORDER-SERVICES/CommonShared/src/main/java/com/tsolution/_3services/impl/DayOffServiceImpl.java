package com.tsolution._3services.impl;

import com.tsolution._1entities.DayOff;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution._2repositories.DayOffRepository;
import com.tsolution._3services.DayOffService;
import com.tsolution.config.LocalDateTimeConverter;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.SingleSignOnUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class DayOffServiceImpl extends BaseServiceImpl implements DayOffService {

    private SingleSignOnUtils singleSignOnUtils;

    @Autowired
    protected DayOffRepository dayOffRepository;

    @Autowired
    public DayOffServiceImpl(SingleSignOnUtils singleSignOnUtils) {
        this.singleSignOnUtils = singleSignOnUtils;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ResponseEntity<Object> create(String acceptLanguage, DayOff entities) throws BusinessException {

        if (entities == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }

        //validate status
        Stream<Integer> status = Stream.of(StatusType.DELETED.getValue(),StatusType.RUNNING.getValue(),StatusType.WAITING.getValue());
        if (entities.getStatus()==null || "".equals(entities.getStatus())){
            entities.setStatus(StatusType.RUNNING.getValue());
        }else{
            DayOff finalEntities = entities;
            boolean statustype = status.anyMatch(s->s.equals(finalEntities.getStatus())); //.stream().anyMatch(s -> s.equals(finalEntities.getStatus()));
            if (!statustype){
                throw new BusinessException(this.translator.toLocale("day.off.status.not.exisits"));
            }
        }


        if (entities.getReason().isEmpty()|| entities.getReason() == null){
            throw new BusinessException(this.translator.toLocale("day.off.reason.not.empty"));

        }

        this.validFromDateToDate(entities.getFromDate(),entities.getToDate(),Boolean.TRUE,LocalDateTime.now());

        if (entities.getFromDate().isBefore(LocalDateTime.now().plusDays(2))){
            throw new BusinessException(this.translator.toLocale("dayoff.input.fromdate.can.not.less.than.3days"));
        }

        List<DayOff> oDayoff = this.dayOffRepository.find(entities.getStaff().getId(),LocalDateTimeConverter.convertToJavaSqlDate(entities.getFromDate()),LocalDateTimeConverter.convertToJavaSqlDate(entities.getToDate()),entities.getSpec(),StatusType.RUNNING.getValue());
        if (oDayoff != null){
            this.translator.toLocale("dayoff.this.existed");
        }

        entities = this.dayOffRepository.save(entities);
        return new ResponseEntity<>(entities, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> update(String acceptLanguage, Long dayOffId, DayOff entities) throws BusinessException {
        if (entities == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }

        if (entities == null || entities.getId() == null || !dayOffId.equals(entities.getId())) {
            throw new BusinessException(
                    this.translator.toLocale("dayoff.input.missing.id"));
        }
        Optional<DayOff> oDayoff = this.dayOffRepository.findById(entities.getId());
        if (oDayoff == null){
            throw new BusinessException(this.translator.toLocale("dayofff.id.not.exists"));
        }

        DayOff newDayOff = oDayoff.get();
        newDayOff.setStatus(StatusType.DELETED.getValue());
        newDayOff = this.dayOffRepository.save(newDayOff);
        return new ResponseEntity<>(newDayOff,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findAll() {
        return new ResponseEntity<>(this.dayOffRepository.findAll(),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return new ResponseEntity<>(this.dayOffRepository.findById(id),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> find(Long staffId, LocalDateTime fdate, LocalDateTime tdate,Integer spec, Integer status) throws BusinessException {
          return new ResponseEntity<>(this.dayOffRepository.find(staffId, LocalDateTimeConverter.convertToJavaSqlDate(fdate), LocalDateTimeConverter.convertToJavaSqlDate(tdate),spec,status), HttpStatus.OK);
    }
}
