package com.tsolution._3services.impl;

import com.tsolution._1entities.BillPackage;
import com.tsolution._1entities.BillPackageRouting;
import com.tsolution._2repositories.BillPackageRoutingRepository;
import com.tsolution._3services.BillPackageRoutingService;
import com.tsolution.excetions.BusinessException;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;


@Service
public class BillPackageRoutingServiceImpl extends BaseServiceImpl implements BillPackageRoutingService {
    @Autowired
    BillPackageRoutingRepository billPackageRoutingRepository;

    private static final Logger log =  LogManager.getLogger(BillPackageRoutingServiceImpl.class);

    @Override
    public ResponseEntity<Object> findAll() {
        return null;
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> create(List<BillPackageRouting> entities) throws BusinessException {
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> update(Long id, BillPackageRouting source) throws BusinessException {
        if(id == null || source == null || source.getId() == null || id != source.getId()){
            throw new BusinessException(this.translator.toLocale("billpackagerouting.update.missing.info"));
        }
        BillPackageRouting billPackageRouting = this.billPackageRoutingRepository.save(this.addOrEditBillPackageRouting(source));
        return new ResponseEntity<>(billPackageRouting, HttpStatus.OK);
    }
    private void validateBillPackageRouting(BillPackageRouting billPackageRouting) throws BusinessException {
        if(billPackageRouting.getQuantity() == null){
            throw new BusinessException(this.translator.toLocale("common.input.missing.quantity "));
        }
        if(billPackageRouting.getLength() == null){
            throw new BusinessException(this.translator.toLocale("common.input.missing.length"));
        }
        if(billPackageRouting.getWidth() == null){
            throw new BusinessException(this.translator.toLocale("common.input.missing.width"));
        }
        if(billPackageRouting.getHeight() == null){
            throw new BusinessException(this.translator.toLocale("common.input.missing.height"));
        }
        if(billPackageRouting.getTotalWeight() == null){
            throw new BusinessException(this.translator.toLocale("common.input.missing.weight"));
        }
        if(billPackageRouting.getFromWarehouseId() == null){
            throw new BusinessException(this.translator.toLocale("common.input.missing.fromwarehouse"));
        }
        if(billPackageRouting.getToWarehouseId() == null){
            throw new BusinessException(this.translator.toLocale("common.input.missing.towarehouse"));
        }
    }
    private BillPackageRouting addOrEditBillPackageRouting(BillPackageRouting billPackageRouting) throws BusinessException {
        BillPackageRouting result;
        validateBillPackageRouting(billPackageRouting);
        if(billPackageRouting.getId() == null){
            result = this.billPackageRoutingRepository.save(billPackageRouting);
        }
        else{
            Long id = billPackageRouting.getId();
            Optional<BillPackageRouting> opackageRouting = this.billPackageRoutingRepository.findById(id);
            if(!opackageRouting.isPresent()){
                throw new BusinessException(this.translator.toLocaleByFormatString("billpackagerouting.id.not.exisits ", billPackageRouting.getId()));
            }
             result = opackageRouting.get();
             result.setWidth(billPackageRouting.getWidth());
             result.setHeight(billPackageRouting.getHeight());
             result.setLength(billPackageRouting.getLength());
             result.setNote(billPackageRouting.getNote());
             result.setTotalWeight(billPackageRouting.getTotalWeight());
             result.setQuantity(billPackageRouting.getQuantity());
        }
        return result;
    }

}
