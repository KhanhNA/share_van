package com.tsolution.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;


public class DateUtil {

    private static Logger log = LogManager.getLogger(StringUtils.class);

    public static final String DATE_FORMAT_NOW = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_FORMAT_STR = "dd/MM/yyyy";
    public static final String DATE_FORMAT_ATTRIBUTE = "yyyy-MM-dd";
    public static final String DATE_NEW_FORMAT_STR = "MM/dd/yyyy";
    public static final String DATE_FORMAT_VISIT = "dd-MM-yyyy";
    public static final String DATE_FORMAT_EXCEL_FILE = "yyyyMMddHHmmss";
    public static final String DATE_FORMAT_STOCKTRAN = "yyyyMMdd";

    public static final String DATE_FORMAT_STOCKTRAN_UPDATED = "yyMMdd";

    public static final String DATETIME_FORMAT_STR = "dd/MM/yyyy HH:mm";

    public static final String DATETIME_ATTRIBUTE = "yyyy-MM-dd HH:mm";

    public static final String DATE_FORMAT_CREATEDATE = "yyyyMMddHHmmss";

    /** The Constant SECOND. */
    public static final long SECOND = 1000;

    /** The Constant MINUTE. */
    public static final long MINUTE = DateUtil.SECOND * 60;

    /** The Constant HOUR. */
    public static final long HOUR = DateUtil.MINUTE * 60;

    /** The Constant DAY. */
    public static final long DAY = DateUtil.HOUR * 24;

    protected static final int[] arrDays = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    public static final String DATE_FORMAT_NOW_MILLISECONDS = "dd/MM/yyyy HH:mm:ss.SSS";

    /**
     * Return 0 if equals, 1 if date1 > date2, -1 if date1 < date2
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int compareDateWithoutTime(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);
        if (cal1.equals(cal2)) {
            return 0;
        } else if (cal1.after(cal2)) {
            return 1;
        } else {
            return -1;
        }
    }

    public static Date getShortDateFromDates(Date date, int dayNumber) {

        if (dayNumber == 0) {
            return date;
        }
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, dayNumber);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    public static Date addDate(Date input, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(input);
        cal.add(Calendar.DATE, days);

        return cal.getTime();
    }

    public static Date addMonth(Date input, int months) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(input);
        cal.add(Calendar.MONTH, months);

        return cal.getTime();
    }

    public static Date addHour(Date input, int hours) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(input);
        cal.add(Calendar.HOUR, hours);

        return cal.getTime();
    }

    public static Date getOnlyDate(Date input) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(input);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    public static int getWorkingDateInMonth(int month, int year) {
        int numDate = 0;

        Calendar cal = new GregorianCalendar(year, month - 1, 1);

        do {
            int day = cal.get(Calendar.DAY_OF_WEEK);

            if (day != Calendar.SUNDAY) {
                ++numDate;
            }

            cal.add(Calendar.DAY_OF_YEAR, 1);
        } while (cal.get(Calendar.MONTH) == (month - 1));

        return numDate;
    }

    public static Date getLastDateInMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int lDay = DateUtil.arrDays[cal.get(Calendar.MONTH)];
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        if ((month == 1) && ((year % 4) == 0) && (((year % 100) != 0) || ((year % 400) == 0))) {
            lDay = 29;
        }
        return DateUtil.parse(
                new StringBuilder().append(lDay).append("/").append(month + 1).append("/").append(year).toString(),
                "d/M/yyyy");
    }


    /**
     * Now.
     *
     * @return the date
     */
    public static Date now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.DATE_FORMAT_NOW);
        try {
            return sdf.parse(sdf.format(cal.getTime()));
        } catch (ParseException e) {
            return new Date();
        }
    }

    public static int getAge(Date birthday) {
        Calendar cBirthDay = Calendar.getInstance();
        Calendar currentDate = Calendar.getInstance();
        cBirthDay.setTime(birthday);
        int age = currentDate.get(Calendar.YEAR) - cBirthDay.get(Calendar.YEAR);
        if (age < 0) {
            return 0;
        }
        if ((currentDate.get(Calendar.MONTH) > cBirthDay.get(Calendar.MONTH))
                || ((currentDate.get(Calendar.MONTH) == cBirthDay.get(Calendar.MONTH))
                && (currentDate.get(Calendar.DATE) > cBirthDay.get(Calendar.DATE)))) {
            age += 1;
        }
        return age;
    }

    /**
     * Now.
     *
     * @param formatDate
     *            the format date
     * @return the date
     */
    public static Date now(String formatDate) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(formatDate);
        try {
            return sdf.parse(sdf.format(cal.getTime()));
        } catch (ParseException e) {
            return new Date();
        }
    }

    /**
     * Format.
     *
     * @param d
     *            the d
     * @param format
     *            the format
     * @return the string
     */
    public static String format(Date d, String format) {
        if (d == null) {
            return " ";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(d);
    }

    /**
     * Format.
     *
     * @param d
     *            the d
     * @return the string
     */
    public static String format(Date d) {
        return DateUtil.format(d, DateUtil.DATE_FORMAT_NOW);
    }

    /**
     * Parses the.
     *
     * @param str
     *            the str
     * @param format
     *            the format
     * @return the date
     */
    public static Date parse(String str, String format) {
        DateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.parse(str);
        } catch (ParseException e) {
            return new Date();
        }
    }

    /**
     * Parses the.
     *
     * @param str
     *            the str
     * @return the date
     */
    public static Date parse(String str) {
        return DateUtil.parse(str, DateUtil.DATE_FORMAT_NOW);
    }

    /**
     * Gets the hour.
     *
     * @param date
     *            the date
     * @return the hour
     */
    public static int getHour(Date date) {
        String hour = null;
        DateFormat f = new SimpleDateFormat("HH");
        try {
            hour = f.format(date);
            return Integer.parseInt(hour);
        } catch (Exception e) {
            DateUtil.log.error(e.getMessage(), e);
            return -1;
        }
    }

    /**
     * Gets the minute.
     *
     * @param date
     *            the date
     * @return the minute
     */
    public static int getMinute(Date date) {
        String minute = null;
        DateFormat f = new SimpleDateFormat("mm");
        try {
            minute = f.format(date);
            return Integer.parseInt(minute);
        } catch (Exception e) {
            DateUtil.log.error(e.getMessage(), e);
            return -1;
        }
    }

    /**
     * Gets the aMPM.
     *
     * @param date
     *            the date
     * @return the aMPM
     */
    public static String getAMPM(Date date) {
        DateFormat f = new SimpleDateFormat("a");
        try {
            return f.format(date).toUpperCase();
        } catch (Exception e) {
            DateUtil.log.error(e.getMessage(), e);
            return "";
        }
    }

    /**
     * Gets the month.
     *
     * @param date
     *            the date
     * @return the month
     */
    public static int getMonth(Date date) {
        String month = null;
        DateFormat f = new SimpleDateFormat("MM");
        try {
            month = f.format(date);
            return Integer.parseInt(month);
        } catch (Exception e) {
            DateUtil.log.error(e.getMessage(), e);
            return -1;
        }
    }

    /**
     * Gets the year.
     *
     * @param date
     *            the date
     * @return the year
     */
    public static int getYear(Date date) {
        String year = null;
        DateFormat f = new SimpleDateFormat("yyyy");
        try {
            year = f.format(date);
            return Integer.parseInt(year);
        } catch (Exception e) {
            DateUtil.log.error(e.getMessage(), e);
            return -1;
        }
    }

    /**
     * Gets the day.
     *
     * @param date
     *            the date
     * @return the day
     */
    public static int getDay(Date date) {
        String day = null;
        DateFormat f = new SimpleDateFormat("dd");
        try {
            day = f.format(date);
            return Integer.parseInt(day);
        } catch (Exception e) {
            DateUtil.log.error(e.getMessage(), e);
            return -1;
        }
    }

    /**
     * Compare to.
     *
     * @param date1
     *            the date1
     * @param date2
     *            the date2
     * @return the int
     */
    public static int compareTo(Date date1, Date date2) {
        return DateUtil.compareTo(date1, date2, false);
    }

    /**
     * Compare to.
     *
     * @param date1
     *            the date1
     * @param date2
     *            the date2
     * @param ignoreMilliseconds
     *            the ignore milliseconds
     * @return the int
     */
    public static int compareTo(Date date1, Date date2, boolean ignoreMilliseconds) {

        if ((date1 != null) && (date2 == null)) {
            return -1;
        } else if ((date1 == null) && (date2 != null)) {
            return 1;
        } else if ((date1 == null)) {
            return 0;
        }

        long time1 = date1.getTime();
        long time2 = date2.getTime();

        if (ignoreMilliseconds) {
            time1 = time1 / DateUtil.SECOND;
            time2 = time2 / DateUtil.SECOND;
        }

        if (time1 == time2) {
            return 0;
        } else if (time1 < time2) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * Compare date with HOUR.
     *
     * @param date1
     *            the date1
     * @param date2
     *            the date2
     * @return -1 if date1 is before date2, 0 if date1 is equal with date2, 1 if
     *         date1 is after date2
     */
    public static int compare(Date date1, Date date2) {

        if ((date1 != null) && (date2 == null)) {
            return -1;
        } else if ((date1 == null) && (date2 != null)) {
            return 1;
        } else if ((date1 == null)) {
            return 0;
        }

        long time1 = date1.getTime() / DateUtil.HOUR;
        long time2 = date2.getTime() / DateUtil.HOUR;

        if (time1 == time2) {
            return 0;
        } else if (time1 < time2) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * Compare two date.
     *
     * @param date1
     *            the date1
     * @param date2
     *            the date2
     * @return the int
     */
    public static int compareTwoDate(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.HOUR_OF_DAY, 0);
        cal2.set(Calendar.SECOND, 0);
        cal2.set(Calendar.MILLISECOND, 0);
        if (cal1.equals(cal2)) {
            return 0;
        } else if (cal1.after(cal2)) {
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Equals.
     *
     * @param date1
     *            the date1
     * @param date2
     *            the date2
     * @return true, if successful
     */
    public static boolean equals(Date date1, Date date2) {
        if (DateUtil.compareTo(date1, date2) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Equals.
     *
     * @param date1
     *            the date1
     * @param date2
     *            the date2
     * @param ignoreMilliseconds
     *            the ignore milliseconds
     * @return true, if successful
     */
    public static boolean equals(Date date1, Date date2, boolean ignoreMilliseconds) {

        if (!ignoreMilliseconds) {
            return DateUtil.equals(date1, date2);
        }

        long time1 = 0;

        if (date1 != null) {
            time1 = date1.getTime() / DateUtil.SECOND;
        }

        long time2 = 0;

        if (date2 != null) {
            time2 = date2.getTime() / DateUtil.SECOND;
        }

        if (time1 == time2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets the minutes between.
     *
     * @param startDate
     *            the start date
     * @param endDate
     *            the end date
     * @return the minutes between
     */
    public static long getMinutesBetween(Date startDate, Date endDate) {

        TimeZone timeZone = TimeZone.getTimeZone("GMT");
        int offset = timeZone.getRawOffset();

        Calendar startCal = new GregorianCalendar(timeZone);
        startCal.setTime(startDate);
        startCal.add(Calendar.MILLISECOND, offset);

        Calendar endCal = new GregorianCalendar(timeZone);
        endCal.setTime(endDate);
        endCal.add(Calendar.MILLISECOND, offset);

        long milis1 = startCal.getTimeInMillis();
        long milis2 = endCal.getTimeInMillis();

        long diff = milis2 - milis1;
        long minutesBetween = diff / (60 * 1000);
        return minutesBetween;
    }

    /**
     * Gets the days between.
     *
     * @param startDate
     *            the start date
     * @param endDate
     *            the end date
     * @return the days between
     */
    public static int getDaysBetween(Date startDate, Date endDate) {

        TimeZone timeZone = TimeZone.getTimeZone("GMT");

        int offset = timeZone.getRawOffset();

        Calendar startCal = new GregorianCalendar(timeZone);

        startCal.setTime(startDate);
        startCal.add(Calendar.MILLISECOND, offset);

        Calendar endCal = new GregorianCalendar(timeZone);

        endCal.setTime(endDate);
        endCal.add(Calendar.MILLISECOND, offset);

        int daysBetween = 0;

        while (DateUtil.beforeByDay(startCal.getTime(), endCal.getTime())) {
            startCal.add(Calendar.DAY_OF_MONTH, 1);

            daysBetween++;
        }

        return daysBetween;
    }

    /**
     * Before by day.
     *
     * @param date1
     *            the date1
     * @param date2
     *            the date2
     * @return true, if successful
     */
    public static boolean beforeByDay(Date date1, Date date2) {
        long millis1 = DateUtil._getTimeInMillis(date1);
        long millis2 = DateUtil._getTimeInMillis(date2);

        if (millis1 < millis2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * _get time in millis.
     *
     * @param date
     *            the date
     * @return the long
     */
    private static long _getTimeInMillis(Date date) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);

        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DATE);
        int hour = 0;
        int minute = 0;
        int second = 0;

        cal.set(year, month, day, hour, minute, second);

        long millis = cal.getTimeInMillis() / DateUtil.DAY;

        return millis;
    }

    /**
     * @author LuanDV
     * @since 04/04/2011 - Created.
     * @param currentDate
     * @param lastCheckInTime
     * @return
     */
    public static boolean isNewWeek(Date currentDate, Date lastCheckInTime) {
        Date firstDayOfWeek = DateUtil.getFirstDayOfWeek(currentDate);
        if (DateUtil.compareTwoDate(lastCheckInTime, firstDayOfWeek) == -1) {
            return true;
        }
        return false;
    }

    public static Date getFirstDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dow = cal.get(Calendar.DAY_OF_WEEK);
        // System.out.println(dow);
        cal.add(Calendar.DAY_OF_YEAR, (dow * -1) + 2);
        return cal.getTime();
    }

    public static Date getFirstDayNextWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dow = cal.get(Calendar.DAY_OF_WEEK);
        // System.out.println(dow);
        cal.add(Calendar.DAY_OF_YEAR, (dow * -1) + 9);
        return cal.getTime();
    }

    public static Date getNextDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_WEEK, 1);
        Date nextDay = cal.getTime();
        return nextDay;
    }

    public static Date getFirstDateInMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        return DateUtil.parse(
                new StringBuilder().append("01").append("/").append(month + 1).append("/").append(year).toString(),
                "d/M/yyyy");
    }

    public static Date getPreviousDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_WEEK, -1);
        Date nextDay = cal.getTime();
        return nextDay;
    }

    public static Date getNextWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, 7);
        Date nextWeek = cal.getTime();
        return nextWeek;
    }

    public static Date getNextMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 1);
        Date nextMonth = cal.getTime();
        return nextMonth;
    }

    public static Date getFirstNextMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DATE, 1);
        Date nextMonth = cal.getTime();
        return nextMonth;
    }

    public static Date getNextYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, 1);
        Date nextYear = cal.getTime();
        return nextYear;
    }

    public static Date addDay(Date date, Integer numDay) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, numDay);
        Date nextTime = cal.getTime();
        return nextTime;
    }

    public static List<Date> getListFirstDayOfWeek(Date startDate, Date endDate) {
        Date dayOfWeek = DateUtil.getFirstDayOfWeek(startDate);
        List<Date> ListFirstDayOfWeekWeek = new ArrayList<>();
        Date nextWeek = dayOfWeek;
        do {
            ListFirstDayOfWeekWeek.add(nextWeek);
            nextWeek = DateUtil.getNextWeek(nextWeek);
        } while (DateUtil.compareTwoDate(nextWeek, endDate) < 0);

        return ListFirstDayOfWeekWeek;
    }

    public static String toDateString(Date date, String format) {
        String dateString = "";
        if (date == null) {
            return dateString;
        }
        Object[] params = new Object[] { date };

        try {
            dateString = MessageFormat.format("{0,date," + format + "}", params);
        } catch (Exception e) {
            DateUtil.log.error(e.getMessage(), e);
        }
        return dateString;
    }

    public static String convertFormatStrFromAtt(String value) {
        if (!StringUtils.isNullOrEmpty(value)) {
            Date dateTmp = DateUtil.parse(value, DateUtil.DATE_FORMAT_ATTRIBUTE);
            return DateUtil.toDateString(dateTmp, DateUtil.DATE_FORMAT_STR);
        }
        return "";
    }

    public static String convertFormatAttFromStr(String value) {
        if (!StringUtils.isNullOrEmpty(value)) {
            Date dateTmp = DateUtil.parse(value, DateUtil.DATE_FORMAT_STR);
            return DateUtil.toDateString(dateTmp, DateUtil.DATE_FORMAT_ATTRIBUTE);
        }
        return "";
    }

    public static String toMonthYearString(Date date) {
        String dateStr = "";
        try {
            dateStr = new SimpleDateFormat("MM/yyyy").format(date);
        } catch (Exception e) {
            DateUtil.log.error(e.getMessage(), e);
        }
        return dateStr;
    }

    /**
     * @author tungtt21
     * @param date
     * @return
     */
    public static String toDateSimpleFormatString(Date date) {
        String dateString = "";
        String format;
        if (date == null) {
            return dateString;
        }
        Object[] params = new Object[] { date };
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        format = DateUtil.DATE_FORMAT_STR;

        try {
            dateString = MessageFormat.format("{0,date," + format + "}", params);
        } catch (Exception e) {
            DateUtil.log.error(e.getMessage(), e);
        }
        return dateString;
    }

    /**
     * @author tungmt
     * @since 24/8/2015
     * @param date
     *            - ngay can them thoi gian
     * @return Date + time
     */
    public static Date addTime(Date date) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(DateUtil.now());

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        cal1.set(Calendar.DATE, cal.get(Calendar.DATE));
        cal1.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
        cal1.set(Calendar.DAY_OF_WEEK_IN_MONTH, cal.get(Calendar.DAY_OF_WEEK_IN_MONTH));
        cal1.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR));
        cal1.set(Calendar.MONTH, cal.get(Calendar.MONTH));
        cal1.set(Calendar.YEAR, cal.get(Calendar.YEAR));

        date = cal1.getTime();
        return date;
    }

    public static long dateDiffMilisecond(Date from, Date to) {
        Calendar cFrom = Calendar.getInstance();
        Calendar cTo = Calendar.getInstance();
        cFrom.setTime(from);
        cTo.setTime(to);
        long diff = Math.abs(cFrom.getTimeInMillis() - cTo.getTimeInMillis());
        return diff;
    }
}
