package com.tsolution._2repositories;

import com.tsolution._1entities.ParkingPoint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ParkingPointRepository extends JpaRepository<ParkingPoint, Long>, JpaSpecificationExecutor<ParkingPoint> {
    @Query(value = "select * from parking_point pp where pp.fleet_id = :fleetId and pp.status = 1",
            nativeQuery = true)
    List<ParkingPoint> findByFleetId(@Param("fleetId") Long id);
    @Query(value = "select pp.name from parking_point pp where pp.fleet_id = :fleetId and pp.status = 1",
            nativeQuery = true)
    List<String> findNameByFleetId(@Param("fleetId") Long id);
    @Query(value = "select * from parking_point pp where pp.id = :id and pp.status = 1",
            nativeQuery = true)
    Optional<ParkingPoint> findByActiveById(@Param("id") Long id);
}
