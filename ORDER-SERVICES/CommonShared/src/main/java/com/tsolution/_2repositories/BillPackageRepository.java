package com.tsolution._2repositories;

import com.tsolution._1entities.BillPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BillPackageRepository extends JpaRepository<BillPackage, Long>, JpaSpecificationExecutor<BillPackage> {

}