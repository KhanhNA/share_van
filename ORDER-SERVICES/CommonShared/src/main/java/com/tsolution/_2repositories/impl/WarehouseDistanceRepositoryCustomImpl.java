package com.tsolution._2repositories.impl;

import com.tsolution._1entities.WarehouseDistance;
import com.tsolution._2repositories.WarehouseDistanceRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class WarehouseDistanceRepositoryCustomImpl  implements WarehouseDistanceRepositoryCustom {

    @PersistenceContext
    private EntityManager em;
    @Override
    public List<WarehouseDistance> findListWarehouseDistanceByCustomerId(Long id) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sb.append(" FROM warehouse_distance wd  WHERE 1=1 ");
        sb.append(" AND wd.customer_id = :id AND wd.status = 1");
        params.put("id",id);
        StringBuilder strQuery = new StringBuilder(" SELECT wd.* ");
        strQuery.append(sb);
        return BaseRepository.getResultListNativeQuery(this.em,strQuery.toString(),params, WarehouseDistance.class);
    }
}
