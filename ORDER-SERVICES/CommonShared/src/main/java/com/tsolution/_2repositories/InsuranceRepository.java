package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.Insurance;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface InsuranceRepository extends JpaRepository<Insurance, Long>, JpaSpecificationExecutor<Insurance> {
    Optional<Insurance> findByInsuranceCode(String code);

    @Query(value = "SELECT COUNT(*) FROM Insurance i  WHERE i.insurance_code like LOWER(CONCAT(?1,'%'))",
            countQuery = "select count(*) from Insurance",
            nativeQuery = true)
    Long getOneResult(String param);

    @Query(value = "SELECT * FROM Insurance i  WHERE i.insurance_code like LOWER(CONCAT('%',?1,'%'))",
            countQuery = "select count from Insurance",
            nativeQuery = true)
    Optional<Insurance> getInsuranceByCodeByCode(String code);

    @Query(value = "SELECT * FROM Insurance i  WHERE i.status = ?1",
            countQuery = "select count(*) from Insurance",
            nativeQuery = true)
    List<Insurance> findAvaiable(Integer status);
}