package com.tsolution._2repositories.impl;

import com.tsolution._1entities.Staff;
import com.tsolution._2repositories.StaffRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;
import org.apache.poi.util.StringUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

@Repository
public class StaffRepositoryCustomImpl implements StaffRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Page<Staff> findStaff(Integer fleetId,String userName,String fullName,String objType,String email,String phoneNumber,Integer status,Pageable pageable) throws BusinessException {
        StringBuilder query = new StringBuilder();
        StringBuilder subQuery = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        query.append(" From staff s left join fleet f on f.id = s.fleet_id ");
        query.append(" where ");
        if(fleetId != null){
            query.append("(f.id = :fleet_id or f.parent_id =:fleet_id) ");
            params.put("fleet_id", fleetId);
        }
        if(!StringUtils.isNullOrEmpty(userName )){
            subQuery.append(" OR LOWER(s.user_name) LIKE LOWER(CONCAT('%',:user_name,'%'))");
            params.put("user_name", userName);
        }
        if(!StringUtils.isNullOrEmpty(fullName)) {
            subQuery.append(" OR LOWER(s.full_name) LIKE LOWER(CONCAT('%',:full_name,'%'))");
            params.put("full_name", fullName);
        }
        if(!StringUtils.isNullOrEmpty(objType)){
            subQuery.append(" OR s.object_type =:object_type");
            params.put("object_type", objType);
        }
        if(!StringUtils.isNullOrEmpty(email)) {
            subQuery.append(" OR LOWER(s.email) LIKE LOWER(CONCAT('%',:email,'%'))");
            params.put("email", email);
        }
        if(!StringUtils.isNullOrEmpty(phoneNumber)) {
            subQuery.append(" OR LOWER(s.phone) LIKE LOWER(CONCAT('%',:phone,'%'))");
            params.put("phone", phoneNumber);
        }
        if(status != null){
            subQuery.append(" OR s.status =:status");
            params.put("status", status);
        }
        if(subQuery != null && subQuery.length() > 3){
            query.append(" And ( ");
            query.append(subQuery.substring(3));
            query.append(" ) ");
        }
        StringBuilder queryGetCount = new StringBuilder();
        queryGetCount.append("Select count(s.id)");
        queryGetCount.append(query);

        StringBuilder queryGetList = new StringBuilder();
        queryGetList.append("Select s.*");
        queryGetList.append(query);

        return BaseRepository.getPagedNativeQuery(this.em, queryGetList.toString(), queryGetCount.toString(), params,
                pageable, Staff.class);
    }
}
