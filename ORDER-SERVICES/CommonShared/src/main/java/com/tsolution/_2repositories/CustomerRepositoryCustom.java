package com.tsolution._2repositories;

import com.tsolution._1entities.Customer;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CustomerRepositoryCustom {
    Page<Customer> find(Customer customer, Pageable pageable) throws BusinessException;
}
