package com.tsolution._2repositories;

import com.tsolution._1entities.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.ObjAttachment;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

import java.util.List;
import java.util.Optional;

public interface ObjAttachmentRepository
        extends JpaRepository<ObjAttachment, Long>, JpaSpecificationExecutor<ObjAttachment> {
    @Query(value = "Select * from obj_attachment where obj_id = ? and attach_type = ? and attach_name = ? and status = 1", countQuery = "select count(*) from obj_attachment", nativeQuery = true)
    Collection<ObjAttachment> findByObjId(long id, int attachType, String attachName);

    @Query(value = "Select * from obj_attachment where id = ? and status = 1", nativeQuery = true)
    Optional<ObjAttachment> findActiveById(long id);

    @Query("SELECT u FROM ObjAttachment u WHERE u.objId = ?1 and u.status = 1 and u.attachName =?2")
    List<ObjAttachment> findListObjAttachmentByObjId(Long id, String attachName);

    @Query("SELECT u FROM ObjAttachment u WHERE u.objId = ?1 and u.status = 1 and u.attachName =?2")
    Optional<ObjAttachment> findListObjAttachmentByAttachNameImage(Long id, String attachName);

    @Query("SELECT u FROM ObjAttachment u WHERE u.objId = ?1 and u.id =?2")
    Optional<ObjAttachment> findEmplyeeImageUpdate(Long idEmployee, Long idAttachment);
}