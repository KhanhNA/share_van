package com.tsolution._2repositories.impl;

import com.tsolution._1entities.DriverVan;
import com.tsolution._1entities.dto.VanDto;
import com.tsolution._2repositories.VanRepositoryCustom;
import com.tsolution.utils.CommonUtil;
import com.tsolution.utils.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
public class VanRepositoryCustomImpl implements VanRepositoryCustom {

    private static final Logger log = LogManager.getLogger(VanRepositoryCustomImpl.class);
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<DriverVan> setDriverForVanInDay(Long driver_id, Long van_id) {
        return null;
    }

    @Override
    public Page<VanDto> findByCondition(VanDto searchData, Pageable pageable) {
        StringBuilder fromClause = new StringBuilder();

        String select = " SELECT v.id id, v.licence_plate licencePlate, CONCAT(apv.param_name,' ', LOWER(v.color)) model, " +
                " v.year year, v.van_type_id vanTypeId, v.vehicle_tonnage vehicleTonnage, v.status_car statusCar, " +
                " date_format(v.inspection_due_date, '%d/%m/%Y') inspectionDueDate, pp.name parkingPointName ";

        fromClause.append(" FROM van v LEFT JOIN parking_point pp ON v.parking_point_id = pp.id ");
        fromClause.append(" LEFT JOIN apparam_value apv on v.app_param_value_id = apv.id ");
        fromClause.append(" LEFT JOIN app_param apr ON apv.param_id = apr.id ");
        fromClause.append(" WHERE v.fleet_id = :fleetId ");
        fromClause.append(appendSearchQuery(searchData));

        String query = select + fromClause;

        Query searchQuery = createMerchantNativeQuery(searchData, query + StringUtils.getOrderBy(pageable));
        Query countQuery = createMerchantNativeQuery(searchData, "SELECT count(1) " + fromClause.toString());
        searchQuery.setParameter("fleetId", searchData.getFleetId());
        countQuery.setParameter("fleetId", searchData.getFleetId());

        searchQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        searchQuery.setMaxResults(pageable.getPageSize());

        try {
            List<Object> rawResult = (List<Object>) searchQuery.getResultList();
            BigInteger total = (BigInteger) countQuery.getSingleResult();
            return new PageImpl<>(mapResult(rawResult), pageable, total.longValue());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    private List<VanDto> mapResult(List<Object> list) {
        List<VanDto> listResult = new ArrayList<>();
        for (Object obj : list) {
            Object[] temp = (Object[]) obj;
            VanDto res = new VanDto();
            res.setId(Long.parseLong(temp[0].toString()));
            res.setLicencePlate((String) temp[1]);
            res.setMultiCode((String) temp[2]);
            res.setYear(Integer.parseInt(temp[3].toString()));
            res.setVanTypeId((Integer)temp[4]);
            res.setVehicleTonnage(temp[5].toString());
            res.setStatusCar((Integer) temp[6]);
            res.setInspectionDueDate(temp[7].toString());
            res.setParkingPointName((String) temp[8]);
            listResult.add(res);
        }
        return listResult;
    }

    private Query createMerchantNativeQuery(VanDto searchData, String query) {
        Query nativeQuery = em.createNativeQuery(query);
        setParams(searchData, nativeQuery);
        return nativeQuery;
    }

    private void setParams(VanDto searchData, Query nativeQuery) {
        if (searchData.getVanTypeId() != null) {
            nativeQuery.setParameter("vanTypeId", searchData.getVanTypeId());
        }
        if (searchData.getFuelTypeId() != null) {
            nativeQuery.setParameter("fuelTypeId", searchData.getFuelTypeId());
        }
        if (searchData.getYear() != null && searchData.getYear() != 0) {
            nativeQuery.setParameter("year", searchData.getYear());
        }
        if (searchData.getTonnageFrom() != null) {
            nativeQuery.setParameter("tonnageFrom", searchData.getTonnageFrom());
        }
        if (searchData.getTonnageTo() != null && searchData.getTonnageTo() != 0) {
            nativeQuery.setParameter("tonnageTo", searchData.getTonnageTo());
        }
        if (searchData.getStatusCar() != null) {
            nativeQuery.setParameter("statusCar", searchData.getStatusCar());
        }
        if (searchData.getStatusAvailable() != null) {
            nativeQuery.setParameter("statusAvailable", searchData.getStatusAvailable());
        }
        if (searchData.getInspectionDueDate() != null && !CommonUtil.isBlank(searchData.getInspectionDueDate())) {
            nativeQuery.setParameter("inspectionDueDate", searchData.getInspectionDueDate());
        }
        if (searchData.getMultiCode() != null && !CommonUtil.isBlank(searchData.getMultiCode())) {
            nativeQuery.setParameter("multiCode", searchData.getMultiCode().trim());
        }
        if (searchData.getParkingPointId() != null && searchData.getParkingPointId() != null) {
            nativeQuery.setParameter("parkingPointId", searchData.getParkingPointId());
        }
    }

    private String appendSearchQuery(VanDto searchData) {
        String result = "";
        if (searchData.getVanTypeId() != null) {
            result += " AND v.van_type_Id = :vanTypeId ";
        }
        if (searchData.getFuelTypeId() != null) {
            result += " AND v.fuel_type_id = :fuelTypeId ";
        }
        if (searchData.getYear() != null && searchData.getYear() != 0) {
            result += " AND v.year = :year ";
        }
        if (searchData.getTonnageFrom() != null) {
            result += " AND v.vehicle_tonnage >= :tonnageFrom ";
        }
        if (searchData.getTonnageTo() != null && searchData.getTonnageTo() != 0) {
            result += " AND v.vehicle_tonnage <= :tonnageTo ";
        }
        if (searchData.getStatusCar() != null) {
            result += " AND v.status_car = :statusCar ";
        }
        if (searchData.getStatusAvailable() != null) {
            result += " AND v.status_available = :statusAvailable ";
        }
        if (searchData.getParkingPointId() != null && searchData.getParkingPointId() != 0) {
            result += " AND v.parking_point_id = :parkingPointId ";
        }
        if (searchData.getInspectionDueDate() != null && !CommonUtil.isBlank(searchData.getInspectionDueDate())) {
            result += " AND DATE_ADD(v.inspection_due_date,INTERVAL -1 DAY) < :inspectionDueDate ";
        }
        if (!CommonUtil.isBlank(searchData.getMultiCode())) {
            result += " AND ( LOWER(apv.param_name) LIKE LOWER(CONCAT('%', :multiCode ,'%')) ";
            result += " OR LOWER(v.licence_plate) LIKE LOWER(CONCAT('%', :multiCode ,'%')) ";
            result += " OR LOWER(v.color) LIKE LOWER(CONCAT('%', :multiCode ,'%')) ";
            result += " )";
        }
        return result;
    }

}
