package com.tsolution._2repositories;

import com.tsolution._1entities.RoutingPlanDay;
import com.tsolution.excetions.BusinessException;

import java.time.LocalDateTime;
import java.util.List;

public interface RoutingPlanDayRepositoryCustom {
    List<RoutingPlanDay> getRoutPlanForVanToday(Long id) throws BusinessException;
    List<RoutingPlanDay> getRoutPlanForVanByDate(Long vanId, LocalDateTime fDate, LocalDateTime tDate) throws BusinessException;
    List<RoutingPlanDay> getNextDestinationInRouting(Long routingId) throws BusinessException;


}
