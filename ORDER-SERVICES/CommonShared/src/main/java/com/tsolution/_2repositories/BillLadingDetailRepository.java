package com.tsolution._2repositories;

import com.tsolution._1entities.BillLadingDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BillLadingDetailRepository extends JpaRepository<BillLadingDetail, Long>, JpaSpecificationExecutor<BillLadingDetail> {

}