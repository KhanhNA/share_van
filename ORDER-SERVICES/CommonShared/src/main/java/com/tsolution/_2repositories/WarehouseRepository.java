package com.tsolution._2repositories;

import com.tsolution._1entities.Warehouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
//import com.tsolution._2repositories.sql.WarehouseSql;

public interface WarehouseRepository
		extends JpaRepository<Warehouse, Long>, JpaSpecificationExecutor<Warehouse>, WarehouseRepositoryCustom {
	List<Warehouse> findAllByStatus(Integer status);
}
