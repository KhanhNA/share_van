package com.tsolution._2repositories.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Translator;

public class BaseRepository {
	private static final String COMMON_REPOSITORY_CONVERT_RESULT_FAIL = "common.repository.convert.result.fail";
	@Autowired
	private static Translator translator;
	private static final Logger log = LogManager.getLogger(BaseRepository.class);

	private BaseRepository() {
	}

	public static <T> List<T> getResultListNativeQuery(EntityManager em, String strQuery, Map<String, Object> params,
			Class<T> clazz) throws BusinessException {
		Query nativeQuery = em.createNativeQuery(strQuery, clazz);
		params.forEach(nativeQuery::setParameter);
		final List<T> resultList = new ArrayList<>();
		for (final Object obj : nativeQuery.getResultList()) {
			if (clazz.isInstance(obj)) {
				resultList.add(clazz.cast(obj));
				continue;
			}
			throw new BusinessException(
					BaseRepository.translator.toLocale(BaseRepository.COMMON_REPOSITORY_CONVERT_RESULT_FAIL));
		}
		return resultList;
	}

	public static Long getLongResult(EntityManager em, String strQuery, Map<String, Object> params) {
		Query nativeCountQuery = em.createNativeQuery(strQuery);
		params.forEach(nativeCountQuery::setParameter);
		Long total = 0L;
		try {
			total = Long.valueOf(nativeCountQuery.getSingleResult().toString());
		} catch (NumberFormatException | NoResultException e) {
			BaseRepository.log.error(e.getMessage(), e);
		}
		return total;
	}

	public static <T> Page<T> getPagedNativeQuery(EntityManager em, String strQuery, String strCountQuery,
			Map<String, Object> params, Pageable pageable, Class<T> clazz) throws BusinessException {
		Query nativeQuery = em.createNativeQuery(strQuery, clazz);
		int firstResult = pageable.getPageNumber() * pageable.getPageSize();
		nativeQuery.setFirstResult(firstResult);
		nativeQuery.setMaxResults(pageable.getPageSize());
		params.forEach(nativeQuery::setParameter);
		final List<T> resultList = new ArrayList<>();
		for (final Object obj : nativeQuery.getResultList()) {
			if (clazz.isInstance(obj)) {
				resultList.add(clazz.cast(obj));
				continue;
			}
			throw new BusinessException(
					BaseRepository.translator.toLocale(BaseRepository.COMMON_REPOSITORY_CONVERT_RESULT_FAIL));
		}

		Long total = BaseRepository.getLongResult(em, strCountQuery, params);

		return new PageImpl<>(resultList, pageable, total);
	}
}
