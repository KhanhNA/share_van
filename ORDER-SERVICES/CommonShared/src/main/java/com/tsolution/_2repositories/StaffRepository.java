package com.tsolution._2repositories;

import com.tsolution._1entities.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface StaffRepository extends JpaRepository<Staff, Long>, JpaSpecificationExecutor<Staff>,StaffRepositoryCustom {
  //  Optional<Staff> findByStaffCode(String staffCode);
  //  List<Staff> getAllByStatus(Integer status);

  //  List<Staff> get(Integer status);
    Optional<Staff> findByUserName(String username);
  /*  @Query(value = "SELECT s.id FROM staff s where s.status = ?1 ", nativeQuery = true)
    List<Staff> getAllStaffIdByStatus(Integer status);*/

    @Query(value = "Select s.id from staff s where s.status =?1", nativeQuery = true)
    List<Integer> getAllStaffIdByStatus(Integer status);

    @Query(value = "Select d.id from driver_van d where d.staff_id =?1 and d.status =?2", nativeQuery = true)
    List<Integer> getStaffIdByStatusRunningInDrivervan(Long id, Integer status);

}