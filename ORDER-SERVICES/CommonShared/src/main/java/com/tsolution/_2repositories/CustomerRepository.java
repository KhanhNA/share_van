package com.tsolution._2repositories;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.tsolution._1entities.Customer;
import com.tsolution._2repositories.sql.CustomerSql;

public interface CustomerRepository extends JpaRepository<Customer, Long>, JpaSpecificationExecutor<Customer>, CustomerRepositoryCustom {
	Boolean existsCustomerByCode(String code);

	Optional<Customer> findByCode(String code);

	@Query(value = " SELECT DISTINCT c.* "
			+ CustomerSql.FIND_CUSTOMER_RENT_WAREHOUSE, countQuery = " SELECT COUNT(DISTINCT c.id) "
					+ CustomerSql.FIND_CUSTOMER_RENT_WAREHOUSE, nativeQuery = true)
	Page<Customer> findCustomerRentWarehouse(Long warehouseId, Pageable pageable);

	@Query(value = "SELECT COUNT(*) FROM Customer c  WHERE c.code like LOWER(CONCAT(?1,'%'))",
			countQuery = "select count(*) from Customer",
			nativeQuery = true)
	Long getOneResult(String param);

	@Query(value = "SELECT * FROM Customer c  WHERE c.code like LOWER(CONCAT('%',?1,'%'))",
			countQuery = "select count from Customer",
			nativeQuery = true)
	Optional<Customer> getCustomerByCodeByCode(String code);

}
