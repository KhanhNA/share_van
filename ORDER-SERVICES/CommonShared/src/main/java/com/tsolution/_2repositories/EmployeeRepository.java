package com.tsolution._2repositories;

import com.tsolution._1entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;


public interface EmployeeRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {
    Optional<Employee> findByCode(String code);

    Optional<Employee> findByIdAndObjectType(Long id, Integer objectType);

    @Query(value = "SELECT * FROM Employee e  WHERE e.id = ?1 ",
            countQuery = "select count from Employee",
            nativeQuery = true)
    Optional<Employee> findById(Long id);


    @Query(value = "SELECT COUNT(*) FROM Employee e  WHERE e.employee_code like LOWER(CONCAT(?1,'%'))",
            countQuery = "select count(*) from Employee",
            nativeQuery = true)
    Long getOneResult(String param);



    @Query(value = "SELECT * FROM Employee e  WHERE e.employee_code like LOWER(CONCAT('%',?1,'%'))",
            countQuery = "select count from Employee",
            nativeQuery = true)
    Optional<Employee> getEmployeeByCodeByCode(String code);
}

