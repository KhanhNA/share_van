package com.tsolution._2repositories.impl;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.tsolution._1entities.User;
import com.tsolution._2repositories.UserRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class UserRepositoryCustomImpl implements UserRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Page<User> find(User user, Pageable pageable) throws BusinessException {
		StringBuilder sb = new StringBuilder();
		sb.append(" FROM users u ");
		sb.append("	WHERE (:status is null OR u.status = :status) AND ");
		sb.append("		  (:username is null OR LOWER(u.username) like LOWER(CONCAT('%',:username,'%'))) AND ");
		sb.append("		  (:firstName is null OR LOWER(u.first_name) like LOWER(CONCAT('%',:firstName,'%'))) AND ");
		sb.append("		  (:lastName is null OR LOWER(u.last_name) like LOWER(CONCAT('%',:lastName,'%'))) AND ");
		sb.append("		  (:tel is null OR LOWER(u.tel) like LOWER(CONCAT('%',:tel,'%'))) AND ");
		sb.append("		  (:email is null OR LOWER(u.email) like LOWER(CONCAT('%',:email,'%'))) AND ");
		sb.append("		  (:type is null OR LOWER(u.type) like LOWER(CONCAT('%',:type,'%'))) AND ");
		sb.append("		  (:organizationId is null OR u.organization_id = :organizationId) ");
		sb.append(" ORDER BY u.username, u.first_name, u.last_name ");

		Map<String, Object> params = new HashMap<>();
		params.put("username", user.getUsername());
		params.put("firstName", user.getFirstName());
		params.put("lastName", user.getLastName());
		params.put("tel", user.getTel());
		params.put("email", user.getEmail());
		params.put("status", user.getStatus());
		params.put("type", user.getType() == null ? null : user.getType().getValue());
		params.put("organizationId", user.getOrganizationId());

		StringBuilder strQuery = new StringBuilder(" SELECT u.* ");
		strQuery.append(sb);

		StringBuilder strCountQuery = new StringBuilder(" SELECT count(u.username) ");
		strCountQuery.append(sb);

		return BaseRepository.getPagedNativeQuery(this.em, strQuery.toString(), strCountQuery.toString(), params,
				pageable, User.class);
	}
}
