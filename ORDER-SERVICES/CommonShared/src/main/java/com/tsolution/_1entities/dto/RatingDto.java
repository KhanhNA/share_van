package com.tsolution._1entities.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import java.util.ArrayList;
import java.util.List;

import java.io.Serializable;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class RatingDto implements Serializable {
    private static final long serialVersionUID = -66259723250072947L;
    private Long driverId;
    private String driverName;
    private String driverAddress;
    private String driverPhone;
    private List<Integer> evaluationCriteria;
    private String imageURL;
    private String ratingAverage;

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverAddress() {
        return driverAddress;
    }

    public void setDriverAddress(String driverAddress) {
        this.driverAddress = driverAddress;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public List<Integer> getEvaluationCriteria() {
        return evaluationCriteria;
    }

    public void setEvaluationCriteria(List<Integer> evaluationCriteria) {
        this.evaluationCriteria = evaluationCriteria;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getRatingAverage() {
        return ratingAverage;
    }

    public void setRatingAverage(String ratingAverage) {
        this.ratingAverage = ratingAverage;
    }

}
