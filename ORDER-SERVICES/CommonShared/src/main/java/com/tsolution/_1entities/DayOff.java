package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Table(name = "day_off")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class DayOff extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "staff_id",nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Staff staff;


    @Column(name = "from_date")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime fromDate;

    @Column(name = "to_date")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime toDate;

    @Column(name = "reason")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String reason;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "spec")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer spec;

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public LocalDateTime getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDateTime fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDateTime getToDate() {
        return toDate;
    }

    public void setToDate(LocalDateTime toDate) {
        this.toDate = toDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSpec() {
        return spec;
    }

    public void setSpec(Integer spec) {
        this.spec = spec;
    }

    public String toString() {
        return "DayOff{" +
                "staffId=" + staff.getId() +
                ",fromDate=" + fromDate +
                ", toDate=" + toDate +
                ", reason=" + reason +
                ", status=" + status +
                ", spec=" + spec +
                "}";
    }
}