package com.tsolution._1entities.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.*;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.Entity;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class WarehouseParcelDto implements Serializable {

    private static final long serialVersionUID = -6625972325007290087L;
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Warehouse warehouse;

    @JsonView(JsonEntityViewer.Human.Summary.class)
    private List<BillPackageRouting> packageRoutingList;

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public List<BillPackageRouting> getPackageRoutingList() {
        return packageRoutingList;
    }

    public void setPackageRoutingList(List<BillPackageRouting> packageRoutingList) {
        this.packageRoutingList = packageRoutingList;
    }
}
