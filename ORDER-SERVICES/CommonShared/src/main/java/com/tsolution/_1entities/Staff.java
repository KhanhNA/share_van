package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution._1entities.oauth2.RoleDto;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Table(name = "staff")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Staff extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "app_param_value_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long appParamValueId;

    @Column(name = "staff_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String staffCode;

    @Column(name = "full_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String fullName;

    @Column(name = "province")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String province;

    @Column(name = "country")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String country;

    @Column(name = "address")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String address;

    @Column(name = "phone")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String phone;

    @Column(name = "email")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String email;

    @Column(name = "hire_date")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime hireDate;

    @Column(name = "leave_date")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView({ JsonEntityViewer.Human.Summary.class })
    private LocalDateTime leaveDate;

    @Column(name = "birth_date")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView({ JsonEntityViewer.Human.Summary.class })
    private LocalDateTime birthDate;

    @Column(name = "labor_rate")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer laborRate;

    @Column(name = "billing_rate")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer billingRate;

    @Column(name = "SSN")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer SSN;

    @Column(name = "latitude")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double latitude;

    @Column(name = "longitude")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double longitude;

    @Column(name = "state_province")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String stateProvince;

    @Column(name = "imei")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String imei;

    @Column(name = "point")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer point;

    @Column(name = "id_no")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String idNo;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private DriverLicense driverLicence;

    @Column(name = "first_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String firstName;

    @Column(name = "last_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String lastName;

    @Column(name = "user_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String userName;

    @Column(name = "fleet_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long fleetId;

    @OneToOne
    @JoinColumn(name = "fleet_id", referencedColumnName = "id",insertable = false,updatable = false)
    private Fleet fleet;

    @OneToOne
    @JoinColumn(name = "app_param_value_id", referencedColumnName = "id",insertable = false,updatable = false)
    private ApparamValue apparamValue;

    @Column(name = "object_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String objectType;

    @Column(name = "gender")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer gender;

    @Transient
    private List<RoleDto> userRoles;

    @Transient
    private String password;

    @Transient
    private List<ObjAttachment> listAttachments;
    @Transient
    private List<ObjAttachment> listRemovedAttachments;
    @Transient
    private List<ObjAttachment> listImages;
    @Transient
    private List<ObjAttachment> listRemovedImages;
    @Transient
    private HashMap ratingMap;

    public ApparamValue getApparamValue() {
        return apparamValue;
    }

    public void setApparamValue(ApparamValue apparamValue) {
        this.apparamValue = apparamValue;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Long getFleetId() {
        return fleetId;
    }

    public void setFleetId(Long fleetId) {
        this.fleetId = fleetId;
    }

    public Fleet getFleet() {
        return fleet;
    }

    public void setFleet(Fleet fleet) {
        this.fleet = fleet;
    }

    public HashMap getRatingMap() {
        return ratingMap;
    }

    public void setRatingMap(HashMap ratingMap) {
        this.ratingMap = ratingMap;
    }

    public List<ObjAttachment> getListAttachments() {
        return listAttachments;
    }

    public void setListAttachments(List<ObjAttachment> listAttachments) {
        this.listAttachments = listAttachments;
    }

    public List<ObjAttachment> getListRemovedAttachments() {
        return listRemovedAttachments;
    }

    public void setListRemovedAttachments(List<ObjAttachment> listRemovedAttachments) {
        this.listRemovedAttachments = listRemovedAttachments;
    }

    public List<ObjAttachment> getListImages() {
        return listImages;
    }

    public void setListImages(List<ObjAttachment> listImages) {
        this.listImages = listImages;
    }

    public List<ObjAttachment> getListRemovedImages() {
        return listRemovedImages;
    }

    public void setListRemovedImages(List<ObjAttachment> listRemovedImages) {
        this.listRemovedImages = listRemovedImages;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public DriverLicense getDriverLicence() {
        return driverLicence;
    }

    public void setDriverLicence(DriverLicense driverLicence) {
        this.driverLicence = driverLicence;
    }

    public List<RoleDto> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<RoleDto> userRoles) {
        this.userRoles = userRoles;
    }

    public Long getAppParamValueId() {
        return appParamValueId;
    }

    public void setAppParamValueId(Long appParamValueId) {
        this.appParamValueId = appParamValueId;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getHireDate() {
        return hireDate;
    }

    public void setHireDate(LocalDateTime hireDate) {
        this.hireDate = hireDate;
    }

    public LocalDateTime getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(LocalDateTime leaveDate) {
        this.leaveDate = leaveDate;
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getLaborRate() {
        return laborRate;
    }

    public void setLaborRate(Integer laborRate) {
        this.laborRate = laborRate;
    }

    public Integer getBillingRate() {
        return billingRate;
    }

    public void setBillingRate(Integer billingRate) {
        this.billingRate = billingRate;
    }

    public Integer getSSN() {
        return SSN;
    }

    public void setSSN(Integer SSN) {
        this.SSN = SSN;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }
}
