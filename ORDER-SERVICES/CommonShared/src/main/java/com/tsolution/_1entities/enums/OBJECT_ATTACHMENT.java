package com.tsolution._1entities.enums;

public enum OBJECT_ATTACHMENT {
    CLAIMREPORT("CLAIMREPORT"),
    IMAGE("IMAGE"),
    STAFF("STAFF"),
    WAREHOUSE("WAREHOUSE"),
    EQUIPMENT("EQUIPMENT"),
    PARCEL("PARCEL"),
    CUSTOMER("CUSTOMER"),
    VAN("VAN"),
    EMPLOYEE("EMPLOYEE"),
    OBJ_ATTACHMENT_DRIVER_LICENSE("DRIVERLICENSE");

    private String value;

    OBJECT_ATTACHMENT(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
