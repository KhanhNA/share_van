package com.tsolution._1entities.enums;

public enum CustomerStatus {
    ACTIVE(true),
    DEACTIVE(false);

    private boolean value;

    CustomerStatus(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return this.value;
    }
}
