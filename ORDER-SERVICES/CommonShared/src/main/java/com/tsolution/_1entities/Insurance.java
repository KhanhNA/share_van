package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.*;

@Entity
@Table(name = "insurance")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Insurance extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "insurance_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String insuranceCode;

    @Column(name = "insurance_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String insuranceName;

    @OneToOne
    @JoinColumn(name = "vender_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Vendor vendor;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "from_date")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime fromDate;

    @Column(name = "to_date")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime toDate;

    @Column(name = "deductible")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double deductible;

    @Column(name = "note")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String note;

    public String getInsuranceCode() {
        return insuranceCode;
    }

    public void setInsuranceCode(String insuranceCode) {
        this.insuranceCode = insuranceCode;
    }

    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDateTime fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDateTime getToDate() {
        return toDate;
    }

    public void setToDate(LocalDateTime toDate) {
        this.toDate = toDate;
    }

    public Double getDeductible() {
        return deductible;
    }

    public void setDeductible(Double deductible) {
        this.deductible = deductible;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


}