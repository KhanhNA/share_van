package com.tsolution._1entities.enums;

public enum BillLadingStatus {
    NEW(0), APPROVED(1), REJECTED(2),CANCEL(4);
    private Integer value;

    BillLadingStatus(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}
