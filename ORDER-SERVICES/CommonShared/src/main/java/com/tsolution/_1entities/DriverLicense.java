package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Table(name = "driver_license")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class DriverLicense implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long id;

    @Column(name = "license_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String licenseCode;

    @Column(name = "license_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String licenseType;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "range_date")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView({ JsonEntityViewer.Human.Summary.class })
    private LocalDateTime rangeDate;

    @Column(name = "end_date")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView({ JsonEntityViewer.Human.Summary.class })
    private LocalDateTime endDate;

    @Column(name = "staff_id")
    @JsonView({ JsonEntityViewer.Human.Summary.class })
    private Long staffId;

    /*@OneToOne(mappedBy = "driverLicence")*/
    @OneToOne
    @JoinColumn(name = "staff_id", referencedColumnName = "id",insertable = false,updatable = false)
    private Staff user;

    @Transient
    private List<ObjAttachment> listAttachments;

    @Transient
    private List<ObjAttachment> listAttachmentRemove;

    public List<ObjAttachment> getListAttachmentRemove() {
        return listAttachmentRemove;
    }

    public void setListAttachmentRemove(List<ObjAttachment> listAttachmentRemove) {
        this.listAttachmentRemove = listAttachmentRemove;
    }

    public List<ObjAttachment> getListAttachments() {
        return listAttachments;
    }

    public void setListAttachments(List<ObjAttachment> listAttachments) {
        this.listAttachments = listAttachments;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Staff getUser() {
        return user;
    }

    public void setUser(Staff user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicenseCode() {
        return licenseCode;
    }

    public void setLicenseCode(String licenseCode) {
        this.licenseCode = licenseCode;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getRangeDate() {
        return rangeDate;
    }

    public void setRangeDate(LocalDateTime rangeDate) {
        this.rangeDate = rangeDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

}
