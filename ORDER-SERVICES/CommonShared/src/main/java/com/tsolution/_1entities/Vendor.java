package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tsolution._1entities.base.SuperEntity;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "vendor")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Vendor extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "vendor_name")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private String vendorName;

    @Column(name = "contact_name")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private String contactName;

    @Column(name = "address1")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private String address1;

    @Column(name = "address2")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private String address2;

    @Column(name = "city")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private String city;

    @Column(name = "state_province")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private String stateProvince;

    @Column(name = "postal_code")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private String postalCode;

    @Column(name = "phone1")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private String phone1;

    @Column(name = "phone2")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private String phone2;

    @Column(name = "fax")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private String fax;

    @Column(name = "website")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private String website;

    @Column(name = "payment_type")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private Integer paymentType;

    @Column(name = "pay_by_day")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private LocalDateTime payByDay;

    @Column(name = "discount")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private Double discount;

    @Column(name = "discount_type")
    @JsonIdentityInfo(generator = JSOGGenerator.class)
    private Integer discountType;

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public LocalDateTime getPayByDay() {
        return payByDay;
    }

    public void setPayByDay(LocalDateTime payByDay) {
        this.payByDay = payByDay;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Integer getDiscountType() {
        return discountType;
    }

    public void setDiscountType(Integer discountType) {
        this.discountType = discountType;
    }

    public String toString() {
      return "Vendor{vendorName=" + vendorName + 
        ", contactName=" + contactName + 
        ", address1=" + address1 + 
        ", address2=" + address2 + 
        ", city=" + city + 
        ", stateProvince=" + stateProvince + 
        ", postalCode=" + postalCode + 
        ", phone1=" + phone1 + 
        ", phone2=" + phone2 + 
        ", fax=" + fax + 
        ", website=" + website + 
        ", paymentType=" + paymentType + 
        ", payByDay=" + payByDay + 
        ", discount=" + discount + 
        ", discountType=" + discountType + 
        "}";
    }
}