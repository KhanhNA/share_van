package com.tsolution._1entities.enums;

public enum BillCycleType {

    MONTH(1),
    WEEK(2),
    SCHEDULE(3),
    DeliveryImmediately(4);
    private Integer value;

    public Integer getValue() {
        return value;
    }

    BillCycleType(Integer value) {
        this.value = value;
    }
}

