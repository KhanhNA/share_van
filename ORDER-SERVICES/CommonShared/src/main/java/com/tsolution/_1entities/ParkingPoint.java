package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeTimeHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalTime;

@Entity
@Table(name = "parking_point")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ParkingPoint extends SuperEntity {
    private static final long serialVersionUID = 4192052448526910279L;

    @Column(name = "fleet_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long fleetId;

    @Column(name = "name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String name;

    @Column(name = "phone_number")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String phoneNumber;

    @Column(name = "latitude")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Float latitude;

    @Column(name = "longitude")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Float longitude;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "address")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String address;

    @JsonFormat(pattern = Constants.TIME_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeTimeHandler.class)
    @Column(name = "day_ready_time", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalTime dayReadyTime;

    @JsonFormat(pattern = Constants.TIME_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeTimeHandler.class)
    @Column(name = "day_due_time", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalTime dayDueTime;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalTime getDayReadyTime() {
        return dayReadyTime;
    }

    public void setDayReadyTime(LocalTime dayReadyTime) {
        this.dayReadyTime = dayReadyTime;
    }

    public LocalTime getDayDueTime() {
        return dayDueTime;
    }

    public void setDayDueTime(LocalTime dayDueTime) {
        this.dayDueTime = dayDueTime;
    }

    public Long getFleetId() {
        return fleetId;
    }

    public void setFleetId(Long fleetId) {
        this.fleetId = fleetId;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
