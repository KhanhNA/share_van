package com.tsolution._1entities.enums;

public enum EXCEL_TEMPLATE_TYPE {
    VAN("van.xlsx"),
    STAFF("staff.xlsx"),
    EMPLOYEE("employee.xlsx"),
    WAREHOUSE("warehouse.xlsx");

    private String value;

    EXCEL_TEMPLATE_TYPE(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
