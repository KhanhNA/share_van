package com.tsolution._1entities.enums;

public enum NOTIFICATION_OBJECT_TYPE {
    DRIVER_MANAGER("DRIVER_MANAGER"),
    STOCK_KEEPER("STOCK_KEEPER"),
    DRIVER("DRIVER"),
    GROUP_STOCK_MANAGER("GROUP_STOCK_MANAGER"),
    ADMIN("SHAREVAN");

    private String value;

    NOTIFICATION_OBJECT_TYPE(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
