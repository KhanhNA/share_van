package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "bill_service")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class BillService extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "service_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String serviceCode;

    @Column(name = "service_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String serviceName;

    @Column(name = "service_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer serviceType;

    @Column(name = "price")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double price;

    @Column(name = "description")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String description;

    @Column(name = "phone")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String phone;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Integer getServiceType() {
        return serviceType;
    }

    public void setServiceType(Integer serviceType) {
        this.serviceType = serviceType;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String toString() {
      return "Service{serviceCode=" + serviceCode + 
        ", serviceName=" + serviceName + 
        ", serviceType=" + serviceType + 
        ", price=" + price + 
        ", description=" + description + 
        ", phone=" + phone + 
        ", status=" + status + 
        "}";
    }
}