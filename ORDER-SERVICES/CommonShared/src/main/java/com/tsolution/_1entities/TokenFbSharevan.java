package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "token_fb_sharevan")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class TokenFbSharevan extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "object_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long objectId;

    @Column(name = "object_type", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer objectType;

    @Column(name = "token", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String token;

    @Column(name = "imei")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String imei;

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Integer getObjectType() {
        return objectType;
    }

    public void setObjectType(Integer objectType) {
        this.objectType = objectType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    @Override
    public String toString() {
        return "TokenFbSharevan{" +
                "objectId=" + objectId +
                ", objectType=" + objectType +
                ", token='" + token + '\'' +
                ", imei='" + imei + '\'' +
                '}';
    }
}