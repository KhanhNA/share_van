package com.tsolution._1entities.enums;

public enum NOTIFICATION_OBJECT_TYPE_STATUS {
    GROUP_STOCK_MANAGER(0),

    STOCK_KEEPER(1),

    DRIVER_MANAGER(2),DRIVER(3), SHAREVAN_ADMIN(4);

    private Integer value;

    public Integer getValue() {
        return value;
    }

    NOTIFICATION_OBJECT_TYPE_STATUS(Integer value) {
        this.value = value;
    }

}
