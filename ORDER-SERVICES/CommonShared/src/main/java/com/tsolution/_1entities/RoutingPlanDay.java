package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.dto.WarehouseParcelDto;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "routing_plan_day")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class RoutingPlanDay extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "date_plan")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime datePlan;

    @Column(name = "routing_plan_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String routingPlanCode;

    @Column(name = "van_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long vanId;



    public String getRoutingPlanCode() {
        return routingPlanCode;
    }

    public void setRoutingPlanCode(String routingPlanCode) {
        this.routingPlanCode = routingPlanCode;
    }

    @Column(name = "routing_plan_day_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer type;


    @Column(name = "routing_van_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long routingVanId;

//    @Column(name = "billing_lading_detail_id")
//    @JsonView(JsonEntityViewer.Human.Summary.class)
//    private Long billingLadingDetailId;

    @Column(name = "order_number")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer order;

    @ManyToOne
    @JoinColumn(name = "warehouse_id", referencedColumnName = "id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Warehouse warehouse;

    @Column(name = "warehouse_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer warehouseType;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

//    @Column(name = "bill_lading_bill_type")
//    @JsonView(JsonEntityViewer.Human.Summary.class)
//    private Integer billLadingBillType;

    @Column(name = "capacity_expected")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer capacityExpected;

    @Column(name = "expected_from_time")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime expectedFromTime;

    @Column(name = "expected_to_time")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime expectedToTime;

    @Column(name = "actual_time")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime actualTime;


    @Transient
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private List<WarehouseParcelDto> warehouseParcelDtos;

    @Transient
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Rating rating;

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public List<WarehouseParcelDto> getWarehouseParcelDtos() {
        return warehouseParcelDtos;
    }

    public void setWarehouseParcelDtos(List<WarehouseParcelDto> warehouseParcelDtos) {
        this.warehouseParcelDtos = warehouseParcelDtos;
    }

    public LocalDateTime getDatePlan() {
        return datePlan;
    }

    public void setDatePlan(LocalDateTime datePlan) {
        this.datePlan = datePlan;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }


//    public Long getRoutingVanId() {
//        return routingVanId;
//    }
//
//    public void setRoutingVanId(Long routingVanId) {
//        this.routingVanId = routingVanId;
//    }

//    public Long getBillingLadingDetailId() {
//        return billingLadingDetailId;
//    }
//
//    public void setBillingLadingDetailId(Long billingLadingDetailId) {
//        this.billingLadingDetailId = billingLadingDetailId;
//    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

//    public Long getWarehouseId() {
//        return warehouseId;
//    }
//
//    public void setWarehouseId(Long warehouseId) {
//        this.warehouseId = warehouseId;
//    }

    public Integer getWarehouseType() {
        return warehouseType;
    }

    public void setWarehouseType(Integer warehouseType) {
        this.warehouseType = warehouseType;
    }

//    public Integer getStatusVanMotion() {
//        return statusVanMotion;
//    }
//
//    public void setStatusVanMotion(Integer statusVanMotion) {
//        this.statusVanMotion = statusVanMotion;
//    }

//    public Integer getBillLadingBillType() {
//        return billLadingBillType;
//    }
//
//    public void setBillLadingBillType(Integer billLadingBillType) {
//        this.billLadingBillType = billLadingBillType;
//    }

    public Integer getCapacityExpected() {
        return capacityExpected;
    }

    public void setCapacityExpected(Integer capacityExpected) {
        this.capacityExpected = capacityExpected;
    }

    public LocalDateTime getExpectedFromTime() {
        return expectedFromTime;
    }

    public void setExpectedFromTime(LocalDateTime expectedFromTime) {
        this.expectedFromTime = expectedFromTime;
    }

    public LocalDateTime getExpectedToTime() {
        return expectedToTime;
    }

    public void setExpectedToTime(LocalDateTime expectedToTime) {
        this.expectedToTime = expectedToTime;
    }

    public LocalDateTime getActualTime() {
        return actualTime;
    }

    public void setActualTime(LocalDateTime actualTime) {
        this.actualTime = actualTime;
    }

    public Long getVanId() {
        return vanId;
    }

    public void setVanId(Long vanId) {
        this.vanId = vanId;
    }

    public Long getRoutingVanId() {
        return routingVanId;
    }

    public void setRoutingVanId(Long routingVanId) {
        this.routingVanId = routingVanId;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String toString() {
      return "RoutingPlanDay{datePlan=" + datePlan + 
        ", type=" + type + 
      //  ", routingVanId=" + routingVanId +
      //  ", billingLadingDetailId=" + billingLadingDetailId +
        ", order=" + order + 
     //   ", warehouseId=" + warehouseId +
        ", warehouseType=" + warehouseType + 
     //   ", statusVanMotion=" + statusVanMotion +
       // ", billLadingBillType=" + billLadingBillType +
        ", capacityExpected=" + capacityExpected + 
        ", expectedFromTime=" + expectedFromTime + 
        ", expectedToTime=" + expectedToTime + 
        ", actualTime=" + actualTime + 
        "}";
    }
}