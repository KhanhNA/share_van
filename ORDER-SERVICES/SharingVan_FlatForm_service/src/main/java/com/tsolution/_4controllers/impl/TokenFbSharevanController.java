package com.tsolution._4controllers.impl;


import com.tsolution._1entities.TokenFbSharevan;
import com.tsolution._3services.TokenFbSharevanService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping("/token_firebase_sharevan")
public class TokenFbSharevanController extends BaseController {
    
    @Autowired
    private TokenFbSharevanService tokenFbSharevanService;
    
    @GetMapping("/all")
    @ApiOperation(value = "Finds all token sharevan",
            notes = "Multiple status values can be provided with comma seperated strings",
            response = TokenFbSharevan.class,
            responseContainer = "List")
    public ResponseEntity<Object> findAll() {
        return this.tokenFbSharevanService.findAll();
    }

    @GetMapping("/{id}")
//    @PreAuthorize("hasAuthority('get/customers/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.tokenFbSharevanService.findById(id);
    }

    @PostMapping
    @ApiOperation(value = "save new device token and IMEI for driver or employee",
            notes = "EMPLOYEE : 0; STORE_KEEPER : 1; FLEET_MANAGER : 2; DRIVER: 3; SHARING_VAN_ADMIN: 4 ",
            response = TokenFbSharevan.class,
            responseContainer = "Object")
    public ResponseEntity<Object> create(
            @RequestHeader("Authorization") String authorization,
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody TokenFbSharevan tokenFbSharevan)
            throws BusinessException, ParseException {
        return this.tokenFbSharevanService.create(acceptLanguage, tokenFbSharevan);
    }

    @PatchMapping("/{id}")
//    @PreAuthorize("hasAuthority('patch/claim_reports/{id}')")
    public ResponseEntity<Object> update(
            @RequestHeader("Authorization") String authorization,
            @RequestHeader("Accept-Language") String acceptLanguage,
            @PathVariable("id") Long id,
            @RequestBody TokenFbSharevan tokenFbSharevan
    ) throws BusinessException, ParseException {
        return this.tokenFbSharevanService.update(acceptLanguage, id, tokenFbSharevan);
    }


}
