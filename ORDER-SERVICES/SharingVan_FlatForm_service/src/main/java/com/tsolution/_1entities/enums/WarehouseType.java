package com.tsolution._1entities.enums;

public enum WarehouseType {



    UP(1),      /* Warehouse to pick up goods on the truck */
    DOWN(2);    /* Warehouse receives goods from trucks */
    private Integer value;

    public Integer getValue() {
        return value;
    }

    WarehouseType(Integer value) {
        this.value = value;
    }
}
