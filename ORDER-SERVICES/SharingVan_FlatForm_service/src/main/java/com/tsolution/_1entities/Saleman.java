package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "saleman")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Saleman extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "saleman_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String salemanCode;

    @Column(name = "saleman_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String salemanName;

    @Column(name = "address")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String address;

    @Column(name = "phone")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String phone;

    @Column(name = "username")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String username;

    @Column(name = "password")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String password;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    public String getSalemanCode() {
        return salemanCode;
    }

    public void setSalemanCode(String salemanCode) {
        this.salemanCode = salemanCode;
    }

    public String getSalemanName() {
        return salemanName;
    }

    public void setSalemanName(String salemanName) {
        this.salemanName = salemanName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String toString() {
      return "Saleman{salemanCode=" + salemanCode + 
        ", salemanName=" + salemanName + 
        ", address=" + address + 
        ", phone=" + phone + 
        ", username=" + username + 
        ", password=" + password + 
        ", status=" + status + 
        "}";
    }
}