package com.ns.tsutils;


import lombok.Getter;
import lombok.Setter;
import retrofit2.Retrofit;

import static com.ns.tsutils.TsUtils.checkNotNull;

@Getter
@Setter
public class BaseSOService<T> {
    private Retrofit mRetrofit;
    private T service;
    public BaseSOService(Retrofit retrofit, T s){
        mRetrofit = retrofit;
        service = s;
    }
    public T getApi(final Class<T> service) {
        checkNotNull(service);
        checkNotNull(mRetrofit);
        return mRetrofit.create(service);
    }
    public void createApi(final Class<T> s) {
        checkNotNull(service);
        checkNotNull(mRetrofit);
        service = mRetrofit.create(s);
    }
}
