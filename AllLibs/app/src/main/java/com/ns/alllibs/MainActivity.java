package com.ns.alllibs;

import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;

import com.nextsolution.graphql.api.AWSClientBase;
import com.ns.alllibs.ui.main.MainViewModel;
import com.ns.walletlib.DialogInputWalletPassword;
import com.ns.walletlib.MainWalletFragment;
import com.ns.walletlib.Wallet;
import com.ns.walletlib.WalletFragment;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new WalletFragment(45L, "0978100914"))
                    .commitNow();

        }
        AWSClientBase.requestApolloLogin(getApplicationContext(),
                "http://dev.nextsolutions.com.vn:5002",null,
                "0978100914", "abc@123", this::loginSuccessNew);

    }

    private void loginSuccessNew(com.apollographql.apollo.api.Response response, Throwable throwable, Object... objects) {
        System.out.println("okkkkkkkkkkkk");
    }

    @Override
    public int getLayoutRes() {
        return R.layout.main_activity;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MainViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }



}
