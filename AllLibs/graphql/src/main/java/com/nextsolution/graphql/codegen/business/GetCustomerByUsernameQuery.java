package com.nextsolution.graphql.codegen.business;

import com.apollographql.apollo.api.Query;
import java.lang.String;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.dto.Customer;
import java.util.Collections;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import java.lang.Object;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;
import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import java.io.IOException;

@Generated("Apollo GraphQL")
public final class GetCustomerByUsernameQuery implements Query<GetCustomerByUsernameQuery.Data, GetCustomerByUsernameQuery.Data, GetCustomerByUsernameQuery.Variables> {
  public static String OPERATION_DEFINITION = "query GetCustomerByUsername($username: String!) {   getCustomerByUsername(username: $username) {     ...Customer   } }";
  public static String QUERY_DOCUMENT = OPERATION_DEFINITION;
  public static OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "GetCustomerByUsername";
    }
  };
  private GetCustomerByUsernameQuery.Variables variables;
  @Override
   public String queryDocument() {
    return QUERY_DOCUMENT;
  }
  
  @Override
   public GetCustomerByUsernameQuery.Data wrapData(GetCustomerByUsernameQuery.Data data) {
    return data;
  }
  
  @Override
   public GetCustomerByUsernameQuery.Variables variables() {
    return variables;
  }
  
  @Override
   public ResponseFieldMapper<GetCustomerByUsernameQuery.Data> responseFieldMapper() {
    return new Data.Mapper();
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Override
   public OperationName name() {
    return OPERATION_NAME;
  }
  
  public String operationId() {
    return "06414a47d440111a840521d223efed8d";
  }
  
  public GetCustomerByUsernameQuery(@Nonnull String username) {
    Utils.checkNotNull(username, "username == null");
    this.variables = new GetCustomerByUsernameQuery.Variables(username);
  }
  @lombok.Getter
  @lombok.Setter
  public static class Data implements Operation.Data {
    private @Nullable Customer getCustomerByUsername;
    private volatile String $toString;
    private volatile int $hashCode;
    private volatile boolean $hashCodeMemoized;
    static ResponseField[] $responseFields = {
        ResponseField.forObject("getCustomerByUsername", "getCustomerByUsername", new UnmodifiableMapBuilder<String, Object>(1).put("username", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "username").build()).build(), true, Collections.<ResponseField.Condition>emptyList())
      };
    public Data(@Nullable Customer getCustomerByUsername) {
      this.getCustomerByUsername = getCustomerByUsername;
    }
    
    @Override
     public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "getCustomerByUsername=" + getCustomerByUsername + ", "
          + "}";
      }
      
      return $toString;
    }
    
    public Data() {
      
    }
    
    @Override
     public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.getCustomerByUsername == null) ? (that.getCustomerByUsername == null) : this.getCustomerByUsername.equals(that.getCustomerByUsername));
      }
      
      return false;
    }
    
    @Override
     public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (getCustomerByUsername == null) ? 0 : getCustomerByUsername.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      
      return $hashCode;
    }
    
    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeObject($responseFields[0], getCustomerByUsername != null ? getCustomerByUsername.marshaller() : null);
        }
      };
    }
    public static final class Mapper implements ResponseFieldMapper<Data> {
      private Customer.Mapper getCustomerByUsernameFieldMapper = new Customer.Mapper();
      @Override
       public Data map(ResponseReader reader) {
        final Customer getCustomerByUsername = reader.readObject($responseFields[0], new ResponseReader.ObjectReader<Customer>() {
                  @Override
                  public Customer read(ResponseReader reader) {
                    return getCustomerByUsernameFieldMapper.map(reader);
                  }
                });
        return new Data(getCustomerByUsername);
      }
    }
    
  }
  

  public static final class Builder {
    private @Nullable String username;
    Builder() {
      
    }
    
    public Builder username(@Nullable String username) {
      this.username = username;
      return this;
    }
    
    public GetCustomerByUsernameQuery build() {
      return new GetCustomerByUsernameQuery(username);
    }
  }
  

  public static final class Variables extends Operation.Variables {
    private @Nonnull String username;
    private transient Map<String, Object> valueMap = new LinkedHashMap<>();
    public String username() {
      return username;
    }
    
    public Variables(@Nonnull String username) {
      this.username = username;
      this.valueMap.put("username", username);
    }
    
    @Override
     public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }
    
    @Override
     public InputFieldMarshaller marshaller() {
      return new InputFieldMarshaller() {
        @Override
        public void marshal(InputFieldWriter writer) throws IOException {
          writer.writeString("username", username);
        }
      };
    }
  }
  
}

