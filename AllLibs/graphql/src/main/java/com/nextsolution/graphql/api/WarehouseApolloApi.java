package com.nextsolution.graphql.api;


import com.nextsolution.graphql.codegen.business.GetCustWarehouseQuery;

public class WarehouseApolloApi {
    public static void getCustWarehouse(Integer custId, ResponseApollo result, Object... params) {
        GetCustWarehouseQuery query = GetCustWarehouseQuery.builder().custId(custId).build();
        AWSClientBase.apolloQuery(query, result, params);
    }

}
