package com.nextsolution.graphql.codegen.business;

import com.apollographql.apollo.api.Query;
import java.lang.String;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.internal.Utils;
import java.lang.Integer;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.dto.RoutingVanDay;
import java.util.Collections;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import java.lang.Object;
import java.util.List;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;
import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import java.io.IOException;

@Generated("Apollo GraphQL")
public final class GetRoutingVanDayQuery implements Query<GetRoutingVanDayQuery.Data, GetRoutingVanDayQuery.Data, GetRoutingVanDayQuery.Variables> {
  public static String OPERATION_DEFINITION = "query GetRoutingVanDay($vanId: Int!, $date: String!) {   getRoutingVanDay(vanId: $vanId, date: $date) {     ...RoutingVanDay   } }";
  public static String QUERY_DOCUMENT = OPERATION_DEFINITION;
  public static OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "GetRoutingVanDay";
    }
  };
  private GetRoutingVanDayQuery.Variables variables;
  @Override
   public String queryDocument() {
    return QUERY_DOCUMENT;
  }
  
  @Override
   public GetRoutingVanDayQuery.Data wrapData(GetRoutingVanDayQuery.Data data) {
    return data;
  }
  
  @Override
   public GetRoutingVanDayQuery.Variables variables() {
    return variables;
  }
  
  @Override
   public ResponseFieldMapper<GetRoutingVanDayQuery.Data> responseFieldMapper() {
    return new Data.Mapper();
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Override
   public OperationName name() {
    return OPERATION_NAME;
  }
  
  public String operationId() {
    return "5d3341c2df641899117cff5ed0b1ac07";
  }
  
  public GetRoutingVanDayQuery(@Nonnull Integer vanId, @Nonnull String date) {
    Utils.checkNotNull(vanId, "vanId == null");
    Utils.checkNotNull(date, "date == null");
    this.variables = new GetRoutingVanDayQuery.Variables(vanId, date);
  }
  @lombok.Getter
  @lombok.Setter
  public static class Data implements Operation.Data {
    private @Nullable List<RoutingVanDay> getRoutingVanDay;
    private volatile String $toString;
    private volatile int $hashCode;
    private volatile boolean $hashCodeMemoized;
    static ResponseField[] $responseFields = {
        ResponseField.forList("getRoutingVanDay", "getRoutingVanDay", new UnmodifiableMapBuilder<String, Object>(2).put("vanId", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "vanId").build()).put("date", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "date").build()).build(), true, Collections.<ResponseField.Condition>emptyList())
      };
    public Data(@Nullable List<RoutingVanDay> getRoutingVanDay) {
      this.getRoutingVanDay = getRoutingVanDay;
    }
    
    @Override
     public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "getRoutingVanDay=" + getRoutingVanDay + ", "
          + "}";
      }
      
      return $toString;
    }
    
    public Data() {
      
    }
    
    @Override
     public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.getRoutingVanDay == null) ? (that.getRoutingVanDay == null) : this.getRoutingVanDay.equals(that.getRoutingVanDay));
      }
      
      return false;
    }
    
    @Override
     public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (getRoutingVanDay == null) ? 0 : getRoutingVanDay.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      
      return $hashCode;
    }
    
    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeList($responseFields[0], getRoutingVanDay, new ResponseWriter.ListWriter() {
            @Override
            public void write(Object value, ResponseWriter.ListItemWriter listItemWriter) {
              listItemWriter.writeObject(((RoutingVanDay) value).marshaller());
            }
          });
        }
      };
    }
    public static final class Mapper implements ResponseFieldMapper<Data> {
      private RoutingVanDay.Mapper getRoutingVanDayFieldMapper = new RoutingVanDay.Mapper();
      @Override
       public Data map(ResponseReader reader) {
        final List<RoutingVanDay> getRoutingVanDay = reader.readList($responseFields[0], new ResponseReader.ListReader<RoutingVanDay>() {
          @Override
          public RoutingVanDay read(ResponseReader.ListItemReader listItemReader) {
            return listItemReader.readObject(new ResponseReader.ObjectReader<RoutingVanDay>() {
                      @Override
                      public RoutingVanDay read(ResponseReader reader) {
                        return getRoutingVanDayFieldMapper.map(reader);
                      }
                    });
          }
        });
        return new Data(getRoutingVanDay);
      }
    }
    
  }
  

  public static final class Builder {
    private @Nullable Integer vanId;
    private @Nullable String date;
    Builder() {
      
    }
    
    public Builder vanId(@Nullable Integer vanId) {
      this.vanId = vanId;
      return this;
    }
    
    public Builder date(@Nullable String date) {
      this.date = date;
      return this;
    }
    
    public GetRoutingVanDayQuery build() {
      return new GetRoutingVanDayQuery(vanId, date);
    }
  }
  

  public static final class Variables extends Operation.Variables {
    private @Nonnull Integer vanId;
    private @Nonnull String date;
    private transient Map<String, Object> valueMap = new LinkedHashMap<>();
    public Integer vanId() {
      return vanId;
    }
    
    public String date() {
      return date;
    }
    
    public Variables(@Nonnull Integer vanId, @Nonnull String date) {
      this.vanId = vanId;
      this.valueMap.put("vanId", vanId);
      this.date = date;
      this.valueMap.put("date", date);
    }
    
    @Override
     public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }
    
    @Override
     public InputFieldMarshaller marshaller() {
      return new InputFieldMarshaller() {
        @Override
        public void marshal(InputFieldWriter writer) throws IOException {
          writer.writeInt("vanId", vanId);
          writer.writeString("date", date);
        }
      };
    }
  }
  
}

