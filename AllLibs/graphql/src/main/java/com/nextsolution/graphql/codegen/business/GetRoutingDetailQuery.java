package com.nextsolution.graphql.codegen.business;

import com.apollographql.apollo.api.Query;
import java.lang.String;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.internal.Utils;
import java.lang.Integer;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.dto.BillPackageRoutingPlan;
import java.util.Collections;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import java.lang.Object;
import java.util.List;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;
import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import java.io.IOException;

@Generated("Apollo GraphQL")
public final class GetRoutingDetailQuery implements Query<GetRoutingDetailQuery.Data, GetRoutingDetailQuery.Data, GetRoutingDetailQuery.Variables> {
  public static String OPERATION_DEFINITION = "query GetRoutingDetail($id: Int!) {   getBillPackageRoutingPlan(routingPlanDayId: $id) {     ...BillPackageRoutingPlan   } }";
  public static String QUERY_DOCUMENT = OPERATION_DEFINITION;
  public static OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "GetRoutingDetail";
    }
  };
  private GetRoutingDetailQuery.Variables variables;
  @Override
   public String queryDocument() {
    return QUERY_DOCUMENT;
  }
  
  @Override
   public GetRoutingDetailQuery.Data wrapData(GetRoutingDetailQuery.Data data) {
    return data;
  }
  
  @Override
   public GetRoutingDetailQuery.Variables variables() {
    return variables;
  }
  
  @Override
   public ResponseFieldMapper<GetRoutingDetailQuery.Data> responseFieldMapper() {
    return new Data.Mapper();
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Override
   public OperationName name() {
    return OPERATION_NAME;
  }
  
  public String operationId() {
    return "50fce0d5603a386d52cf251220687805";
  }
  
  public GetRoutingDetailQuery(@Nonnull Integer id) {
    Utils.checkNotNull(id, "id == null");
    this.variables = new GetRoutingDetailQuery.Variables(id);
  }
  @lombok.Getter
  @lombok.Setter
  public static class Data implements Operation.Data {
    private @Nullable List<BillPackageRoutingPlan> getBillPackageRoutingPlan;
    private volatile String $toString;
    private volatile int $hashCode;
    private volatile boolean $hashCodeMemoized;
    static ResponseField[] $responseFields = {
        ResponseField.forList("getBillPackageRoutingPlan", "getBillPackageRoutingPlan", new UnmodifiableMapBuilder<String, Object>(1).put("routingPlanDayId", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "id").build()).build(), true, Collections.<ResponseField.Condition>emptyList())
      };
    public Data(@Nullable List<BillPackageRoutingPlan> getBillPackageRoutingPlan) {
      this.getBillPackageRoutingPlan = getBillPackageRoutingPlan;
    }
    
    @Override
     public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "getBillPackageRoutingPlan=" + getBillPackageRoutingPlan + ", "
          + "}";
      }
      
      return $toString;
    }
    
    public Data() {
      
    }
    
    @Override
     public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.getBillPackageRoutingPlan == null) ? (that.getBillPackageRoutingPlan == null) : this.getBillPackageRoutingPlan.equals(that.getBillPackageRoutingPlan));
      }
      
      return false;
    }
    
    @Override
     public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (getBillPackageRoutingPlan == null) ? 0 : getBillPackageRoutingPlan.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      
      return $hashCode;
    }
    
    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeList($responseFields[0], getBillPackageRoutingPlan, new ResponseWriter.ListWriter() {
            @Override
            public void write(Object value, ResponseWriter.ListItemWriter listItemWriter) {
              listItemWriter.writeObject(((BillPackageRoutingPlan) value).marshaller());
            }
          });
        }
      };
    }
    public static final class Mapper implements ResponseFieldMapper<Data> {
      private BillPackageRoutingPlan.Mapper getBillPackageRoutingPlanFieldMapper = new BillPackageRoutingPlan.Mapper();
      @Override
       public Data map(ResponseReader reader) {
        final List<BillPackageRoutingPlan> getBillPackageRoutingPlan = reader.readList($responseFields[0], new ResponseReader.ListReader<BillPackageRoutingPlan>() {
          @Override
          public BillPackageRoutingPlan read(ResponseReader.ListItemReader listItemReader) {
            return listItemReader.readObject(new ResponseReader.ObjectReader<BillPackageRoutingPlan>() {
                      @Override
                      public BillPackageRoutingPlan read(ResponseReader reader) {
                        return getBillPackageRoutingPlanFieldMapper.map(reader);
                      }
                    });
          }
        });
        return new Data(getBillPackageRoutingPlan);
      }
    }
    
  }
  

  public static final class Builder {
    private @Nullable Integer id;
    Builder() {
      
    }
    
    public Builder id(@Nullable Integer id) {
      this.id = id;
      return this;
    }
    
    public GetRoutingDetailQuery build() {
      return new GetRoutingDetailQuery(id);
    }
  }
  

  public static final class Variables extends Operation.Variables {
    private @Nonnull Integer id;
    private transient Map<String, Object> valueMap = new LinkedHashMap<>();
    public Integer id() {
      return id;
    }
    
    public Variables(@Nonnull Integer id) {
      this.id = id;
      this.valueMap.put("id", id);
    }
    
    @Override
     public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }
    
    @Override
     public InputFieldMarshaller marshaller() {
      return new InputFieldMarshaller() {
        @Override
        public void marshal(InputFieldWriter writer) throws IOException {
          writer.writeInt("id", id);
        }
      };
    }
  }
  
}

