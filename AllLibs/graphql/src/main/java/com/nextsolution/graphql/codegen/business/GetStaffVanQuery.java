package com.nextsolution.graphql.codegen.business;

import com.apollographql.apollo.api.Query;
import java.lang.String;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.dto.StaffVan;
import java.util.Collections;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import java.lang.Object;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;
import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import java.io.IOException;

@Generated("Apollo GraphQL")
public final class GetStaffVanQuery implements Query<GetStaffVanQuery.Data, GetStaffVanQuery.Data, GetStaffVanQuery.Variables> {
  public static String OPERATION_DEFINITION = "query GetStaffVan($userName: String!, $date: String!) {   getStaffVan(userName: $userName, date: $date) {     ...StaffVan   } }";
  public static String QUERY_DOCUMENT = OPERATION_DEFINITION;
  public static OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "GetStaffVan";
    }
  };
  private GetStaffVanQuery.Variables variables;
  @Override
   public String queryDocument() {
    return QUERY_DOCUMENT;
  }
  
  @Override
   public GetStaffVanQuery.Data wrapData(GetStaffVanQuery.Data data) {
    return data;
  }
  
  @Override
   public GetStaffVanQuery.Variables variables() {
    return variables;
  }
  
  @Override
   public ResponseFieldMapper<GetStaffVanQuery.Data> responseFieldMapper() {
    return new Data.Mapper();
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Override
   public OperationName name() {
    return OPERATION_NAME;
  }
  
  public String operationId() {
    return "5cfda4975ad5c4fc5f8ceb3af4f22882";
  }
  
  public GetStaffVanQuery(@Nonnull String userName, @Nonnull String date) {
    Utils.checkNotNull(userName, "userName == null");
    Utils.checkNotNull(date, "date == null");
    this.variables = new GetStaffVanQuery.Variables(userName, date);
  }
  @lombok.Getter
  @lombok.Setter
  public static class Data implements Operation.Data {
    private @Nullable StaffVan getStaffVan;
    private volatile String $toString;
    private volatile int $hashCode;
    private volatile boolean $hashCodeMemoized;
    static ResponseField[] $responseFields = {
        ResponseField.forObject("getStaffVan", "getStaffVan", new UnmodifiableMapBuilder<String, Object>(2).put("userName", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "userName").build()).put("date", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "date").build()).build(), true, Collections.<ResponseField.Condition>emptyList())
      };
    public Data(@Nullable StaffVan getStaffVan) {
      this.getStaffVan = getStaffVan;
    }
    
    @Override
     public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "getStaffVan=" + getStaffVan + ", "
          + "}";
      }
      
      return $toString;
    }
    
    public Data() {
      
    }
    
    @Override
     public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.getStaffVan == null) ? (that.getStaffVan == null) : this.getStaffVan.equals(that.getStaffVan));
      }
      
      return false;
    }
    
    @Override
     public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (getStaffVan == null) ? 0 : getStaffVan.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      
      return $hashCode;
    }
    
    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeObject($responseFields[0], getStaffVan != null ? getStaffVan.marshaller() : null);
        }
      };
    }
    public static final class Mapper implements ResponseFieldMapper<Data> {
      private StaffVan.Mapper getStaffVanFieldMapper = new StaffVan.Mapper();
      @Override
       public Data map(ResponseReader reader) {
        final StaffVan getStaffVan = reader.readObject($responseFields[0], new ResponseReader.ObjectReader<StaffVan>() {
                  @Override
                  public StaffVan read(ResponseReader reader) {
                    return getStaffVanFieldMapper.map(reader);
                  }
                });
        return new Data(getStaffVan);
      }
    }
    
  }
  

  public static final class Builder {
    private @Nullable String userName;
    private @Nullable String date;
    Builder() {
      
    }
    
    public Builder userName(@Nullable String userName) {
      this.userName = userName;
      return this;
    }
    
    public Builder date(@Nullable String date) {
      this.date = date;
      return this;
    }
    
    public GetStaffVanQuery build() {
      return new GetStaffVanQuery(userName, date);
    }
  }
  

  public static final class Variables extends Operation.Variables {
    private @Nonnull String userName;
    private @Nonnull String date;
    private transient Map<String, Object> valueMap = new LinkedHashMap<>();
    public String userName() {
      return userName;
    }
    
    public String date() {
      return date;
    }
    
    public Variables(@Nonnull String userName, @Nonnull String date) {
      this.userName = userName;
      this.valueMap.put("userName", userName);
      this.date = date;
      this.valueMap.put("date", date);
    }
    
    @Override
     public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }
    
    @Override
     public InputFieldMarshaller marshaller() {
      return new InputFieldMarshaller() {
        @Override
        public void marshal(InputFieldWriter writer) throws IOException {
          writer.writeString("userName", userName);
          writer.writeString("date", date);
        }
      };
    }
  }
  
}

