package com.nextsolution.graphql.codegen.dto;

import java.util.Arrays;
import com.apollographql.apollo.api.GraphqlFragment;
import java.util.List;
import java.lang.String;
import java.util.Collections;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.dto.Fleet;
import com.nextsolution.graphql.codegen.dto.Staff;
import com.nextsolution.graphql.codegen.dto.Van;
import javax.annotation.Nonnull;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;

@lombok.Getter
@lombok.Setter
public class StaffVan implements GraphqlFragment {
  private @Nonnull String __typename;
  private @Nullable Fleet fleet;
  private @Nullable Staff staff;
  private @Nullable Van van;
  private volatile String $toString;
  private volatile int $hashCode;
  private volatile boolean $hashCodeMemoized;
  static ResponseField[] $responseFields = {
      ResponseField.forString("__typename", "__typename", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forObject("fleet", "fleet", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forObject("staff", "staff", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forObject("van", "van", null, true, Collections.<ResponseField.Condition>emptyList())
    };
  public static String FRAGMENT_DEFINITION = "fragment StaffVan on StaffVanDto {   fleet {     ...Fleet   }   staff {     ...Staff   }   van {     ...Van   } }";
  public static List<String> POSSIBLE_TYPES = Collections.unmodifiableList(Arrays.asList("StaffVanDto"));
  public StaffVan(@Nonnull String __typename, @Nullable Fleet fleet, @Nullable Staff staff, @Nullable Van van) {
    this.__typename = Utils.checkNotNull(__typename, "__typename == null");
    this.fleet = fleet;
    this.staff = staff;
    this.van = van;
  }
  
  @Override
   public String toString() {
    if ($toString == null) {
      $toString = "StaffVan{"
        + "__typename=" + __typename + ", "
        + "fleet=" + fleet + ", "
        + "staff=" + staff + ", "
        + "van=" + van + ", "
        + "}";
    }
    
    return $toString;
  }
  
  public StaffVan() {
    
  }
  
  @Override
   public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof StaffVan) {
      StaffVan that = (StaffVan) o;
      return this.__typename.equals(that.__typename) && ((this.fleet == null) ? (that.fleet == null) : this.fleet.equals(that.fleet)) && ((this.staff == null) ? (that.staff == null) : this.staff.equals(that.staff)) && ((this.van == null) ? (that.van == null) : this.van.equals(that.van));
    }
    
    return false;
  }
  
  @Override
   public int hashCode() {
    if (!$hashCodeMemoized) {
      int h = 1;
      h *= 1000003;
      h ^= __typename.hashCode();
      h *= 1000003;
      h ^= (fleet == null) ? 0 : fleet.hashCode();
      h *= 1000003;
      h ^= (staff == null) ? 0 : staff.hashCode();
      h *= 1000003;
      h ^= (van == null) ? 0 : van.hashCode();
      $hashCode = h;
      $hashCodeMemoized = true;
    }
    
    return $hashCode;
  }
  
  public ResponseFieldMarshaller marshaller() {
    return new ResponseFieldMarshaller() {
      @Override
      public void marshal(ResponseWriter writer) {
        writer.writeString($responseFields[0], __typename);
        writer.writeObject($responseFields[1], fleet != null ? fleet.marshaller() : null);
        writer.writeObject($responseFields[2], staff != null ? staff.marshaller() : null);
        writer.writeObject($responseFields[3], van != null ? van.marshaller() : null);
      }
    };
  }
  public static final class Mapper implements ResponseFieldMapper<StaffVan> {
    private Fleet.Mapper fleetFieldMapper = new Fleet.Mapper();
    private Staff.Mapper staffFieldMapper = new Staff.Mapper();
    private Van.Mapper vanFieldMapper = new Van.Mapper();
    @Override
     public StaffVan map(ResponseReader reader) {
      final String __typename = reader.readString($responseFields[0]);
      final Fleet fleet = reader.readObject($responseFields[1], new ResponseReader.ObjectReader<Fleet>() {
                @Override
                public Fleet read(ResponseReader reader) {
                  return fleetFieldMapper.map(reader);
                }
              });
      final Staff staff = reader.readObject($responseFields[2], new ResponseReader.ObjectReader<Staff>() {
                @Override
                public Staff read(ResponseReader reader) {
                  return staffFieldMapper.map(reader);
                }
              });
      final Van van = reader.readObject($responseFields[3], new ResponseReader.ObjectReader<Van>() {
                @Override
                public Van read(ResponseReader reader) {
                  return vanFieldMapper.map(reader);
                }
              });
      return new StaffVan(__typename, fleet, staff, van);
    }
  }
  
}

