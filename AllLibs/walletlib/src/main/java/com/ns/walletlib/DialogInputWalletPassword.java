package com.ns.walletlib;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.ns.walletlib.databinding.DialogInputPasswordBinding;
import com.tsolution.base.BaseActivity;

import com.tsolution.base.BaseFragmentInterface;
import com.tsolution.base.BaseViewModel;

public class DialogInputWalletPassword extends BottomSheetDialogFragment implements BaseFragmentInterface {
    private LoginVM loginVM;
    private WalletVM walletVM;
    private CallBack callBack;
    DialogInputPasswordBinding binding;
    public DialogInputWalletPassword(WalletVM vm, CallBack dismissCallBack) {
        this.callBack = dismissCallBack;
        this.walletVM = vm;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        super.onCreateView(inflater, container, savedInstanceState);
//        DialogInputPasswordBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_input_password, container, false);
//        walletVM = ViewModelProviders.of(this).get(WalletVM.class);
//        binding.setViewModel(walletVM);

        View view = createView(inflater, container, savedInstanceState);
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        passwordChangedEvent();

//        walletVM.getAccountPayment().observe(getViewLifecycleOwner(), accountPayment-> {
//            if(accountPayment != null){
//                String token = String.format("Basic %s", Base64.encodeToString(
//                                String.format("%s:%s", walletVM.merchantCode, binding.txtPassword.getText()).getBytes(), Base64.DEFAULT));
//                callBack.callBack(accountPayment, token);
//                dismiss();
//            }else {
//                binding.lbPassword.setError(getString(R.string.invalid_password));
//            }
//        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        binding.root.setMinHeight(height / 2);

        return view;
    }
    //
    private void passwordChangedEvent(){
        binding.txtPassword.requestFocus();
        binding.txtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() == 6){
//                    walletVM.getWalletAmount(getContext(), walletVM.merchantCode, charSequence.toString());

                    login(charSequence);
                }else {
                    binding.lbPassword.setError(null);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
    private void login(CharSequence password){
//        HttpHelper.requestLogin("0978100914", "123113", getContext(), this:: loginSuccess);

        loginVM.getWallet(DialogInputWalletPassword.this.getContext(), walletVM, password.toString(), this::loginSuccess);
    }

    private void loginSuccess(Object... objects) {

        System.out.println("login status:");
        AccountPayment accountPayment = (AccountPayment)objects[2];
        if(accountPayment != null){
            accountPayment.setClientAccount(walletVM.getAccountPayment().get().getClientAccount());
            accountPayment.setClientId(walletVM.getAccountPayment().get().getClientId());
            WalletVM.isLogin = true;
        }else {
            WalletVM.isLogin = false;
        }
        walletVM.setAccountPaymentValue(accountPayment);
        this.dismiss();
    }

    //override method
    @Override
    public void setBinding(ViewDataBinding binding) {
        this.binding = (DialogInputPasswordBinding)binding;
    }

    @Override
    public void setViewModel(BaseViewModel viewModel) {
        loginVM = (LoginVM) viewModel;
    }

    @Override
    public void setRecycleView(RecyclerView recycleView) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return (BaseActivity)getActivity();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_input_password;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        callBack.callBack(walletVM, null);
    }

    public interface CallBack{
        void callBack(WalletVM walletVM, String token);
    }
}