
package com.ns.walletlib;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.ns.tsutils.TsUtils;
import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Wallet extends TsBaseModel {

    @SerializedName("accountId")
    @Expose
    private Long accountId;

    @SerializedName("clientId")
    @Expose
    private Integer clientId;

    @SerializedName("savingsProductId")
    @Expose
    private Integer savingsProductId;

    @SerializedName("savingsProductName")
    @Expose
    private String savingsProductName;

    @SerializedName("accountNo")
    @Expose
    private String accountNo;

    @SerializedName("currencyCode")
    @Expose
    private String currencyCode;

    @SerializedName("currencyName")
    @Expose
    private String currencyName;

    @SerializedName("accountBalance")
    @Expose
    private Double accountBalance;

    @SerializedName("availableBalance")
    @Expose
    private Double availableBalance;

    public String getAccountBalanceStr(){
        return TsUtils.formatCurrency(accountBalance);
    }

}
