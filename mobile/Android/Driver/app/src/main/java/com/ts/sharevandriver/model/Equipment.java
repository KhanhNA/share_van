package com.ts.sharevandriver.model;


import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.google.android.material.textfield.TextInputLayout;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.utils.StringUtils;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Equipment extends BaseModel {
    private Integer id;
    private Integer assignation_log_id;
    private String equipment_part_code;
    private String name;
    private String description;
    private String unit_measure;
    private String uri_path;
    private Integer quantity_take;//số lượng nhận
    private Integer quantity_return;//số lượng trả

    //properties for ui
    private boolean isselect;

    public Integer getQuantity_return(){
        if(quantity_return == null){
            return 0;
        }
        return quantity_return;
    }
    public Integer getQuantity_take(){
        if(quantity_take == null){
            return 0;
        }
        return quantity_take;
    }

    public void setQuantity_take_str(String str) {
        if(str.length() == 0){
            str = "0";
        }
        this.quantity_take = Integer.parseInt(str);
    }

    public String getQuantity_take_str() {
        return this.quantity_take != null ? quantity_take + "" : "0";
    }

    public void setQuantity_return_str(String str) {
        if(str.length() == 0){
            str = "0";
        }
        this.quantity_return = Integer.parseInt(str);
    }

    public String getQuantity_return_str() {
        return this.quantity_return != null ? quantity_return + "" : "0";
    }

}
