package com.ts.sharevandriver.adapter.provider;

import android.view.View;

import com.chad.library.BR;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.provider.BaseNodeProvider;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public class SecondRoutingNodeProvider extends BaseNodeProvider {
    private final BaseViewModel baseViewModel;
    private final int idItemLayout;

    @Override
    public int getItemViewType() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return idItemLayout;
    }

    public SecondRoutingNodeProvider(int idItemLayout, BaseViewModel baseViewModel) {
        this.baseViewModel = baseViewModel;
        this.idItemLayout = idItemLayout;
    }

    @Override
    public void convert(@NonNull BaseViewHolder holder, @Nullable BaseNode data) {
        if (data == null) {
            return;
        }
        ViewDataBinding binding = DataBindingUtil.bind(holder.itemView);
        if (binding != null) {
            binding.setVariable(BR.viewHolder, data);
            if (baseViewModel != null) {
                binding.setVariable(BR.viewModel, baseViewModel);
            }
            binding.executePendingBindings();
        }
    }

    @Override
    public void onClick(@NonNull BaseViewHolder helper, @NonNull View view, BaseNode data, int position) {
    }
}
