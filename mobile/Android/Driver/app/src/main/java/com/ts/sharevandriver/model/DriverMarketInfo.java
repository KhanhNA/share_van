package com.ts.sharevandriver.model;

import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.utils.StringUtils;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverMarketInfo extends BaseModel {
    private Long driver_id;
    private Area from_area;
    private Area to_area;
    private String from_address;
    private String to_address;
    private Double available_capacity;
    private Double available_weight;
    private String from_date_assign;
    private String to_date_assign;

    public String getAvailable_capacity_str() {
        return this.available_capacity != null ? AppController.getInstance().formatNumber(available_capacity) : "";
    }

    public String getAvailable_weight_str() {
        return this.available_weight != null ? AppController.getInstance().formatNumber(available_weight) : "";
    }

    public void setAvailable_weight_str(String weightStr) {
        if (!StringUtils.isNullOrEmpty(weightStr)) {
            if (weightStr.charAt(0) == '.') {
                weightStr = weightStr.replace(".", "0.");
            }
            weightStr = weightStr.replace(",", "");
            this.available_weight = (Double.parseDouble(weightStr));
        } else {
            this.available_weight = (0d);
        }
    }

    public void setAvailable_capacity_str(String capacity_str) {
        if (!StringUtils.isNullOrEmpty(capacity_str)) {
            if (capacity_str.charAt(0) == '.') {
                capacity_str = capacity_str.replace(".", "0.");
            }
            capacity_str = capacity_str.replace(",", "");
            this.available_capacity = (Double.parseDouble(capacity_str));
        } else {
            this.available_capacity = (0d);
        }
    }
}
