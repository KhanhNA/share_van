package com.ts.sharevandriver.model;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.widget.TextView;

import com.haibin.calendarview.Calendar;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.enums.DayOffStatus;
import com.ts.sharevandriver.enums.DayOffType;
import com.tsolution.base.BaseModel;

import java.text.SimpleDateFormat;

import androidx.databinding.BindingAdapter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DayOff extends BaseModel {
    private Long id;
    private Long group_request_id;
    private Integer driver_id;

    private OdooDate request_day;
    private String date_value;
    private String reason;
    private String type;
    private String status;
    private Integer approver_id;
    private String approver_name;
    private String phone;


    private transient DayOff otherPart;//nếu 1 ngày chia thành 2 lần nghỉ. Thì biến này sẽ lưu lần thứ 2
    private transient Calendar calendar;

    public DayOff() {
        this.type = DayOffType.WHOLE_DAY;
    }

    public DayOff(String date, String type) {
        this.date_value = date;
        String[] value = date_value.split("-");
        Calendar calendar = new Calendar();
        int color = 0xFFF6AF36;//màu pending

        calendar.setYear(Integer.parseInt(value[0]));
        calendar.setMonth(Integer.parseInt(value[1]));
        calendar.setDay(Integer.parseInt(value[2]));
        calendar.setSchemeColor(color);
        calendar.setScheme(DayOffType.WHOLE_DAY);

        this.calendar = calendar;
        this.request_day = new OdooDate(calendar.getTimeInMillis());
        this.type = type;
    }

    public void setCalendarValue(Calendar calendar) {
        this.calendar = calendar;
        this.date_value = calendar.getYear() + "-" + calendar.getMonth() + "-" + calendar.getDay();
        this.request_day = new OdooDate(calendar.getTimeInMillis());
    }


    public String getCalendarStr() {
        if (calendar == null)
            return "";
        return calendar.getDay() + " - " + calendar.getMonth() + " - " + calendar.getYear();
    }

    /**
     * format trường request_day thành đinh dạng giống như calendar.toString()
     * để tiện lấy đối tượng DayOff trong mapDayOff
     * Key: calendar.toString();
     * value: dayOff
     *
     * @return key của map
     */
    @SuppressLint("SimpleDateFormat")
    public String getDateFormatted() {
        if (request_day == null)
            return "";
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
        return formatDate.format(request_day);
    }

    @SuppressLint("SetTextI18n")
    @BindingAdapter({"dayOffStartDate", "dayOffEndDate"})
    public static void typeStr(TextView view, DayOff start, DayOff end) {
        if (start == null || end == null) return;
        SpannableStringBuilder spanBuilder = new SpannableStringBuilder();

        SpannableString spanStart;
        SpannableString spanEnd = null;
        Resources rs = view.getResources();
        if (start.getCalendar().differ(end.getCalendar()) == 0) {
            String txtDate;
            if (start.getType().equals(DayOffType.WHOLE_DAY)) {
                txtDate = rs.getString(R.string.whole_day);
            } else if (start.getType().equals(DayOffType.MORNING)) {
                txtDate = rs.getString(R.string.morning);
            } else {
                txtDate = rs.getString(R.string.afternoon);
            }
            spanStart = new SpannableString(txtDate + " " + start.getCalendarStr());
            spanStart.setSpan(new StyleSpan(Typeface.BOLD), 0, txtDate.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

//            view.setText(lbStart + " " + start.getCalendarStr());

        } else {
            String lbStart = rs.getString(R.string.from);
            String txtStart;
            if(start.getType().equals(DayOffType.WHOLE_DAY)){
                txtStart = " ";
            }
            else if (start.getType().equals(DayOffType.MORNING)) {
                txtStart = " " + rs.getString(R.string.morning);
            } else {
                txtStart = " " + rs.getString(R.string.afternoon);
            }

            String lbEnd = rs.getString(R.string.to);
            String txtEnd;
            if (end.getType().equals(DayOffType.WHOLE_DAY)) {
                txtEnd = " " + rs.getString(R.string.whole_day_end);
            } else if (end.getType().equals(DayOffType.MORNING)) {
                txtEnd = " " + rs.getString(R.string.morning);
            } else {
                txtEnd = " " + rs.getString(R.string.afternoon);
            }
            spanStart = new SpannableString(lbStart + txtStart + " " + start.getCalendarStr());
            spanStart.setSpan(new StyleSpan(Typeface.BOLD), lbStart.length(), lbStart.length() + txtStart.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            spanEnd = new SpannableString(lbEnd + txtEnd + " " + end.getCalendarStr());
            spanEnd.setSpan(new StyleSpan(Typeface.BOLD), lbEnd.length(), lbEnd.length() + txtEnd.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

//            view.setText(lbStart  + " " + start.getCalendarStr() + " " + lbEnd + " " + end.getCalendarStr());
        }
        if (spanEnd != null) {
            spanBuilder.append(spanStart).append(" ").append(spanEnd);
        } else {
            spanBuilder.append(spanStart);
        }
        view.setText(spanBuilder);

    }

    @BindingAdapter({"dayOffStatus","isCancel"})
    public static void statusStr(TextView view, String status,Boolean isCancel) {
        if (status == null) return;
        Resources rs = view.getResources();
        if (status.equals(DayOffStatus.ACCEPTED)) {
            view.setTextColor(rs.getColor(R.color.primaryColor));
            view.setText(rs.getString(R.string.accepted));
        } else if (status.equals(DayOffStatus.DENIED)) {
            view.setTextColor(rs.getColor(R.color.color_price));
            view.setText(rs.getString(R.string.denied));
        } else if (status.equals(DayOffStatus.WAITING)) {
            if(isCancel){
                view.setTextColor(rs.getColor(R.color.color_pending));
                view.setText(rs.getString(R.string.waiting_confirm));
            }else{
                view.setTextColor(rs.getColor(R.color.color_price));
                view.setText(rs.getString(R.string.Not_approved));
            }
        } else {
            view.setTextColor(rs.getColor(R.color.color_price));
            view.setText(rs.getString(R.string.canceled_by_you));
        }
    }
}
