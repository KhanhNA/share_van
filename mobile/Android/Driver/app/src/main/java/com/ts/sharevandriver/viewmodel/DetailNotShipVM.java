package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.api.BiddingApi;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.enums.EnumBidding;
import com.ts.sharevandriver.model.BiddingInformation;
import com.ts.sharevandriver.model.BiddingVehicle;
import com.ts.sharevandriver.model.Cargo;
import com.ts.sharevandriver.model.CargoType;
import com.ts.sharevandriver.model.ConfirmBiddingResponse;
import com.tsolution.base.BaseViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetailNotShipVM extends BaseViewModel<BiddingInformation> {
    private ObservableBoolean isLoading = new ObservableBoolean();
    private String confirmBiddingStatus;
    private HashMap<String, Cargo> mapCargo;
    private ObservableField<HashMap<String, Integer>> cargo_ids = new ObservableField<>();
    private ObservableBoolean isEdit = new ObservableBoolean();// biến thể hiện có đang trong trạng thái edit không
    private boolean isEdited;//biến thể hiện đã thay đổi chưa.

    private List<CargoType> cargoTypeList;
    private List<CargoType> cargoTypeListClone;// lưu lại cargoType trong trường hợp discard edit thì rollBack lại.

    public DetailNotShipVM(@NonNull Application application) {
        super(application);
        model.set(new BiddingInformation());
        mapCargo = new HashMap<>();
        cargo_ids.set(new HashMap<>());
        cargoTypeList = new ArrayList<>();
        cargoTypeListClone = new ArrayList<>();
    }

    public void getOrderDetailList(int biddingId, RunUi runUi) {
        isLoading.set(true);
        BiddingApi.getOrderDetail(biddingId, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if (o != null) {
                    model.set((BiddingInformation) o);
                    setMapCargoQrCode((BiddingInformation) o);
                    runUi.run("getBiddingOrderDetailSuccess");
                }
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
            }
        });

    }

    private void setMapCargoQrCode(BiddingInformation biddingInformation) {
        mapCargo = new HashMap<>();
        //danh sách cargotype của bidding
        List<CargoType> cargoTypes = biddingInformation.getCargo_type();
        if (cargoTypes != null && cargoTypes.size() > 0) {
            for (CargoType cargoType : cargoTypes) {
                if (cargoType.getCargos() != null) {
                    for (Cargo cargo : cargoType.getCargos()) {
                        mapCargo.put(cargo.getQrCode(), cargo);
                    }
                }
            }
        }
        //danh sách cargotype thực nhận
        List<CargoType> vehicleCargoType = biddingInformation.getBidding_vehicles().getCargo_types();
        if (cargoTypes != null && vehicleCargoType.size() > 0) {
            for (CargoType cargoType : vehicleCargoType) {
                if (cargoType.getCargos() != null) {
                    cargoType.setSelectedCargo(new HashMap<>());
                    for (Cargo cargo : cargoType.getCargos()) {
                        cargoType.getSelectedCargo().put(cargo.getQrCode(), cargo.getId());
                        cargo_ids.get().put(cargo.getQrCode(), cargo.getId());
                    }
                }
            }
            cargo_ids.notifyChange();
        }
    }

    public boolean isValidReceiveGoods() {
        return false;
    }

    public int getStatusOrder() {
        BiddingVehicle biddingVehicles = getModelE().getBidding_vehicles();
        if ("2".equals(biddingVehicles.getBidding_order_return().getStatus())
        ) {
            // trả hàng
            return EnumBidding.EnumStatusOrder.SHIPPING_IS_SUCCESSFUL;
        }
        if ("-1".equals(biddingVehicles.getBidding_order_return().getStatus())
                || "-1".equals(biddingVehicles.getBidding_order_receive().getStatus())) {
            return EnumBidding.EnumStatusOrder.CANCEL_ORDER;
        }
        if ("0".equals(biddingVehicles.getBidding_order_receive().getStatus())) {
            // chưa vận chuyển
            return EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED;
        }
        if ("1".equals(biddingVehicles.getBidding_order_receive().getStatus())) {
            // đang vận chuyển
            return EnumBidding.EnumStatusOrder.BEING_TRANSPORTED;
        }

        return EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED;
    }

    public void handleConfirmBidding(int type, RunUi runUi) {
        String currentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
        if (type == EnumBidding.EnumStatusOrder.BEING_TRANSPORTED) {
            confirmBidding(getModelE().getId() + "", mapToList(cargo_ids.get()), "" + type, currentTime, runUi);
        } else {
            confirmBidding(getModelE().getId() + "", mapToList(cargo_ids.get()), "", currentTime, runUi);
        }
    }

    private List<Integer> mapToList(HashMap<String, Integer> cargo_ids) {
        List<Integer> list = new ArrayList<>();
        for (Map.Entry<String, Integer> id : cargo_ids.entrySet()) {
            list.add(id.getValue());
        }
        return list;
    }

    public void confirmBidding(String bidding_order_id, List<Integer> cargo_ids, String type, String confirm_time, RunUi runUi) {
        isLoading.set(true);
        BiddingApi.confirmBidding(bidding_order_id, cargo_ids, type, confirm_time, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if (o != null) {
                    OdooResultDto<ConfirmBiddingResponse> response = (OdooResultDto<ConfirmBiddingResponse>) o;
                    if (response.getRecords() != null && response.getRecords().size() > 0) {
                        List<Integer> listCargoInvalid = response.getRecords().get(0).getList_cargo_invalid();
                        StringBuilder stringBuilder = new StringBuilder();
                        if (listCargoInvalid.size() > 0) {
                            for (int i = 0; i < listCargoInvalid.size(); i++) {
                                stringBuilder.append(listCargoInvalid.get(i)).append(",");
                            }
                            String cargoInvalid = stringBuilder.length() > 0 ? stringBuilder.substring(0, stringBuilder.length() - 1) : "";
                            if (cargoInvalid.isEmpty()) {
                                confirmBiddingStatus = EnumBidding.EnumStatus.SUCCESS;
                            } else {
                                confirmBiddingStatus = cargoInvalid + " " + getApplication().getApplicationContext().getString(R.string.is_exist_in_list_receive);
                            }
                        } else {
                            confirmBiddingStatus = EnumBidding.EnumStatus.SUCCESS;
                        }
                    }

                } else {
                    confirmBiddingStatus = getApplication().getApplicationContext().getString(R.string.check_empty_list_cargo);
                }
                runUi.run("confirmBiddingStatus", confirmBiddingStatus);
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                confirmBiddingStatus = error.getMessage() + "";
                runUi.run("confirmBiddingStatus", confirmBiddingStatus);
            }
        });
    }

    public void cloneList(List<CargoType> originList, List<CargoType> targetList) {
        targetList.clear();
        for(CargoType cargoType : originList){
            CargoType clone = (CargoType) cargoType.clone();
            targetList.add(clone);
        }
    }
}
