package com.ts.sharevandriver.viewmodel;

import android.app.Application;
import android.database.Observable;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.databinding.ObservableList;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.api.DriverApi;
import com.ts.sharevandriver.api.SharingOdooResponse;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.model.WalletDTO;
import com.tsolution.base.BaseViewModel;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WalletChildVM extends BaseViewModel {
    ObservableBoolean isRefresh = new ObservableBoolean();
    ObservableBoolean emptyData = new ObservableBoolean();
    ObservableBoolean onlyRead = new ObservableBoolean();
    ObservableList<WalletDTO> walletDTOS = new ObservableArrayList<>();
    ObservableList<String> month_history = new ObservableArrayList<>();
    HashMap<String, Date> dateHashMap = new HashMap<>();
    String receive_type;
    ObservableInt month = new ObservableInt();
    ObservableInt year = new ObservableInt();
    int offset = 0;
    int total_record = 0;

    public WalletChildVM(@NonNull Application application) {
        super(application);
        emptyData.set(true);
        onlyRead.set(false);
        setTime();
    }

    public void getData(Boolean loadMore, RunUi runUi) {
        if (!loadMore) {
            walletDTOS.clear();
            offset = 0;
            total_record = 0;
        }
        getWalletDriver(runUi);
    }

    public void loadMore(RunUi runUi) {
        offset += 10;
        if (offset < total_record) {
            getData(true, runUi);
        } else {
            runUi.run("noMore");
        }
    }

    private void getWalletDriver(RunUi runUi) {
        DriverApi.getWalletDriver(receive_type, month.get(), year.get(), offset, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<WalletDTO> resultDto = (OdooResultDto<WalletDTO>) o;
                isRefresh.set(false);
                if (resultDto != null) {
                    walletDTOS.addAll(resultDto.getRecords());
                    total_record = resultDto.getTotal_record();
                    runUi.run(Constants.SUCCESS_API);
                } else {
                    emptyData.set(true);
                    runUi.run(Constants.FAIL_API);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    private void setTime() {
        Date date = new Date(); // your date
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        year.set(cal.get(Calendar.YEAR));
        month.set(cal.get(Calendar.MONTH) + 1);

        for (int i = 0; i < 6; i++) {
            int month;
            int year;
            String data;
            if ((this.month.get() - i) < 1) {
                month = this.month.get() + 12 - i;
                year = this.year.get() - 1;

                if (month < 10) {
                    data="0" + month + "/" + year;
                } else
                    data=month + "/" + year;
            } else {
                month = this.month.get() - i;
                year = this.year.get();
                if (month < 10) {
                    data="0" + month + "/" + year;
                } else
                    data=month + "/" + year;
            }
            month_history.add(data);
            cal.set(Calendar.YEAR,year);
            cal.set(Calendar.MONTH,month-1);
            Date date1= cal.getTime();
            this.dateHashMap.put(data,date1);
        }
    }
}
