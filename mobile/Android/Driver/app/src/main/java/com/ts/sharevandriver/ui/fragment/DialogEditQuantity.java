package com.ts.sharevandriver.ui.fragment;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.databinding.DialogEditQuantityBinding;
import com.ts.sharevandriver.model.BillPackage;


@SuppressLint("ValidFragment")
public class DialogEditQuantity extends DialogFragment {
    private BillPackage billPackage;
    private OnConfirm onClickListener;
    DialogEditQuantityBinding binding;
    int quantity;
    int originQuantity;

    public DialogEditQuantity(BillPackage billPackage, int originQuantity, OnConfirm onClickListener) {
        super();
        this.billPackage = billPackage;
        this.onClickListener = onClickListener;
        this.quantity = originQuantity;
        this.originQuantity = originQuantity;

    }

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_edit_quantity, container, false);

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        binding.txtMessage.setText(getString(R.string.package_code) + ": "
                + billPackage.getQr_char() + "\n"
                + getString(R.string.item_name) + ": " + billPackage.getItem_name());

        binding.lbEditText.setSuffixText("/" + originQuantity);
        binding.txtEditText.setText(originQuantity + "");

        binding.txtEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() > 0){
                    quantity = Integer.parseInt(charSequence.toString());
                    if(quantity > originQuantity){
                        binding.btnConfirm.setEnabled(false);
                    }else {
                        binding.btnConfirm.setEnabled(true);
                    }
                }else {
                    binding.btnConfirm.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.btnDismiss.setOnClickListener(v-> this.dismiss());
        binding.btnCancel.setOnClickListener(v-> this.dismiss());
        binding.btnConfirm.setOnClickListener(v-> {
            this.dismiss();
            onClickListener.getEditText(quantity);
        });
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public interface OnConfirm{
        void getEditText(Integer quantity);
    }

}
