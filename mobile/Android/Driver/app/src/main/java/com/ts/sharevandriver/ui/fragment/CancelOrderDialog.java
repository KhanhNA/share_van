package com.ts.sharevandriver.ui.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.ScreenUtils;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.adapter.GridImageAdapter;
import com.ts.sharevandriver.adapter.ImageBaseAdapter;
import com.ts.sharevandriver.databinding.CancelOrderDialogBinding;
import com.ts.sharevandriver.viewmodel.CancelOrderDialogVM;
import com.ts.sharevandriver.widget.FullyGridLayoutManager;
import com.ts.sharevandriver.widget.GlideEngine;

import java.lang.ref.WeakReference;
import java.util.List;

public class CancelOrderDialog extends DialogFragment {
    CancelOrderDialogBinding mBinding;
    View.OnClickListener onClickListener;
    CancelOrderDialogVM cancelOrderDialogVM;
    public TextInputLayout editText;
    private GridImageAdapter imageSelectAdapter;
    CancelOrderDialog.IChooseImage iChooseImage;
    ImageBaseAdapter imageBaseAdapter;

    public CancelOrderDialog(View.OnClickListener onClickListener, CancelOrderDialog.IChooseImage iChooseImage) {
        this.onClickListener = onClickListener;
        this.iChooseImage = iChooseImage;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.cancel_order_dialog, container, false);
        cancelOrderDialogVM = ViewModelProviders.of(this).get(CancelOrderDialogVM.class);
        mBinding.setViewModel(cancelOrderDialogVM);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        initView();
        setUpRecycleView();
//        initAdapterImage(savedInstanceState);
        return mBinding.getRoot();
    }

    public void selectImages() {
        iChooseImage.chooseImages();
    }

    public void addImage(Uri localMedia) {
        cancelOrderDialogVM.getListImage().add(localMedia.toString());
        imageBaseAdapter.notifyItemInserted(cancelOrderDialogVM.getListImage().size() - 1);
        cancelOrderDialogVM.getIsNotEmptyImage().set(true);
    }

    private void setUpRecycleView() {
        imageBaseAdapter = new ImageBaseAdapter(getActivity(), R.layout.item_image_100dp, cancelOrderDialogVM.getListImage(),false);
        mBinding.rcImages.setAdapter(imageBaseAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        mBinding.rcImages.setLayoutManager(layoutManager);
    }

    private void initAdapterImage(Bundle savedInstanceState) {

        FullyGridLayoutManager manager = new FullyGridLayoutManager(getContext(),
                3, GridLayoutManager.VERTICAL, false);
        mBinding.rcImages.setLayoutManager(manager);

//        FullyGridLayoutManager manager = new FullyGridLayoutManager(getContext(),
//                3, GridLayoutManager.VERTICAL, false);
//        mBinding.rcImages.setLayoutManager(manager);
//
//        mBinding.rcImages.addItemDecoration(new GridSpacingItemDecoration(4,
//                ScreenUtils.dip2px(getContext(), 8), false));
//
//
//        imageSelectAdapter = new GridImageAdapter(getContext(), this::pickImage);
//        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList("selectorList") != null) {
//            imageSelectAdapter.setList(savedInstanceState.getParcelableArrayList("selectorList"));
//        }
//        imageSelectAdapter.setOnClear(() -> cancelOrderDialogVM.getIsNotEmptyImage().set(false));
//        mBinding.rcImages.setAdapter(imageSelectAdapter);
//        imageSelectAdapter.setSelectMax(3);
//        imageSelectAdapter.setOnItemClickListener((v, position) -> openOptionImage(position));
    }

    public void pickImage() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .maxSelectNum(6)
                .selectionMedia(imageSelectAdapter.getData())
                .forResult(new CancelOrderDialog.MyResultCallback(imageSelectAdapter, cancelOrderDialogVM));
    }


    private void initView() {
        mBinding.btnCancel.setOnClickListener(v -> {
            this.dismiss();
        });
        mBinding.btnDismiss.setOnClickListener(v -> {
            this.dismiss();
        });
        mBinding.btnConfirm.setOnClickListener(onClickListener);
        editText = mBinding.lbEditText;
        mBinding.btnAddImage.setOnClickListener(v -> selectImages());
        mBinding.txtEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mBinding.lbEditText.setErrorEnabled(false);
            }
        });
    }

    public String getDescription() {
        return mBinding.txtEditText.getText().toString();
    }

    public List<LocalMedia> getListImage() {
        return cancelOrderDialogVM.getLstFileSelected();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }

    private static class MyResultCallback implements OnResultCallbackListener<LocalMedia> {
        private WeakReference<GridImageAdapter> mAdapterWeakReference;
        private CancelOrderDialogVM viewModel;

        public MyResultCallback(GridImageAdapter adapter, CancelOrderDialogVM viewModel) {
            super();
            this.mAdapterWeakReference = new WeakReference<>(adapter);
            this.viewModel = viewModel;
        }

        @Override
        public void onResult(List<LocalMedia> result) {
            viewModel.setDataFileChoose(result);
            if (mAdapterWeakReference.get() != null) {
                mAdapterWeakReference.get().setList(result);
                mAdapterWeakReference.get().notifyDataSetChanged();
            }
        }

        @Override
        public void onCancel() {
        }
    }

    private void openOptionImage(int position) {
        List<LocalMedia> selectList = imageSelectAdapter.getData();
        if (selectList.size() > 0) {
            LocalMedia media = selectList.get(position);
            String mimeType = media.getMimeType();
            int mediaType = PictureMimeType.getMimeType(mimeType);
            if (mediaType == PictureConfig.TYPE_VIDEO) {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .externalPictureVideo(TextUtils.isEmpty(media.getAndroidQToPath()) ? media.getPath() : media.getAndroidQToPath());
            } else {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .isNotPreviewDownload(true)
                        .openExternalPreview(position, selectList);
            }
        }
    }

    public interface IChooseImage {
        void chooseImages();
    }
}
