package com.ts.sharevandriver.service.notification_service;

import com.ts.sharevandriver.service.location_service.LocLatLng;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotificationVehicleBody {
    private Integer vehicleId;

    private String name;
    private Boolean active;
    private Integer companyId;

    private String licensePlate;
    private String vinSn;
    private Integer color;
    private Integer seats;
    private String modelYear;
    private Integer doors;
    //    private Double latitude;
//    private Double longitude;
    private List<LocLatLng> locLatLngs;
    private Integer vehicleType;
    private Double engineSize;


}
