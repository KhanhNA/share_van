package com.ts.sharevandriver.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.chip.Chip;
import com.king.zxing.Intents;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.adapter.XBaseAdapter;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.databinding.ActivityDetailNotShipBinding;
import com.ts.sharevandriver.databinding.ItemContentCargoBinding;
import com.ts.sharevandriver.enums.EnumBidding;
import com.ts.sharevandriver.model.Cargo;
import com.ts.sharevandriver.model.CargoType;
import com.ts.sharevandriver.model.Depot;
import com.ts.sharevandriver.ui.fragment.DialogConfirm;
import com.ts.sharevandriver.utils.ToastUtils;
import com.ts.sharevandriver.utils.TsUtils;
import com.ts.sharevandriver.viewmodel.DetailNotShipVM;
import com.ts.sharevandriver.widget.QrScannerActivity;
import com.ts.sharevandriver.widget.SnapOnScrollListener;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;


public class DetailNotShipActivity extends BaseActivity<ActivityDetailNotShipBinding> {
    final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private final int REQUEST_CODE_CAMERA = 0X01;

    int biddingId;
    int type;
    int prePosition = 0;
    DetailNotShipVM detailNotShipVM;

    XBaseAdapter cargoAdapter;
    XBaseAdapter cargoContentAdapter;

    DialogConfirm dialogConfirmReceive;
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        detailNotShipVM = (DetailNotShipVM) viewModel;
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        getDataBundle();

        initToolbar();
        initView();
        event();
        getData();
    }

    private void event() {
        binding.btnReceiveGoods.setOnClickListener(v -> receiveGoods());
        binding.btnQR.setOnClickListener(v -> openQRScanActivity());

        binding.btnEdit.setOnClickListener(v -> {
            detailNotShipVM.cloneList(detailNotShipVM.getCargoTypeList(), detailNotShipVM.getCargoTypeListClone());
            detailNotShipVM.getIsEdit().set(true);
            cargoContentAdapter.notifyDataSetChanged();

        });
        binding.btnDiscard.setOnClickListener(v -> {
            if(detailNotShipVM.isEdited()){
                dialogConfirmReceive = new DialogConfirm(getString(R.string.CONFIRM)
                        , getString(R.string.msg_cancel_edit)).setOnClickListener(v1 -> {
                            detailNotShipVM.cloneList(detailNotShipVM.getCargoTypeListClone(), detailNotShipVM.getCargoTypeList());
                            detailNotShipVM.getIsEdit().set(false);
                            cargoContentAdapter.notifyDataSetChanged();
                        });
                dialogConfirmReceive.show(getSupportFragmentManager(), dialogConfirmReceive.getTag());
            }else {
                detailNotShipVM.getIsEdit().set(false);
                cargoContentAdapter.notifyDataSetChanged();
            }
        });
    }

    @AfterPermissionGranted(REQUEST_CODE_CAMERA)
    private void openQRScanActivity() {
        String[] perms = {Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            Intent intent = new Intent(this, QrScannerActivity.class);
            intent.putExtra("QR_SCANNER", getString(R.string.qr_scan));
            startActivityForResult(intent, Constants.REQUEST_CODE_SCAN);
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.permissions_not_granted),
                    REQUEST_CODE_CAMERA, perms);
        }
    }

    private void receiveGoods() {
        if (detailNotShipVM.isValidReceiveGoods() && type == EnumBidding.EnumStatusOrder.BEING_TRANSPORTED) {
            ToastUtils.showToast(getString(R.string.check_empty_list_cargo));
        } else {
            dialogConfirmReceive = new DialogConfirm(getString(R.string.confirm_receive_goods), "").setOnClickListener(v->{
                if (type == EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION};
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(
                                    permissions,
                                    LOCATION_PERMISSION_REQUEST_CODE);
                        }
                        return;
                    }
                    fusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, location1 -> {
                                // Got last known location. In some rare situations this can be null.
                                if (location1 != null) {
                                    Depot fromDepot = detailNotShipVM.getModelE().getFrom_depot();
                                    float[] results = new float[1];
                                    Location.distanceBetween(fromDepot.getLatitude()
                                            , fromDepot.getLongitude()
                                            , location1.getLatitude()
                                            , location1.getLongitude(), results);

                                    if (results[0] < StaticData.getOdooSessionDto().getDistance_check_point()) {

                                        detailNotShipVM.handleConfirmBidding(type, DetailNotShipActivity.this::runUi);

                                    } else {
                                        ToastUtils.showToast(this
                                                , getString(R.string.update_fail_check_your_location)
                                                , getResources().getDrawable(R.drawable.ic_not_listed_location_24));
                                    }
                                } else {
                                    ToastUtils.showToast(this
                                            , getString(R.string.can_not_get_location)
                                            , getResources().getDrawable(R.drawable.ic_not_listed_location_24));
                                }
                            });
                } else {
                    detailNotShipVM.handleConfirmBidding(type, this::runUi);
                }

                dialogConfirmReceive.dismiss();
            });
            dialogConfirmReceive.show(getSupportFragmentManager(), null);
        }
    }

    private void initView() {
        cargoAdapter = new XBaseAdapter(R.layout.item_title_cargo, detailNotShipVM.getCargoTypeList(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                int position = ((CargoType) o).index - 1;

                binding.rcCargoType.smoothScrollToPosition(position);
                binding.rcCargoTypeContent.scrollToPosition(position);

            }

            @Override
            public void onItemLongClick(View view, Object o) {
                onItemClick(view, o);
            }
        });
        binding.rcCargoType.setAdapter(cargoAdapter);
        binding.rcCargoType.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        cargoContentAdapter = initCargoContentAdapter(type);
        binding.rcCargoTypeContent.setAdapter(cargoContentAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        binding.rcCargoTypeContent.setLayoutManager(linearLayoutManager);
        SnapHelper helper = new PagerSnapHelper();
        helper.attachToRecyclerView(binding.rcCargoTypeContent);

        binding.rcCargoTypeContent.addOnScrollListener(new SnapOnScrollListener(helper, position -> {
            if (position != prePosition) {
                detailNotShipVM.getCargoTypeList().get(position).checked = true;
                detailNotShipVM.getCargoTypeList().get(prePosition).checked = false;
                cargoAdapter.notifyItemChanged(position);
                cargoAdapter.notifyItemChanged(prePosition);
                binding.rcCargoType.smoothScrollToPosition(position);

                prePosition = position;

            }
        }));


    }

    private XBaseAdapter initCargoContentAdapter(int type) {
        return new XBaseAdapter(R.layout.item_content_cargo, detailNotShipVM.getCargoTypeList(), DetailNotShipActivity.this) {
            @Override
            public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
                ((ItemContentCargoBinding) viewHolder.itemProductBinding).cgCargo.removeAllViews();
                //hiển thị danh sách cargo của cargo type
                CargoType cargoType = detailNotShipVM.getCargoTypeList().get(position);
                cargoType.setScreenType(type);
                super.onBindViewHolder(viewHolder, position);


                if (cargoType.getSelectedCargo() == null) return;
                for (Map.Entry<String, Integer> entry : cargoType.getSelectedCargo().entrySet()) {
                    Chip chip = new Chip(DetailNotShipActivity.this);
                    chip.setTag(entry.getKey());
                    chip.setTextSize(12);
                    chip.setTextColor(getResources().getColor(R.color.primaryColor));
                    chip.setText(entry.getKey());
                    chip.setCloseIconVisible(type == EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED || detailNotShipVM.getIsEdit().get());
                    chip.setCheckable(false);

                    ((ItemContentCargoBinding) viewHolder.itemProductBinding).cgCargo.addView(chip);

                    chip.setOnCloseIconClickListener(v -> {
                        dialogConfirmReceive = new DialogConfirm(getString(R.string.confirm_delete), "").setOnClickListener(v1 -> {
                            ((ItemContentCargoBinding) viewHolder.itemProductBinding).cgCargo.removeView(v);
                            cargoType.getSelectedCargo().remove(v.getTag());
                            detailNotShipVM.getCargo_ids().get().remove(v.getTag());
                            detailNotShipVM.getCargo_ids().notifyChange();
                            dialogConfirmReceive.dismiss();
                            detailNotShipVM.setEdited(true);

                        });
                        dialogConfirmReceive.show(getSupportFragmentManager(), dialogConfirmReceive.getTag());

                    });

                }
            }
        };
    }


    private void initToolbar() {
        switch (type) {
            case 0:
                binding.txtTitle.setText(R.string.not_yet_delivery);
                break;
            case 1:
                binding.txtTitle.setText(R.string.in_transit);
                break;
            case 2:
                binding.txtTitle.setText(R.string.shipped);
                break;
        }
        binding.btnBack.setOnClickListener(v -> onBackPressed());

    }

    private void getData() {
        detailNotShipVM.getOrderDetailList(biddingId, this::runUi);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getBiddingOrderDetailSuccess":
                if (TsUtils.isNotNull(detailNotShipVM.getModelE().getCargo_type())) {
                    type = detailNotShipVM.getStatusOrder();
                    if (type == 0) {
                        binding.btnQR.setVisibility(View.VISIBLE);
                        detailNotShipVM.getCargoTypeList().addAll(detailNotShipVM.getModelE().getCargo_type());
                        detailNotShipVM.getIsEdit().set(true);
                    } else if (type == 1) {
                        detailNotShipVM.getCargoTypeList().addAll(detailNotShipVM.getModelE().getBidding_vehicles().getCargo_types());
                        binding.llEdit.setVisibility(View.VISIBLE);
                        binding.btnQR.setVisibility(View.VISIBLE);
                        binding.btnReceiveGoods.setText(R.string.update_list_cargo);
                    } else {
                        detailNotShipVM.getCargoTypeList().addAll(detailNotShipVM.getModelE().getBidding_vehicles().getCargo_types());
                    }

                    initToolbar();
                    if (detailNotShipVM.getCargoTypeList().size() > 0) {
                        detailNotShipVM.getCargoTypeList().get(0).checked = true;
                    }
                    cargoAdapter.update(detailNotShipVM.getCargoTypeList());
                    cargoContentAdapter.update(detailNotShipVM.getCargoTypeList());
                }
                break;
            case "confirmBiddingStatus":
                String s = (String) objects[1];
                if (s.equals(EnumBidding.EnumStatus.SUCCESS)) {
                    ToastUtils.showToast(this, getString(R.string.confirm_receive_goods_success), getResources().getDrawable(R.drawable.ic_done_white_48dp));
                    finish();
                } else {
                    ToastUtils.showToast(s);
                }
                break;
        }
    }

    private void getDataBundle() {
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            type = intent.getExtras().getInt(Constants.KEY_PAGE);
            biddingId = intent.getExtras().getInt(Constants.OBJECT_SERIALIZABLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                }
                receiveGoods();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isScanCodeAndResultOk(requestCode, resultCode)) {
            String qr = data.getStringExtra(Intents.Scan.RESULT);
            if (detailNotShipVM.getMapCargo().get(qr) != null) {
                if (detailNotShipVM.getCargo_ids().get().get(qr) == null) {
                    detailNotShipVM.getCargo_ids().get().put(qr, detailNotShipVM.getMapCargo().get(qr).getId());
                    detailNotShipVM.getCargo_ids().notifyChange();
                    detailNotShipVM.setEdited(true);
                    ToastUtils.showToast(qr);
                } else {
                    ToastUtils.showToast(this,R.string.this_cargo_was_scanned);
                }
                notifyAdapterCargo(detailNotShipVM.getMapCargo().get(qr));

            } else {
                ToastUtils.showToast(this, R.string.invalid_cargo);
            }
        }
    }

    //cập nhật danh sách cargo của cargoType
    private void notifyAdapterCargo(Cargo cargo) {
        List<CargoType> cargoTypes = detailNotShipVM.getModelE().getCargo_type();
        for (int i = 0; i < cargoTypes.size(); i++) {
            CargoType cargoType = cargoTypes.get(i);
            if (cargoType.getId().equals(cargo.getSizeId())) {
                if (cargoType.getSelectedCargo() == null) {
                    cargoType.setSelectedCargo(new HashMap<>());
                }
                cargoContentAdapter.notifyItemChanged(i);

                cargoType.getSelectedCargo().put(cargo.getQrCode(), cargo.getId());
                binding.rcCargoType.smoothScrollToPosition(i);
                binding.rcCargoTypeContent.scrollToPosition(i);

                return;
            }
        }
    }

    private boolean isScanCodeAndResultOk(int requestCode, int resultCode) {
        return requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_detail_not_ship;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DetailNotShipVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

}

