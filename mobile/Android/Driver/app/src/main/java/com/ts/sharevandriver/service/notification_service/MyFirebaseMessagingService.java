package com.ts.sharevandriver.service.notification_service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.api.DriverApi;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.enums.ClickAction;
import com.ts.sharevandriver.enums.ObjectStatus;
import com.ts.sharevandriver.model.NotificationModel;
import com.ts.sharevandriver.model.Vehicle;
import com.ts.sharevandriver.service.location_service.DatabaseHelper;
import com.ts.sharevandriver.service.location_service.TrackingService;
import com.ts.sharevandriver.ui.activity.LoginActivityV2;
import com.ts.sharevandriver.ui.activity.MainActivity;
import com.ts.sharevandriver.utils.ToastUtils;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import org.greenrobot.eventbus.EventBus;

import java.util.Objects;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String TAG = "NOTIFICATION_FIREBASE_FROM";
    public static String NOTIFICATION_CHANNEL_ID = "share_vans_notification";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        // phương thức được gọi khi đang  mở app
        Log.d(TAG, remoteMessage.getFrom());

        //final ObjectMapper mapper = new ObjectMapper();
        final NotificationService notificationService = NotificationService.of(remoteMessage.getData());//mapper.convertValue(remoteMessage.getData(), NotificationService.class);
        EventBus.getDefault().post(notificationService);
        if (notificationService != null) {
            sendNotification(notificationService);
        }
    }


    private void sendNotification(NotificationService notification) {
        // hide vehicle when completed all routing
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                .create();
        NotificationModel notificationModel = gson.fromJson(notification.getMess_object(), NotificationModel.class);

        if (ObjectStatus.THREE.equals(notificationModel.getObject_status())) {
            logout();
            return;
        }
        if ((ObjectStatus.SIX.equals(notificationModel.getObject_status()) || ObjectStatus.FOUR.equals(notificationModel.getObject_status())) && notificationModel.getClick_action().equals(ClickAction.MAIN_ACTIVITY)) {
            if (StaticData.getDriver().getVehicle() != null) {
                StaticData.getDriver().setVehicle(null);
            }
        } else if (ObjectStatus.FIVE.equals(notificationModel.getObject_status()) && notificationModel.getClick_action().equals(ClickAction.VEHICLE_INFO)) {
            try {
                getVansInfo(Integer.parseInt(notificationModel.getItem_id()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //logout when edit phone number driver

        Intent notificationIntent = new Intent(notification.getClick_action());

        if (notification.getItem_id() != null) {
            notificationIntent.putExtra(Constants.ITEM_ID, "" + notification.getItem_id());
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
            // Configure the notification channel.
            notificationChannel.setDescription(notification.getBody());
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
//                .setSound(Uri.parse("android.resource://"
//                        + getPackageName() + "/" + R.raw.notification_bell))
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_vans_green)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentTitle(notification.getTitle())
                .setContentIntent(pendingIntent)
                .setContentText(notification.getBody());
        assert notificationManager != null;
        notificationManager.notify(1, notificationBuilder.build());
    }

    private void logout() {
        // Delete token firebase
        DriverApi.logout();
        // Delete user & pass
        AppController.getInstance().getSharePre().edit().putString(Constants.MK, "").apply();
        // Delete cache
        StaticData.setOdooSessionDto(null);
        StaticData.setDriver(null);
        StaticData.sessionCookie = "";
        // Delete local db
        Objects.requireNonNull(this).deleteDatabase(DatabaseHelper.DATABASE_NAME);
        // Intent loginActivity
        Intent intent = new Intent(this, LoginActivityV2.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }

    public void getVansInfo(Integer log_id) {
        DriverApi.getVansInfo(log_id, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                StaticData.getDriver().setVehicle((Vehicle) o);
            }

            @Override
            public void onFail(Throwable error) {
            }
        });
    }
}
