package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableDouble;

import com.ts.sharevandriver.api.DriverApi;
import com.ts.sharevandriver.api.SharingOdooResponse;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.StaticData;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WalletVM extends BaseViewModel {
    ObservableBoolean showMoney = new ObservableBoolean();
    double totalMoney;
    public WalletVM(@NonNull Application application) {
        super(application);
        showMoney.set(false);
        getTotalAmount();
    }
    public String getTotalMoneyStr(){
        return AppController.getInstance().formatCurrency(totalMoney);
    }
    private void getTotalAmount(){
        DriverApi.getTotalAmount(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                Double totalAmount= (Double) o;
                if(totalAmount!=null){
                    totalMoney=totalAmount;
                }else{
                    totalMoney=0;
                }
            }

            @Override
            public void onFail(Throwable error) {
                totalMoney=0;
            }
        });
    }
}
