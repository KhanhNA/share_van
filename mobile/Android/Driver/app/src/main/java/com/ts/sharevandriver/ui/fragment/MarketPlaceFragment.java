package com.ts.sharevandriver.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.adapter.HaiSer;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.polyak.iconswitch.IconSwitch;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.databinding.MarketPlaceFragmentBinding;
import com.ts.sharevandriver.enums.EnumBidding;
import com.ts.sharevandriver.model.BiddingInformation;
import com.ts.sharevandriver.model.ParkingPoint;
import com.ts.sharevandriver.socket_notification.SocketIO;
import com.ts.sharevandriver.socket_notification.SocketIONavigation;
import com.ts.sharevandriver.socket_notification.SocketResult;
import com.ts.sharevandriver.ui.activity.DetailNotShipActivity;
import com.ts.sharevandriver.viewmodel.MarketPlaceVM;
import com.ts.sharevandriver.widget.AnimationUtil;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.MainActivity;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import static com.ts.sharevandriver.service.notification_service.MyFirebaseMessagingService.NOTIFICATION_CHANNEL_ID;

public class MarketPlaceFragment extends BaseFragment implements OnMapReadyCallback, SocketIO.onMessageSocket {
    private static final int LOCATION_SETTING_CODE = 111;

    private static final String TAG = "MarketPlaceFragment";
    protected SocketIONavigation mSocketIONavigation;
    private final String channel = "marketmessage";


    private GoogleMap mMap;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;

    private MarketPlaceFragmentBinding mBinding;
    private MarketPlaceVM marketPlaceVM;

    FusedLocationProviderClient mFusedLocationProviderClient;
    BottomSheetInfoDriver bottomSheetInfoDriver;
    BiddingMarketDialogFragment biddingMarketDialog;

    SocketResult socketResult;
    AlertDialog alert;
    String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};
    @Override
    public void onResume() {
        super.onResume();
        if (marketPlaceVM.isOnReceiveRout()) {
            showDialogAccept(socketResult, false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;

        marketPlaceVM = (MarketPlaceVM) viewModel;
        mBinding = (MarketPlaceFragmentBinding) binding;
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity());
        mSocketIONavigation = new SocketIO() {
            @Override
            public void connect(String uri, String channel, onMessageSocket onMessageSocket) {
                super.connect(uri, channel, onMessageSocket);
                mSocket.on(Socket.EVENT_CONNECT, args -> {
                    Log.e("Socket", "connect: SocketIO");
                    marketPlaceVM.setSocketId(getContext(), mSocket.id(), MarketPlaceFragment.this::runUi);
                });
            }
        };

        placeAutoComplete();
        initView();
        initMap();
        marketPlaceVM.getDriverInfo(this::runUi);
        return view;
    }

    private void initView() {
        initBottomSheet();
        mBinding.iconSwitch.setCheckedChangeListener(current -> {

            if (mBinding.iconSwitch.getChecked() == IconSwitch.Checked.RIGHT) {
                if (marketPlaceVM.getDriverMarketInfo().get().getDriver_id() == null) {
                    bottomSheetInfoDriver.show(getChildFragmentManager(), bottomSheetInfoDriver.getTag());
                } else {
                    mSocketIONavigation.connect(AppController.URL_SOCKET_, channel, this::messageSocket);
                    AnimationUtil.slideUp(mBinding.ctInfo, true);
                    marketPlaceVM.getIsOnline().set(true);
                }
            } else {
                marketPlaceVM.getIsOnline().set(false);
                marketPlaceVM.setSocketId(getContext(), "", MarketPlaceFragment.this::runUi);
                mSocketIONavigation.disconnect(channel);
                AnimationUtil.slideDown(mBinding.ctInfo, false);
            }
        });
        mBinding.ctInfo.setOnClickListener(v -> {
            bottomSheetInfoDriver.show(getChildFragmentManager(), bottomSheetInfoDriver.getTag());
        });
    }

    private void initBottomSheet() {
        bottomSheetInfoDriver = new BottomSheetInfoDriver(marketPlaceVM, status -> {
            if (!status) {
                mBinding.iconSwitch.setChecked(IconSwitch.Checked.LEFT);
                AnimationUtil.slideDown(mBinding.ctInfo, false);
            } else {
                marketPlaceVM.getDriverMarketInfo().notifyChange();
                mSocketIONavigation.connect(AppController.URL_SOCKET_, channel, this::messageSocket);
                AnimationUtil.slideUp(mBinding.ctInfo, true);
            }
        });
    }


    @Override
    public void messageSocket(Object... args) {
        if (args == null || args.length == 0) return;
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OdooRelType.class, new HaiSer<>())
                .registerTypeAdapter(OdooDate.class, Adapter.DATE)
                .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                .create();
        Log.e("Socket", "messageSocket:MarketMessage " + args[0]);
        JSONObject data = (JSONObject) args[0];
        Log.d("Socket", "call: " + data.toString());
        socketResult = gson.fromJson(data.toString(), SocketResult.class);
        showDialogAccept(socketResult, true);
        marketPlaceVM.setOnReceiveRout(true);
    }

    private void showDialogAccept(SocketResult socketResult, boolean createNotification) {
        try {
            BiddingInformation biddingInformation = socketResult.getResult().getRecords().get(0);
            if (createNotification) {
                createNotification(biddingInformation);
            }
            if (biddingMarketDialog != null) {
                biddingMarketDialog.setBiddingInformation(biddingInformation);
            } else {
                biddingMarketDialog = new BiddingMarketDialogFragment(marketPlaceVM, biddingInformation, new BiddingMarketDialogFragment.mAction() {
                    @Override
                    public void onAccept() {
                        marketPlaceVM.acceptRout(biddingInformation.getId(), true, MarketPlaceFragment.this::runUi);
                    }

                    @Override
                    public void onReject() {
                        marketPlaceVM.acceptRout(biddingInformation.getId(), false, MarketPlaceFragment.this::runUi);

                    }
                });
            }
            biddingMarketDialog.show(getChildFragmentManager(), "biddingMarketDialog");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createNotification(BiddingInformation biddingInformation) {
        Intent notificationIntent = new Intent("com.ts.sharevandriver.TARGET_MAIN_ACTIVITY");
        String notificationTitle = getString(R.string.msg_have_assign_shipment);
        String notificationBody = getString(R.string.from) + " " + biddingInformation.getFrom_depot().getAddress() + ", "
                + getString(R.string.to) + " " + biddingInformation.getTo_depot().getAddress() + ", "
                + getString(R.string.distance) + " " + AppController.getInstance().formatNumber(biddingInformation.getDistance() / 1000) + "km, "
                + getString(R.string.price) + " " + AppController.getInstance().formatCurrency(biddingInformation.getPrice());
        PendingIntent pendingIntent = PendingIntent.getActivity(getContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //
        NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
            // Configure the notification channel.
            notificationChannel.setDescription(notificationTitle);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.GREEN);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getContext(), NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
//                .setSound(Uri.parse("android.resource://"
//                        + getPackageName() + "/" + R.raw.notification_bell))
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_vans_green)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .setContentTitle(notificationTitle)
                .setContentText(notificationBody);
        assert notificationManager != null;
        notificationManager.notify(1, notificationBuilder.build());
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {


        mMap = googleMap;
        init();
        getDeviceLocation();

        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);


    }

    /**
     * khởi tạo map
     */
    private void init() {

        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(getActivity()));
        mBinding.icGps.setOnClickListener(view -> {
            getDeviceLocation();
        });

        mMap.setOnInfoWindowClickListener(marker -> {

        });
        hideSoftKeyboard();

        mMap.setOnInfoWindowCloseListener(marker -> {


        });


    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "connectFail":
                Toast.makeText(getContext(), R.string.can_not_connect, Toast.LENGTH_SHORT).show();
                break;
            case "acceptSuccess":
                gotoBiddingDetail((Integer) objects[1]);
                break;
            case "acceptFail":
                Toast.makeText(getContext(), R.string.msg_fail_to_accept_market_order, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void gotoBiddingDetail(Integer biddingId) {
        Intent intent = new Intent(getActivity(), DetailNotShipActivity.class);
        intent.putExtra(Constants.OBJECT_SERIALIZABLE, biddingId);
        intent.putExtra(Constants.KEY_PAGE, EnumBidding.EnumStatusOrder.NOT_YET_SHIPPED);
        startActivity(intent);
    }


    private void buildAlertMessageNoGps() {
        if (alert == null) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(getString(R.string.msg_turn_on_gps))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.ok), (dialog, id)
                            -> startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_SETTING_CODE));
            alert = builder.create();
        }
        if (!alert.isShowing()) {
            alert.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOCATION_SETTING_CODE) {
            if (AppController.getInstance().checkHighAccuracyLocationMode()) {
                initMap();
            } else {
                buildAlertMessageNoGps();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                }
                //initialize our map
                initMap();
            }
        }

    }

    private void initMap() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            Log.d(TAG, "initMap: initializing map");
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    /**
     * khởi tạo thanh tìm kiếm địa chỉ theo map
     */
    private void placeAutoComplete() {
//         Initialize Places.
        if (getActivity() != null) {
            Places.initialize(getActivity().getApplicationContext(), AppController.API_GOOGLE_MAP_SEARCH);
        }
        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        if (autocompleteFragment != null) {
            ((EditText) autocompleteFragment.getView().findViewById(R.id.places_autocomplete_search_input)).setTextSize(15.0f);
            autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG));
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(@NonNull Place place) {
                    if (place.getLatLng() != null) {
                        moveCamera(place.getLatLng(), place.getName() + "");
                    }
                }

                @Override
                public void onError(@NonNull Status status) {

                }
            });
        }
    }


    /**
     * lấy vị trí hiện tại
     */
    private void getDeviceLocation() {

        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(permissions, LOCATION_PERMISSION_REQUEST_CODE);
            return;
        }
//        final Task location = mFusedLocationProviderClient.getLastLocation();
//        location.addOnCompleteListener(task -> {
//            if (task.isSuccessful() && task.getResult() != null) {
//                Location currentLocation = (Location) task.getResult();
//
//                moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
//                        "My Location");
//
//            } else {
//                buildAlertMessageNoGps();
//            }
//        });
        mFusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            moveCamera(new LatLng(location.getLatitude(), location.getLongitude()),
                                    "My Location");
                        } else {
                            buildAlertMessageNoGps();
                        }
                    }
                });
    }

    /**
     * Di chuyển camera đến vị tri @latLng
     *
     * @param latLng vị trí cần di chuyển đến
     */
    private void moveCamera(LatLng latLng, String title) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, MarketPlaceFragment.DEFAULT_ZOOM), 2000, null);

        hideSoftKeyboard();
    }

    private void hideSoftKeyboard() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.market_place_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MarketPlaceVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onDestroy() {
        marketPlaceVM.getIsOnline().set(false);
        mSocketIONavigation.disconnect(channel);
        super.onDestroy();

    }
}
