package com.ts.sharevandriver.model;




import androidx.annotation.Nullable;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillService extends BaseModel {
    private Integer id;
    private String name;
    private String display_name;
    private Integer type;
    private Double price;
    private String description;
    private String status;
    private String service_code;
    private String partner_name;

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null)
            return false;
        return this.id.equals(((BillService) obj).id);
    }
}
