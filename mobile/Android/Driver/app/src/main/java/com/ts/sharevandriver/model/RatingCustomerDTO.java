package com.ts.sharevandriver.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tsolution.base.BaseModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RatingCustomerDTO extends BaseModel {
    private Integer rating_place_id;
    private Long driver_id;
    private Long employee_id;
    private String note;
    private int rating;
    private List<Integer> list_badges;
    private List<String> list_badge_names;
    private String type="ROUTING";
    private String uri_path;
}

