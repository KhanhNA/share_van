package com.ts.sharevandriver.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.google.android.gms.maps.model.LatLng;
import com.ts.sharevandriver.api.DriverApi;
import com.ts.sharevandriver.api.RoutingApi;
import com.ts.sharevandriver.api.SOSApi;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.model.ShareVanRoutingPlan;
import com.ts.sharevandriver.model.section.ItemRoutingNote;
import com.ts.sharevandriver.model.section.RootNode;
import com.ts.sharevandriver.utils.Distance;
import com.ts.sharevandriver.utils.Duration;
import com.ts.sharevandriver.utils.Route;
import com.ts.sharevandriver.utils.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import androidx.databinding.ObservableField;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MapsVM extends BaseViewModel {
    private ObservableBoolean isLoading = new ObservableBoolean();
    private List<ShareVanRoutingPlan> routingPlans;
    private Route route;
    private RootNode currentNode;
    private ObservableField<RootNode> selectedRoutNode = new ObservableField<>();
    private ObservableBoolean isNotNextWarehouse = new ObservableBoolean(); // check tra còn kho tiếp theo không , True: hết kho, false : còn kho
    private LatLng currentRoutingLocation;
    private List<BaseNode> lstCartNode;
    private HashMap<String, List<RootNode>> hashMapLocation = new HashMap<>();


    public MapsVM(@NonNull Application application) {
        super(application);
        routingPlans = new ArrayList<>();
        lstCartNode = new ArrayList<>();
        isNotNextWarehouse.set(false);
    }

    /**
     * lấy danh sách routing plan
     *
     * @param runUi callBack
     */
    public void getRouting(RunUi runUi) {
        routingPlans.clear();
        isLoading.set(true);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String currentDay = format.format(new Date());
//        RoutingApi.getRouting(currentDay, StaticData.getDriver().getId(), o -> {
        ArrayList<Integer> status = new ArrayList<>();
        status.add(0);
        status.add(1);
        status.add(2);
        status.add(3);
        status.add(4);
        RoutingApi.getRouting(currentDay, StaticData.getDriver().getId(), "", status, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                routingPlans.addAll((List<ShareVanRoutingPlan>) o);
                setDataToListNode();
                isLoading.set(false);
                runUi.run("getRouting");
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
            }
        });
    }

    /**
     * lấy danh sách lat long đường đi của tài xế để vẽ lộ trình
     *
     * @param start điểm bắt đầu
     * @param end   điểm kết thúc
     */
    public void getDestination(LatLng start, LatLng end, String color, RunUi runUi) {
        String requestUrl = "https://maps.googleapis.com/maps/api/directions/json?" +
                "mode=driving&"
                + "transit_routing_preference=less_driving&"
                + "origin=" + start.latitude + "," + start.longitude + "&"
                + "destination=" + end.latitude + "," + end.longitude + "&"
                + "key=" + AppController.API_GOOGLE_MAP_DESTINATION;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                requestUrl, null,
                response -> {
                    try {
                        if (response == null)
                            return;
                        JSONArray jsonArray = response.getJSONArray("routes");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonRoute = jsonArray.getJSONObject(i);
                            route = new Route();

                            JSONObject overview_polylineJson = jsonRoute.getJSONObject("overview_polyline");
                            JSONArray jsonLegs = jsonRoute.getJSONArray("legs");
                            JSONObject jsonLeg = jsonLegs.getJSONObject(0);
                            JSONObject jsonDistance = jsonLeg.getJSONObject("distance");
                            JSONObject jsonDuration = jsonLeg.getJSONObject("duration");
                            JSONObject jsonEndLocation = jsonLeg.getJSONObject("end_location");
                            JSONObject jsonStartLocation = jsonLeg.getJSONObject("start_location");

                            route.distance = new Distance(jsonDistance.getString("text"), jsonDistance.getInt("value"));
                            route.duration = new Duration(jsonDuration.getString("text"), jsonDuration.getInt("value"));
                            route.endAddress = jsonLeg.getString("end_address");
                            route.startAddress = jsonLeg.getString("start_address");
                            route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
                            route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
                            route.points = decodePoly(overview_polylineJson.getString("points"));

//                                JSONObject route = jsonArray.getJSONObject(i);
//                                JSONObject poly = route.getJSONObject("overview_polyline");
//                                String polyline = poly.getString("points");
//                                polyLineList = decodePoly(polyline);

                        }
                        runUi.run("drawDestination", color);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        requestQueue.add(jsonObjectRequest);
    }

    /**
     * lấy khoảng cách và thời gian từ điểm đến điểm
     *
     * @param from  từ
     * @param to    đến
     * @param runUi callback
     */
    public void getDirection(LatLng from, LatLng to, RunUi runUi) {
        isLoading.set(true);
        DriverApi.getDirection(from, to, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                runUi.run("getDirectionSuccess", o);
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
            }
        });
    }


    /**
     * trả về location điểm đến tiếp theo
     *
     * @return
     */
    public ItemRoutingNote currentLocationRoutingPlan() {
        isNotNextWarehouse.set(true);
        for (int i = 0; i < lstCartNode.size(); i++) {
            RootNode rootNode = (RootNode) lstCartNode.get(i);
            for (BaseNode childNode : rootNode.getChildNode()) {
                ItemRoutingNote rout = (ItemRoutingNote) childNode;
                if (rout.getStatus() == 0 || rout.getStatus() == 1 || rout.getStatus() == 4) {
                    currentNode = rootNode;
                    isNotNextWarehouse.set(false);
                    return rout;
                }
            }
        }
        return null;
    }

    /**
     * gom nhóm routing theo kho
     */
    private void setDataToListNode() {
        ErrorHandler.create().run(() -> {
            lstCartNode.clear();
            if (TsUtils.isNotNull(routingPlans)) {
                String currentWarehouse = "";//kho hiện tại
                List<BaseNode> childItems = null;
                for (ShareVanRoutingPlan routing : routingPlans) {
                    //nếu tuyến mới có kho giống kho hiện tại thì gom vào list
                    if (routing.getWarehouse_name().equals(currentWarehouse)) {
                        assert childItems != null;
                        childItems.add(new ItemRoutingNote(routing.getId()
                                , routing.getRouting_plan_day_code()
                                , routing.getOrder_number()
                                , routing.getBill_routing_name()
                                , routing.getType()
                                , routing.getStatus()
                                , routing.getLatitude()
                                , routing.getLongitude()
                                , routing.isCheck_point(),
                                routing.getTrouble_type()));
                    } else {//nếu ko phải thì thêm nhóm mới vào list Node
                        childItems = new ArrayList<>();
                        RootNode entity = new RootNode(childItems, routing.getWarehouse_name(), routing.getAddress(), routing.getPhone(), routing.getLatitude(), routing.getLongitude(), routing.getLatitude() + "," + routing.getLongitude());
                        childItems.add(new ItemRoutingNote(routing.getId()
                                , routing.getRouting_plan_day_code()
                                , routing.getOrder_number()
                                , routing.getBill_routing_name()
                                , routing.getType()
                                , routing.getStatus()
                                , routing.getLatitude()
                                , routing.getLongitude()
                                , routing.isCheck_point(),
                                routing.getTrouble_type()));

                        entity.setExpanded(false);
                        lstCartNode.add(entity);
                        currentWarehouse = routing.getWarehouse_name();
                    }
                }
            }
            if (lstCartNode.size() > 0) {
                ((RootNode) lstCartNode.get(0)).setExpanded(true);
            }

        });
        setHashMapLocation();
        setStatusListNode();
    }

    private void setStatusListNode() {
        for (int i = 0; i < lstCartNode.size(); i++) {
            BaseNode baseNode = lstCartNode.get(i);
            for (BaseNode item : baseNode.getChildNode()) {
                ItemRoutingNote note = (ItemRoutingNote) item;
                if (note.getStatus() == 0 || note.getStatus() == 1|| note.getStatus() == 4) {
                    ((RootNode) lstCartNode.get(i)).setStatus();
                    break;
                }
            }
        }
    }

    private void setHashMapLocation() {
        for (int i = 0; i < lstCartNode.size(); i++) {
            RootNode rootNode = (RootNode) lstCartNode.get(i);
            List<RootNode> rootNodeList = new ArrayList<>();
            if (hashMapLocation.get(rootNode.getLat_lng()) == null) {
                rootNodeList.add(rootNode);
                hashMapLocation.put(rootNode.getLat_lng(), rootNodeList);
            } else {
                rootNodeList.addAll(hashMapLocation.get(rootNode.getLat_lng()));
                rootNodeList.add(rootNode);
                hashMapLocation.put(rootNode.getLat_lng(), rootNodeList);
            }
        }
    }


    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


    public void updateSosStatus(RunUi runUi) {
        SOSApi.updateSosStatus(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                if ("200".equals(o)) {
                    runUi.run("updateSosSuccess");
                } else {
                    runUi.run("updateSosFail");
                }
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("updateSosFail");
            }
        });
    }
}
