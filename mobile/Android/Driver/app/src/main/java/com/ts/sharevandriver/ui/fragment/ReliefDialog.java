package com.ts.sharevandriver.ui.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.model.LatLng;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.adapter.XBaseAdapter;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.databinding.ReliefDialogBinding;
import com.ts.sharevandriver.model.SOSNumber;
import com.ts.sharevandriver.utils.StringUtils;
import com.ts.sharevandriver.viewmodel.ReliefDialogVM;
import com.tsolution.base.listener.AdapterListener;

import javax.annotation.Resource;
import javax.annotation.Resources;


public class ReliefDialog extends DialogFragment {
    ReliefDialogVM reliefDialogVM;
    ReliefDialogBinding mBinding;
    XBaseAdapter adapter;
    View.OnClickListener onClickListener;
    DialogConfirm confirm;
    SOSNumber sosNumber;
    Integer REQUEST_PHONE_CALL=999;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.relief_dialog, container, false);
        reliefDialogVM = ViewModelProviders.of(this).get(ReliefDialogVM.class);
        mBinding.setViewModel(reliefDialogVM);
        initView();
        reliefDialogVM.getSOSNumbers(this::runUi);
        return mBinding.getRoot();
    }
    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.SUCCESS_API:
                adapter.notifyDataSetChanged();
                break;

            case Constants.FAIL_API:
                break;
        }
    }
    private void initView(){
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        setUpRecycleView();
        mBinding.btnClose.setOnClickListener(v->{
            dismiss();
        });
    }

    private void setUpRecycleView(){
        adapter= new XBaseAdapter(R.layout.relief_dialog_item, reliefDialogVM.getListSosNumbers(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
//                showDialogConfirm(o);
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        mBinding.rcSOSNumber.setAdapter(adapter);
        LinearLayoutManager layoutManager= new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false);
        mBinding.rcSOSNumber.setLayoutManager(layoutManager);
    }
//    private void showDialogConfirm(Object o){
//        sosNumber= (SOSNumber) o;
//        if(sosNumber.getPhone() !=null){
//            confirm= new DialogConfirm(getResources().getString(R.string.SOSNumber_title),
//                    getString(R.string.call) + ": " + sosNumber.getPhone()).setOnClickListener(v->{
//                gotoCallPhoneSOS(sosNumber.getPhone());
//
//            });
//            confirm.show(getFragmentManager(),"ABC");
//        }
//    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }


//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if(requestCode==REQUEST_PHONE_CALL){
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
//                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sosNumber.getPhone()));// Initiates the Intent
//                startActivity(intent);
//                confirm.dismiss();
//                dismiss();
//            }
//        }
//    }
//
//    private void gotoCallPhoneSOS(String phoneNumber) {
//        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
//        }
//        else
//        {
//            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));// Initiates the Intent
//            startActivity(intent);
//            confirm.dismiss();
//            dismiss();
//        }
//    }
}
