package com.ts.sharevandriver.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.adapter.PagerAdapter;
import com.ts.sharevandriver.adapter.XBaseAdapter;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.databinding.StatisticalFragmentBinding;
import com.ts.sharevandriver.model.StatisticalWalletDTO;
import com.ts.sharevandriver.viewmodel.StatisticalVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

public class StatisticalFragment extends BaseFragment {
    StatisticalVM statisticalVM;
    StatisticalFragmentBinding mBinding;
    XBaseAdapter adapterTitle;
    int currentPosition = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        statisticalVM = (StatisticalVM) viewModel;
        mBinding = (StatisticalFragmentBinding) binding;
        initView();
        getData();
        return view;
    }

    private void initView() {
        setUpRecycleView();
    }

    private void setUpViewPager() {
        PagerAdapter myPagerAdapter = new PagerAdapter(getChildFragmentManager());
        for (StatisticalWalletDTO item : statisticalVM.getStatisticalWalletDTOS()) {
            StatisticalChildFragment viewPagerItem = new StatisticalChildFragment(item.getMonthInt(),item.getYearInt(),item);
            myPagerAdapter.addFragment(viewPagerItem);
        }

        mBinding.frameContainer.setAdapter(myPagerAdapter);
        mBinding.frameContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position != currentPosition) {
                    statisticalVM.getStatisticalWalletDTOS().get(position).checked = true;
                    statisticalVM.getStatisticalWalletDTOS().get(currentPosition).checked = false;
                    adapterTitle.notifyItemChanged(position);
                    adapterTitle.notifyItemChanged(currentPosition);
                    currentPosition = position;
                    mBinding.rcTitle.smoothScrollToPosition(position);
                }

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void getData() {
        statisticalVM.getStatisticalWallet(this::runUi);
    }


    private void setUpRecycleView() {
        adapterTitle = new XBaseAdapter(R.layout.statistical_title_item, statisticalVM.getStatisticalWalletDTOS(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                int position = ((StatisticalWalletDTO) o).index - 1;
                statisticalVM.getStatisticalWalletDTOS().get(position).checked = true;
                statisticalVM.getStatisticalWalletDTOS().get(currentPosition).checked = false;
                adapterTitle.notifyItemChanged(position);
                adapterTitle.notifyItemChanged(currentPosition);
                mBinding.frameContainer.setCurrentItem(position);
                currentPosition = position;
            }

            @Override
            public void onItemLongClick(View view, Object o) {
            }
        });
        mBinding.rcTitle.setAdapter(adapterTitle);
        mBinding.rcTitle.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));


    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.SUCCESS_API:
                adapterTitle.notifyDataSetChanged();
                setUpViewPager();
                if (statisticalVM.getStatisticalWalletDTOS() != null && statisticalVM.getStatisticalWalletDTOS().size() > 0)
                    statisticalVM.getStatisticalWalletDTOS().get(0).checked = true;
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.statistical_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return StatisticalVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcContent;
    }
}
