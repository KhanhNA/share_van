package com.ts.sharevandriver.viewmodel;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.view.View;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.api.MarketPlaceApi;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.model.ApiResponseModel;
import com.ts.sharevandriver.model.Area;
import com.ts.sharevandriver.model.DriverMarketInfo;
import com.ts.sharevandriver.socket_notification.SocketResult;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarketPlaceVM extends BaseViewModel {
    private ObservableBoolean isLoading = new ObservableBoolean();
    private ObservableField<DriverMarketInfo> driverMarketInfo = new ObservableField<>();
    private List<Area> listArea;
    private ObservableBoolean isOnline = new ObservableBoolean(false);
    private boolean onReceiveRout;


    public MarketPlaceVM(@NonNull Application application) {
        super(application);
        listArea = new ArrayList<>();
        driverMarketInfo.set(new DriverMarketInfo());
    }


    public boolean isValid(Context context) {
        boolean isValid = true;
        DriverMarketInfo info = driverMarketInfo.get();
        if (info.getFrom_area() == null) {
            isValid = false;
            addError("fromArea", context.getString(R.string.please_select_from_area), true);
        } else {
            clearErro("fromArea");
        }

        if (info.getTo_area() == null) {
            isValid = false;
            addError("toArea", context.getString(R.string.please_select_to_area), true);
        } else {
            clearErro("toArea");
        }
        if (info.getAvailable_capacity() == null) {
            isValid = false;
            addError("capacity", context.getString(R.string.please_enter_available_capacity), true);
        } else {
            clearErro("capacity");
        }

        if (info.getAvailable_weight() == null) {
            isValid = false;
            addError("weight", context.getString(R.string.please_enter_available_weight), true);
        } else {
            clearErro("weight");
        }

        return isValid;
    }

    public void getAreas(View v, RunUi runUi) {
        isLoading.set(true);
        MarketPlaceApi.getAreas(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                listArea.addAll((Collection<? extends Area>) o);
                runUi.run("getAreaSuccess", v);
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run("getAreaFail");
            }
        });
    }


    public void registerInfo(RunUi runUi) {
        isLoading.set(true);
        MarketPlaceApi.registerInfo(driverMarketInfo.get(), new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                isOnline.set(true);
                runUi.run("connectSuccess");
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                isOnline.set(false);
                runUi.run("connectFail");
            }
        });
    }


    public void setSocketId(Context context, String socketId, RunUi runUi) {
        Log.e("Socket", "socket id: " + socketId);
        MarketPlaceApi.setSocketId(context, socketId, new IResponse() {
            @Override
            public void onSuccess(Object o) {
            }

            @Override
            public void onFail(Throwable error) {
                isOnline.set(false);
                runUi.run("connectFail");
            }
        });
    }

    public void getDriverInfo(RunUi runUi) {
        isLoading.set(true);
        MarketPlaceApi.getDriverInfo(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                driverMarketInfo.set(((List<DriverMarketInfo>) o).get(0));
                runUi.run("getDriverMarketInfoSuccess");

            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
            }
        });
    }

    public void acceptRout(Integer bidding_package_id, boolean isAccept, RunUi runUi) {
        isLoading.set(true);
        MarketPlaceApi.acceptOrder(bidding_package_id, isAccept, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                ApiResponseModel model = (ApiResponseModel) o;
                if (model.id != null) {
                    if (isAccept) {
                        runUi.run("acceptSuccess", model.id);
                    }
                } else {
                    runUi.run("acceptFail");
                }
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run("acceptFail");
            }
        });
    }
}
