package com.ts.sharevandriver.ui.fragment;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.databinding.DialogChangeConfigBinding;
import com.ts.sharevandriver.utils.StringUtils;

@SuppressLint("ValidFragment")
public class DialogServerConfig extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogChangeConfigBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_change_config, container, false);

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        SharedPreferences sharedPreferences = AppController.getInstance().getSharePre();
        SharedPreferences.Editor editor = AppController.getInstance().getEditor();

        binding.txtMessage.setText("Nếu thay đổi link server thì bấm confirm -> thoát app rồi vào lại." +
                "\nNếu thay đổi database thì ko cần thoát app..");

        String urlLogin = sharedPreferences.getString("U_LOGIN", AppController.SERVER_URL);
        String urlLocation = sharedPreferences.getString("U_LOCATION", AppController.LOCATION_SERVICE_URL);
        String urlLogistic = sharedPreferences.getString("U_DATABASE", AppController.DATABASE);

        binding.edLogin.setText(urlLogin);
        binding.edLocationServer.setText(urlLocation);
        binding.edDatabase.setText(urlLogistic);

        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        binding.btnCancel.setOnClickListener(v -> this.dismiss());
        binding.btnConfirm.setOnClickListener(v -> {
            if (StringUtils.isNotNullAndNotEmpty(binding.edLogin.getText().toString())) {
                editor.putString("U_LOGIN", binding.edLogin.getText().toString()).apply();
            }
            if (StringUtils.isNotNullAndNotEmpty(binding.edLocationServer.getText().toString())) {
                editor.putString("U_LOCATION", binding.edLocationServer.getText().toString()).apply();
            }
            if (StringUtils.isNotNullAndNotEmpty(binding.edDatabase.getText().toString())) {
                editor.putString("U_DATABASE", binding.edDatabase.getText().toString()).apply();
            }
            dismiss();
        });
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

}
