package com.ts.sharevandriver.model;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ts.sharevandriver.base.AppController;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingInformation extends BaseModel {
    private transient int biddingOrderStatus;

    //bidding information
    private Integer id;
    private String cargo_number;
    private String cargo_status;
    private String confirm_time;
    private OdooDateTime max_date;
    private String from_depot_id;
    private String to_depot_id;
    private Double distance;
    private String size_id;
    private Integer quantity;
    private OdooDateTime from_receive_time;
    private OdooDateTime to_receive_time;
    private OdooDateTime from_return_time;
    private OdooDateTime to_return_time;
    private String biding_order_id;
    private OdooDateTime create_date;
    private String product_type_id;
    private String biding_id;
    private String company_id;
    private String driver_name;
    private String phone;
    private String van_id;
    private Double total_weight;
    private Integer total_cargo;

    private SizeStandard size_standard;
    private Double price;
    private Depot from_depot;
    private Depot to_depot;
    private String bidding_status;
    private String bidding_type;
    private int from_latitude;
    private int to_latitude;
    private String type;
    private String status;
    private boolean isCheckDuration = true;
    private String bidding_order_number;
    private Integer create_uid;
    private String driver_id;
    private Integer cargo_id;
    private String note;
    private String bidding_order_receive_id;
    private String bidding_order_return_id;
    private Integer bidding_vehicle_id;
    private Integer price_bidding_order;
    private boolean isDurationCountdow = false;
    private String write_date;
    private int bidding_package_id;
    private OdooDateTime max_confirm_time;
    private BiddingVehicle bidding_vehicles;

    private Integer accept_time_package;//thời gian để xác nhận - đối với đơn ở market place

    //list cargo của cả đơn
    private List<CargoType> cargo_type;


    public String getDistanceStr() {
        if (distance == null) return 0 + " km";
        return AppController.getInstance().formatNumber(distance / 1000) + " km";
    }

    public String getTotalWeightStr() {
        if (total_weight == null) return 0 + " kg";
        return AppController.getInstance().formatNumber(total_weight) + " kg";
    }

    public String getPriceStr() {
        if (price == null) return "";
        return AppController.getInstance().formatCurrency(price);
    }

}
