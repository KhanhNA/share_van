package com.ts.sharevandriver.ui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.entity.node.BaseNode;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.appbar.AppBarLayout;
import com.king.zxing.Intents;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.ScreenUtils;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.adapter.GridImageAdapter;
import com.ts.sharevandriver.adapter.ImageBaseAdapter;
import com.ts.sharevandriver.adapter.ItemRoutAdapter;
import com.ts.sharevandriver.adapter.XBaseAdapter;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.databinding.ActivityRoutingDetailBinding;
import com.ts.sharevandriver.enums.RoutingPlanDayStatus;
import com.ts.sharevandriver.enums.RoutingType;
import com.ts.sharevandriver.enums.TroubleType;
import com.ts.sharevandriver.enums.VehicleStateStatus;
import com.ts.sharevandriver.model.BillPackage;
import com.ts.sharevandriver.model.ShareVanRoutingPlan;
import com.ts.sharevandriver.model.section.ItemRoutingNote;
import com.ts.sharevandriver.model.section.RootNode;
import com.ts.sharevandriver.service.location_service.NetworkManager;
import com.ts.sharevandriver.ui.fragment.DialogConfirm;
import com.ts.sharevandriver.ui.fragment.EditBillPackageFragment;
import com.ts.sharevandriver.ui.fragment.NotContactCustomerDialog;
import com.ts.sharevandriver.ui.fragment.RatingCustomerFragment;
import com.ts.sharevandriver.ui.fragment.RoutingPlanFragment;
import com.ts.sharevandriver.utils.ToastUtils;
import com.ts.sharevandriver.viewmodel.BillDetailVM;
import com.ts.sharevandriver.widget.AnimationUtil;
import com.ts.sharevandriver.widget.FullyGridLayoutManager;
import com.ts.sharevandriver.widget.GlideEngine;
import com.ts.sharevandriver.widget.OnSingleClickListener;
import com.ts.sharevandriver.widget.QrScannerActivity;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.AdapterListener;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * @author Good_Boy
 */
public class RoutingDetailActivity extends BaseActivity<ActivityRoutingDetailBinding> implements NetworkManager.NetworkHandler {
    final int PICK_IMAGE_PERMISSIONS_REQUEST_CODE = 999;
    final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private final int REQUEST_CODE_CAMERA = 0X01;
    private final String FROM_ROUTING_DETAIL_ACTIVITY = "FROM_ROUTING_DETAIL_ACTIVITY";
    private final AppBarLayout.OnOffsetChangedListener appBarOffsetChangedListener = (appBarLayout, verticalOffset) -> {
        animateFabButton(Math.abs(verticalOffset) != appBarLayout.getTotalScrollRange());

    };
    boolean isOnline;
    NetworkManager networkManager;
    BillDetailVM billDetailVM;
    FusedLocationProviderClient location;
    DialogConfirm confirm;
    NotContactCustomerDialog notContactCustomerDialog;
    //root_node
    ItemRoutAdapter routNodeAdapter;
    RootNode rootNode;
    ItemRoutingNote currentRout;
    int node_position;// vị trí của rooting node trong root node
    //data result
    int status;
    boolean isChangeData = false;
    boolean isCancelOrder = false;
    private String FROM_FRAGMENT = "";
    private GridImageAdapter imageSelectAdapter;
    private ImageBaseAdapter imageAdapter;
    private XBaseAdapter billPackageAdapter;
    private int position;
    private HashMap<Integer, Integer> hashMapStatusRouting = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        billDetailVM = (BillDetailVM) viewModel;
//        location = LocationServices.getFusedLocationProviderClient(this);
        initToolbar();
        initView();

        binding.mainAppbar.addOnOffsetChangedListener(appBarOffsetChangedListener);


        initAdapterImage(savedInstanceState);
    }

    private void initToolbar() {
        binding.txtTitle.setText(R.string.bill_lading_detail);
        binding.btnBack.setOnClickListener(v -> onBackPressed());

    }

    @Override
    protected void onStart() {
        super.onStart();
        networkManager = new NetworkManager(this, this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }

    private void initLayoutManager() {
        FullyGridLayoutManager manager = new FullyGridLayoutManager(this,
                4, GridLayoutManager.VERTICAL, false);
        binding.rcImages.setLayoutManager(manager);

        binding.rcImages.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(this, 8), false));
    }

    private void initAdapterImage(Bundle savedInstanceState) {
        initLayoutManager();
        //list select image
        imageSelectAdapter = new GridImageAdapter(this, this::pickImage);
        imageSelectAdapter.setSelectMax(12);
        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList("selectorList") != null) {
            imageSelectAdapter.setList(savedInstanceState.getParcelableArrayList("selectorList"));
        }
        imageSelectAdapter.setOnClear(() -> {
            billDetailVM.isNotEmptyImage.set(false);
        });
        binding.rcImages.setAdapter(imageSelectAdapter);
        imageSelectAdapter.setOnItemClickListener((v, position) -> {
            openOptionImage(position);
        });
        //list rout image
        imageAdapter = new ImageBaseAdapter(this, R.layout.item_image_100dp, billDetailVM.getListImageRouting());
        binding.rcRoutingImages.setAdapter(imageAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        binding.rcRoutingImages.setLayoutManager(linearLayoutManager);

    }

    private void openOptionImage(int position) {
        List<LocalMedia> selectList = imageSelectAdapter.getData();
        if (selectList.size() > 0) {
            LocalMedia media = selectList.get(position);
            String mimeType = media.getMimeType();
            int mediaType = PictureMimeType.getMimeType(mimeType);
            if (mediaType == PictureConfig.TYPE_VIDEO) {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .externalPictureVideo(TextUtils.isEmpty(media.getAndroidQToPath()) ? media.getPath() : media.getAndroidQToPath());
            } else {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .isNotPreviewDownload(true)
                        .openExternalPreview(position, selectList);
            }
        }
    }


    @AfterPermissionGranted(PICK_IMAGE_PERMISSIONS_REQUEST_CODE)
    public void selectImages() {
        String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, permissions)) {
            pickImage();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.permissions_not_granted),
                    PICK_IMAGE_PERMISSIONS_REQUEST_CODE, permissions);
        }

    }


    public void cancelOrder() {
        billDetailVM.cancelOrder(this::runUi);
    }

    @AfterPermissionGranted(REQUEST_CODE_CAMERA)
    private void openQRScanActivity() {
        if (!billDetailVM.routingPlan.get().getQr_gen_check() && !billDetailVM.routingPlan.get().getSo_type()) {
            ToastUtils.showToast(this, getResources().getString(R.string.qr_code_has_not_been_initialized), getResources().getDrawable(R.drawable.ic_danger));
        } else if (billDetailVM.getChecked().get()) {
            ToastUtils.showToast(this, getResources().getString(R.string.scan_qr_code_complete), getResources().getDrawable(R.drawable.ic_danger));
        } else {
            String[] perms = {Manifest.permission.CAMERA};
            if (EasyPermissions.hasPermissions(this, perms)) {
                gotoScanQrCode();
            } else {
                // Do not have permissions, request them now
                EasyPermissions.requestPermissions(this, getString(R.string.permissions_not_granted),
                        REQUEST_CODE_CAMERA, perms);
            }
        }
    }

    private void gotoScanQrCode() {
        Intent intent = new Intent(this, QrScannerActivity.class);
        Bundle bundle = new Bundle();
        if (!billDetailVM.routingPlan.get().getSo_type()) {
            billDetailVM.routingPlanClone.getList_bill_package().clear();
            for (BillPackage item : billDetailVM.getPackageList()) {
                billDetailVM.routingPlanClone.getList_bill_package().add((BillPackage) item.clone());
            }
            bundle.putString(Constants.FROM_ACTIVITY, FROM_ROUTING_DETAIL_ACTIVITY);
            bundle.putBoolean(Constants.MULTI_SCAN_QR_CODE, true);
            bundle.putSerializable(Constants.DATA, billDetailVM.routingPlanClone);
            intent.putExtra(Constants.QR_DATA, billDetailVM.getQrDataChecked());
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, Constants.REQUEST_CODE_SCAN);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
                pickImage();
            } else if (requestCode == REQUEST_CODE_CAMERA) {
                gotoScanQrCode();
            }
        } else {
            Toast.makeText(this, R.string.permissions_not_granted, Toast.LENGTH_SHORT).show();
        }

    }


    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isScanCodeAndResultOk(requestCode, resultCode)) {
            billDetailVM.getChecked().set(data.getBooleanExtra(Constants.COMPLETE, false));
            billDetailVM.routingPlanClone = (ShareVanRoutingPlan) data.getSerializableExtra(Constants.DATA);
            billDetailVM.setQrDataChecked((HashMap<Integer, HashMap<String, Boolean>>) data.getSerializableExtra(Constants.QR_DATA));
            billDetailVM.getPackageList().clear();
            for (BillPackage item : billDetailVM.routingPlanClone.getList_bill_package()) {
                billDetailVM.getPackageList().add((BillPackage) item.clone());
                if (!item.getQuantity_import().equals(item.getQuantityQrChecked())) {
                    billDetailVM.mapEditPackage.get().put(item.getQr_char(), true);
                } else {
                    if (billDetailVM.mapEditPackage.get().get(item.getQr_char()) != null) {
                        billDetailVM.mapEditPackage.get().remove(item.getQr_char());
                    }
                }
            }
            billDetailVM.mapEditPackage.notifyChange();
            billPackageAdapter.notifyDataSetChanged();

        } else if (requestCode == Constants.RESULT_FRAGMENT && resultCode == Constants.RESULT_OK) {
            int starNumber = data.getIntExtra(Constants.DATA_RESULT, 1);
            binding.btnStarCustomerRating.setText(starNumber + "");
            billDetailVM.routingPlan.get().setStatus(2);
            billDetailVM.getIsRatingCustomer().set(true);
        } else if (requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK) {
            String result = "";
            if (data != null) {
                result = data.getStringExtra(Intents.Scan.RESULT);
            }
            if (result.equals(billDetailVM.routingPlan.get().getQr_so())) {
                billDetailVM.getChecked().set(true);
                ToastUtils.showToast(this, getString(R.string.success), getResources().getDrawable(R.drawable.ic_check));
                billDetailVM.setValidQrSo(true);
            } else {
                billDetailVM.getChecked().set(false);
                ToastUtils.showToast(this, getString(R.string.invalid_qr_code), getResources().getDrawable(R.drawable.picture_icon_warning));
            }
        }
    }

    private void pickImage() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .maxSelectNum(12)
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .selectionMode(PictureConfig.MULTIPLE)
                .selectionMedia(imageSelectAdapter.getData())
                .forResult(new RoutingDetailActivity.MyResultCallback(imageSelectAdapter, billDetailVM));
    }


    private void initView() {
        Intent intent = getIntent();
        String routingCode = intent.getStringExtra(Constants.ITEM_ID);
        if (intent.hasExtra(Constants.FROM_FRAGMENT)) {
            FROM_FRAGMENT = intent.getStringExtra(Constants.FROM_FRAGMENT);
        }
        position = intent.getIntExtra(RoutingPlanFragment.POSITION, 0);
        billDetailVM.getIsRequire().set(intent.getBooleanExtra(RoutingPlanFragment.IS_REQUIRE, false));
        rootNode = (RootNode) intent.getSerializableExtra(RoutingPlanFragment.ROOT_NODE);
        if (intent.hasExtra(RoutingPlanFragment.HASH_MAP_STATUS_ROUTING)) {
            hashMapStatusRouting = (HashMap<Integer, Integer>) intent.getSerializableExtra(RoutingPlanFragment.HASH_MAP_STATUS_ROUTING);
        }
        if (intent.hasExtra(RoutingPlanFragment.LIST_ROUTING_PLAN_ID) && billDetailVM.getIsRequire().get()) {
            billDetailVM.setListRoutingPlanId(intent.getIntegerArrayListExtra(RoutingPlanFragment.LIST_ROUTING_PLAN_ID));
        }
        isChangeData = intent.getBooleanExtra("IS_CHANGE", false);
        node_position = intent.getIntExtra(RoutingPlanFragment.NODE_POSITION, 0);
        if (rootNode != null) {
            List<BaseNode> noteList = rootNode.getChildNode();
            if (noteList.size() > 1) {
                if (node_position >= 0) {
                    currentRout = ((ItemRoutingNote) rootNode.getChildNode().get(node_position));
                    currentRout.checked = true;
                } else {
                    currentRout = (ItemRoutingNote) rootNode.getChildNode().get(0);
                }
                routNodeAdapter = new ItemRoutAdapter(billDetailVM, rootNode.getChildNode(), new AdapterListener() {
                    @Override
                    public void onItemClick(View view, Object o) {
                        int position = ((ItemRoutingNote) o).position;
                        binding.rcRouts.smoothScrollToPosition(position);
                        currentRout = ((ItemRoutingNote) rootNode.getChildNode().get(position));
                        currentRout.checked = true;
                        routNodeAdapter.notifyItemChanged(position);
                        if (node_position >= 0) {
                            ((ItemRoutingNote) noteList.get(node_position)).checked = false;
                            routNodeAdapter.notifyItemChanged(node_position);
                        }
                        node_position = position;
                        billDetailVM.getBillDetail(currentRout.getRouting_plan_day_code(), RoutingDetailActivity.this::runUi);
                    }

                    @Override
                    public void onItemLongClick(View view, Object o) {

                    }
                });
                binding.rcRouts.setAdapter(routNodeAdapter);
                binding.rcRouts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
                binding.rcRouts.smoothScrollToPosition(node_position);
            } else if (noteList.size() == 1) {
                currentRout = (ItemRoutingNote) rootNode.getChildNode().get(0);
            }
        }

        if (routingCode != null) {
            billDetailVM.getBillDetail(routingCode, this::runUi);
        }

        billPackageAdapter = new XBaseAdapter(R.layout.item_bill_package
                , billDetailVM.getPackageList(), this) {
            @Override
            public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
                super.onBindViewHolder(viewHolder, position);
                viewHolder.bindWithVM(billDetailVM);
            }
        };
        binding.rcBillPackage.setAdapter(billPackageAdapter);

        binding.txtRoutingCodeWarehouseReturn.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Good_Boy", billDetailVM.getRoutingPlan().get().getRouting_plan_day_code());
                clipboard.setPrimaryClip(clip);
                showToast();
            }
        });
        binding.txtRoutingCode.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Good_Boy", billDetailVM.getRoutingPlan().get().getBill_routing_name());
                clipboard.setPrimaryClip(clip);
                showToast();
            }
        });

    }

    public void showToast() {
        Toast.makeText(this, getResources().getString(R.string.COPIED_TO_CLIPBOARD), Toast.LENGTH_SHORT).show();
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onItemClick(View v, Object o) {
        if (o == null) {
            switch (v.getId()) {
                case R.id.btnConfirm:
                    if(isOnline){
                        showDialogConfirm();
                    }
                    else{
                        ToastUtils.showToast(getResources().getString(R.string.network_error));
                    }
                    break;
                case R.id.llShowDialogSendNotification:
                    showDialogSendNotification();
                    break;
                case R.id.btnShowQR:
                    showOrHideQR();
                    break;
                case R.id.btnReport:
                case R.id.icClear:
                    editBillPackage();
                    break;
                case R.id.btnAddImg:
                    selectImages();
                    break;
                case R.id.btnGotoRatingCustomer:
                    gotoRatingCustomerFragment();
                    break;
                case R.id.btnQR:
                    openQRScanActivity();
                    break;
            }
            return;
        }

        BillPackage billPackage = (BillPackage) o;
        super.onItemClick(v, o);
        if (v.getId() == R.id.btnEdit) {
            if (billPackage.getQuantityQrChecked() == 0) {
                ToastUtils.showToast(getString(R.string.Do_not_modify_the_package));
            } else {
                showDialogEditBillPackage(billPackage);
            }
        }
    }

    private void showDialogEditBillPackage(BillPackage billPackage) {
        EditBillPackageFragment editDialog =
                new EditBillPackageFragment(billPackage,
                        billDetailVM.routingPlan.get().getList_bill_package().get(billPackage.index - 1).getQuantity_import(),
                        billDetailVM.routingPlan.get().getType(),
                        selected -> {
                            if (billDetailVM.checkIsUpdate(selected, billPackage)) {
                                selected.setEdited(true);
                                billDetailVM.mapEditPackage.get().put(selected.getQr_char(), true);
                            } else {
                                selected.setEdited(false);
                                billDetailVM.mapEditPackage.get().remove(selected.getQr_char());
                            }
                            billDetailVM.mapEditPackage.notifyChange();
                            billDetailVM.getPackageList().set(billPackage.index - 1, selected);
                            billPackageAdapter.notifyItemChanged(billPackage.index - 1);
                        });
        editDialog.show(getSupportFragmentManager(), editDialog.getTag());
    }

    public void showDialogConfirm() {
        if (billDetailVM.routingPlan.get().getType().equals(RoutingType.Pickup) && !billDetailVM.isEdit.get()) {
            if (!billDetailVM.getChecked().get()) {
                ToastUtils.showToast(this, getResources().getString(R.string.Scan_qr_code_not_yet_complete), getResources().getDrawable(R.drawable.ic_danger));
                return;
            }
        }
        confirm = new DialogConfirm(getString(R.string.CONFIRM)
                , getString(R.string.msg_confirm_update_order_status)).setOnClickListener(v -> {
            confirmRouting(false);
            confirm.dismiss();
        });
        confirm.show(getSupportFragmentManager(), confirm.getTag());
    }

    public void editBillPackage() {
        if (billDetailVM.getIsEdit().get()) {
            if (billDetailVM.getMapEditPackage().get().size() > 0) {
                confirm = new DialogConfirm(getString(R.string.CONFIRM)
                        , getString(R.string.msg_cancel_edit)).setOnClickListener(v -> {
                    billDetailVM.getIsEdit().set(false);
                    billDetailVM.cloneList();
                    billDetailVM.getMapEditPackage().get().clear();
                    billPackageAdapter.notifyDataSetChanged();
                    confirm.dismiss();
                });
                confirm.show(getSupportFragmentManager(), confirm.getTag());
            } else {
                billDetailVM.getIsEdit().set(false);
                billPackageAdapter.notifyDataSetChanged();
            }
        } else {
            billDetailVM.getIsEdit().set(true);
            billPackageAdapter.notifyDataSetChanged();
        }
    }

    public void showOrHideQR() {
        billDetailVM.getIsShowQR().set(!billDetailVM.getIsShowQR().get());
    }

    public void showRatingCustomerMessenger() {
        confirm = new DialogConfirm(getString(R.string.rating_customer_title), getString(R.string.rating_customer_description), true)
                .setOnClickListener(v -> {
                    billDetailVM.updateRatingCustomer();
                    confirm.dismiss();
                    if (v.getId() == R.id.btnConfirm) {
                        gotoRatingCustomerFragment();
                    }
                }).setAnimationFile("animation/rating.json");
        confirm.show(getSupportFragmentManager(), "abc");
    }


    public void showDialogSendNotification() {
        if (billDetailVM.getIsRequire().get() && !billDetailVM.routingPlan.get().isArrived_check()) {
            confirm = new DialogConfirm(getString(R.string.has_arrived_in_warehouse), getString(R.string.msg_has_arroved_warehouse))
                    .setOnClickListener(v -> {
                        confirmRouting(true);
                        confirm.dismiss();
                    }).setAnimationFile("animation/arrived_animation.json");
            confirm.show(getSupportFragmentManager(), "send_notification");
        } else {
            notContactCustomerDialog = new NotContactCustomerDialog(v -> {
                billDetailVM.getListImageNotContactCustomer().clear();
                if (notContactCustomerDialog.getListImage() != null) {
                    billDetailVM.setListImageNotContactCustomer(notContactCustomerDialog.getListImage());
                    billDetailVM.setTime_notContactCustomer(notContactCustomerDialog.getDate());
                    billDetailVM.notContactCustomer(RoutingDetailActivity.this::runUi);
                    notContactCustomerDialog.dismiss();
                }
            }, (bv, isChecked) -> {
                if (isChecked) {
                    notContactCustomerDialog.notContactCustomerDialogVM.getIsChooseDate().set(true);
                    billDetailVM.setType(1);
                } else {
                    notContactCustomerDialog.notContactCustomerDialogVM.getIsChooseDate().set(false);
                    billDetailVM.setType(0);
                }
            });
            notContactCustomerDialog.show(getSupportFragmentManager(), "duong_dz");
        }
    }


    private void animateFabButton(boolean visible) {
        if (visible) {
            AnimationUtil.zoomOutVisible(binding.llQR);
        } else {
            AnimationUtil.zoomInGone(binding.llQR);
        }
    }

    @SuppressLint("MissingPermission")
    @AfterPermissionGranted(LOCATION_PERMISSION_REQUEST_CODE)
    private void confirmRouting(boolean isConfirmArrived) {
        if (!VehicleStateStatus.Shipping.equals(AppController.getVehicleStatus())) {
            Toast.makeText(this, R.string.You_have_not_received_a_car, Toast.LENGTH_LONG).show();
            return;
        }
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};

        if (!EasyPermissions.hasPermissions(this, permissions)) {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.permissions_not_granted),
                    REQUEST_CODE_CAMERA, permissions);
        } else {
            if (AppController.getInstance().checkHighAccuracyLocationMode()) {
                LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                Location location1 = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//            double longitude = location1.getLongitude();
//            double latitude = location1.getLatitude();

//            location.getLastLocation()
//                    .addOnSuccessListener(this, location1 -> {
                if (location1 != null) {
                    float[] results = new float[1];
                    Location.distanceBetween(billDetailVM.routingPlan.get().getLatitude()
                            , billDetailVM.routingPlan.get().getLongitude()
                            , location1.getLatitude()
                            , location1.getLongitude(), results);

                    if (results[0] < StaticData.getOdooSessionDto().getDistance_check_point()) {
                        if (isConfirmArrived) {
                            billDetailVM.confirmArrived(this::runUi);
                        } else {
                            billDetailVM.confirmRouting(binding.txtNote.getText().toString(), this::runUi);
                        }
                    } else {
                        ToastUtils.showToast(this
                                , getString(R.string.update_fail_check_your_location_warehouse)
                                , getResources().getDrawable(R.drawable.ic_not_listed_location_24));
                    }
                } else {
                    ToastUtils.showToast(this
                            , getString(R.string.can_not_get_location)
                            , getResources().getDrawable(R.drawable.ic_not_listed_location_24));
                }
//                    });
            }

        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.GET_ROUTING_DETAIL:
//                binding.loadingView.setVisibility(View.GONE);
                imageAdapter.notifyDataSetChange();
                billPackageAdapter.notifyDataSetChanged();
                if (billDetailVM.routingPlan.get().getStatus().equals(RoutingPlanDayStatus.COMPLETED)) {
                    binding.ctRating.setVisibility(View.VISIBLE);
                } else {
                    binding.ctRating.setVisibility(View.GONE);
                }
                if (!billDetailVM.routingPlan.get().isArrived_check() && billDetailVM.routingPlan.get().getStatus().equals(RoutingPlanDayStatus.SHIPPING) && billDetailVM.getIsRequire().get()) {
                    showDialogSendNotification();
                } else if (billDetailVM.routingPlan.get().getStatus().equals(RoutingPlanDayStatus.COMPLETED) // tuyến đã hoàn thành
                        && !billDetailVM.routingPlan.get().isRating_customer() // đã dánh giá
                        && !billDetailVM.routingPlan.get().isFirst_rating_customer() // không phải vào lần đầu tiên
                        && !TroubleType.RETURN.equals(billDetailVM.routingPlan.get().getTrouble_type()) //không phải đơn hoàn trả
                        && billDetailVM.routingPlan.get().getAccept_time() != null) {
                    if ((Calendar.getInstance().getTime().getTime() - billDetailVM.routingPlan.get().getAccept_time().getTime()) / (1000 * 60 * 60) < StaticData.getOdooSessionDto().getRating_customer_duration_key())
                        showRatingCustomerMessenger();
                }
                if (!billDetailVM.getIsRequire().get() && billDetailVM.routingPlan.get().getStatus().equals(RoutingPlanDayStatus.SHIPPING) && !FROM_FRAGMENT.equals("ROUTING_HISTORY_FRAGMENT")) {
                    Toast.makeText(this, R.string.you_are_not_shipping, Toast.LENGTH_SHORT).show();
                }
                break;
            case Constants.CONFIRM_SUCCESS:
                if (rootNode != null) {
                    if (currentRout != null) {
                        currentRout.setStatus((Integer) objects[1]);
                    }
                    hashMapStatusRouting.put(((ItemRoutingNote) rootNode.getChildNode().get(currentRout.position)).getId(), (Integer) objects[1]);
                    isChangeData = true;
                } else {
                    status = (Integer) objects[1];
                }
                getNextRout();
                break;
            case Constants.CONFIRM_FAIL:
                Toast.makeText(this, R.string.confirm_fail, Toast.LENGTH_SHORT).show();
                break;
            case Constants.INVALID_QR_SO:
                ToastUtils.showToast(this, getString(R.string.msg_check_qr_so), getResources().getDrawable(R.drawable.picture_icon_warning));
                break;
            case Constants.CONFIRM_ARRIVED_FIRST:
                ToastUtils.showToast(this, getString(R.string.confirm_arrived_first), getResources().getDrawable(R.drawable.picture_icon_warning));
                break;
            case Constants.CONFIRM_ARRIVED_FAIL:
                ToastUtils.showToast(this, getString(R.string.msg_confirm_arrived_fail), getResources().getDrawable(R.drawable.picture_icon_warning));
                break;
            case Constants.CANCEL_ORDER_SUCCESS:
                if (rootNode != null) {
                    if (currentRout != null)
                        currentRout.setStatus((Integer) objects[1]);
                } else {
                    status = (Integer) objects[1];
                }
                isChangeData = true;
                isCancelOrder = true;
                billDetailVM.getIsLoading().set(false);
                ToastUtils.showToast(this, getString(R.string.cancel_order_success), getResources().getDrawable(R.drawable.ic_check));
                getNextRout();
                break;
            case Constants.CANCEL_ORDER_FAIL:
                billDetailVM.getIsLoading().set(false);
                ToastUtils.showToast(this, getString(R.string.cancel_order_fail), getResources().getDrawable(R.drawable.ic_close14));
                break;
            case Constants.NOT_CONTACT_CUSTOMER_SUCCESS:
                if (rootNode != null) {
                    if (currentRout != null)
                        currentRout.setStatus((Integer) objects[1]);
                } else {
                    status = (Integer) objects[1];
                }
                isChangeData = true;
                isCancelOrder = true;
                onBackPressed();
                ToastUtils.showToast(this, getString(R.string.success), getResources().getDrawable(R.drawable.ic_check));
                break;
            case Constants.NOT_CONTACT_CUSTOMER_FAIL:
                billDetailVM.getIsLoading().set(false);
                billDetailVM.getListImageNotContactCustomer().clear();
                ToastUtils.showToast(this, getString(R.string.FAIL), getResources().getDrawable(R.drawable.ic_warning));
                break;
        }

    }


    private void getNextRout() {
        if (rootNode != null) {
            List<BaseNode> childNode = rootNode.getChildNode();
            boolean isNext = false;
            for (int i = 0; i < childNode.size(); i++) {
                ItemRoutingNote itemRoutingNote = (ItemRoutingNote) childNode.get(i);
                if (itemRoutingNote.getStatus() == 0) {
                    routNodeAdapter.onItemClick(null, itemRoutingNote);
                    isNext = true;
                    break;
                }
            }
            if (!isNext) {
                billDetailVM.getChecked().set(false);
                Intent intent = getIntent();
                if (currentRout != null)
                    intent.putExtra("STATUS", currentRout.getStatus());
                intent.putExtra("IS_CANCEL_ORDER", isCancelOrder);
                intent.putExtra("HASH_MAP_STATUS_ROUTING", hashMapStatusRouting);
                intent.putExtra("IS_CHANGE", isChangeData);
                setResult(Activity.RESULT_OK, intent);
                AppController.getInstance().stopLocationService();
                finish();
            }
        } else {
            // one
            Intent intent = getIntent();
            intent.putExtra("POSITION", position);
            intent.putExtra("STATUS", status);
            if (status == 2) {
                isCancelOrder = true;
            }
            intent.putExtra("IS_CANCEL_ORDER", isCancelOrder);
            setResult(Activity.RESULT_OK, intent);
            AppController.getInstance().stopLocationService();
            finish();
        }
    }

    public void gotoRatingCustomerFragment() {
        Intent intent = new Intent(this, CommonActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.ROUTING_ID, billDetailVM.routingPlan.get().getId());
        bundle.putSerializable(Constants.FRAGMENT, RatingCustomerFragment.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.RESULT_FRAGMENT);
    }

    @Override
    public void onBackPressed() {
        if (isChangeData) {
            Intent intent = getIntent();
            intent.putExtra("IS_CANCEL_ORDER", isCancelOrder);
            if (hashMapStatusRouting != null) {
                intent.putExtra("HASH_MAP_STATUS_ROUTING", hashMapStatusRouting);
            }
            intent.putExtra("IS_CHANGE", isChangeData);
            setResult(Constants.RESULT_OK, intent);//trường hợp ko confirm hết
        }
        super.onBackPressed();
    }

    private boolean isScanCodeAndResultOk(int requestCode, int resultCode) {
        return requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Constants.RESULT_OK;
    }

    @Override
    public int
    getLayoutRes() {
        return R.layout.activity_routing_detail;
    }


    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BillDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            binding.tvNetwork.setVisibility(View.VISIBLE);
            binding.progressLayout.setVisibility(View.VISIBLE);
            binding.progressLayout.setOnClickListener(v->{
                Log.d("progress_click", "click");
            });
        } else {
            binding.progressLayout.setVisibility(View.GONE);
            binding.tvNetwork.setVisibility(View.GONE);
        }
    }

    private static class MyResultCallback implements OnResultCallbackListener<LocalMedia> {
        private WeakReference<GridImageAdapter> mAdapterWeakReference;
        private BillDetailVM billDetailVM;

        public MyResultCallback(GridImageAdapter adapter, BillDetailVM billDetailVM) {
            super();
            this.mAdapterWeakReference = new WeakReference<>(adapter);
            this.billDetailVM = billDetailVM;
        }

        @Override
        public void onResult(List<LocalMedia> result) {
            billDetailVM.setDataFileChoose(result);
            if (mAdapterWeakReference.get() != null) {
                mAdapterWeakReference.get().setList(result);
                mAdapterWeakReference.get().notifyDataSetChanged();
            }
        }

        @Override
        public void onCancel() {
        }
    }
}
