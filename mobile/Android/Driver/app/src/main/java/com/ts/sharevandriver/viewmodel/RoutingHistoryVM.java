package com.ts.sharevandriver.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.api.RoutingApi;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.enums.RoutingPlanDayStatus;
import com.ts.sharevandriver.model.ShareVanRoutingPlan;
import com.tsolution.base.BaseViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoutingHistoryVM extends BaseViewModel {

    private int currentPage = 0;
    private int totalPage = 0;
    ObservableBoolean isNotData = new ObservableBoolean();
    ObservableField<String> txtSearch = new ObservableField<>();
    ObservableField<Integer> statusRouting = new ObservableField<>();

    private List<ShareVanRoutingPlan> routingPlans;
    private List<Integer> listStatusRouting = new ArrayList<>();

    ObservableBoolean isLoading = new ObservableBoolean();


    public RoutingHistoryVM(@NonNull Application application) {
        super(application);
        txtSearch.set("");
        routingPlans = new ArrayList<>();
        isNotData.set(true);
        statusRouting.set(0);
        //get All status history routing
        listStatusRouting.add(0);//Chưa nhận hàng
        listStatusRouting.add(1);//Lái xe đã xác nhận
        listStatusRouting.add(2);//Thành công - kho đã xác nhận
        listStatusRouting.add(3);//Đã hủy
        listStatusRouting.add(4);//Chờ xác nhận thay đổi
    }

    public void getRoutingHistory(List<Integer> status, Date fromDate, Date toDate, boolean isLoadMore, RunUi runUi) {
        if (!isLoadMore) {
            currentPage = 0;
            totalPage = 0;
            routingPlans.clear();
        }
        isLoading.set(true);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String from = format.format(fromDate);
        String to = format.format(toDate);

        RoutingApi.getRoutingHistory(from, to, StaticData.getDriver().getId(), status, currentPage, txtSearch.get(), new IResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<ShareVanRoutingPlan> result = (OdooResultDto<ShareVanRoutingPlan>) o;
                routingPlans.addAll(result.getRecords());
                if (result.getTotal_record() % 10 == 0) {
                    totalPage = result.getTotal_record() / 10;
                } else {
                    totalPage = result.getTotal_record() / 10 + 1;
                }
                runUi.run("getHistorySuccess");
                isLoading.set(false);
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("noMore");
                isLoading.set(false);

            }

        });
    }

    public void getMoreHistory(Date fromDate, Date toDate, RunUi runUi) {
        currentPage += 1;
        if (totalPage > 0 && currentPage < totalPage) {
            List<Integer> integers= new ArrayList<>();
            if(statusRouting.get()!=0){
                integers.add(statusRouting.get()-1);
            }else{
                integers.addAll(listStatusRouting);
            }

            getRoutingHistory(integers, fromDate, toDate, true, runUi);
        } else {
            runUi.run("noMore");
        }

    }

    public void onRefresh(Date fromDate, Date toDate, RunUi runUi) {
        List<Integer> listStatus = new ArrayList<>();
        if(statusRouting.get()!=0){

            listStatus.add(statusRouting.get()-1);
        }
        else{
            listStatus.addAll(listStatusRouting);
        }
        getRoutingHistory(listStatus, fromDate, toDate, false, runUi);
    }

}
