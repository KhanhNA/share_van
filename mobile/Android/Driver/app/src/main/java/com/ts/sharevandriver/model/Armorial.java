package com.ts.sharevandriver.model;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Armorial extends BaseModel {
    private Long id;
    private String name;
    private Integer len;
    private String icon;
    private String description;

    public String getLenStr(){
        if(len==null || len==0){
            return "0";
        }
        return len+"";
    }
}
