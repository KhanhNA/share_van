package com.ts.sharevandriver.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ts.sharevandriver.BR;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.model.section.ItemRoutingNote;
import com.ts.sharevandriver.viewmodel.BillDetailVM;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import lombok.Getter;

@Getter
public class ItemRoutAdapter extends RecyclerView.Adapter<ItemRoutAdapter.ViewHolder> implements AdapterListener {
    private AdapterListener adapterListener;
    private List datas;
    private BillDetailVM billDetailVM;

    public ItemRoutAdapter(BillDetailVM billDetailVM, List lst, AdapterListener listener) {
        this.billDetailVM = billDetailVM;
        this.datas = lst;
        this.adapterListener = listener;
    }

    public void update(List list){
        this.datas = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_cate_rout, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        ItemRoutingNote bm = (ItemRoutingNote) (this.datas).get(position);
        bm.position = position ;
        viewHolder.bind(bm);
        viewHolder.bindWithVM(billDetailVM);
    }

    @Override
    public int getItemCount() {
        return this.datas == null ? 0 : (this.datas).size();
    }

    public void onItemClick(View v, Object o) {
        if (adapterListener != null) {
            adapterListener.onItemClick(v, o);
        }
    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewDataBinding itemProductBinding;

        public ViewHolder(ViewDataBinding view) {
            super(view.getRoot());
            this.itemProductBinding = view;
            view.getRoot().setOnLongClickListener(v -> {
                adapterListener.onItemLongClick(v, datas.get(getAdapterPosition()));
                return true;
            });
            view.getRoot().setOnClickListener(v -> {
                adapterListener.onItemClick(v, datas.get(getAdapterPosition()));
            });
        }

        public void bind(Object obj) {
            itemProductBinding.setVariable(BR.viewHolder, obj);
//            itemProductBinding.setVariable(BR.viewModel, viewModel);
            itemProductBinding.setVariable(BR.listener, adapterListener);
            itemProductBinding.executePendingBindings();
        }

        public void bindWithVM(BaseViewModel viewModel) {
            itemProductBinding.setVariable(BR.viewModel, viewModel);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
