package com.ts.sharevandriver.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.ScreenUtils;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.adapter.GridImageAdapter;
import com.ts.sharevandriver.adapter.ImageBaseAdapter;
import com.ts.sharevandriver.adapter.XBaseAdapter;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.databinding.VansInfoFragmentBinding;
import com.ts.sharevandriver.enums.ClickAction;
import com.ts.sharevandriver.enums.ObjectStatus;
import com.ts.sharevandriver.enums.VehicleStateStatus;
import com.ts.sharevandriver.model.Equipment;
import com.ts.sharevandriver.model.NotificationModel;
import com.ts.sharevandriver.service.location_service.NetworkManager;
import com.ts.sharevandriver.service.notification_service.NotificationService;
import com.ts.sharevandriver.ui.fragment.DialogConfirm;
import com.ts.sharevandriver.ui.fragment.ProgressFragment;
import com.ts.sharevandriver.utils.ToastUtils;
import com.ts.sharevandriver.viewmodel.VansVM;
import com.ts.sharevandriver.widget.FullyGridLayoutManager;
import com.ts.sharevandriver.widget.GlideEngine;
import com.ts.sharevandriver.widget.OnSingleClickListener;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;
import java.util.List;


public class VansInfoActivity extends BaseActivity<VansInfoFragmentBinding> implements NetworkManager.NetworkHandler {
    public static final String ACTION_UPDATE_DIRECTION = "KEY_UPDATE_DIRECTION";
    private static final int LOCATION_SETTING_CODE = 111;
    final int PICK_IMAGE_PERMISSIONS_REQUEST_CODE = 999;
    final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    VansVM vansVM;
    ImageBaseAdapter imageAdapter;
    FusedLocationProviderClient location;
    DialogConfirm confirm;
    AlertDialog alert;
    private GridImageAdapter imageSelectAdapter;
    private XBaseAdapter equipmentAdapter;
    private NetworkManager networkManager;
    private boolean isOnline;
    ProgressFragment progressFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vansVM = (VansVM) viewModel;
        location = LocationServices.getFusedLocationProviderClient(this);

        initToolbar();
        initView();
        getData();
    }

    private void initToolbar() {
        binding.toolbar.setTitle(R.string.vehicle_info);
        getBaseActivity().setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void initView() {
        imageAdapter = new ImageBaseAdapter(this, R.layout.item_image, vansVM.getBaseModelsE());
        binding.rcVansImg.setAdapter(imageAdapter);
        binding.rcVansImg.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        equipmentAdapter = new XBaseAdapter(R.layout.item_equipment, vansVM.getVehicle().get().getEquipments(), this);
        binding.rcEquipment.setAdapter(equipmentAdapter);
        initAdapterImage();

        Intent intent = getIntent();
        if (intent.hasExtra(Constants.ITEM_ID)) {
            String id = intent.getStringExtra(Constants.ITEM_ID);
            int log_id = Integer.parseInt(id);
            vansVM.getVansInfo(log_id, this::runUi);
        }
    }

    private void initLayoutManager() {
        FullyGridLayoutManager manager = new FullyGridLayoutManager(this,
                4, GridLayoutManager.VERTICAL, false);
        binding.rcSelectImages.setLayoutManager(manager);

        binding.rcSelectImages.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(this, 8), false));
    }

    private void initAdapterImage() {
        initLayoutManager();
        //list select image
        imageSelectAdapter = new GridImageAdapter(this, this::pickImage);
        imageSelectAdapter.setSelectMax(12);
        imageSelectAdapter.setOnClear(() -> {
            vansVM.isNotEmptyImage.set(false);
        });
        binding.rcSelectImages.setAdapter(imageSelectAdapter);
        imageSelectAdapter.setOnItemClickListener((v, position) -> {
            openOptionImage(position);
        });

    }

    private void buildAlertMessageNoGps() {
        if (alert == null) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.msg_turn_on_gps))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.ok), (dialog, id)
                            -> startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_SETTING_CODE));
            alert = builder.create();
        }
        if (!alert.isShowing()) {
            alert.show();
        }
    }

    private void openOptionImage(int position) {
        List<LocalMedia> selectList = imageSelectAdapter.getData();
        if (selectList.size() > 0) {
            LocalMedia media = selectList.get(position);
            String mimeType = media.getMimeType();
            int mediaType = PictureMimeType.getMimeType(mimeType);
            if (mediaType == PictureConfig.TYPE_VIDEO) {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .externalPictureVideo(TextUtils.isEmpty(media.getAndroidQToPath()) ? media.getPath() : media.getAndroidQToPath());
            } else {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .isNotPreviewDownload(true)
                        .openExternalPreview(position, selectList);
            }
        }
    }

    public void showDialogConfirmUpdate() {
        confirm = new DialogConfirm(getString(R.string.CONFIRM)
                , getString(R.string.msg_confirm_update_vehicle_status)).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                updateStatus();
                confirm.dismiss();
            }
        });
        confirm.show(getSupportFragmentManager(), confirm.getTag());

    }

    private void updateStatus() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                        permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
            return;
        }
        location.getLastLocation()
                .addOnSuccessListener(this, location1 -> {
                    // Got last known location. In some rare situations this can be null.
                    if (location1 != null && StaticData.getDriver().getVehicle().getParking_point().getLatitude() != null && StaticData.getDriver().getVehicle().getParking_point().getLongitude() != null) {
                        float[] results = new float[1];
                        Location.distanceBetween(StaticData.getDriver().getVehicle().getParking_point().getLatitude()
                                , StaticData.getDriver().getVehicle().getParking_point().getLongitude()
                                , location1.getLatitude()
                                , location1.getLongitude(), results);

                        if (results[0] < StaticData.getOdooSessionDto().getDistance_check_point()) {
                            String status;
                            if (StaticData.getDriver().getVehicle().getStatus().equals(VehicleStateStatus.Shipping)) {
                                status = VehicleStateStatus.ConfirmReturn;
                            } else {
                                status = VehicleStateStatus.ConfirmTake;
                            }
                            vansVM.updateVanStatus(binding.txtNote.getText() + "", location1, status, VansInfoActivity.this::runUi);
                        } else {
                            ToastUtils.showToast(this
                                    , getString(R.string.update_fail_check_your_location)
                                    , getResources().getDrawable(R.drawable.ic_not_listed_location_24));
                        }
                    } else {
//                        ToastUtils.showToast(this
//                                , getString(R.string.can_not_get_location)
//                                , getResources().getDrawable(R.drawable.ic_not_listed_location_24));
                        buildAlertMessageNoGps();
                    }
                });
    }

    public void selectImages() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            pickImage();
        }

    }

    @Override
    public void onItemClick(View v, Object o) {
        Equipment equipment = (Equipment) o;
        RecyclerView.ViewHolder holder = binding.rcEquipment.findViewHolderForAdapterPosition(((Equipment) o).index - 1);
        assert holder != null;
        EditText txtTake = holder.itemView.findViewById(R.id.etQuantityTake);
        EditText txtReturn = holder.itemView.findViewById(R.id.etQuantityReturn);
        switch (v.getId()) {
            case R.id.checkbox:
                equipment.setIsselect(!equipment.isIsselect());
                equipmentAdapter.notifyItemChanged(equipment.index - 1);
                break;
            case R.id.minusQuantity:
                if (StaticData.getDriver().getVehicle().getStatus().equals(VehicleStateStatus.Shipping)) {
                    if (equipment.getQuantity_return() > 0) {
                        equipment.setQuantity_return(equipment.getQuantity_return() - 1);
                    }
                    if (equipment.getQuantity_return() > equipment.getQuantity_take()) {
                        equipment.setQuantity_return(equipment.getQuantity_take());
                    }
                    txtReturn.setText(equipment.getQuantity_return_str());
                } else {
                    if (equipment.getQuantity_take() > 0) {
                        equipment.setQuantity_take(equipment.getQuantity_take() - 1);
                        txtTake.setText(equipment.getQuantity_take_str());
                    }
                }
                break;
            case R.id.addQuantity:
                if (StaticData.getDriver().getVehicle().getStatus().equals(VehicleStateStatus.Shipping)) {
                    if (equipment.getQuantity_return() < equipment.getQuantity_take()) {
                        equipment.setQuantity_return(equipment.getQuantity_return() + 1);
                    } else {
                        equipment.setQuantity_return(equipment.getQuantity_take());
                    }
                    txtReturn.setText(equipment.getQuantity_return_str());
                } else {
                    if (equipment.getQuantity_take() < 99999) {
                        equipment.setQuantity_take(equipment.getQuantity_take() + 1);
                        txtTake.setText(equipment.getQuantity_take_str());
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                Toast.makeText(this, R.string.permissions_not_granted, Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                }
                updateStatus();
            }
        }
    }

    private void pickImage() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .maxSelectNum(12)
                .isWeChatStyle(true)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .selectionMode(PictureConfig.MULTIPLE)
                .selectionMedia(imageSelectAdapter.getData())
                .forResult(new VansInfoActivity.MyResultCallback(imageSelectAdapter, vansVM));
    }

    private void getData() {
        if (StaticData.getDriver().getVehicle() != null) {
            vansVM.getVansInfo(StaticData.getDriver().getAssignation_log().getId(), this::runUi);
        } else {
            int log_id = 0;
            try {
                log_id = Integer.parseInt(getIntent().getStringExtra(Constants.ITEM_ID));
            } catch (Exception e) {
                e.printStackTrace();
            }
            vansVM.getVansInfo(log_id, this::runUi);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOCATION_SETTING_CODE) {
            progressFragment = new ProgressFragment(getString(R.string.get_your_loaction), new ProgressFragment.IProgressFragment() {
                @Override
                public void handleProgress() {
                    progressFragment.dismiss();
                    if (AppController.getInstance().checkHighAccuracyLocationMode()) {
//                updateStatus();
                    } else {
                        ToastUtils.showToast(getString(R.string.please_set_high_accuracy_location_mode));
                        buildAlertMessageNoGps();
                    }
                }
            });
            progressFragment.show(getSupportFragmentManager(), "abc");

        }
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getVansSuccess":
                runOnUiThread(() -> {
                    imageAdapter.notifyDataSetChange();
                    equipmentAdapter.update(vansVM.getVehicle().get().getEquipments());
                });
                break;
            case "updateSuccess":
                ToastUtils.showToast(this, getString(R.string.update_success), getResources().getDrawable(R.drawable.ic_done_white_48dp));
                setResult(Activity.RESULT_OK, getIntent());
                onBackPressed();
                break;
            case "updateFail":
                ToastUtils.showToast((String) objects[1]);
                break;
            case "needToCompleteRout":
                ToastUtils.showToast(this, R.string.msg_need_to_complete_rout);
                break;
            case "needToFinishCalendar":
                ToastUtils.showToast(String.format(getString(R.string.msg_need_to_finish_driver_calendar), ((String) objects[1])));
                break;
            case "Get Info Success":
                ToastUtils.showToast("Edit checking point");
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.vans_info_fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        networkManager = new NetworkManager(this, this);
        networkManager.start();
        isOnline = networkManager.isOnline();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        networkManager.stop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(NotificationService notificationService) {
        try {
            String clickAction = notificationService.getClick_action();

            if (ClickAction.ROUTING.equals(clickAction) || ClickAction.MAIN_ACTIVITY.equals(clickAction) || ClickAction.ROUTING_DETAIL.equals(clickAction)) {

                //Send action broadcast to main activity handle refresh map draw direction
                Intent intent = new Intent(ACTION_UPDATE_DIRECTION);
                this.sendBroadcast(intent);
            }

            Gson gsonn = new GsonBuilder()
                    .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                    .create();
            NotificationModel notificationModell = gsonn.fromJson(notificationService.getMess_object(), NotificationModel.class);
            if (ObjectStatus.FOUR.equals(notificationModell.getObject_status()) && notificationModell.getClick_action().equals(ClickAction.MAIN_ACTIVITY)) {
                vansVM.getUserInfo(this::runUi);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return VansVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            binding.tvNetwork.setVisibility(View.VISIBLE);
            binding.progressLayout.setVisibility(View.VISIBLE);
            binding.progressLayout.setOnClickListener(v->{
                Log.d("click_progress", "onNetworkUpdate");
            });
        } else {
            binding.tvNetwork.setVisibility(View.GONE);
            binding.progressLayout.setVisibility(View.GONE);
        }
    }

    private static class MyResultCallback implements OnResultCallbackListener<LocalMedia> {
        private WeakReference<GridImageAdapter> mAdapterWeakReference;
        private VansVM vansVM;

        public MyResultCallback(GridImageAdapter adapter, VansVM vansVM) {
            super();
            this.mAdapterWeakReference = new WeakReference<>(adapter);
            this.vansVM = vansVM;
        }

        @Override
        public void onResult(List<LocalMedia> result) {
            vansVM.setDataFileChoose(result);
            if (mAdapterWeakReference.get() != null) {
                mAdapterWeakReference.get().setList(result);
                mAdapterWeakReference.get().notifyDataSetChanged();
            }
        }

        @Override
        public void onCancel() {
        }
    }

}
