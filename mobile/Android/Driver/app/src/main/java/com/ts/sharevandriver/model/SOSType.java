package com.ts.sharevandriver.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SOSType extends BaseModel {
    private Integer id;
    private String name;
    private String code;
    private String description;
    private String status;
    private String note;
}
