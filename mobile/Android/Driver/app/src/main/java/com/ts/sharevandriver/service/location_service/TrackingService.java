/*
 * Copyright 2012 - 2019 Anton Tananaev (anton@traccar.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ts.sharevandriver.service.location_service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.service.notification_service.MyFirebaseMessagingService;
import com.ts.sharevandriver.ui.activity.MainActivity;
import com.ts.sharevandriver.ui.activity.SplashActivity;

import static com.ts.sharevandriver.service.notification_service.MyFirebaseMessagingService.NOTIFICATION_CHANNEL_ID;

public class TrackingService extends Service {
    private static final String TAG = TrackingService.class.getSimpleName();

    private PowerManager.WakeLock wakeLock;
    private TrackingController trackingController;

    private String deviceId = "";
    private double distance = 0;
    private double angle = 0;
    private long interval = 30 * 1000; // khoảng thời gian request location - đơn vị giấy.

    private void createNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                getApplicationContext(),
                0,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_location);
        builder.setContentTitle(getString(R.string.tracking_service));
        builder.setDefaults(NotificationCompat.DEFAULT_ALL);
        builder.setContentText(getString(R.string.running));
        builder.setAutoCancel(false);
        builder.setContentIntent(pendingIntent);
        builder.setPriority(NotificationCompat.PRIORITY_MAX);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            if (notificationManager != null
                    && notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID) == null) {
                NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Location service",
                        NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription(getString(R.string.this_channel_use_by_location_service));
                notificationManager.createNotificationChannel(channel);
            }
        }
        startForeground(Constants.LOCATION_SERVICE_ID, builder.build());
    }


    @SuppressLint("WakelockTimeout")
    @Override
    public void onCreate() {
        Log.i(TAG, "service create");

        createNotification();

        try{
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
                wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
                wakeLock.acquire();

                trackingController = new TrackingController(this);
                interval = StaticData.getOdooSessionDto().getDuration_request() * 1000;
                deviceId = StaticData.getDriver().getVehicle().getUniqueid();
                trackingController.setConfig(deviceId, interval, distance, angle);
                trackingController.start();
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            AutostartReceiver.completeWakefulIntent(intent);
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "service destroy");

        stopForeground(true);

        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
        }
        if (trackingController != null) {
            trackingController.stop();
        }
    }

}
