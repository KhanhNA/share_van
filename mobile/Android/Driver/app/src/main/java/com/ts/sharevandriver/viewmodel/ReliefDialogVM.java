package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;

import com.ts.sharevandriver.api.SOSApi;
import com.ts.sharevandriver.api.SharingOdooResponse2;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.model.SOSNumber;
import com.ts.sharevandriver.model.SOSType;
import com.tsolution.base.BaseViewModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReliefDialogVM extends BaseViewModel {
    ObservableList<SOSNumber> listSosNumbers;
    public ReliefDialogVM(@NonNull Application application) {
        super(application);
        listSosNumbers = new ObservableArrayList<>();
    }
    public void getSOSNumbers(RunUi runUi){
        SOSApi.getSOSNumber(new SharingOdooResponse2() {
            @Override
            public void onSuccess(Object o) {
                if(o != null){
                    listSosNumbers.addAll((List<SOSNumber>) o);
                    runUi.run(Constants.SUCCESS_API);
                }
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run(Constants.FAIL_API);
            }
        });
    }
}
