package com.ts.sharevandriver.enums;

import androidx.annotation.IntDef;
import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class EnumBidding {
    private static EnumBidding mInstance = null;

    public static EnumBidding getInstance() {
        if (mInstance == null) {
            mInstance = new EnumBidding();
        }
        return mInstance;
    }


    @Retention(RetentionPolicy.SOURCE)
    @IntDef({EnumBiddingBehavior.NOT_SHIPPING, EnumBiddingBehavior.SHIPPING, EnumBiddingBehavior.SHIPPING_DONE})
    public @interface EnumBiddingBehavior {
        int NOT_SHIPPING = 1;
        int SHIPPING = 2;
        int SHIPPING_DONE = 3;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({EnumSortBiddingBehavior.PRICE_ASCENDING, EnumSortBiddingBehavior.SORT_DESCENDING_PRICES, EnumSortBiddingBehavior.DESCENDING_DISTANCE,
            EnumSortBiddingBehavior.EAR_LIEST, EnumSortBiddingBehavior.ASCENDING_DISTANCE, EnumSortBiddingBehavior.OLD})
    public @interface EnumSortBiddingBehavior {
        int PRICE_ASCENDING = 1;
        int SORT_DESCENDING_PRICES = 2;
        int ASCENDING_DISTANCE = 3;
        int DESCENDING_DISTANCE = 4;
        int EAR_LIEST = 5;
        int OLD = 6;
    }


    @Retention(RetentionPolicy.SOURCE)
    @IntDef({EnumStatusManagerCargo.GET_ALL, EnumStatusManagerCargo.GET_CARGO_BIDDING, EnumStatusManagerCargo.GET_CARO_NOT_BIDDING})
    public @interface EnumStatusManagerCargo {
        int GET_ALL = 0;
        int GET_CARGO_BIDDING = 1;
        int GET_CARO_NOT_BIDDING = 2;
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({EnumStatus.SUCCESS, EnumStatus.FAIL, EnumStatus.LOADING})
    public @interface EnumStatus {
        String LOADING = "LOADING";
        String SUCCESS = "SUCCESS";
        String FAIL = "FAIL";
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({EnumStatusOrder.NOT_YET_SHIPPED,
            EnumStatusOrder.BEING_TRANSPORTED, EnumStatusOrder.RETURNS,
            EnumStatusOrder.SHIPPING_IS_SUCCESSFUL, EnumStatusOrder.CANCEL_ORDER})
    public @interface EnumStatusOrder {
        int NOT_YET_SHIPPED = 0;
        int BEING_TRANSPORTED = 1;
        int RETURNS = 2;
        int SHIPPING_IS_SUCCESSFUL = 3;
        int CANCEL_ORDER = 4;
    }
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({EnumActionNotification.ACTION_DETAIL})
    public @interface EnumActionNotification{
        int ACTION_DETAIL = 1;
    }
}
