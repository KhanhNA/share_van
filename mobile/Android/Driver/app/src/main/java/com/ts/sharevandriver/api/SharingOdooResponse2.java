package com.ts.sharevandriver.api;

import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.ts.sharevandriver.base.IResponse;
import com.workable.errorhandler.ErrorHandler;

public abstract class SharingOdooResponse2<T> implements IResponse<T> {

    public IOdooResponse getResponse(Class<T> clazz){
        return (IOdooResponse<T>) (o, volleyError) -> {
            if(volleyError != null){
                volleyError.printStackTrace();
                ErrorHandler.create().handle(volleyError);
                return;
            }
            onSuccess(o);
        };
    }

}
