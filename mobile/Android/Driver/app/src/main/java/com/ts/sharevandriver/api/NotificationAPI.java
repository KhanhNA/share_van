package com.ts.sharevandriver.api;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.model.NotificationModel;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

public class NotificationAPI extends BaseApi {
    private static final String GET_NOTIFICATION = "/notification";
    private static final String SEEN_NOTIFICATION = "/notification/is_read";
    private static final String IS_NOTIFICATION = "/notification/check_user_read_message";
    public static void getNotifications(String type, Integer page, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("type",type);
            params.put("offset", page * 10);
            params.put("limit", 10);
            mOdoo.callRoute(GET_NOTIFICATION, params, new SharingOdooResponse<OdooResultDto<NotificationModel>>() {
                @Override
                public void onSuccess(OdooResultDto<NotificationModel> notifications) {
                    if(notifications != null) {
                        result.onSuccess(notifications);
                    }else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getNotificationById(String id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("id", id);
            mOdoo.callRoute(GET_NOTIFICATION, params, new SharingOdooResponse<OdooResultDto<NotificationModel>>() {
                @Override
                public void onSuccess(OdooResultDto<NotificationModel> notifications) {
                    if(notifications != null && notifications.getRecords().size() > 0) {
                        result.onSuccess(notifications.getRecords().get(0));
                    }else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void seenNotification(Long id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("id",id);
            mOdoo.callRoute(SEEN_NOTIFICATION, params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    result.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void isNotification( IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            mOdoo.callRoute(IS_NOTIFICATION, params, new SharingOdooResponse<OdooResultDto<NotificationModel>>() {
                @Override
                public void onSuccess(OdooResultDto<NotificationModel> notificationDTOOdooResultDto) {
                    if(notificationDTOOdooResultDto != null){
                        result.onSuccess(notificationDTOOdooResultDto);
                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }
}
