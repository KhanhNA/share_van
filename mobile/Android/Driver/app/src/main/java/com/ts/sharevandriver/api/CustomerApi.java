package com.ts.sharevandriver.api;


import com.android.volley.VolleyError;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.model.Driver;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class CustomerApi extends BaseApi {
    public static String CHANGE_PASSWORD = "/server/change_password";

    public static void changePassword(String oldPassword, String newPassword, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("old_password", oldPassword);
            params.put("new_password", newPassword);
            mOdoo.callRoute(CHANGE_PASSWORD, params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    result.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }


    public static void getDriver(IResponse result) {
        JSONObject params = new JSONObject();
        try {

            mOdoo.callRoute("/fleet/driver", params, new SharingOdooResponse<OdooResultDto<Driver>>() {
                @Override
                public void onSuccess(OdooResultDto<Driver> shareVanRoutingPlans) {
                    result.onSuccess(shareVanRoutingPlans);
                    StaticData.sessionCookie = mOdoo.getSessionCokie();
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
