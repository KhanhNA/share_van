package com.nextsolution.vancustomer.network;

import com.nextsolution.db.dto.DistanceDTO;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface DistanceService {
    @GET("/location")
    Call<DistanceDTO> getDistance(
            @Query("f_lat") Double from_lat,
            @Query("f_lon") Double from_long,
            @Query("t_lat") Double to_lat,
            @Query("t_lon") Double to_long
    );
}
