package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioGroup;

import com.google.android.material.chip.ChipGroup;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.RecurrentViewModel;
import com.nextsolution.vancustomer.databinding.RecurrentDialogBinding;
import com.nextsolution.vancustomer.model.RecurrentModel;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

public class RecurrentDialog extends DialogFragment {
    RecurrentViewModel recurrentViewModel;
    RecurrentDialogBinding binding;
    OnConfirm onConfirm;
    RecurrentModel recurrentModel;

    public RecurrentDialog(RecurrentModel recurrentModel, OnConfirm onConfirm) {
        this.recurrentModel = recurrentModel;
        this.onConfirm = onConfirm;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.recurrent_dialog, container, false);
        recurrentViewModel = ViewModelProviders.of(this).get(RecurrentViewModel.class);
        binding.setViewModel(recurrentViewModel);
        binding.setListener(this);
        if (recurrentModel != null) {
            recurrentViewModel.getRecurrentModel().set(recurrentModel);
            if (recurrentModel.getDay_of_month() != null) {
                binding.calendarMonth.getSelectedCalendar().setDay(recurrentModel.getDay_of_month());
            }
            if (recurrentModel.getFrequency() == null) {
                recurrentModel.setFrequency(1);
                recurrentViewModel.getRecurrentModel().get().setDay_of_week(Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1);
            }
        } else {
            recurrentViewModel.getRecurrentModel().get().setFrequency(1);
            recurrentViewModel.getRecurrentModel().get().setDay_of_week(Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1);
        }
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());

        binding.btnCancel.setOnClickListener(v -> this.dismiss());
        binding.btnConfirm.setOnClickListener(v -> {
            recurrentViewModel.getRecurrentModel().get().setDay_of_month(binding.calendarMonth.getSelectedCalendar().getDay());
            recurrentViewModel.getRecurrentModel().get().setDay_of_week(getSelectedDayOfWeek());
            onConfirm.onConfirm(recurrentViewModel.getRecurrentModel().get());
            this.dismiss();
        });
        return binding.getRoot();
    }

    private Integer getSelectedDayOfWeek() {
        ChipGroup chipGroup = binding.layoutDayOfWeek.rgDayOfWeek;
        for (int i = 0; i < 7; i++) {
            if (chipGroup.getChildAt(i).getId()
                    == chipGroup.getCheckedChipId())
                return i;
        }
        return null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }

    public void onCheckRecurrentChanged(RadioGroup rg, int id) {
        switch (id) {
            case R.id.daily:
                recurrentViewModel.getRecurrentModel().get().setFrequency(1);
                break;
            case R.id.weekly:
                recurrentViewModel.getRecurrentModel().get().setFrequency(2);
                break;
            case R.id.monthly:
                recurrentViewModel.getRecurrentModel().get().setFrequency(3);
                break;
        }
        recurrentViewModel.getRecurrentModel().notifyChange();
    }

    public interface OnConfirm {
        void onConfirm(RecurrentModel recurrentModel);
    }
}
