package com.nextsolution.vancustomer.customer;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.databinding.ViewDataBinding;

import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.nextsolution.db.dto.BiddingOrder;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.customer.vm.BiddingCargoChildVM;
import com.nextsolution.vancustomer.databinding.BiddingCargoChildFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.INotifyData;
import com.nextsolution.vancustomer.widget.CustomLoadMoreView;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class BiddingCargoChildFragment extends BaseFragment {
    BiddingCargoChildFragmentBinding mBinding;
    BiddingCargoChildVM biddingCargoChildVM;
    BaseAdapterV3 adapterV3;
    INotifyData iNotifyData;
    List<Integer> status;
    Integer index;
    Integer type;
    String preTextSearch = "";

    public BiddingCargoChildFragment(Integer type, List<Integer> status) {
        this.type = type;
        this.status = status;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(BiddingOrder biddingOrder) {
        try {
            biddingCargoChildVM.getBiddingOrders().clear();
            biddingCargoChildVM.getBiddingOrders().add(biddingOrder);
            adapterV3.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        biddingCargoChildVM = (BiddingCargoChildVM) viewModel;
        biddingCargoChildVM.setType(type);
        mBinding = (BiddingCargoChildFragmentBinding) binding;
        initView();
        setUpRecycleView();
        biddingCargoChildVM.getData(type, status, this::runUi);
        initLoadMore();
        return view;
    }


    private void initView() {

        mBinding.edtSearch.setOnEditorActionListener((TextView.OnEditorActionListener) (v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchData();
                return true;
            }
            return false;
        });
        mBinding.swipeLayout.setOnRefreshListener(() -> {
            onRefresh(this.type);
        });

        mBinding.btnSearch.setOnClickListener(v -> searchData());
    }

    public void reLoad() {
        onRefresh(this.type);
    }

    @SuppressLint("RestrictedApi")
    public void openDialogSelectOrderBy(View view) {
//Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(getBaseActivity(), view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu_order_by, popup.getMenu());

        MenuPopupHelper menuHelper = new MenuPopupHelper(getBaseActivity(), (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);

        popup.setOnMenuItemClickListener(item -> {
            boolean orderByNewest = true;
            switch (item.getItemId()) {
                case R.id.action_newest:
                    orderByNewest = true;
                    break;
                case R.id.action_oldest:
                    orderByNewest = false;
                    break;
            }
            if (orderByNewest != biddingCargoChildVM.getOrderByNewest().get()) {
                biddingCargoChildVM.getOrderByNewest().set(orderByNewest);
                biddingCargoChildVM.getData(type, status, this::runUi);
            }
            return true;
        });

        menuHelper.show();//showing popup menu
    }

    private void searchData() {
        if (!preTextSearch.equals(biddingCargoChildVM.getTxtSearch().get())) {
            preTextSearch = biddingCargoChildVM.getTxtSearch().get();
            biddingCargoChildVM.getData(type, status, this::runUi);
        }
    }

    private void initLoadMore() {
        adapterV3.getLoadMoreModule().setOnLoadMoreListener(() -> {
            biddingCargoChildVM.loadMore(type, status, this::runUi);
        });
        adapterV3.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapterV3.getLoadMoreModule().setAutoLoadMore(true);
    }

    public void onRefresh(Integer type) {
        this.type = type;
        biddingCargoChildVM.setOffset(0);
        biddingCargoChildVM.getData(type, status, this::runUi);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.GET_DATA_SUCCESS:
                adapterV3.getLoadMoreModule().loadMoreComplete();
                adapterV3.notifyDataSetChanged();
                break;
            case Constants.GET_DATA_FAIL:
                adapterV3.getLoadMoreModule().loadMoreComplete();
                break;
            case "noMore":
                adapterV3.getLoadMoreModule().loadMoreEnd();
                break;
        }
    }

    private void setUpRecycleView() {
        adapterV3 = new BaseAdapterV3(R.layout.bidding_cargo_child_item, biddingCargoChildVM.getBiddingOrders(), this) {
            @Override
            protected void convert(@NonNull BaseDataBindingHolder holder, Object o) {
                //  Binding
                ((BaseModel) o).index = holder.getAdapterPosition() + 1;
                ViewDataBinding binding = holder.getDataBinding();
                if (binding != null) {
                    binding.setVariable(BR.viewModel, biddingCargoChildVM);
                    binding.setVariable(BR.viewHolder, o);
                    if (listenerAdapter != null) {
                        binding.setVariable(BR.listenerAdapter, this.listenerAdapter);
                    }
                    binding.executePendingBindings();
                }
            }
        };
        mBinding.recyclerView.setAdapter(adapterV3);
    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        Intent intent = new Intent(getActivity(), CommonActivity.class);
        Bundle bundle = new Bundle();
        BiddingOrder biddingOrder = (BiddingOrder) o;
        index = ((BiddingOrder) o).index - 1;
        switch (v.getId()) {
            case R.id.itemBiddingCargo:
                bundle.putInt(Constants.DATA, biddingOrder.getId());
                bundle.putSerializable(Constants.FRAGMENT, BiddingCargoDetailFragment.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, Constants.REQUEST_FRAGMENT_RESULT);
                break;
            case R.id.btnMap:
                bundle.putInt("BIDDING_ORDER_ID", biddingOrder.getId());
                bundle.putString(Constants.FROM_FRAGMENT, "BIDDING_CARGO_CHILD_FRAGMENT");
                bundle.putInt(Constants.DEPOT_TYPE, type);
                bundle.putSerializable(Constants.FRAGMENT, MapsFragment.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.REQUEST_FRAGMENT_RESULT && resultCode == Constants.RESULT_OK) {
            onRefresh(this.type);
            iNotifyData.reloadData(true);
        }
    }

    public void setINotifyData(INotifyData iNotifyData) {
        this.iNotifyData = iNotifyData;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.bidding_cargo_child_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BiddingCargoChildVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.recyclerView;
    }
}
