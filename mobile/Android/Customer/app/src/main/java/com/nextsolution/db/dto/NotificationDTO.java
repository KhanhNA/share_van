package com.nextsolution.db.dto;


import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationDTO extends BaseModel {
    private Integer id;
    private Integer notification_id;
    private Boolean is_read;
    private Integer create_uid;
    private OdooDateTime create_date;
    private String sent_date;
    private String title;
    private String content;
    private String type;
    private String click_action;
    private String message_type;
    private String item_id;
    private String object_status;
    private String image_256;
    private String description;
    private Integer total_message_not_seen;
}
