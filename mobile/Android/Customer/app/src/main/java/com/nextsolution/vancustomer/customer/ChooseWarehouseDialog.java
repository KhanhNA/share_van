package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.ChooseWarehouseDialogVM;
import com.nextsolution.vancustomer.databinding.ChooseWarehouseDialogBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.tsolution.base.listener.AdapterListener;


public class ChooseWarehouseDialog extends DialogFragment {


    ChooseWarehouseDialogVM chooseWarehouseDialogVM;
    ChooseWarehouseDialogBinding mBinding;
    XBaseAdapter adapter;
    AdapterListener adapterListener;

    public ChooseWarehouseDialog(AdapterListener adapterListener) {
        this.adapterListener = adapterListener;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.choose_warehouse_dialog, container, false);
        chooseWarehouseDialogVM = ViewModelProviders.of(this).get(ChooseWarehouseDialogVM.class);
        mBinding.setViewModel(chooseWarehouseDialogVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        setUpRecycleView();
        chooseWarehouseDialogVM.getWareHouse(this::runUI);
        return mBinding.getRoot();
    }

    public void getData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            int position = bundle.getInt(Constants.DATA, -1);
            chooseWarehouseDialogVM.getWarehouses().get(0).checked = false;
            chooseWarehouseDialogVM.getWarehouses().get(position).checked = true;
        }

    }

    public void setUpRecycleView() {
        adapter = new XBaseAdapter(R.layout.choose_warehouse_dialog_item, chooseWarehouseDialogVM.getWarehouses(), adapterListener);
        mBinding.chooseWarehouse.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mBinding.chooseWarehouse.setLayoutManager(layoutManager);
    }

    public void runUI(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.GET_DATA_FAIL:
                ToastUtils.showToast(getActivity(), getResources().getString(R.string.NOT_FOUND_DATA), getResources().getDrawable(R.drawable.ic_fail));
                break;
            case Constants.GET_DATA_SUCCESS:
                getData();
                adapter.notifyDataSetChanged();
                break;
            case "companyNull":
                ToastUtils.showToast(getActivity(), getResources().getString(R.string.company_not_found), getResources().getDrawable(R.drawable.ic_fail));
                break;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
}
