package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.enums.StatusType;

import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class BillLadingDetail extends BaseModel {
    private Double latitude;
    private Integer area_id;
    private Double longitude;
    private String approved_type;
    private String status;
    private String name;
    private Integer id;
    private String name_seq = "New";
    private Double max_price;
    private Double min_price;
    private Double total_weight;

    private Warehouse warehouse;
    private String address;
    private String warehouse_name;
    private String phone;
    private String warehouse_type;//0. nhan, 1. tra
    private OdooRelType from_bill_lading_detail_id;
    private String description;
    private OdooRelType service_id;

    private OdooDateTime expected_from_time;
    private OdooDateTime expected_to_time;
    private String status_order = StatusType.RUNNING;

    private String order_type;//0: trong zone, 1: ngoài zone  - so với kho xuất hàng.
    private List<BillService> billService = new ArrayList<>();
    private Integer bill_lading_id;

    private List<BillPackage> billPackages = new ArrayList<>();

    //    private Double wlat;
//    private Double wlong;
    private String wjson_address;
    private Integer zone_area_id;

    private List<AreaDistance> areaDistances;
    private Double express_distance;
    private Double price;
    private transient int total_parcel;
    private Double total_volume;

    //    private Date expectedFromDate;
//    private Date expectedToDate;
    public String getExpectedFromTimeStr() {
        if (expected_from_time == null) {
            return "";
        }
        return AppController.formatTime.format(expected_from_time);
    }

    public String getExpectedToTimeStr() {
        if (expected_to_time == null) {
            return "";
        }
        return AppController.formatTime.format(expected_to_time);
    }




    public String getIndex() {
        return String.valueOf(this.index);
    }



}
