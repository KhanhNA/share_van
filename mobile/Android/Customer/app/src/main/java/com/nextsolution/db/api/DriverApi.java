package com.nextsolution.db.api;


import com.nextsolution.db.dto.Partner;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.network.SOService;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverApi extends BaseApi {


    public static void updateToken(String fcmToken, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("fcm_token", fcmToken);

            mOdoo.callRoute("/server/save_token", params, new SharingOdooResponse<OdooResultDto<String>>() {
                @Override
                public void onSuccess(OdooResultDto<String> obj) {
                    result.onSuccess(obj);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }


    }

    public static void deleteToken() {
        SOService soService = mOdoo.getClient().createService(SOService.class);

        Call<ResponseBody> call = soService.logOut(mOdoo.getSessionCokie(), StaticData.getOdooSessionDto().getUid() + "");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


}
