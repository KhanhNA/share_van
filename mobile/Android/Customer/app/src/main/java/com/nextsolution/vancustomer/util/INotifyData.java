package com.nextsolution.vancustomer.util;

public interface INotifyData {
    void reloadData(boolean b);
}
