package com.nextsolution.vancustomer.util;

public interface IntentConstants {
    String INTENT_START_MAIN = "startMain";
    String INTENT_INPUT_SUCCES = "inputSucess";
    String INTENT_INPUT_FAIL = "inputFail";
    String INTENT_INPUT_CODE = "inputCode";
    String INTENT_REGISTER_SUCCESS = "registerSuccess";
    String INTENT_PHONE_NUMBER_CONFLICT = "PHONE_NUMBER_CONFLICT";

}
