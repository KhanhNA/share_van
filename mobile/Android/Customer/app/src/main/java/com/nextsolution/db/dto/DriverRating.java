package com.nextsolution.db.dto;

import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverRating extends BaseModel {

    private Long driver_id;
    private Integer routing_plan_day_id;
    private Integer num_rating;
    private List<Integer> rating_badges;
    private String description;
    private Long employee_id;
}
