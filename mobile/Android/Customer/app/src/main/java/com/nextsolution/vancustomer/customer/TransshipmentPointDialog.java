package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.nextsolution.db.dto.Area;
import com.nextsolution.db.dto.AreaDistance;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.TransshipmentPointDialogVM;
import com.nextsolution.vancustomer.databinding.TransshipmentPointDialogBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.generated.callback.OnClickListener;
import com.tsolution.base.listener.AdapterListener;

import java.util.ArrayList;
import java.util.List;

public class TransshipmentPointDialog extends DialogFragment {
    TransshipmentPointDialogVM transshipmentPointDialogVM;
    TransshipmentPointDialogBinding mBinding;
    XBaseAdapter adapter;
    List<AreaDistance> areaDistances;
    String nameWareHouseExport;
    String nameWareHouseImport;

    public TransshipmentPointDialog(List<AreaDistance> areaDistances,String nameWareHouseExport,String nameWarehouseImport) {
        this.areaDistances = areaDistances;
        areaDistances.remove(areaDistances.size()-1);
        this.nameWareHouseExport =nameWareHouseExport;
        this.nameWareHouseImport =nameWarehouseImport;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.transshipment_point_dialog, container, false);
        transshipmentPointDialogVM = ViewModelProviders.of(this).get(TransshipmentPointDialogVM.class);
        mBinding.setViewModel(transshipmentPointDialogVM);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        mBinding.btnClose.setOnClickListener(v -> {
            this.dismiss();
        });
        mBinding.lbNameExport.setText(nameWareHouseExport);
        mBinding.lbNameImport.setText(nameWareHouseImport);
        setUpRecycleView();
        return mBinding.getRoot();
    }


    private void setUpRecycleView() {
        adapter = new XBaseAdapter(R.layout.transshipment_point_dialog_item, areaDistances, new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {

            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mBinding.rcContents.setAdapter(adapter);
        mBinding.rcContents.setLayoutManager(linearLayoutManager);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
}
