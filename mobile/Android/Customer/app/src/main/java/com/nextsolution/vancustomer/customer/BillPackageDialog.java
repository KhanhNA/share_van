package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.BillPackageDialogVM;
import com.nextsolution.vancustomer.databinding.BillPackageDialogBinding;
import com.nextsolution.vancustomer.enums.Constants;

public class BillPackageDialog extends DialogFragment {
    BillPackageDialogVM billPackageDialogVM;
    BillPackageDialogBinding mBinding;
    BillPackage billPackage = new BillPackage();
    String qrCode;

    public BillPackageDialog(BillPackage billPackage, String qrCode) {
        this.billPackage = billPackage;
        this.qrCode = qrCode;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.bill_package_dialog, container, false);
        billPackageDialogVM = ViewModelProviders.of(this).get(BillPackageDialogVM.class);
        mBinding.setViewModel(billPackageDialogVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        getData();
        new CountDownTimer(4000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                autoDismiss();
            }
        }.start();
        return mBinding.getRoot();
    }

    private void autoDismiss() {
        this.dismiss();
    }

    public void getData() {
        mBinding.txtTitle.setText(qrCode);
        billPackageDialogVM.getBillPackage().set(billPackage);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
}
