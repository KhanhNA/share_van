package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.CustomerApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.MyRankingCustomer;
import com.nextsolution.db.dto.RankingDTO;
import com.nextsolution.db.dto.Partner;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyRankingVM extends BaseViewModel {
    ObservableField<MyRankingCustomer> myRankingCustomer= new ObservableField<>();
    ObservableField<Integer> totalPointNextRank= new ObservableField<>();
    ObservableField<RankingDTO> rankCurrent= new ObservableField<>();
    ObservableList<RankingDTO> listRanking = new ObservableArrayList<>();
    ObservableField<Partner> infoCustomer = new ObservableField<>();
    int index_rank_current=0;
    public MyRankingVM(@NonNull Application application) {
        super(application);
        infoCustomer.set(StaticData.getPartner());
    }
    public void getData(RunUi runUi){
        CustomerApi.getRankingCustomer(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(o,runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });

    }
    private void getDataSuccess(Object o,RunUi runUi){
        MyRankingCustomer myRankingCustomer= (MyRankingCustomer) o;
        if(myRankingCustomer!=null && myRankingCustomer.getList_rank()!=null && myRankingCustomer.getList_rank().size()>0){
            this.myRankingCustomer.set(myRankingCustomer);
            listRanking.addAll(myRankingCustomer.getList_rank());
            handleData(myRankingCustomer);
            runUi.run(Constants.GET_DATA_SUCCESS);
        }
        else{
            runUi.run(Constants.GET_DATA_FAIL);
        }
    }

    //getCurrent_rank()=="1" .rank hien tai
    private void handleData(MyRankingCustomer myRankingCustomer){
        int check_rank=0;
        for(RankingDTO item: myRankingCustomer.getList_rank()){
            if(item.getCurrent_rank().equals("1")){
                totalPointNextRank.set(item.getTo_point());
                index_rank_current = check_rank;
                rankCurrent.set(item);
                return;
            }
            check_rank++;
        }
    }

}
