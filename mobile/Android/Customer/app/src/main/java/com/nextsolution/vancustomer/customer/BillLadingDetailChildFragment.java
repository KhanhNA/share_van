package com.nextsolution.vancustomer.customer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.BillLadingDetailVM;
import com.nextsolution.vancustomer.databinding.BillLadingDetailChildFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class BillLadingDetailChildFragment extends BaseFragment {
    BillLadingDetailChildFragmentBinding mBinding;
    BillLadingDetailVM billLadingDetailVM;
    XBaseAdapter adapter;
    XBaseAdapter adapterService;
    BillLadingDetail billLadingDetail;

    public BillLadingDetailChildFragment(BillLadingDetail billLadingDetail) {
        this.billLadingDetail =billLadingDetail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= super.onCreateView(inflater, container, savedInstanceState);
        billLadingDetailVM= (BillLadingDetailVM) viewModel;
        mBinding= (BillLadingDetailChildFragmentBinding) binding;
        getData();
        initView();
        return view;
    }
    public void initView(){
        try{
            if(billLadingDetailVM.getBillLadingDetail().get().getBillPackages().size()>0){
                setUpRecycleView();
            }else {
                mBinding.llListProducts.setVisibility(View.GONE);
                mBinding.line2.setVisibility(View.GONE);
            }
            if(billLadingDetailVM.getBillLadingDetail().get().getBillService().size()>0){
                setUpRecycleViewAttachService();
            }else{
                mBinding.line.setVisibility(View.GONE);
                mBinding.lbAttachServices.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getData(){
            billLadingDetailVM.getBillLadingDetail().set(this.billLadingDetail);
            for (BillPackage item: billLadingDetail.getBillPackages()) {
                billLadingDetailVM.getList_warehouse_name().add(item.getProduct_type_name());
            }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpRecycleView(){
        adapter = new XBaseAdapter(R.layout.bill_lading_detail_item,billLadingDetailVM.getBillLadingDetail().get().getBillPackages(), this);
        mBinding.infoBillLading.setAdapter(adapter);
    }
    private void setUpRecycleViewAttachService(){
        adapterService = new XBaseAdapter(R.layout.service_bill_ladding_detail_item, billLadingDetailVM.getBillLadingDetail().get().getBillService(), this);
        mBinding.rcAttachServices.setAdapter(adapterService);
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mBinding.rcAttachServices.setLayoutManager(layoutManager);
    }


    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.GET_DATA_SUCCESS)) {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(View v, Object o) {
//        if(v.getId()==R.id.itemInfoBillLading){
//        }
    }


    @Override
    public int getLayoutRes() {
        return R.layout.bill_lading_detail_child_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BillLadingDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.info_bill_lading;
    }


}
