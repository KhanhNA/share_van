package com.nextsolution.vancustomer.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.nextsolution.db.api.BaseApi;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.model.UserInfo;
import com.nextsolution.vancustomer.util.ApiResponse;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.LanguageUtils;
import com.nextsolution.vancustomer.util.NetworkUtils;
import com.nextsolution.vancustomer.util.StringUtils;
import com.nextsolution.vancustomer.viewmodel.LoginVM;
import com.ns.odoolib_retrofit.wrapper.OdooClient;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

public class SplashActivity extends BaseActivity {
    Bundle extras;
    SharedPreferences preferences = AppController.getInstance().getSharePre();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        try {
//            OdooV2.createInstance(getApplicationContext(), AppController.BASE_URL).setOnConnect(this);
        // TODO: 23/09/2020 configServer: OdooClient client = new OdooClient(this, AppController.BASE_URL);
        OdooClient client = new OdooClient(this, preferences.getString("U_LOGIN", AppController.SERVER_URL) + ":8070");
//            BaseApi.setOdoo(client);
        onConnect(client, null);
//        } catch (OdooVersionException e) {
//            e.printStackTrace();
//        }
        //
        if (getIntent() != null) {
            extras = getIntent().getExtras();
        }

    }

    private void startLogin() {
        startActivity(new Intent(SplashActivity.this, LoginActivityV2.class));
        // close splash activity
        finish();
    }

    private void consumeResponse(ApiResponse apiResponse) {

        switch (apiResponse.status) {
            case SUCCESS:
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                if (extras != null) {
                    intent.putExtras(extras);
                }
                startActivity(intent);
                finish();
                break;
            case NOT_CONNECT:
            case ERROR:
                startLogin();
                break;

            default:
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return LoginVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    public void onConnect(OdooClient odooV2, Throwable volleyError) {
        LanguageUtils.loadLocale(SplashActivity.this);
        BaseApi.setOdoo(odooV2);
        LoginVM loginVM = new LoginVM(getApplication());
        //
        if (getIntent() != null) {
            extras = getIntent().getExtras();
        }
        //
        if (preferences != null) {
            String userName = preferences.getString(Constants.USER_NAME, "");
            String pass = preferences.getString(Constants.MK, "");
            if (StringUtils.isNotNullAndNotEmpty(userName) && StringUtils.isNotNullAndNotEmpty(pass) && NetworkUtils.isNetworkConnected(SplashActivity.this)) {
                loginVM.getModel().set(UserInfo.builder().userName(userName).passWord(pass).build());
                loginVM.requestLogin();
            } else {
                startLogin();
            }
        } else {
            startLogin();
        }
        loginVM.loginResponse().observe(this, this::consumeResponse);
    }
}
