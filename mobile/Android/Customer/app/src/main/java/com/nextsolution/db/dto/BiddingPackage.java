package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.base.AppController;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingPackage extends BaseModel {
    Integer id;
    String bidding_package_number;
    String status;
    OdooDateTime confirm_time;
    OdooDateTime release_time;
    OdooDateTime bidding_time;
    Integer max_count;
    Double distance;
    Double from_latitude;
    Double from_longitude;
    Double to_latitude;
    Double to_longitude;
    OdooDateTime from_receive_time;
    OdooDateTime to_receive_time;
    OdooDateTime from_return_time;
    OdooDateTime to_return_time;
    OdooDateTime create_date;
    OdooDateTime write_date;
    Double price_origin;
    Double price;
    Integer countdown_time;
    Integer price_time_change;
    Double price_level_change;
    List<CargoTypes> cargo_types;
    BiddingOrder bidding_order;
}
