package com.nextsolution.db.dto;

import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HistoryRatingPointDTO extends BaseModel {
    Long id;
    Integer point;
    Integer type;
    String create_date;
    String name;
    String uri_path;
}
