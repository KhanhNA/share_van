package com.nextsolution.vancustomer.enums;

/**
 * Created by ${Saquib} on 03-05-2018.
 */

public enum Status {
    LOADING,
    SUCCESS,
    ERROR,
    NOT_CONNECT
}