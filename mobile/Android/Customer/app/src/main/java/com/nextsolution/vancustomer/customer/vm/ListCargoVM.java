package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.DepotAPI;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.CargoDTO;
import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListCargoVM extends BaseViewModel {
    ObservableField<Boolean> emptyData= new ObservableField<>();
    ObservableField<CargoTypes> cargoTypes =  new ObservableField<>();
    ObservableField<String> cargoCode= new ObservableField<>();
    ObservableList<CargoDTO> listData= new ObservableArrayList<>();
    Integer offset= Constants.FIRST_PAGE;
    Integer size_id=0;
    private Integer totalPage = 0;
    public ListCargoVM(@NonNull Application application) {
        super(application);
        CargoTypes cargoTypes= new CargoTypes();
        cargoTypes.setType(AppController.getInstance().getResources().getString(R.string.ALL));
        this.getCargoTypes().set(cargoTypes);
        emptyData.set(true);
    }
    public void getListCargo(RunUi runUi){
        offset=0;
        getData(offset,false,runUi);
    }
    public void getData(Integer offset,Boolean loadMore,RunUi runUi){
        DepotAPI.getCargosByDepotId(size_id, cargoCode.get(), offset, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(o,runUi,loadMore);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });

    }
    public void getDataSuccess(Object o,RunUi runUi,Boolean loadMore){
        try{
            OdooResultDto<CargoDTO> odooResultDto= (OdooResultDto<CargoDTO>) o;
            if(odooResultDto.getRecords()!=null){
                totalPage=odooResultDto.getTotal_record();
                if(!loadMore)
                listData.clear();
                listData.addAll(odooResultDto.getRecords());
                emptyData.set(false);
                runUi.run(Constants.GET_DATA_SUCCESS);
            }else{
                emptyData.set(true);
            }

        }catch (Exception e){
            emptyData.set(true);
            listData.clear();
            runUi.run(Constants.GET_DATA_SUCCESS);
        }

    }


    public void loadMore(RunUi runUi){
        offset += 10;
        Log.d("listCargo", "loadMore"+offset);
        if (offset < totalPage) {
            getData(offset,true,runUi);
        } else {
            runUi.run("noMore");
        }
    }
}
