package com.nextsolution.vancustomer.scan;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.SparseArray;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.StaticData;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.io.IOException;

public class QRCodeScannerFragment extends BaseFragment {
    private static final int REQUEST_CAMERA = 1;
    private SurfaceView mCameraPreview;
    private CameraSource mCameraSource;

    private void initCam(View view){
        Display display = getBaseActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mCameraPreview = view.findViewById(R.id.camera_view);

        BarcodeDetector mBarcodeDetector = new BarcodeDetector.Builder(getBaseActivity()).setBarcodeFormats(Barcode.QR_CODE).build();

        mCameraSource = new CameraSource.Builder(getBaseActivity(), mBarcodeDetector).setFacing(
                CameraSource.CAMERA_FACING_BACK)
                .setAutoFocusEnabled(true)
                .build();

        mCameraPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {

                try {
                    mCameraSource.start(mCameraPreview.getHolder());
                } catch (IOException e) {
//                    Log.e(Constants.ERROR_PROCESS, e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                mCameraSource.stop();
            }
        });

        mBarcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                SparseArray<Barcode> barcodeList = detections.getDetectedItems();
                if (barcodeList != null && barcodeList.size() > 0) {
                    Vibrator v = (Vibrator) getBaseActivity().getSystemService(Context.VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        //deprecated in API 26
                        v.vibrate(500);
                    }
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(StaticData.RESULT_SCAN, barcodeList.valueAt(0).displayValue);
                    getBaseActivity().setResult(Activity.RESULT_OK, returnIntent);
                    getBaseActivity().finish();
                }
            }
        });
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);

        if (ActivityCompat.checkSelfPermission(getBaseActivity(),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {


            if (ActivityCompat.shouldShowRequestPermissionRationale(getBaseActivity(),
                    Manifest.permission.CAMERA)) {
                final String[] permissions = new String[]{Manifest.permission.CAMERA};
                ActivityCompat.requestPermissions(getBaseActivity(), permissions, 24);
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(getBaseActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }


           return this.binding.getRoot();
        }
        initCam(this.binding.getRoot());
        return this.binding.getRoot();
    }




    @Override
    public int getLayoutRes() {
        return R.layout.fragment_scan;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return QRCodeScannerVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
