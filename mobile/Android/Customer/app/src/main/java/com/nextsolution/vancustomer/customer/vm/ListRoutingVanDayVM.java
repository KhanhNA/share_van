package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ListRoutingVanDayVM extends BaseViewModel {
    private ObservableBoolean isLoading = new ObservableBoolean();
    private ObservableBoolean showMoreInfoWarehouse = new ObservableBoolean();
    private ObservableBoolean showMoreInfoWarehouseExport = new ObservableBoolean();
    private ObservableBoolean showMoreInfoDriver = new ObservableBoolean();

    //check duoc confirm hay khong
    private ObservableBoolean isConfirm = new ObservableBoolean();

    private ObservableBoolean isNotEmptyImage = new ObservableBoolean(false);
    private ObservableField<RoutingDay> routingDay = new ObservableField<>();
    private List<String> listImage = new ArrayList<>();
    private List<LocalMedia> lstFileSelected;
    private List<BillPackage> listBillPackages = new ArrayList<>();
    private String resultQrCode;
    private boolean isSuccess = false; // check quet het ma qr chua
    /**
     * hashMap<bill_package_id,quantityQrChecked>
     * id bill_package
     * quantityQrChecked số qr bill_package tương ứng đã quét
     */
    private HashMap<Integer, HashMap<String, Boolean>> qrDataChecked = new HashMap<>();

    public ListRoutingVanDayVM(@NonNull Application application) {
        super(application);
        isLoading.set(true);
        showMoreInfoWarehouse.set(false);
        showMoreInfoWarehouseExport.set(false);
    }

    public void getData(String routingPlanDayCode, RunUi runUi) {
        isLoading.set(true);
        OrderApi.getRoutingDayPlanDetail(routingPlanDayCode, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getDataSuccess(Object o, RunUi runUi) {
        OdooResultDto<RoutingDay> resultDto = (OdooResultDto<RoutingDay>) o;
        if (resultDto != null && resultDto.getRecords() != null && resultDto.getRecords().size() > 0) {
            routingDay.set(resultDto.getRecords().get(0));
            listBillPackages.clear();
            listBillPackages.addAll(routingDay.get().getList_bill_package());
            genQrCode(routingDay.get().getList_bill_package());
            listImage.addAll(routingDay.get().getImage_urls());
            if (routingDay.get().getDate_plan().getDate() == (new Date()).getDate()) {
                isConfirm.set(true);
            } else {
                isConfirm.set(false);
            }
            runUi.run(Constants.GET_DATA_SUCCESS);
        } else {
            runUi.run(Constants.GET_DATA_FAIL);
        }
        isLoading.set(false);
    }

    public boolean checkConfirm() {
        if (isSuccess) {
            return true;
        }
        for (BillPackage item : listBillPackages) {
            if (!item.isQrChecked()) {
                return false;
            }
        }
        return true;
    }

    public void setDataFileChoose(List<LocalMedia> albumFiles) {
        lstFileSelected = albumFiles;
        if (albumFiles != null && albumFiles.size() > 0) {
            isNotEmptyImage.set(true);
        } else {
            isNotEmptyImage.set(false);
        }
    }


    public void confirmRouting(String description, RunUi runUi) {
        isLoading.set(true);
        RoutingApi.confirmRouting(description, routingDay.get(), lstFileSelected, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if (o != null) {
                    runUi.run("confirmSuccess");
                } else {
                    runUi.run("confirmFail");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    private void genQrCode(List<BillPackage> billPackages) {
        for (BillPackage temp : billPackages) {
            String[] strQr = temp.getQr_char().split(",");
            HashMap<String, Boolean> tempHashMap = new HashMap<>();
            for (String item : strQr) {
                tempHashMap.put(item, false);
            }
            qrDataChecked.put(temp.getBill_package_id(), tempHashMap);
        }
    }
}
