package com.nextsolution.db.dto;


import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Insurance extends BaseModel implements IModel{
    private Integer id;
    private String code;
    private String name;
    private Vendor vendor;
    private String status;
    private Double amount;
    private String display_name;
    private String description;
    @Override
    public String getModelName() {
        return "sharevan.insurance";
    }
}