package com.nextsolution.vancustomer.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;

public class SetErrorEditText {
    public static void setInputTextHandle(EditText editText, TextInputLayout textInputLayout){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    textInputLayout.setError(null);
                }
            }
        });
    }
    public static Boolean checkNullOrEmpty(EditText editText,TextInputLayout textInputLayout,String strError){
        if(StringUtils.isNullOrEmpty(editText.getText().toString())){
            textInputLayout.setError(strError);
            return true;
        }
        return false;
    }
    public static Boolean checkNullOrEmpty(EditText editText,String strError){
        if(StringUtils.isNullOrEmpty(editText.getText().toString())){
            editText.setError(strError);
            return true;
        }
        return false;
    }
}
