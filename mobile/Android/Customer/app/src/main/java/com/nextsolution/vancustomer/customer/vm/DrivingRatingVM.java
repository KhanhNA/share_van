package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.IResponse;
import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.dto.Driver;
import com.nextsolution.db.dto.DriverRating;
import com.nextsolution.db.dto.RatingBadges;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
public class DrivingRatingVM extends BaseViewModel {
    ObservableBoolean  emptyData= new ObservableBoolean();
    ObservableBoolean isLoading= new ObservableBoolean();
    ObservableBoolean  isRating= new ObservableBoolean();
    ObservableField<Driver> driver;
    ObservableBoolean  readOnly= new ObservableBoolean();
    ObservableField<String> description= new ObservableField<>();
    ObservableList<RatingBadges> listRatingBadges ;
    HashMap<String, List<RatingBadges>> hashMapRatingBadges = new HashMap<>();

    public DrivingRatingVM(@NonNull Application application) {
        super(application);
        isLoading.set(false);
        description.set(Constants.EMPTY_STRING);
        listRatingBadges = new ObservableArrayList<>();
        driver=new ObservableField<>();
        readOnly.set(false);
        emptyData.set(false);
        isRating.set(true);
    }

    public void getRatingBadges(RunUi runUi){
        OrderApi.getListRatingBadges(new IResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(true);
                getDataSuccess(o,runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
    public void getDataSuccess(Object o,RunUi runUi){
            OdooResultDto<RatingBadges> resultDto= (OdooResultDto<RatingBadges>) o;
            listRatingBadges.clear();
            if(resultDto!=null && resultDto.getRecords() != null && resultDto.getRecords().size()>0){
//                listRatingBadges.addAll(resultDto.getRecords());
                setDataHashMapRatingBadges(resultDto.getRecords());
                setData(listRatingBadges);
                runUi.run(Constants.GET_DATA_SUCCESS);
            }


    }
    public void drivingRating(Integer number_star,Integer routing_plan_day_id,RunUi runUi){
        Long driver_id = driver.get().getId();
        List<Integer> integerList= new ArrayList<>();
        if (listRatingBadges != null) {
            for (int i = 0; i < listRatingBadges.size(); i++) {
                if(listRatingBadges.get(i).isCheck()){
                    integerList.add(listRatingBadges.get(i).getId());
                }
            }
        }
        DriverRating driverRating= new DriverRating();
        driverRating.setDriver_id(driver_id);
        driverRating.setDescription(description.get());
        driverRating.setEmployee_id(StaticData.getPartner().getId());
        driverRating.setNum_rating(number_star);
        driverRating.setRating_badges(integerList);
        driverRating.setRouting_plan_day_id(routing_plan_day_id);
        OrderApi.drivingRating(driverRating, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                Boolean aBoolean= (Boolean) o;
                    if(aBoolean){
                        runUi.run(Constants.SUCCESS);
                    }else{
                        runUi.run(Constants.FAIL);
                    }

            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
    public void setDataHashMapRatingBadges(List<RatingBadges> data) {
        for (RatingBadges item : data) {
            List<RatingBadges> listTemp = new ArrayList<>();
            if (hashMapRatingBadges.get(item.getRating_level()) != null) {
                listTemp.addAll(hashMapRatingBadges.get(item.getRating_level()));
            }
            listTemp.add(item);
            hashMapRatingBadges.put(item.getRating_level(), listTemp);
        }
        if (hashMapRatingBadges.get("1") != null)
            listRatingBadges.addAll(hashMapRatingBadges.get("1"));
    }
}
