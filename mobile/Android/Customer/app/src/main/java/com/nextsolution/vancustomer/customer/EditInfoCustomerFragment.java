package com.nextsolution.vancustomer.customer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.ScreenUtils;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.GridImageAdapter;
import com.nextsolution.vancustomer.customer.vm.EditInfoCustomerVM;
import com.nextsolution.vancustomer.databinding.CreateAccountStep2Binding;
import com.nextsolution.vancustomer.databinding.EditInfoCustomerFragmentBinding;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.ui.fragment.DialogConfirm;
import com.nextsolution.vancustomer.util.SetErrorEditText;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.util.ZoomImage;
import com.nextsolution.vancustomer.widget.FullyGridLayoutManager;
import com.nextsolution.vancustomer.widget.GlideEngine;
import com.nextsolution.vancustomer.widget.OnSingleClickListener;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class EditInfoCustomerFragment extends BaseFragment {
    EditInfoCustomerVM editInfoCustomerVM;
    EditInfoCustomerFragmentBinding mBinding;
    final int PICK_IMAGE_PERMISSIONS_REQUEST_CODE = 999;
    DialogConfirm dialogConfirm;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        editInfoCustomerVM = (EditInfoCustomerVM) viewModel;
        mBinding = (EditInfoCustomerFragmentBinding) binding;
        mBinding.txtTitle.setText(getResources().getString(R.string.EDIT_INFO));
        initView();
        return view;
    }

    private void initView() {
        SetErrorEditText.setInputTextHandle(mBinding.lbCity,mBinding.ipCity);
        SetErrorEditText.setInputTextHandle(mBinding.lbZip,mBinding.ipZip);
        SetErrorEditText.setInputTextHandle(mBinding.lbMail,mBinding.ipMail);
        SetErrorEditText.setInputTextHandle(mBinding.lbStreet,mBinding.ipStreet);
    }

    public void backUpAvatar() {
        editInfoCustomerVM.getChangeAvatar().set(false);
    }

    public Boolean checkEmptyEditText() {
        boolean check = true;
        if(SetErrorEditText.checkNullOrEmpty(mBinding.lbStreet,mBinding.ipStreet,getResources().getString(R.string.pass_empty))){
            check= false;
        }
        if(SetErrorEditText.checkNullOrEmpty(mBinding.lbCity,mBinding.ipCity,getResources().getString(R.string.pass_empty))){
            check= false;
        }
        if(SetErrorEditText.checkNullOrEmpty(mBinding.lbZip,mBinding.ipZip,getResources().getString(R.string.pass_empty))){
            check= false;
        }
        if (editInfoCustomerVM.getIsEditAvatar().get()) {
            if (TextUtils.isEmpty(mBinding.lbMail.getText().toString())) {
                mBinding.ipMail.setError(getResources().getString(R.string.pass_empty));
                check = false;
            } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.lbMail.getText().toString()).matches()) {
                check = false;
                mBinding.ipMail.setError(getString(R.string.Incorrect_email_format));
            }
        }
        return check;
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        mBinding.btnEdit.revertAnimation();
        switch (action) {
            case Constants.GET_DATA_SUCCESS:
                ToastUtils.showToast(getActivity(), getString(R.string.Edit_info_success), getResources().getDrawable(R.drawable.ic_check));
                setResult();
                getActivity().onBackPressed();
                break;
            case Constants.FAIL:
                ToastUtils.showToast(getActivity(), getString(R.string.Edit_info_fail), getResources().getDrawable(R.drawable.ic_close16));
                break;
        }

    }

    public void zoomAvatar() {
        if (!editInfoCustomerVM.getChangeAvatar().get()) {
            ZoomImage.zoom(this, editInfoCustomerVM.getInfoCustomer().get().getUri_path());
        } else {
            ZoomImage.zoom(this, editInfoCustomerVM.getImg1().get());
        }
    }

    private void setResult() {
        Intent intent = new Intent();
        getBaseActivity().setResult(Constants.RESULT_OK, intent);
    }


    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.btnBack) {
            getBaseActivity().onBackPressed();
        } else if (view.getId() == R.id.btnEdit) {
            if (checkEmptyEditText()) {
                showDialogConfirm();
            } else {
                mBinding.btnEdit.revertAnimation();
            }
        } else if (view.getId() == R.id.btnChangeAvatar) {
            selectImages();
        }
    }

    public void selectImages() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            pickImage();
        }

    }

    private void pickImage() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .isSingleDirectReturn(true)
                .maxSelectNum(1)
                .forResult(new OnResultCallbackListener() {
                    @Override
                    public void onResult(List result) {
                        if (result != null && result.size() > 0) {
                            editInfoCustomerVM.setUrl_avatar((LocalMedia) result.get(0));
                            editInfoCustomerVM.getImg1().set(editInfoCustomerVM.getUrl_avatar().getCompressPath());
                            editInfoCustomerVM.getChangeAvatar().set(true);
                        }
                    }

                    @Override
                    public void onCancel() {
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            for (int item : grantResults) {
                if (item != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            pickImage();
        }
    }

    public void showDialogConfirm() {
        dialogConfirm = new DialogConfirm(getString(R.string.Do_you_want_change_info),
                "", new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mBinding.btnEdit.startAnimation();
                dialogConfirm.dismiss();
                if (editInfoCustomerVM.getChangeAvatar().get()) {
                    editInfoCustomerVM.editAvatar(EditInfoCustomerFragment.this::runUi);
                }
                editInfoCustomerVM.updateCustomer(EditInfoCustomerFragment.this::runUi);
            }
        });
        dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.REQUEST_CODE && resultCode == Constants.EDIT_INFO_SUCCESS) {
            editInfoCustomerVM.getInfoCustomer().set(StaticData.getPartner());
        } else {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.Edit_info_fail), getResources().getDrawable(R.drawable.ic_fail));
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.edit_info_customer_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return EditInfoCustomerVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
