package com.nextsolution.vancustomer.customer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolution.db.dto.Driver;
import com.nextsolution.db.dto.RatingBadges;
import com.nextsolution.db.dto.RatingDriverDTO;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.DrivingRatingVM;
import com.nextsolution.vancustomer.databinding.DrivingRatingFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.StringUtils;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class DrivingRatingFragment extends BaseFragment {
    XBaseAdapter adapter;
    //    ImageBaseAdapter imageBaseAdapter;
    DrivingRatingFragmentBinding mBinding;
    DrivingRatingVM drivingRatingVM;
    Integer routing_plan_day_id;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (DrivingRatingFragmentBinding) binding;
        drivingRatingVM = (DrivingRatingVM) viewModel;
        initView();
        setUpRecycleView();
        getData();
        return view;
    }
    public void initView(){
        mBinding.toolbar.setOnClickListener(v -> {
            setResultData();
            getActivity().onBackPressed();
        });
        mBinding.rbRatingStar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Integer number_star = Math.round(ratingBar.getRating());
                drivingRatingVM.getListRatingBadges().clear();
                if(drivingRatingVM.getHashMapRatingBadges().get(number_star+"")!=null){
                    drivingRatingVM.getListRatingBadges().addAll(drivingRatingVM.getHashMapRatingBadges().get(number_star+""));
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.GET_DATA_SUCCESS)) {
            adapter.notifyDataSetChanged();
        } else if (action.equals(Constants.SUCCESS)) {
            setResultData();
            showToast(getString(R.string.RATING_SUCCESS));
        } else if (action.equals(Constants.FAIL)) {
            mBinding.toolbar.setEnabled(true);
            showToast(getString(R.string.RATING_FAIL));
        }

    }

    private void setResultData() {
        Intent intent = new Intent();
        getBaseActivity().setResult(Constants.RESULT_OK, intent);
    }

    public void showToast(String text_toast) {
        if (text_toast.equals(getResources().getString(R.string.RATING_SUCCESS))) {
            ToastUtils.showToast(getActivity(), text_toast, getResources().getDrawable(R.drawable.ic_check));
            getActivity().onBackPressed();
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    getActivity().onBackPressed();
//                }
//            }, 3000);
        } else {
            ToastUtils.showToast(getActivity(), text_toast, getResources().getDrawable(R.drawable.ic_fail));
        }

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.btnSave) {
            mBinding.toolbar.setEnabled(false);
            Integer number_star = Math.round(mBinding.rbRatingStar.getRating());
            drivingRatingVM.getIsLoading().set(false);
            drivingRatingVM.drivingRating(number_star, routing_plan_day_id, this::runUi);
        } else if (view.getId() == R.id.toolbar) {
            getActivity().onBackPressed();
        }
    }

    public void getData() {
        Intent intent = getActivity().getIntent();
        try {
            if (intent != null) {
                if (intent.hasExtra(Constants.MODEL)) {
                    drivingRatingVM.getDriver().set((Driver) intent.getSerializableExtra(Constants.MODEL));
                    this.routing_plan_day_id = intent.getIntExtra("routing_plan_day_id", -1);
                    drivingRatingVM.getReadOnly().set(intent.getBooleanExtra("Read_only", false));
                    if (drivingRatingVM.getReadOnly().get()) {
                        mBinding.toolbar.setText(getResources().getString(R.string.driver_rating_history));
                        RatingDriverDTO rating_driver = (RatingDriverDTO) intent.getSerializableExtra("Rating_driver");
                        if (rating_driver != null && (rating_driver.getNum_rating() != null || rating_driver.getBadges_routings() != null)) {
                            mBinding.rbRatingStar.setRating(rating_driver.getNum_rating());
                            if (!StringUtils.isNullOrEmpty(rating_driver.getNote()))
                                mBinding.txtDescription.setText(getString(R.string.NOTE) + ": " + rating_driver.getNote());
                            mBinding.rbRatingStar.setIsIndicator(true);
                            drivingRatingVM.getListRatingBadges().clear();
                            drivingRatingVM.getListRatingBadges().addAll(rating_driver.getBadges_routings());
                            adapter.notifyDataSetChanged();
                        } else {
                            drivingRatingVM.getEmptyData().set(true);
                        }
                        drivingRatingVM.getIsRating().set(false);
                    } else {
                        drivingRatingVM.getRatingBadges(this::runUi);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onItemClick(View v, Object o) {
        if (v.getId() == R.id.itemBadges && !drivingRatingVM.getReadOnly().get()) {
            Integer position = ((RatingBadges) o).index - 1;
            if (drivingRatingVM.getListRatingBadges().get(position).isCheck()) {
                drivingRatingVM.getListRatingBadges().get(position).setCheck(false);
            } else {
                drivingRatingVM.getListRatingBadges().get(position).setCheck(true);
            }
            adapter.notifyItemChanged(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setUpRecycleView() {
        adapter = new XBaseAdapter(R.layout.rate_badges_item, drivingRatingVM.getListRatingBadges(), this);
        mBinding.rcRateBadges.setAdapter(adapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3, RecyclerView.VERTICAL, false);
        mBinding.rcRateBadges.setLayoutManager(layoutManager);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.driving_rating_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return DrivingRatingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
