package com.nextsolution.db.dto;

public class Duration {
    public String text;
    public double value;

    public Duration(String text, double value) {
        this.text = text;
        this.value = value;
    }
}