package com.nextsolution.vancustomer.confirm_change;

import android.app.Application;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.api.UpdateBilLadingApi;
import com.nextsolution.db.dto.AreaDistance;
import com.nextsolution.db.dto.BillLading;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.DistanceDTO;
import com.nextsolution.db.dto.Hub;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.model.ApiResponseModel;
import com.tsolution.base.BaseViewModel;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConfirmChangeVM extends BaseViewModel<BillLading> {
    private String fromWareHouseCode;
    public HashMap<Long, BillPackage> mapPickup = new HashMap<>();//key: id billPackage

    private ObservableBoolean isLoading = new ObservableBoolean();
    public ObservableBoolean isValid = new ObservableBoolean();

    public ConfirmChangeVM(@NonNull Application application) {
        super(application);
        fromWareHouseCode = new Date().getTime() + "";
        model.set(new BillLading());
    }

    public void getBillLadingUpdate(String bill_detail_id, RunUi runUi) {
        isLoading.set(true);
        UpdateBilLadingApi.getBillUpdate(bill_detail_id, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    getBillLadingSuccess((BillLading) o, runUi);
                }
                isLoading.set(false);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    private void getBillLadingSuccess(BillLading o, RunUi runUi) {
        //đưa danh sách billPackage vào HashMap
        List<BillPackage> listPickup = o.getArrBillLadingDetail().get(0).getBillPackages();

        //tính toán thông số bill detail pickup
        double pickupWeight = 0;
        int pickupParcel = 0;
        double pickupVol = 0;
        for (BillPackage billPakcage : listPickup) {
            billPakcage.setRest(billPakcage.getQuantity_package());
            billPakcage.setSplitQuantity(0);
            //set billPakcage id gốc
            billPakcage.setFrom_bill_package_id(billPakcage.getOrigin_bill_package().getId());
            mapPickup.put(billPakcage.getId(), billPakcage);

            pickupWeight += billPakcage.getNet_weight() * billPakcage.getQuantity_package();
            pickupParcel += billPakcage.getQuantity_package();
            pickupVol += billPakcage.getTotalVol();
        }
        for (int i = 1; i < o.getArrBillLadingDetail().size(); i++) {
            double total_weight = 0;
            int totalParcel = 0;
            double totalVol = 0;
            BillLadingDetail billLadingDetail = o.getArrBillLadingDetail().get(i);
            for (BillPackage billPackage : billLadingDetail.getBillPackages()) {
                BillPackage pickup = mapPickup.get(billPackage.getFrom_change_bill_package_id());
                if (pickup != null) {
                    pickup.setRest(pickup.getRest() - billPackage.getQuantity_package());
                    pickup.setSplitQuantity(pickup.getSplitQuantity() + billPackage.getQuantity_package());
                    //tính toán thông số bill detail drop
                    total_weight += billPackage.getNet_weight() * billPackage.getQuantity_package();
                    totalParcel += billPackage.getQuantity_package();
                    totalVol += billPackage.getTotalVol();
                }

            }
            //set info billDetail
            billLadingDetail.setTotal_weight(total_weight);
            billLadingDetail.setTotal_parcel(totalParcel);
            billLadingDetail.setTotal_volume(totalVol);
        }

        o.getArrBillLadingDetail().get(0).setTotal_volume(pickupVol);
        o.getArrBillLadingDetail().get(0).setTotal_parcel(pickupParcel);
        o.getArrBillLadingDetail().get(0).setTotal_weight(pickupWeight);

        model.set(o);
        runUi.run("getBillLadingSuccess");
        getDestination(o.getArrBillLadingDetail().get(0), true, runUi);

    }

    public boolean addReceiptWareHouse(BillLadingDetail billLadingDetail) {
        boolean isValid = true;

        List<BillPackage> receiptList = billLadingDetail.getBillPackages();
        boolean isFull = true;
        for (int i = receiptList.size() - 1; i >= 0; i--) {
            if (receiptList.get(i).getQuantity_package() > receiptList.get(i).getRest()) {
                isValid = false;
            }

            if (receiptList.get(i).getQuantity_package() != 0) {
                isFull = false;
            } else {
                isValid = false;
            }
        }

        if (isFull) {
            isValid = false;
            addError("fullPackage", R.string.INVALID_FIELD);
        } else {
            clearErro("fullPackage");
        }

        return isValid;
    }


    public void setCapacity(BillLadingDetail billLadingDetail) {
        double total_weight = 0;
        int totalParcel = 0;
        double totalVol = 0;
        List<BillPackage> billPackages = billLadingDetail.getBillPackages();
        for (BillPackage billPackage : billPackages) {
            total_weight += billPackage.getNet_weight() * billPackage.getQuantity_package();
            totalParcel += billPackage.getQuantity_package();
            totalVol += billPackage.getTotalVol();
        }
        billLadingDetail.setTotal_weight(total_weight);
        billLadingDetail.setTotal_parcel(totalParcel);
        billLadingDetail.setTotal_volume(totalVol);

    }

    public AreaDistance getAreaDistance(Hub from, Hub to, int type) {
        AreaDistance areaDistance = new AreaDistance();
        areaDistance.setType(type);

        areaDistance.setFromLocation(new LatLng(from.getLatitude(), from.getLongitude()));
        areaDistance.setToLocation(new LatLng(to.getLatitude(), to.getLongitude()));

        areaDistance.setFrom_name_seq(from.getName_seq());
        areaDistance.setTo_name_seq(to.getName_seq());
        areaDistance.setFrom_warehouse_name(from.getName());
        areaDistance.setTo_warehouse_name(to.getName());

        return areaDistance;
    }

    /**
     * Tính tiền
     */
    public void priceCalculate() {
        isLoading.set(true);
        OrderApi.getPrice(getApplication().getApplicationContext(), getModelE(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                List<BillLadingDetail> billLadingDetails = ((BillLading)o).getArrBillLadingDetail();
                for(int i = 0; i < billLadingDetails.size(); i++){
                    getModelE().getArrBillLadingDetail().get(i).setPrice(billLadingDetails.get(i).getPrice());
                }
                getModelE().setTotal_amount(((BillLading) o).getTotal_amount());
                model.notifyChange();
                isLoading.set(false);
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                isValid.set(false);
            }
        });

    }

    public void callUpdateBilLading(RunUi runUi) {
        isLoading.set(true);

        model.get().setStartDateStr(model.get().getStart_date());
        OrderApi.updateOder(model.get(), true, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if (o != null && ((ApiResponseModel) o).status == 200) {
                    runUi.run("updateOrderSuccess");
                } else {
                    runUi.run("updateOrderFail");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void callCancelBill(String reason,RunUi runUi) {
        isLoading.set(true);
        OrderApi.cancelOrder(model.get().getOrigin_bill_lading_detail_id(), reason, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if (o != null && ((ApiResponseModel) o).status == 200) {
                    runUi.run("updateOrderSuccess");
                } else {
                    runUi.run("updateOrderFail");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });

    }

    /**
     * Tính toán khoảng cách từ kho -> hub của kho đó hoặc ngươc lại.
     *
     * @param billLadingDetail lấy kho cần tính toán từ billLadingDetail.
     * @param isWarehouseToHub tính từ kho -> hub hay từ hub -> kho.
     * @param runUi            listener thay đổi giao diện.
     */
    public void getDestination(BillLadingDetail billLadingDetail, boolean isWarehouseToHub, RunUi runUi) {
        isLoading.set(true);
        LatLng start;
        LatLng end;
        if (isWarehouseToHub) {
            start = new LatLng(billLadingDetail.getWarehouse().getLatitude(), billLadingDetail.getWarehouse().getLongitude());
            end = new LatLng(billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getLatitude(),
                    billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getLongitude());
        } else {
            end = new LatLng(billLadingDetail.getWarehouse().getLatitude(), billLadingDetail.getWarehouse().getLongitude());
            start = new LatLng(billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getLatitude(),
                    billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getLongitude());
        }

        RoutingApi.getDistanceByLatAndLong(start, end, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if (o != null) {
                    //biến isWarehouseToHub có thể dùng để xác định gọi đây có phải tính toán cho ko xuất hay ko
                    runUi.run("getDistanceSuccess", billLadingDetail, (DistanceDTO) o, isWarehouseToHub);
                } else {
                    runUi.run("getDistanceFail");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
//        String requestUrl = "https://maps.googleapis.com/maps/api/directions/json?" +
//                "mode=driving&"
//                + "transit_routing_preference=less_driving&"
//                + "origin=" + start.latitude + "," + start.longitude + "&"
//                + "destination=" + end.latitude + "," + end.longitude + "&"
//                + "key=" + AppController.API_GOOGLE_MAP_DESTINATION;
//
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
//                requestUrl, null,
//                response -> {
//                    try {
//                        if (response == null)
//                            return;
//                        JSONArray jsonArray = response.getJSONArray("routes");
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONObject jsonRoute = jsonArray.getJSONObject(i);
//                            route = new Route();
//
//                            JSONObject overview_polylineJson = jsonRoute.getJSONObject("overview_polyline");
//                            JSONArray jsonLegs = jsonRoute.getJSONArray("legs");
//                            JSONObject jsonLeg = jsonLegs.getJSONObject(0);
//                            JSONObject jsonDistance = jsonLeg.getJSONObject("distance");
//                            JSONObject jsonDuration = jsonLeg.getJSONObject("duration");
//                            JSONObject jsonEndLocation = jsonLeg.getJSONObject("end_location");
//                            JSONObject jsonStartLocation = jsonLeg.getJSONObject("start_location");
//
//                            route.distance = new Distance(jsonDistance.getString("text"), jsonDistance.getInt("value"));
//                            route.duration = new Duration(jsonDuration.getString("text"), jsonDuration.getInt("value"));
//                            route.endAddress = jsonLeg.getString("end_address");
//                            route.startAddress = jsonLeg.getString("start_address");
//                            route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
//                            route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
//                            route.points = decodePoly(overview_polylineJson.getString("points"));
//
//                        }
//                        isLoading.set(false);
//                        //biến isWarehouseToHub có thể dùng để xác định gọi đây có phải tính toán cho ko xuất hay ko
//                        runUi.run("getDistanceSuccess", billLadingDetail, route, isWarehouseToHub);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                }, error -> {
//            runUi.run("getDistanceFail");
//            isLoading.set(false);
//        });
//        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
//        requestQueue.add(jsonObjectRequest);
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
}
