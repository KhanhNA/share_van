package com.nextsolution.vancustomer.widget;

import android.os.SystemClock;
import android.view.View;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.util.NetworkUtils;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.tsolution.base.listener.AdapterListener;

import androidx.databinding.BindingAdapter;

public abstract class OnSingleClickListener implements View.OnClickListener {


    @BindingAdapter({"listener", "object"})
    public static void setOnSingleClickListener(View v, AdapterListener adapterListener, Object o) {
        v.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (!NetworkUtils.isNetworkConnected(v.getContext())) {
                    ToastUtils.showToast(AppController.getInstance().getResources().getString(R.string.network_error));
                    return;
                }
                adapterListener.onItemClick(v, o);
            }
        });
    }

    private static final long MIN_CLICK_INTERVAL = 500;
    private long mLastClickTime;

    public abstract void onSingleClick(View v);

    @Override
    public final void onClick(View v) {
        if (!NetworkUtils.isNetworkConnected(v.getContext())) {
            ToastUtils.showToast(AppController.getInstance().getResources().getString(R.string.network_error));
            return;
        }
        long currentClickTime = SystemClock.elapsedRealtime();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;
        if (elapsedTime <= MIN_CLICK_INTERVAL)
            return;
        onSingleClick(v);
    }

}