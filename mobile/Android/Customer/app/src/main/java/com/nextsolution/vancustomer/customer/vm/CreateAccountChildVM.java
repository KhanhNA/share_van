package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolution.db.api.CustomerApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.AddressDTO;
import com.nextsolution.db.dto.CareerDTO;
import com.nextsolution.db.dto.Customer;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.enums.LocationType;
import com.nextsolution.vancustomer.model.ErrorBodyDTO;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import okhttp3.ResponseBody;
import retrofit2.Response;

@Setter
@Getter
public class CreateAccountChildVM extends BaseViewModel {

    Customer customer = new Customer();
    //Step 1 values
    List<BaseModel> listProvince = new ArrayList<>();
    List<BaseModel> listDistrict = new ArrayList<>();
    List<BaseModel> listCareer = new ArrayList<>();
    ObservableField<String> day = new ObservableField<>();
    ObservableField<String> month = new ObservableField<>();
    ObservableField<String> year = new ObservableField<>();
    ObservableField<String> province = new ObservableField<>();
    ObservableField<String> district = new ObservableField<>();
    ObservableField<String> career = new ObservableField<>();
    ObservableField<String> first_name = new ObservableField<>();
    ObservableField<String> last_name = new ObservableField<>();
    ObservableField<String> phone = new ObservableField<>();
    //Step 2 values
    ObservableField<String> mail = new ObservableField<>();
    ObservableField<String> password = new ObservableField<>();
    ObservableField<String> confirmPassword = new ObservableField<>();
    ObservableField<String> showPass = new ObservableField<>();
    ObservableBoolean isWa = new ObservableBoolean();

    //Step 3
    private List<LocalMedia> lstFileSelected;
    private ObservableBoolean isNotEmptyImage = new ObservableBoolean(false);

    public CreateAccountChildVM(@NonNull Application application) {
        super(application);
        isWa.set(false);
        Calendar maxDate = Calendar.getInstance();
        maxDate.set(Calendar.YEAR, maxDate.get(Calendar.YEAR) - 15);
        Integer yy = maxDate.get(Calendar.YEAR);
        Integer MM = maxDate.get(Calendar.MONTH);
        Integer dd = maxDate.get(Calendar.DAY_OF_MONTH);
        if(dd<10){
            day.set("0"+dd);
        }else{
            day.set(dd+"");

        }
        if(MM<10){
            month.set("0"+(MM+1));
        }else{
            month.set((MM+1)+"");
        }
        year.set(yy+"");
    }

    public void initDataStep1(RunUi runUi) {
        getProvince(runUi);
        getCareer(runUi);
    }

    public void getCareer(RunUi runUi) {
        CustomerApi.getCareer(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<CareerDTO> resultDto = (OdooResultDto<CareerDTO>) o;
                if (resultDto != null && resultDto.getRecords() != null && resultDto.getRecords().size() > 0) {
                    listCareer.addAll(resultDto.getRecords());
                    career.set(resultDto.getRecords().get(0).getName());
                    customer.setCareer_id(resultDto.getRecords().get(0).getId());
                    runUi.run("CHOOSE_CAREER");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getProvince(RunUi runUi) {
        CustomerApi.getArea(null, LocationType.PROVINCE, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<AddressDTO> resultDto = (OdooResultDto<AddressDTO>) o;
                if (resultDto != null && resultDto.getRecords() != null && resultDto.getRecords().size() > 0) {
                    listProvince.addAll(resultDto.getRecords());
                    runUi.run("CHOOSE_PROVINCE");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getDistrict(Integer parent_id, RunUi runUi) {
        CustomerApi.getArea(parent_id, LocationType.DISTRICT, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<AddressDTO> resultDto = (OdooResultDto<AddressDTO>) o;
                if (resultDto != null && resultDto.getRecords() != null && resultDto.getRecords().size() > 0) {
                    listDistrict.addAll(resultDto.getRecords());
                    runUi.run("CHOOSE_DISTRICT");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void setDataFileChoose(List<LocalMedia> albumFiles) {
        lstFileSelected = albumFiles;
        if (albumFiles != null && albumFiles.size() > 0) {
            isNotEmptyImage.set(true);
        } else {
            isNotEmptyImage.set(false);
        }
    }

    public void createAccount(RunUi runUi) {
        CustomerApi.createAccount(customer, lstFileSelected, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                Response<ResponseBody> response = (Response<ResponseBody>) o;
                if (response.body() != null) {
                    runUi.run(Constants.SUCCESS);
                } else {
                    try {

                        ErrorBodyDTO errorBodyDTO = new Gson().fromJson(response.errorBody().string(), ErrorBodyDTO.class);
                        runUi.run(Constants.FAIL, errorBodyDTO.getStatus());
                    } catch (Exception e) {
                        e.printStackTrace();
                        runUi.run(Constants.FAIL);
                    }
                    return;
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
}
