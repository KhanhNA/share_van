package com.nextsolution.vancustomer.util;

/**
 * Trạng thái rotuing day
 * 0: đang vận chuyển
 * 1: lái xe đã đến kho
 * 2: đã xuất/nhập hàng
 * 3: đơn bị hủy
 * 4: đơn có thay đổi đang chờ customer duyệt
 */
public class RoutingDayStatus {
    public static final String SHIPPING = "0";
    public static final String THE_DRIVER_HAS_ARRIVED = "1";
    public static final String COMPLETED = "2";
    public static final String ORDER_CANCELLED = "3";
    public static final String IN_CLAIM = "4";
}
