package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.RoutingDetail;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListRoutingVanDayPagerviewVM extends BaseViewModel {
    ObservableField<Boolean> hideQrCode= new ObservableField<>();
    private ObservableField<Boolean> isSpinner;
    ObservableField<String> qrResultCode=new ObservableField<>();
    private ObservableList<String> warehouseName = new ObservableArrayList<>();
    private ObservableList<BillPackage> billPackages;
    private List<RoutingDetail> routingDetail;
    public void init(Integer id, RunUi runUi){
    }

    public ListRoutingVanDayPagerviewVM(@NonNull Application application) {
        super(application);
        billPackages = new ObservableArrayList<>();
        isSpinner = new ObservableField<>(new Boolean(true));
        routingDetail = new ArrayList<>();
        hideQrCode.set(true);
    }

    public void getListBillPackage(Integer id, RunUi runUi) {
        OrderApi.getDetailRoutingPlanCustomer(id, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getListBillPaackageSuccess(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getListBillPaackageSuccess(Object o, RunUi runUi) {
        OdooResultDto<RoutingDetail> resultDto = (OdooResultDto<RoutingDetail>) o;
        try{
            routingDetail.clear();
            routingDetail.addAll(resultDto.getRecords());
            runUi.run(Constants.GET_DATA_SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public void setDataRecycleView(Integer position){
        billPackages.clear();
        billPackages.addAll(routingDetail.get(position-1).getList_bill_package_export());
        setData(billPackages);

    }
}
