package com.nextsolution.vancustomer.customer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.maps.model.LatLng;
import com.nextsolution.db.dto.AreaDistance;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.BillService;
import com.nextsolution.db.dto.DistanceDTO;
import com.nextsolution.db.dto.ProductType;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.OrderVMV2;
import com.nextsolution.vancustomer.databinding.PickupWarehouseFragmentBinding;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.nextsolution.vancustomer.ui.fragment.DialogConfirm;
import com.nextsolution.vancustomer.ui.fragment.ListDialogFragment;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.KeyboardUtil;
import com.nextsolution.vancustomer.util.SetErrorEditText;
import com.nextsolution.vancustomer.util.StringUtils;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.util.TsUtils;
import com.nextsolution.vancustomer.enums.WarehouseType;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.lifecycle.ViewModelProviders;


public class PickupWarehouseFragment extends BaseFragment implements NetworkManager.NetworkHandler {
    private AddOrderItemFragment dialogAddItem;

    private ListDialogFragment dialogFragment;
    private PickupWarehouseFragmentBinding mBinding;
    private XBaseAdapter adapter;
    private OrderVMV2 orderVM;
    private DialogConfirm dialogDelete;
    private DialogConfirm dialogConfirm;

    boolean isOnline;
    private NetworkManager networkManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        orderVM = ViewModelProviders.of(getBaseActivity()).get(OrderVMV2.class);
        binding.setVariable(BR.viewModel, orderVM);
        mBinding = (PickupWarehouseFragmentBinding) binding;
        initView();
        return binding.getRoot();

    }

    @Override
    public void onStart() {
        super.onStart();
        networkManager = new NetworkManager(getContext(), this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }

    @Override
    public void onStop() {
        super.onStop();
        networkManager.stop();
    }

    private void initView() {
//        Calendar c = Calendar.getInstance();
//        c.set(Calendar.HOUR_OF_DAY, 6);
//        c.set(Calendar.MINUTE, 0);
//        String date = AppController.formatTime.format(c.getTime());

//        mBinding.txtFromDate.setText(date);
//        orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).setExpected_from_time(new OdooDateTime(c.getTime().getTime()));
//
//        c.set(Calendar.HOUR_OF_DAY, 18);
//        c.set(Calendar.MINUTE, 0);
//        date = AppController.formatTime.format(c.getTime());
//        mBinding.txtToDate.setText(date);
//        orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).setExpected_to_time(new OdooDateTime(c.getTime().getTime()));


        adapter = new XBaseAdapter(R.layout.order_item, orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getBillPackages(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                showOptionTemplate(view, (BillPackage) o);
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        recyclerView.setAdapter(adapter);

        //clear data select box
        mBinding.spSelectWareHouse.setEndIconOnClickListener(v -> {
            orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).setWarehouse(null);
            orderVM.getBillLadingField().notifyChange();
        });
        SetErrorEditText.setInputTextHandle(mBinding.etPhone, mBinding.lbPhone);
        SetErrorEditText.setInputTextHandle(mBinding.etWareHouse, mBinding.spSelectWareHouse);
//        mBinding.ipFromDate.setEndIconOnClickListener(v -> {
//            orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).setExpected_from_time(null);
//            orderVM.getBillLadingField().notifyChange();
//        });
//        mBinding.ipToDate.setEndIconOnClickListener(v -> {
//            orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).setExpected_to_time(null);
//            orderVM.getBillLadingField().notifyChange();
//        });

        mBinding.spSelectWareHouse.setEndIconOnClickListener(v -> {
            orderVM.getSelectedWarehouses().remove(orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getWarehouse().getId());
            orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).setWarehouse(null);
            mBinding.etWareHouse.setText(null);
        });

    }

    private void showDialogEditTemplate(BillPackage o) {
        List<BillPackage> billPackages = orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getBillPackages();
        ProductType selectedType = null;
        if (TsUtils.isNotNull(billPackages)) {
            selectedType = billPackages.get(0).getProductType();
        }
        dialogAddItem = new AddOrderItemFragment(billPackages.size(), o, selectedType, orderVM.getListProductType(), bill -> {
            //nếu có 1 thằng trùng
            int exitsPosition = orderVM.compareBillPackage(bill);
            if (exitsPosition > -1 && exitsPosition != o.index - 1) {
                dialogConfirm = new DialogConfirm(getString(R.string.confirm_change),
                        getString(R.string.msg_duplicate_bill_package), v -> {

                    List<BillLadingDetail> billLadingDetails = orderVM.getBillLadingField().get().getArrBillLadingDetail();

                    //lấy ra bill đã tồn tại
                    BillPackage exits = billLadingDetails.get(0).getBillPackages().get(exitsPosition);
                    //set lại giá trị của bill đã tồn tại
                    exits.setQuantity_package(exits.getQuantity_package() + bill.getQuantity_package());
                    //xóa bill trùng tại kho xuất.
                    billLadingDetails.get(0).getBillPackages().remove(o.index - 1);
                    for (int i = 1; i < billLadingDetails.size(); i++) {
                        //edit bill package from second to end
                        BillLadingDetail billLadingDetail = billLadingDetails.get(i);
                        billLadingDetail.getBillPackages().get(exitsPosition).setInfo(exits);
                        //xóa bill trùng tại các kho nhập
                        billLadingDetail.getBillPackages().remove(o.index - 1);
                    }
                    orderVM.getBillLadingField().notifyChange();
                    adapter.notifyDataSetChanged();
                    dialogConfirm.dismiss();
                });
                dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
            } else {
                //nếu số lượng sau khi sửa nhỏ hơn số lượng trước khi sửa thì hiển thị cảnh bảo.
                if (o.getQuantity_package() > bill.getQuantity_package() && orderVM.getBillLadingField().get().getArrBillLadingDetail().size() > 1) {
                    dialogConfirm = new DialogConfirm(getString(R.string.confirm_change),
                            getString(R.string.msg_eidt_quantity_lower), v -> {

                        orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getBillPackages().set(o.index - 1, bill);//set to first element
                        List<BillLadingDetail> billLadingDetails = orderVM.getBillLadingField().get().getArrBillLadingDetail();
                        for (int i = 1; i < billLadingDetails.size(); i++) {
                            //edit bill package from second to end
                            BillLadingDetail billLadingDetail = billLadingDetails.get(i);
                            billLadingDetail.getBillPackages().get(bill.index - 1).setInfo(bill);
                            billLadingDetail.getBillPackages().get(bill.index - 1).setQuantity_package(0);
                            billLadingDetail.getBillPackages().get(bill.index - 1).setSelect(false);
                        }

                        orderVM.getBillLadingField().notifyChange();
                        adapter.notifyDataSetChanged();
                        dialogConfirm.dismiss();
                    });
                    dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
                } else {
                    orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getBillPackages().set(o.index - 1, bill);//set to first element
                    List<BillLadingDetail> billLadingDetails = orderVM.getBillLadingField().get().getArrBillLadingDetail();
                    for (int i = 1; i < billLadingDetails.size(); i++) {
                        //edit bill package from second to end
                        BillLadingDetail billLadingDetail = billLadingDetails.get(i);
                        billLadingDetail.getBillPackages().get(bill.index - 1).setInfo(bill);
                    }
                    orderVM.getBillLadingField().notifyChange();
                    adapter.notifyDataSetChanged();
                }
            }

        });
        dialogAddItem.show(getBaseActivity().getSupportFragmentManager(),
                "add_photo_dialog_fragment");
    }

    @SuppressLint("RestrictedApi")
    private void showOptionTemplate(View view, BillPackage o) {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(getBaseActivity(), view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu_edit_bill, popup.getMenu());

        MenuPopupHelper menuHelper = new MenuPopupHelper(getBaseActivity(), (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    showDialogEditTemplate(o);
                    break;
                case R.id.action_del:
                    showDialogDelete(o);
                    break;
            }
            return true;
        });

        menuHelper.show();//showing popup menu
    }

    private void showDialogDelete(BillPackage o) {
        dialogDelete = new DialogConfirm(getString(R.string.CONFIRM_DELETE), "", v -> {
            for (int i = 0; i < orderVM.getBillLadingField().get().getArrBillLadingDetail().size(); i++) {
                orderVM.getBillLadingField().get().getArrBillLadingDetail().get(i).getBillPackages().remove(o.index - 1);
            }
            orderVM.getBillLadingField().notifyChange();
            adapter.notifyItemRemoved(o.index - 1);
            dialogDelete.dismiss();
        });
        dialogDelete.show(getChildFragmentManager(), dialogDelete.getTag());
    }

    public void popupItem() {
        List<BillPackage> billPackages = orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getBillPackages();
        ProductType selectedType = null;
        if (TsUtils.isNotNull(billPackages)) {
            selectedType = billPackages.get(0).getProductType();
        }
        dialogAddItem = new AddOrderItemFragment(billPackages.size(), null, selectedType, orderVM.getListProductType(), bill -> {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            //nếu có 1 thằng trùng
            int exitsPosition = orderVM.compareBillPackage(bill);
            if (exitsPosition > -1) {
                dialogConfirm = new DialogConfirm(getString(R.string.confirm_change),
                        getString(R.string.msg_duplicate_bill_package), v -> {

                    List<BillLadingDetail> billLadingDetails = orderVM.getBillLadingField().get().getArrBillLadingDetail();

                    //lấy ra bill đã tồn tại
                    BillPackage exits = billLadingDetails.get(0).getBillPackages().get(exitsPosition);
                    //xét lại giá trị quantity của bill đã tồn tại
                    exits.setQuantity_package(exits.getQuantity_package() + bill.getQuantity_package());

                    for (int i = 1; i < billLadingDetails.size(); i++) {
                        //edit bill package from second to end
                        BillLadingDetail billLadingDetail = billLadingDetails.get(i);
                        billLadingDetail.getBillPackages().get(exitsPosition).setInfo(exits);
                    }
                    orderVM.getBillLadingField().notifyChange();
                    adapter.notifyDataSetChanged();
                    dialogConfirm.dismiss();

                });
                dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
            } else {
                bill.setKey_map(Calendar.getInstance().getTimeInMillis() + "");
                orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getBillPackages().add(bill);//add to first element
                for (int i = 1; i < orderVM.getBillLadingField().get().getArrBillLadingDetail().size(); i++) {
                    //clone bill and add to second to end
                    BillPackage clone = (BillPackage) bill.clone();
                    clone.setQuantity_package(0);
                    orderVM.getBillLadingField().get().getArrBillLadingDetail().get(i).getBillPackages().add(clone);
                }
                orderVM.getBillLadingField().notifyChange();
                adapter.notifyDataSetChanged();
            }
        });
        dialogAddItem.show(getBaseActivity().getSupportFragmentManager(),
                "add_photo_dialog_fragment");
    }

    public void selectWarehouse() {
        if (!orderVM.getIsLoading().get()) {
            WarehouseDialog dialog = new WarehouseDialog(orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getWarehouse(),
                    orderVM.getSelectedWarehouses(),
                    ((selectedWareHouse) -> {
                        if (selectedWareHouse.getId() == null) {
                            orderVM.checkIsExistWareHouse(selectedWareHouse, this::runUi);
                        } else {
                            orderVM.getDestination(getContext(), selectedWareHouse, true, this::runUi);
                        }
                    }));
            dialog.show(getBaseActivity().getSupportFragmentManager(), dialog.getTag());
        }
    }


    public void runUi(Object... objects) {
        String action = (String) objects[0];
        switch (action) {
            case "checkWarehouseSuccess":
                orderVM.getDestination(getContext(), (Warehouse) objects[1], true, this::runUi);
                break;
            case "checkWarehouseFail":
            case "getDistanceFail":
                showDialogConfirm(getString(R.string.msg_error_pickup_warehouse));
                break;
            case "getDistanceSuccess":
                onSelectedWarehouse((Warehouse) objects[1], (DistanceDTO) objects[2]);
                String warehouseName = orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getWarehouse().getName();
//                if (warehouseName.length()< 50) {
//                    mBinding.etWareHouse.setText(warehouseName);
//                } else {
//                    mBinding.etWareHouse.setText(warehouseName.substring(0, 45) + "...");
//                }
                break;
            case "warehouseInfoInvalid":
                showDialogConfirm(getString(R.string.msg_error_info_warehouse));
                orderVM.getSelectedWarehouses().remove(objects[1]);
                break;
        }
    }

    private void showDialogConfirm(String msg) {
        dialogConfirm = new DialogConfirm(getString(R.string.pickup),
                msg, true, true, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogConfirm.dismiss();
            }
        });
        dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
    }

    public void selectService() {
        dialogFragment = new ListDialogFragment(R.layout.item_service, R.string.select_attach_service,
                (listVM, runUi) -> {
                    listVM.getService(runUi);
                }
                , this);
        dialogFragment.show(getChildFragmentManager(), dialogFragment.getTag());
    }

    /**
     * @param warehouse   kho lấy hàng - pickup
     * @param distanceDTO thông tin quãng đường từ warehouse to hub
     */
    public void onSelectedWarehouse(Warehouse warehouse, DistanceDTO distanceDTO) {
        List<AreaDistance> distanceList = new ArrayList<>();

        AreaDistance distance = new AreaDistance();
        distance.setType(1);
        distance.setDistance(distanceDTO.getCost());
        distance.setDuration(distanceDTO.getMinutes());
        distance.setFrom_name_seq(warehouse.getName_seq());
        distance.setTo_name_seq(warehouse.getAreaInfo().getHubInfo().getName_seq());
        distance.setFrom_warehouse_name(warehouse.getName());
        distance.setTo_warehouse_name(warehouse.getAreaInfo().getHubInfo().getName());
        distance.setFromLocation(new LatLng(warehouse.getLatitude(), warehouse.getLongitude()));
        distance.setToLocation(new LatLng(warehouse.getAreaInfo().getHubInfo().getLatitude(), warehouse.getAreaInfo().getHubInfo().getLongitude()));

        distanceList.add(distance);

        BillLadingDetail billDetail = orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0);

        billDetail.setAreaDistances(distanceList);

        warehouse.setWarehouse_code(orderVM.getFromWareHouseCode());
        warehouse.setFromWareHouseCode("0");

        billDetail.setLatitude(warehouse.getLatitude());
        billDetail.setLongitude(warehouse.getLongitude());
        billDetail.setWarehouse(warehouse);
        billDetail.setWarehouse_type(WarehouseType.Export);
        billDetail.setWjson_address(warehouse.getWjson_address());
        billDetail.setOrder_type(Constants.TYPE_IN_ZONE);
        billDetail.setAddress(warehouse.getAddress());
        billDetail.setZone_area_id(warehouse.getAreaInfo().getZoneInfo().getId());
        mBinding.etWareHouse.setText(warehouse.getName());
        mBinding.etPhone.setText(warehouse.getPhone());
        mBinding.spSelectWareHouse.setError(null);

        orderVM.getBillLadingField().notifyChange();
    }


    public void nextStep() {
        KeyboardUtil.hideKeyboard(getActivity());
        if (isValidPickupWareHouse()) {
            orderVM.setCapacity(orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0), true);
            EventBus.getDefault().post(1);
        }
    }

    public boolean isValidPickupWareHouse() {
        BillLadingDetail pickup = orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0);
        boolean isValid = true;
        if (SetErrorEditText.checkNullOrEmpty(mBinding.etWareHouse, getResources().getString(R.string.warehouse_is_not_empty))) {
            isValid = false;
        }
        if (pickup.getWarehouse() == null || StringUtils.isNullOrEmpty(pickup.getWarehouse().getPhone())) {
            mBinding.lbPhone.setError(getResources().getString(R.string.warehouse_is_not_empty));
            isValid = false;
        }
        return isValid;
    }

//    public void selectDate(View v) {
//        if (getContext() != null) {
//            Calendar c = Calendar.getInstance();
//            int hour = c.get(Calendar.HOUR_OF_DAY);
//            int minute = c.get(Calendar.MINUTE);
//            TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), (view, hour1, minute1) -> {
//                Calendar calendar = Calendar.getInstance();
//                calendar.set(Calendar.HOUR_OF_DAY, hour1);
//                calendar.set(Calendar.MINUTE, minute1);
//                String date = AppController.formatTime.format(calendar.getTime());
//                switch (v.getId()) {
//                    case R.id.txtFromDate:
//                        ((TextView) v).setText(date);
//                        mBinding.ipFromDate.setError(null);
//                        orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).setExpected_from_time(new OdooDateTime(calendar.getTime().getTime()));
//                        break;
//                    case R.id.txtToDate:
//                        ((TextView) v).setText(date);
//                        mBinding.ipToDate.setError(null);
//                        orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).setExpected_to_time(new OdooDateTime(calendar.getTime().getTime()));
//                        break;
//                }
//            }, hour, minute, DateFormat.is24HourFormat(getActivity()));
//
//            timePickerDialog.show();
//        }
//    }

    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        switch (v.getId()) {
            case R.id.itemService:
                onSelectedService((BillService) o);
                break;
        }
    }

    private void onSelectedService(BillService o) {
        if (orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getBillService().contains(o)) {
            ToastUtils.showToast(R.string.msg_service_exist);
        } else {
            orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getBillService().add(o);
            orderVM.getBillLadingField().notifyChange();
        }
        dialogFragment.dismiss();
    }


    @Override
    public int getLayoutRes() {
        return R.layout.pickup_warehouse_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return OrderVMV2.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.order_items;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            mBinding.progressLayout.setVisibility(View.VISIBLE);
            mBinding.progressLayout.setOnClickListener(v -> {
                Log.d("progress_click", "click");
            });
        } else {
            mBinding.progressLayout.setVisibility(View.GONE);
        }
    }
}
