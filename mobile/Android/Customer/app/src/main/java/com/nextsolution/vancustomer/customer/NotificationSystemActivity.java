package com.nextsolution.vancustomer.customer;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.NotificationSystemVM;
import com.nextsolution.vancustomer.databinding.NotificationSystemActivityBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.nextsolution.vancustomer.util.ZoomImage;
import com.nextsolution.vancustomer.widget.GlideEngine;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

public class NotificationSystemActivity extends BaseActivity implements NetworkManager.NetworkHandler {
    NotificationSystemVM notificationSystemVM;
    NotificationSystemActivityBinding mBinding;

    boolean isOnline;
    private NetworkManager networkManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = (NotificationSystemActivityBinding) binding;
        notificationSystemVM = (NotificationSystemVM) viewModel;
        getData();
    }

    public void getData() {
        try {
            Intent intent = getIntent();
            if (intent.hasExtra(Constants.ID)) {
                int notificationId = intent.getIntExtra(Constants.ID, 0);
                notificationSystemVM.getNotificationById(notificationId);
            }
            if (intent.hasExtra(Constants.ITEM_ID)) {
                String notificationId = intent.getStringExtra(Constants.ITEM_ID);
                notificationSystemVM.getNotificationById(Integer.parseInt(notificationId));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        networkManager = new NetworkManager(this, this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }
    @Override
    public void onStop() {
        super.onStop();
        networkManager.stop();
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.toolbar) {
            this.onBackPressed();
        } else if (view.getId() == R.id.image) {
            ZoomImage.zoom(this,notificationSystemVM.getNotificationDTO().get().getImage_256());
        }
    }

//    public void zoomImage(String url) {
//        List<LocalMedia> localMedia = new ArrayList<>();
//        LocalMedia url_avatar = new LocalMedia();
//        url_avatar.setPath(url);
//        localMedia.add(url_avatar);
//        try {
//            PictureSelector.create(this)
//                    .themeStyle(R.style.picture_WeChat_style)
//                    .isWeChatStyle(true)
//                    .loadImageEngine(GlideEngine.createGlideEngine())
//                    .openExternalPreview(0, localMedia);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public int getLayoutRes() {
        return R.layout.notification_system_activity;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return NotificationSystemVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            mBinding.tvNetwork.setVisibility(View.VISIBLE);
            mBinding.progressLayout.setVisibility(View.VISIBLE);
            mBinding.progressLayout.setOnClickListener(v->{
                Log.d("progress_click", "click");
            });
        } else {
            mBinding.tvNetwork.setVisibility(View.GONE);
            mBinding.progressLayout.setVisibility(View.GONE);
        }
    }
}
