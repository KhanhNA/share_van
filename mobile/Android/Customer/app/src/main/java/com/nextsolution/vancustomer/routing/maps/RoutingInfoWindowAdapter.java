package com.nextsolution.vancustomer.routing.maps;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.vancustomer.R;

public class RoutingInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private RoutingVanDay aRoute;

    private Activity activity;

    private TextView timeRemain, timeRecent, titleUi;

    public RoutingInfoWindowAdapter(Activity activity, RoutingVanDay routingData) {
        this.activity = activity;
        this.aRoute = routingData;

    }

    @Override
    public View getInfoWindow(Marker marker) {
        View mContents = activity.getLayoutInflater().inflate(R.layout.marker_info_contents, null);
        render(marker, mContents);
        // This means that getInfoContents will be called.
        return mContents;

    }


    @Override
    public View getInfoContents(Marker marker) {

        return null;
    }

    private void render(Marker marker, View view) {
        ImageView imageProfile = view.findViewById(R.id.badge);
        titleUi = view.findViewById(R.id.title);
        timeRecent = view.findViewById(R.id.timeRecent);
        timeRemain = view.findViewById(R.id.timeRemain);
//        imageProfile.setImageResource(R.drawable.user);
        setText();
    }

    @SuppressLint({"SimpleDateFormat", "SetTextI18n"})
    private void setText() {

//        if (aRoute != null) {
//            if (aRoute.getCustomer() != null) {
//                if (!StringUtils.isNullOrEmpty(aRoute.getCustomer().getName())) {
//                    titleUi.setText(aRoute.getCustomer().getName());
//                }
//                // convert date time to dd mm yyyy hh mm ss
//                String routingTime = DateUtils.convertFormatDate((String) aRoute.getRouting_time(), DateUtils.APOLLO_DATE_FORMAT, DateUtils.DATE_TIME_FORMAT_VN);
//                timeRecent.setText(routingTime);
//
//                // lay thoi gian hien tai hai ngay
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtils.DATE_TIME_FORMAT_VN);
//                try {
//                    String time = "";
//                    Date routingDate = simpleDateFormat.parse(routingTime);
//                    Date currentDate = DateUtils.getCurrentDate();
//                    if (routingDate != null) {
//                        time = DateUtils.substractDates(routingDate, currentDate);
//                    }
//                    if (DateUtils.compareDate(routingDate,currentDate) == -1 && !DateUtils.isRemainThirtyMinus(routingDate,currentDate)|| DateUtils.compareDate(routingDate,currentDate) == 0){
//                        timeRemain.setText(activity.getResources().getString(R.string.exceed) +" "+ time);
//                        timeRemain.setTextColor(activity.getResources().getColor(R.color.red));
//                    }else if (DateUtils.compareDate(routingDate,currentDate) == -1 && DateUtils.isRemainThirtyMinus(routingDate,currentDate)){
//                        timeRemain.setText(activity.getResources().getString(R.string.remain) +" "+ time);
//                        timeRemain.setTextColor(activity.getResources().getColor(R.color.selection_background_color));
//                    }else if (DateUtils.compareDate(routingDate,currentDate) == 1
//                    ){
//                        timeRemain.setText(activity.getResources().getString(R.string.remain)+" "+ time);
//                    }
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//        } else {
//            titleUi.setText("");
//            timeRecent.setText("");
//            timeRemain.setText("");
//        }

    }
}
