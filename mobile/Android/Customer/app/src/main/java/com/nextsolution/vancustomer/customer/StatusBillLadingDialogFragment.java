package com.nextsolution.vancustomer.customer;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.StatusDialogVM;
import com.nextsolution.vancustomer.databinding.StatusBillLadingDialogFragmentBinding;
import com.nextsolution.vancustomer.util.BillLadingHistoryStatus;

public class StatusBillLadingDialogFragment extends DialogFragment {
    String status;

    StatusDialogVM statusDialogVM;
    StatusBillLadingDialogFragmentBinding mBinding;
    IChooseStatusBillLading iChooseStatusBillLading;

    public StatusBillLadingDialogFragment(String status, IChooseStatusBillLading iChooseStatusBillLading) {
        this.status = status;
        this.iChooseStatusBillLading = iChooseStatusBillLading;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.status_bill_lading_dialog_fragment, container, false);
        statusDialogVM = ViewModelProviders.of(this).get(StatusDialogVM.class);
        mBinding.setViewModel(statusDialogVM);
        mBinding.setListener(this);
        init();
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return mBinding.getRoot();
    }

    public void init() {
        if (BillLadingHistoryStatus.all.toString().equals(status)) {
            mBinding.rbAll.setChecked(true);
        } else if (BillLadingHistoryStatus.running.toString().equals(status)) {
            mBinding.rbRunning.setChecked(true);
        } else if (BillLadingHistoryStatus.finished.toString().equals(status)) {
            mBinding.rbFinished.setChecked(true);
        } else if (BillLadingHistoryStatus.deleted.toString().equals(status)) {
            mBinding.rbDeleted.setChecked(true);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rbAll:
                iChooseStatusBillLading.chooseStatusBillLading(BillLadingHistoryStatus.all.toString());
                break;
            case R.id.rbRunning:
                iChooseStatusBillLading.chooseStatusBillLading(BillLadingHistoryStatus.running.toString());
                break;
            case R.id.rbFinished:
                iChooseStatusBillLading.chooseStatusBillLading(BillLadingHistoryStatus.finished.toString());
                break;
            case R.id.rbDeleted:
                iChooseStatusBillLading.chooseStatusBillLading(BillLadingHistoryStatus.deleted.toString());
                break;
        }
    }

    public interface IChooseStatusBillLading {
        void chooseStatusBillLading(String status);
    }

}
