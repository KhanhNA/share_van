package com.nextsolution.vancustomer.customer;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nextsolution.db.dto.BiddingVehicle;
import com.nextsolution.db.dto.Depot;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.CustomInfoWindowAdapter;
import com.nextsolution.vancustomer.adapter.ListenIndex;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.customer.vm.MapsVM;
import com.nextsolution.vancustomer.databinding.FragmentMapBinding;
import com.nextsolution.vancustomer.model.Position;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.model.Vehicle;
import com.nextsolution.vancustomer.socket.SimpleEchoSocket;
import com.nextsolution.vancustomer.socket.SocketEvent;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.AdapterListener;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.util.FormContentProvider;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.util.Fields;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class MapsFragment extends BaseFragment implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener, SocketEvent {
    private FragmentMapBinding mBinding;
    String routing_Code = "";
    boolean isFollowVehicle = true;
    private static final String TAG = "MapsFragment";
    LatLng startPosition;
    private String FROM_FRAGMENT;
    ListVehicleDialog listVehicleDialog;


    //vars
    private Boolean mLocationPermissionsGranted = false;
    private GoogleMap mMap;
    private Marker marker;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    private final Integer TIME_GET_DISTANCE = 60000;

    CountDownTimer countDownTimer;
    private Boolean checkCountDownTine = true;
    //Bidding cargo map
    private Integer biddingOrderId;
    public int depot_type = 0;
    private HashMap<Integer, Marker> hashMarker = new HashMap<>();
    private int indexFollow = 0;
    private int markerIndexCurrent = 0;
    BiddingVehicle biddingVehicle;


    //widgets
    private FloatingActionButton mGps;
    private MapsVM mapsVM;

    //socket
    HttpClient httpClient = new HttpClient();
    SimpleEchoSocket socket = new SimpleEchoSocket(this);
    WebSocketClient webSocket = new WebSocketClient(httpClient);
    ClientUpgradeRequest request = new ClientUpgradeRequest();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mapsVM = (MapsVM) viewModel;
        mGps = view.findViewById(R.id.ic_gps);
        getData();

        mBinding = (FragmentMapBinding) binding;
        placeAutoComplete();
        getLocationPermission();
        mBinding.toolbar.setTitle(R.string.vehicle_location);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getData() {
        Intent intent = getActivity().getIntent();

        if (intent.hasExtra(Constants.FROM_FRAGMENT)) {
            FROM_FRAGMENT = intent.getStringExtra(Constants.FROM_FRAGMENT);
            switch (FROM_FRAGMENT) {
                case Constants.BIDDING_CARGO_CHILD_FRAGMENT:
                    mapsVM.getIsBiddingCargoMaps().set(true);
                    biddingOrderId = intent.getIntExtra("BIDDING_ORDER_ID", 0);
                    depot_type = intent.getIntExtra(Constants.DEPOT_TYPE, 0);
                    mapsVM.getBiddingOrderDetail(depot_type, biddingOrderId, this::runUi);
                    break;
                case Constants.ORDER_FRAGMENT:
                    if (intent.hasExtra(Constants.ROUTING_CODE)) {
                        this.routing_Code = intent.getStringExtra(Constants.ROUTING_CODE);
                    }
                    break;
            }

        }
    }

    private void setMarkerVehicle(List<Vehicle> vehicleList) {
        if (vehicleList != null && vehicleList.size() > 0) {
            indexFollow = vehicleList.get(0).getId();
        }
        for (Vehicle item : vehicleList) {
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(item.getLatitude(), item.getLongitude()))
                    .title(item.getName())
                    .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(R.layout.driver_marker)))
                    .flat(true));
            marker.setTag(item);
            hashMarker.put(item.getId(), marker);
        }

    }

    //type = 0 nhận hàng, 1 trả hàng
    private void setMarkerDeport(Depot deport, int depot_type) {
        Marker marker;
        if (depot_type == 0) {
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(mapsVM.getBiddingPackage().getFrom_latitude(), mapsVM.getBiddingPackage().getFrom_longitude()))
                    .title(deport.getName())
                    .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(R.layout.map_marker)))
                    .flat(true));
        } else {
            marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(mapsVM.getBiddingPackage().getTo_latitude(), mapsVM.getBiddingPackage().getTo_longitude()))
                    .title(deport.getName())
                    .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(R.layout.map_marker)))
                    .flat(true));
        }
        marker.setTag(deport);

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (mLocationPermissionsGranted) {
            LatLng latLng1;
            switch (view.getId()) {
                case R.id.warehouseLocation:
                    isFollowVehicle = false;
                    latLng1 = new LatLng(mapsVM.getRoutingDay().get().getLatitude(), mapsVM.getRoutingDay().get().getLongitude());
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                            latLng1, 15);
                    mMap.animateCamera(location);
                    break;
                case R.id.vehicleLocation:
                case R.id.txtVehicleLocation:
                    isFollowVehicle = true;
                    if (FROM_FRAGMENT.equals(Constants.ORDER_FRAGMENT)) {
                        if (startPosition == null) {
                            latLng1 = new LatLng(mapsVM.getDriver().get().getLatitude(), mapsVM.getDriver().get().getLongitude());
                            moveDriver(latLng1);
                        } else {
                            moveDriver(startPosition);
                        }
                    } else if (FROM_FRAGMENT.equals(Constants.BIDDING_CARGO_CHILD_FRAGMENT)) {
                        moveDriver(new LatLng(hashMarker.get(indexFollow).getPosition().latitude, hashMarker.get(indexFollow).getPosition().longitude));
                    }

                    break;
                case R.id.lbMyDepot:
                case R.id.txtMyDepot:
                    if (depot_type == 0) {
                        moveDriver(new LatLng(mapsVM.getBiddingPackage().getFrom_latitude(), mapsVM.getBiddingPackage().getFrom_longitude()));
                    } else {
                        moveDriver(new LatLng(mapsVM.getBiddingPackage().getTo_latitude(), mapsVM.getBiddingPackage().getTo_longitude()));
                    }
                    break;
                case R.id.lbToDepot:
                case R.id.txtToDepot:
                    if (depot_type == 1) {
                        moveDriver(new LatLng(mapsVM.getBiddingPackage().getFrom_latitude(), mapsVM.getBiddingPackage().getFrom_longitude()));
                    } else {
                        moveDriver(new LatLng(mapsVM.getBiddingPackage().getTo_latitude(), mapsVM.getBiddingPackage().getTo_longitude()));
                    }
                    break;
            }
        }
    }

    private void getLocationPermission() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (FROM_FRAGMENT.equals(Constants.ORDER_FRAGMENT)) {
            mapsVM.getRouting(this.routing_Code, this::runUi);
        }

        mMap = googleMap;
        init();
        if (mLocationPermissionsGranted) {

            if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
    }

    /**
     * khởi tạo map
     */
    private void init() {

        CustomInfoWindowAdapter customInfoWindowAdapter = new CustomInfoWindowAdapter(getActivity(), new ListenIndex() {
            @Override
            public void onClickIndex(int index) {
                indexFollow = index;
            }
        });
        mMap.setInfoWindowAdapter(customInfoWindowAdapter);
        mMap.setOnInfoWindowClickListener(marker1 -> {
            if (marker1.getTag() instanceof Vehicle)
                gotoBiddingVehicleDetailFragment(biddingVehicle);
        });
        mGps.setOnClickListener(view -> {
            getDeviceLocation();
        });
        hideSoftKeyboard();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (FROM_FRAGMENT.equals(Constants.ORDER_FRAGMENT)) {
            switch (action) {
                case Constants.GET_DATA_SUCCESS:
                    addMarker(mapsVM.getRoutingDay().get());
                    mapsVM.getDriver(mapsVM.getRoutingDay().get().getId(), this::runUi);
                    break;

                case Constants.GET_DATA_FAIL:
                    break;

                case Constants.GET_DRIVER_LOCATION_SUCCESS:
                    addDriverMarker(mapsVM.getDriver().get());
//                    startSocket(mapsVM.getRoutingDay().get().getVehicle().getId());
                    moveDriver(new LatLng(mapsVM.getDriver().get().getLatitude(), mapsVM.getDriver().get().getLongitude()));
                    updateDistance();
                    break;
            }
        } else if (FROM_FRAGMENT.equals(Constants.BIDDING_CARGO_CHILD_FRAGMENT)) {
            switch (action) {
                case Constants.GET_DATA_SUCCESS:
                    break;
                case Constants.GET_DATA_FAIL:
                    break;

                case Constants.GET_DRIVER_LOCATION_SUCCESS:
                    setMarkerVehicle(mapsVM.getVehicles());
                    setMarkerDeport(mapsVM.getBiddingOrder().getFrom_depot(), 0);
                    setMarkerDeport(mapsVM.getBiddingOrder().getTo_depot(), 1);
                    if (depot_type == 0) {
                        moveDriver(new LatLng(mapsVM.getBiddingPackage().getFrom_latitude(), mapsVM.getBiddingPackage().getFrom_longitude()));
                    } else {
                        moveDriver(new LatLng(mapsVM.getBiddingPackage().getTo_latitude(), mapsVM.getBiddingPackage().getTo_longitude()));
                    }
                    LatLng latLng1 = new LatLng(mapsVM.getVehicles().get(markerIndexCurrent).getLatitude(), mapsVM.getVehicles().get(markerIndexCurrent).getLatitude());
                    LatLng latLng2;
                    if (depot_type == 0) {
                        latLng2 = new LatLng(mapsVM.getBiddingPackage().getTo_latitude(), mapsVM.getBiddingPackage().getTo_longitude());
                    } else {
                        latLng2 = new LatLng(mapsVM.getBiddingPackage().getFrom_latitude(), mapsVM.getBiddingPackage().getFrom_longitude());
                    }
                    updateDistance(latLng1, latLng2);
                    break;
            }
        }
    }

    public void moveDriver(LatLng latLng1) {
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                latLng1, 15);
        mMap.animateCamera(location);
    }

    public void updateDistance(LatLng startPosition, LatLng endPosition) {
        mapsVM.getDestination(startPosition, endPosition);
    }


    /**
     * Thêm đánh dấu các kho nhận trả hàng trên google map
     */
    private void addMarker(Object o) {
        if (FROM_FRAGMENT.equals(Constants.ORDER_FRAGMENT)) {
            RoutingDay routingDay = (RoutingDay) o;
            if (routingDay.getLatitude() != null && routingDay.getLongitude() != null) {
                mMap.addMarker(new MarkerOptions()
                        .title(routingDay.getWarehouse_name())
                        .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(R.layout.map_marker)))
                        .position(new LatLng(routingDay.getLatitude(), routingDay.getLongitude()))
                ).setTag(routingDay);
            }

        }

    }

    private void addDriverMarker(RoutingDay routingDay) {
        if (marker != null) {
            marker.remove();
        }
        if (routingDay.getLatitude() != null && routingDay.getLongitude() != null) {
            this.marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(routingDay.getLatitude(), routingDay.getLongitude()))
                    .title(routingDay.getName())
                    .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(R.layout.driver_marker)))
                    .flat(true));
            this.marker.setTag(mapsVM.getDriver().get());
        }
    }


    /**
     * Tạo bitmap từ drawable
     *
     * @return
     */
    private Bitmap loadBitmapFromView(Integer drawable) {
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(drawable, null);
        if (v.getMeasuredHeight() <= 0) {
            v.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
            v.draw(c);
            return b;
        }
        return null;
    }

    public void goToDetail() {
        if (FROM_FRAGMENT.equals(Constants.ORDER_FRAGMENT)) {
            Intent intent = new Intent(getBaseActivity(), ListRoutingVanDayActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.ITEM_ID, this.routing_Code);
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (FROM_FRAGMENT.equals(Constants.BIDDING_CARGO_CHILD_FRAGMENT)) {
            Intent intent = new Intent(getActivity(), CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.DATA, biddingOrderId);
            bundle.putSerializable(Constants.FRAGMENT, BiddingCargoDetailFragment.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, Constants.REQUEST_FRAGMENT_RESULT);
        }
    }

    public void showListVehicleDialog() {
        listVehicleDialog = new ListVehicleDialog(mapsVM.getBiddingVehicle(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                biddingVehicle = (BiddingVehicle) o;
                listVehicleDialog.dismiss();
                indexFollow = biddingVehicle.getVehicle_id();
                moveDriver(hashMarker.get(indexFollow).getPosition());
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        listVehicleDialog.show(getChildFragmentManager(), "ABC");
    }

    public void gotoBiddingVehicleDetailFragment(BiddingVehicle biddingVehicle) {
        Intent intent = new Intent(getActivity(), CommonActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.MODEL, biddingVehicle);
        bundle.putSerializable(Constants.DATA, mapsVM.getBiddingOrder());
        bundle.putSerializable(Constants.FRAGMENT, BiddingCargoVehicleDetailFragment.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.REQUEST_FRAGMENT_RESULT);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mLocationPermissionsGranted = false;

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            try {

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionsGranted = true;
                    initMap();
                } else {
                    mLocationPermissionsGranted = false;
                    return;
                }

            } catch (Exception e) {
                mLocationPermissionsGranted = false;
                e.printStackTrace();
            }
        }

    }


    public void animateMarker(final Marker marker, final LatLng toPosition, float course,
                              final boolean hideMarker) {
        Log.d("vehicle_move", marker.getId() + "");

        startPosition = marker.getPosition();
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 2000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                //quay đầu xe
                marker.setRotation(course);


                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }

                if (isFollowVehicle) {
                    if (FROM_FRAGMENT.equals(Constants.ORDER_FRAGMENT)) {
                        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                                toPosition, 15);
                        mMap.animateCamera(location);
                    } else if (FROM_FRAGMENT.equals(Constants.BIDDING_CARGO_CHILD_FRAGMENT)) {
                        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                                hashMarker.get(indexFollow).getPosition(), 15);
                    }

                }

            }
        });
    }

    private void initMap() {
        Log.d(TAG, "initMap: initializing map");
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * khởi tạo thanh tìm kiếm địa chỉ theo map
     */
    private void placeAutoComplete() {
        if (getActivity() != null) {
            Places.initialize(getActivity().getApplicationContext(), AppController.API_GOOGLE_MAP_SEARCH);
        }
        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        if (autocompleteFragment != null) {
            ((EditText) autocompleteFragment.getView().findViewById(R.id.places_autocomplete_search_input)).setTextSize(15.0f);
            autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG));
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(@NonNull Place place) {
                    if (place.getLatLng() != null) {
                        moveCamera(place.getLatLng(), place.getName() + "");
                    }
                }

                @Override
                public void onError(@NonNull Status status) {

                }
            });
        }
    }


    /**
     * lấy vị trí hiện tại
     */
    private void getDeviceLocation() {

        FusedLocationProviderClient mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        try {
            if (mLocationPermissionsGranted) {
                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        Location currentLocation = (Location) task.getResult();

                        moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                                "My Location");

                    } else {
                        Toast.makeText(getActivity(), "unable to get current location", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
            }
        } catch (SecurityException ignored) {
        }
    }

    /**
     * Di chuyển camera đến vị tri @latLng
     *
     * @param latLng vị trí cần di chuyển đến
     */
    private void moveCamera(LatLng latLng, String title) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, MapsFragment.DEFAULT_ZOOM), 2000, null);
        if (!title.equals("My Location")) {
            MarkerOptions options = new MarkerOptions()
                    .position(latLng)
                    .title(title);
            mMap.addMarker(options);
        }

        hideSoftKeyboard();
    }

    private void hideSoftKeyboard() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_map;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MapsVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void startSocket(Integer deviceId) {
        if (httpClient.isStarted()) return;
        try {
            postData(deviceId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void postData(Integer deviceId) throws Exception {
        httpClient.start();
        Fields fields = new Fields();
        fields.put("email", StaticData.userName);
        fields.put("password", StaticData.password);
        fields.put("deviceId", deviceId + "");
        httpClient.POST(AppController.TRACKING_SOCKET + "/api/session/monitor")
                .content(new FormContentProvider(fields))
                .header(HttpHeader.CONTENT_TYPE, "application/x-www-form-urlencoded; charset=UTF-8")
                .send();

        URI wsUri = URI.create( AppController.WS_SOCKET + "/api/socket");

        webSocket.start();
        webSocket.connect(socket, wsUri, request);
    }

    @Override
    public void handleMessage(Object msg) {

        Position[] positions = (Position[]) msg;
        if (FROM_FRAGMENT.equals(Constants.ORDER_FRAGMENT)) {
            for (Position position : positions) {
                if (position.getDeviceId() == mapsVM.getRoutingDay().get().getVehicle().getId()) {
                    getActivity().runOnUiThread(() -> {
                        //Xe di chuyển
                        mapsVM.getDriver().get().setLongitude(position.getLongitude());
                        mapsVM.getDriver().get().setLatitude(position.getLatitude());
                        animateMarker(marker, new LatLng(position.getLatitude(), position.getLongitude()), position.getCourse(), false);
                    });
                }
            }
        } else if (FROM_FRAGMENT.equals(Constants.BIDDING_CARGO_CHILD_FRAGMENT)) {
            for (Position position : positions) {
                if (hashMarker.containsValue(position.getDeviceId())) {
                    getActivity().runOnUiThread(() -> {
                        //Xe di chuyển
                        animateMarker(hashMarker.get(position.getDeviceId()), new LatLng(position.getLatitude(), position.getLongitude()), position.getCourse(), false);
                    });
                }
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            checkCountDownTine = true;
            if (countDownTimer != null) {
                countDownTimer.onFinish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        checkCountDownTine = false;
        if (countDownTimer != null) {
            countDownTimer.onFinish();
        }

    }

    // Update khoảng cách xe với kho mỗi 60s
    public void updateDistance() {
        try {
            countDownTimer = new CountDownTimer(TIME_GET_DISTANCE, TIME_GET_DISTANCE) {

                @Override
                public void onTick(long millisUntilFinished) {
                    LatLng latLng1 = new LatLng(mapsVM.getDriver().get().getLatitude(), mapsVM.getDriver().get().getLongitude());
                    LatLng latLng2 = new LatLng(mapsVM.getRoutingDay().get().getLatitude(), mapsVM.getRoutingDay().get().getLongitude());
                    mapsVM.getDestination(latLng1, latLng2);
                }

                @Override
                public void onFinish() {
                    if (checkCountDownTine) {
                        updateDistance();
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
