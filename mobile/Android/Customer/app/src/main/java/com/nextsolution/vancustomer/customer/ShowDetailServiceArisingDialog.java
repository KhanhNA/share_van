package com.nextsolution.vancustomer.customer;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolution.db.dto.Driver;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.ShowDetailServiceArisingVM;
import com.nextsolution.vancustomer.databinding.ShowDetailServiceArisingDialogBinding;
import com.nextsolution.vancustomer.model.Vehicle;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.listener.AdapterListener;

public class ShowDetailServiceArisingDialog extends DialogFragment {


    ShowDetailServiceArisingVM showDetailServiceArisingVM;
    ShowDetailServiceArisingDialogBinding mBinding;
    XBaseAdapter adapter;
    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.show_detail_service_arising_dialog, container, false);
        showDetailServiceArisingVM = ViewModelProviders.of(this).get(ShowDetailServiceArisingVM.class);
        mBinding.setViewModel(showDetailServiceArisingVM);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        mBinding.btnClose.setOnClickListener(nv-> dismiss());

        setUpRecycleView();
        getData();
        return mBinding.getRoot();
    }
    private void runUi(Object[] params) {
        String action = (String) params[0];
        switch (action) {
            case Constants.GET_DATA_SUCCESS:
                adapter.notifyDataSetChanged();
                break;
        }

    }

    public void getData(){
        try {
            Bundle bundle= getArguments();
            if(bundle!=null){
                showDetailServiceArisingVM.getDriver().set((Driver) bundle.getSerializable(Constants.MODEL));
                showDetailServiceArisingVM.getVehicle().set((Vehicle) bundle.getSerializable("vehicle"));
                showDetailServiceArisingVM.getServiceArising(bundle.getInt(Constants.DATA,-1),this::runUi);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void setUpRecycleView(){
        adapter= new XBaseAdapter(R.layout.show_detail_service_arising_item, showDetailServiceArisingVM.getSosTypes(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        mBinding.rcServiceArising.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mBinding.rcServiceArising.setLayoutManager(layoutManager);
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }


}
