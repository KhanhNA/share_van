package com.nextsolution.vancustomer.util;

public enum BillLadingHistoryStatus {
    all,
    running,
    finished,
    deleted
}
