package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.nextsolution.db.dto.CargoBillPackage;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.CargoBillPackageDialogVM;
import com.nextsolution.vancustomer.databinding.CargoBillPackageDialogBinding;
import com.nextsolution.vancustomer.enums.Constants;

public class CargoBillPackageDialog extends DialogFragment {
    CargoBillPackageDialogVM billPackageDialogVM;
    CargoBillPackageDialogBinding mBinding;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.cargo_bill_package_dialog, container, false);
        billPackageDialogVM = ViewModelProviders.of(this).get(CargoBillPackageDialogVM.class);
        mBinding.setViewModel(billPackageDialogVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        getData();
        new CountDownTimer(4000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                autoDismiss();
            }
        }.start();
        autoDismiss();
        return mBinding.getRoot();
    }
    private void autoDismiss(){
            this.dismiss();
    }
    public void getData(){
        try{
            Bundle bundle= getArguments();
            billPackageDialogVM.getCargobillPackage().set((CargoBillPackage) bundle.getSerializable(Constants.MODEL));
            billPackageDialogVM.getQr_code().set( bundle.getString(Constants.QR_CODE));
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
}
