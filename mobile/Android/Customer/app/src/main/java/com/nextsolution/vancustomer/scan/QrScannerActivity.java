/*
 * Copyright (C) 2018 Jenly Yu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.nextsolution.vancustomer.scan;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;


import com.king.zxing.CaptureActivity;
import com.king.zxing.camera.FrontLightMode;
import com.nextsolution.db.dto.BiddingVehicle;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.Cargo;
import com.nextsolution.db.dto.CargoBillPackage;
import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.customer.BillPackageDialog;
import com.nextsolution.vancustomer.customer.CargoBillPackageDialog;
import com.nextsolution.vancustomer.customer.CargoTypesDialog;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.RoutingDayType;
import com.nextsolution.vancustomer.util.StatusBarUtils;
import com.nextsolution.vancustomer.util.ToastUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Jenly <a href="mailto:jenly1314@gmail.com">Jenly</a>
 */
public class QrScannerActivity extends CaptureActivity implements View.OnClickListener {

    // Multi scan qrCode
    private String FROM_FRAGMENT;
    private boolean isMultiScanQrCode;// kiểm tra có phải quét multi qr không
    private String qrResult; // qr code quét được
    TextView tvQuantityCheck; // tv hiển thị số lượng quét
    boolean checkScan = true;

    //LIST_CARGO_DETAIL_FRAGMENT
    private ArrayList<Integer> arrQrResult = new ArrayList<>();
    private ArrayList<String> data = new ArrayList<>();
    private List<CargoBillPackage> cargoBillPackages = new ArrayList<>();
    private static final String LIST_CARGO_DETAIL_FRAGMENT = "LIST_CARGO_DETAIL_FRAGMENT";

    //BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT
    private static final String BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT = "BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT";
    private BiddingVehicle biddingVehicle = new BiddingVehicle();
    private List<CargoTypes> listCargoTypes = new ArrayList<>();
    private int indexCargoTypesItem;
    private int quantityQrChecked = 0;
    private int totalQr;


    //LIST_ROUTING_VAN_DAY_ACTIVITY
    private static final String LIST_ROUTING_VAN_DAY_ACTIVITY = "LIST_ROUTING_VAN_DAY_ACTIVITY";
    private HashMap<Integer, HashMap<String,Boolean>> qrDataChecked = new HashMap<>();
    private RoutingDay routingPlan = new RoutingDay();
    private List<BillPackage> listBillPackage = new ArrayList<>();
    private int index = 0;


    @Override
    public int getLayoutId() {
        return R.layout.easy_capture_activity;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        Toolbar toolbar = findViewById(R.id.toolbar);
        StatusBarUtils.immersiveStatusBar(this, toolbar, 0.2f);
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvQuantityCheck = findViewById(R.id.tvQuantityCheck);
        tvTitle.setText(getIntent().getStringExtra("QR_SCANNER"));
        getData();
        getCaptureHelper()
//                .decodeFormats(DecodeFormatManager.QR_CODE_FORMATS)
                .playBeep(true)
                .vibrate(true)
                .frontLightMode(FrontLightMode.AUTO)
                .tooDarkLux(45f)
                .brightEnoughLux(100f)
                .continuousScan(isMultiScanQrCode)
                .supportLuminanceInvert(true);
        findViewById(R.id.ivLeft).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ivLeft) {
            if (isMultiScanQrCode) {
                setDataResult(false);
            }
            onBackPressed();
        }
    }

    // Lấy dữ liệu từ màn hình khác sang
    public void getData() {
        Intent intent = this.getIntent();
        if (intent.hasExtra(Constants.SCAN_MULTI_QR_CODE)) {
            isMultiScanQrCode = intent.getBooleanExtra(Constants.SCAN_MULTI_QR_CODE, false);
            if (isMultiScanQrCode) {
                FROM_FRAGMENT = intent.getStringExtra(Constants.FROM_FRAGMENT);
                switch (FROM_FRAGMENT) {
                    case LIST_CARGO_DETAIL_FRAGMENT:
                        arrQrResult.clear();
                        arrQrResult.addAll(intent.getIntegerArrayListExtra("QR_SCANED"));
                        data.addAll(intent.getStringArrayListExtra(Constants.DATA));
                        cargoBillPackages.addAll(intent.getParcelableArrayListExtra(Constants.MODEL));
                        break;
                    case BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT:
                        biddingVehicle = (BiddingVehicle) intent.getSerializableExtra(Constants.MODEL);
                        listCargoTypes.addAll(biddingVehicle.getCargo_types());
                        getTotalQrCodeBiddingCargoTypes(listCargoTypes);
                        break;
                    case LIST_ROUTING_VAN_DAY_ACTIVITY:
                        qrDataChecked = (HashMap<Integer, HashMap<String, Boolean>>) intent.getSerializableExtra(Constants.QR_DATA);
                        routingPlan = (RoutingDay) intent.getSerializableExtra(Constants.DATA);
                        listBillPackage.addAll(routingPlan.getList_bill_package());
                        break;
                    default:
                        finish();
                        break;
                }
            }
        }
    }

    @Override
    public boolean onResultCallback(String result) {
        if (isMultiScanQrCode) {
            if (checkScan) {
                checkScan = false;
                qrResult=result;
                sleepScan();
            }
        } else {
            Log.d("multi_scan_qr_code", "Not multi");
        }
        return super.onResultCallback(result);
    }

    private void sleepScan() {

        getCaptureHelper().vibrate(false);
        int checkQrCode = handleResult();
        if (checkQrCode == 0) {
//            tvQuantityCheck.setText(); // set text number qr code checked
            showDialog();
        } else if (checkQrCode == 1) {
            showToast(R.string.QrCode_has_been_scanned, R.drawable.ic_danger);
        } else if (checkQrCode == 2) {
            showToast(R.string.QrCode_result_wrong, R.drawable.ic_close16);
        } else {
            setDataResult(true);
//            showToast(R.string.Complete, R.drawable.ic_check);
            finish();
        }
        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                checkScan=true;
                getCaptureHelper().vibrate(true);
            }
        }.start();
    }

    /**
     * show data
     */
    private void showDialog() {
        FragmentManager fm = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        switch (FROM_FRAGMENT) {
            case LIST_CARGO_DETAIL_FRAGMENT:
                CargoBillPackageDialog cargoBillPackageDialog = new CargoBillPackageDialog();
                bundle.putSerializable(Constants.MODEL, cargoBillPackages.get(data.indexOf(qrResult)));
                bundle.putSerializable(Constants.QR_CODE, qrResult);
                cargoBillPackageDialog.setArguments(bundle);
                cargoBillPackageDialog.show(fm, "ABC");
                break;
            case LIST_ROUTING_VAN_DAY_ACTIVITY:
                BillPackageDialog billPackageDialog = new BillPackageDialog(listBillPackage.get(index),qrResult);
                billPackageDialog.show(fm, "ABC");
                break;
            case BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT:
                CargoTypesDialog cargoTypesDialog = new CargoTypesDialog(listCargoTypes.get(indexCargoTypesItem),qrResult);
                cargoTypesDialog.show(fm, "ABC");
                break;
        }
    }


    private void showToast(Integer toast, Integer icon) {
        Log.d("Show_toast", getResources().getString(toast));
        this.runOnUiThread(new Runnable() {
            public void run() {
                ToastUtils.showToast(QrScannerActivity.this, getResources().getString(toast), getResources().getDrawable(icon));
            }
        });
    }

    private void showToast(String toast, Integer icon) {
        Log.d("Show_toast", toast);
        this.runOnUiThread(new Runnable() {
            public void run() {
                ToastUtils.showToast(QrScannerActivity.this, toast, getResources().getDrawable(icon));
            }
        });
    }


    /**
     * use multi scan qr code
     * handle result
     *
     * @return 0 Chưa quét , 1 Đã quét , 2 Sai mã ,3 Hoàn thành
     */
    private Integer handleResult() {
        Integer index = 0;
        switch (FROM_FRAGMENT) {
            case LIST_CARGO_DETAIL_FRAGMENT:
                index = data.indexOf(qrResult);
                if (index < 0) {
                    return -1;
                } else if (arrQrResult.size() == 0 || arrQrResult.indexOf(index) < 0) {
                    arrQrResult.add(index);
                    if (arrQrResult.size() == data.size()) {
                        return 3;
                    }
                    return 0;
                }
                return 2;
            case LIST_ROUTING_VAN_DAY_ACTIVITY:
                index=0;
                for (BillPackage item : listBillPackage) {
                    Boolean check= qrDataChecked.get(item.getBill_package_id()).get(qrResult);
                    if(check!=null){
                        if(check){
                            return 1;
                        }else{
                            qrDataChecked.get(item.getBill_package_id()).put(qrResult,true);
                            item.setQuantityQrChecked(item.getQuantityQrChecked()+1);
                            if (checkComplete()) {
                                return 3;
                            }
                            this.index=index;
                            return 0;
                        }
                    }
                    index++;
                }
                return 2;
            case BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT:
                indexCargoTypesItem = 0;
                for (CargoTypes item : listCargoTypes) {
                    // check da quet
                    if (item.getQrChecked() != null && item.getQrChecked().indexOf(qrResult) >= 0) {
                        return 1;
                    }
                    //index update checked
                    index = 0;
                    // check chua quet
                    for (Cargo cargo : item.getCargos()) {

                        if (cargo.getQr_code().equals(qrResult)) {
                            item.setQrChecked(item.getQrChecked() + qrResult);
                            item.getCargos().get(index).setQrChecked(true);
                            quantityQrChecked++;
                            // check hoàn thánh
                            if (quantityQrChecked == totalQr) {
                                return 3;
                            }
                            return 0;
                        }
                        index++;
                    }
                    indexCargoTypesItem++;
                }
                return 2;
        }
        return null;
    }

    /**
     * Use when multi scan qr code
     * check complete after receiving result
     *
     * @return true: complete, false : not complete
     */
    private boolean checkComplete() {
        for (BillPackage item : listBillPackage) {
            if(routingPlan.getType().equals(RoutingDayType.WAREHOUSE_IMPORT)) {
                if(item.getQuantity_export()-item.getQuantityQrChecked()!=0){
                    return false;
                }
            }
            else{
                if(item.getQuantity_import()-item.getQuantityQrChecked()!=0){
                    return false;
                }
            }
            item.setQrChecked(true);
        }
        return true;
    }

    /**
     * Use when multi ccan qr code
     * Handle data before onBackPressed
     *
     * @param complete check complete
     */
    private void setDataResult(Boolean complete) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        switch (FROM_FRAGMENT) {
            case LIST_CARGO_DETAIL_FRAGMENT:
                bundle.putIntegerArrayList(Constants.DATA_RESULT, arrQrResult);
                intent.putExtras(bundle);
                setResult(Constants.RESULT_OK, intent);
                break;
            case BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT:
                biddingVehicle.getCargo_types().clear();
                biddingVehicle.getCargo_types().addAll(listCargoTypes);
                bundle.putSerializable(Constants.DATA_RESULT, biddingVehicle);
                bundle.putBoolean(Constants.SUCCESS, complete);
                bundle.putInt(Constants.QUANTITY_QR_CHECKED, totalQr);
                intent.putExtras(bundle);
                setResult(Constants.RESULT_OK, intent);
                break;
            case LIST_ROUTING_VAN_DAY_ACTIVITY:
                bundle.putBoolean(Constants.SUCCESS, complete);
                routingPlan.getList_bill_package().clear();
                routingPlan.getList_bill_package().addAll(listBillPackage);
                bundle.putSerializable(Constants.DATA, routingPlan);
                intent.putExtra(Constants.QR_DATA, qrDataChecked);
                intent.putExtras(bundle);
                setResult(Constants.RESULT_OK, intent);
                break;
        }
    }

    // lấy số lượng qr phải quét
    private void getTotalQrCodeBiddingCargoTypes(List<CargoTypes> cargoTypes) {
        if (cargoTypes.size() > 0) {
            for (CargoTypes item : cargoTypes) {
                totalQr += item.getCargo_quantity();
            }
        }
    }
    public Integer getIndex(String qrCode) {
        for (int i = 0; i < cargoBillPackages.size(); i++) {
            if (cargoBillPackages.get(i).getQr_char().equals(qrCode)) {
                return i;
            }
        }
        return -1;
    }

}
