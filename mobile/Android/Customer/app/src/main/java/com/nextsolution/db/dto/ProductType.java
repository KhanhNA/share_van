package com.nextsolution.db.dto;


import com.tsolution.base.BaseModel;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductType extends BaseModel {
    private Integer id;
    private String name_seq;
    private String name;
    private BigDecimal net_weight;
    private String description;
    private String status;
    private Double extra_price;
    
}
