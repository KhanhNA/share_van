package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.DepotAPI;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.RunUi;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

import static com.nextsolution.vancustomer.enums.Constants.GET_DATA_SUCCESS;

@Getter
@Setter
public class ChooseCargoDialogVM extends BaseViewModel {
    Integer position = 0;
    ObservableList<CargoTypes> listData = new ObservableArrayList<>();

    public ChooseCargoDialogVM(@NonNull Application application) {
        super(application);
    }
    public void getCargoTypes(RunUi runUi){
        DepotAPI.getCargoTypes(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(o,runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
    public void getDataSuccess(Object o,RunUi runUi){
        OdooResultDto<CargoTypes> resultDto= (OdooResultDto<CargoTypes>) o;
        try{
            listData.clear();
            CargoTypes cargoTypes= new CargoTypes();
            cargoTypes.setType(AppController.getInstance().getString(R.string.ALL));
            listData.add(cargoTypes);
            listData.addAll(resultDto.getRecords());
            runUi.run(GET_DATA_SUCCESS);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
