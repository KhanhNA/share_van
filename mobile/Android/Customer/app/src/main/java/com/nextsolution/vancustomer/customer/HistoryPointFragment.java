package com.nextsolution.vancustomer.customer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.PagerAdapter;
import com.nextsolution.vancustomer.customer.vm.HistoryPointVM;
import com.nextsolution.vancustomer.databinding.HistoryPointFragmentBinding;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class HistoryPointFragment extends BaseFragment {
    HistoryPointFragmentBinding mBinding;
    HistoryPointVM historyPointVM;
    MenuItem menuItem;
    HistoryPointChildFragment getPoints;
    HistoryPointChildFragment usePoints;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding= (HistoryPointFragmentBinding) binding;
        historyPointVM= (HistoryPointVM) viewModel;
        init();
        return v;
    }
    private void init() {
        mBinding.toolbar.setTitle(R.string.pointHistory);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        setUpViewPager();
    }


    // type = 0 lịch sử cộng điểm
    // type = 1 lịch sử dùng điểm
    private void setUpViewPager() {
        PagerAdapter myPagerAdapter = new PagerAdapter(getChildFragmentManager());
        getPoints = new HistoryPointChildFragment(0);
        usePoints = new HistoryPointChildFragment(1);
        myPagerAdapter.addFragment(getPoints);
        myPagerAdapter.addFragment(usePoints);

        mBinding.frameContainer.setAdapter(myPagerAdapter);
        mBinding.navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.nvGetPoints:
                    mBinding.frameContainer.setCurrentItem(0);
                    break;
                case R.id.nvUsePoints:
                    mBinding.frameContainer.setCurrentItem(1);
                    break;
            }
            return false;
        });
        mBinding.frameContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (menuItem != null) {
                    menuItem.setChecked(false);
                } else {
                    mBinding.navigation.getMenu().getItem(0).setChecked(false);
                }
                mBinding.navigation.getMenu().getItem(position).setChecked(true);
                menuItem = mBinding.navigation.getMenu().getItem(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public int getLayoutRes() {
        return R.layout.history_point_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return HistoryPointVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
