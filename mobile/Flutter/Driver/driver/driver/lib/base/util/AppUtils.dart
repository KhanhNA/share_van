import 'package:driver/model/login/PartnerDto.dart';
import 'package:driver/model/session/OdooSessionDto.dart';
import 'package:get_storage/get_storage.dart';
import 'package:driver/base/auth/token.dart';
import 'package:driver/model/login/UserAuthentication.dart';

class AppUtils{
  static const String keyAccessToken = 'accessToken';
  static PartnerDto partnerDto;
  static const String keyRefreshToken = 'refreshToken';
  static const String keyIsLogin = 'isLogin';
  static const String keyUsername = 'username';
  static UserAuthentication me;
  static OdooSessionDto odooSessionDto;
  static String token;
  static String username;
  static int defaultStoreId;//goi ham dashboard lay duoc defaultStoreId
  static updateToken(Token tk) async {
    final box = GetStorage();
    String newAccessToken = tk.accessToken;
    String newRefreshToken = tk.refreshToken;
    await box.write(AppUtils.keyAccessToken, newAccessToken);
    await box.write(AppUtils.keyRefreshToken, newRefreshToken);
    AppUtils.token = tk.accessToken;
  }
}