import 'package:flutter/material.dart';

class EmptyData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height*0.6,
      child: Center(
        child: Image.asset(
          "assets/icons/no_data.png",
          width: 200,
          height: 150,
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }
}
