import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:driver/base/ui/text/text_ui.dart';
import 'package:driver/base/util/DateUtils.dart';
import 'package:driver/theme/app_theme.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

// abstract class UIDisplay {
//   Widget getDisplay(BaseBlocSuccessState state);
// }
enum ButtonActionType { OK, Cancel }
enum ViewType { Create, Edit, View }

extension ListExtension on List {
  List adds(List lst) {
    for (var i = 0; i < lst.length; i++) {
      this.add(lst[i]);
    }
    return this;
  }

  List ins<T>(int index, T element) {
    this..insert(index, element);
    return this;
  }
}
extension MapExt on Map{
  Map removeNullOrBlankValue({bool removeNull=true, bool removeBlank=true}){

    this.removeWhere((key, value) => (value== null && removeNull) || (GetUtils.isBlank(value) && removeBlank));
    return this;
  }
}

class CommonUI {
  // static const LO = new NumberFormat('vi_VN');
  static String format(Object n, {dateType: ''}) {
    if (n == null) {
      return '';
    }
    switch (n.runtimeType) {
      case String:
        return n;
      case double:
        // return formatDbl(n);
      case int:
        return formatNumber(n);
      case DateTime:
        return DateTimeUtils.convertDateToString(n, dateType);
      default:
        return '';
    }
  }

  static String formatDbl(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 2);
  }

  static String formatNumber(n, {ifnull = '', String locale}) {
    // final oCcy = new NumberFormat.decimalPattern(locale: locale, name: name, customPattern: customPattern);
    return n == null ? ifnull :NumberFormat.decimalPattern(locale??Get.locale.languageCode).format(n);
  }
  static String formatCurrency(n, {ifnull = '', locale,name='MMK', customPattern}) {
    final oCcy = new NumberFormat.currency(locale:locale??Get.locale.languageCode, name: name, customPattern: customPattern);
    return n == null ? ifnull : oCcy.format(n);
  }
  static Future<ButtonActionType> showConfirmDialog({String title, String message, String ok, String cancel, onOk, onCancel}) {
    return Get.defaultDialog(
      title: (title ?? 'titleConfirm').tr,
      content: Text((message ?? 'messageConfirm').tr),
      confirm: OutlinedButton(
          onPressed: () {
            onOk == null ? Get.back(result: ButtonActionType.OK) : onOk();
          },
          style: NormalButtonStyle.primary, //AppTheme.btnPrimary,
          child: Text((ok ?? 'ok').tr, style: AppTheme.btnTextPrimary)),
      cancel: OutlinedButton(
          onPressed: () {
            // Get.dialog(Text('input dialog'))
            onCancel == null ? Get.back(result: ButtonActionType.Cancel) : onCancel();
          },
          style: NormalButtonStyle.primaryOutline, //AppTheme.btnPrimaryOutline,
          child: Text((cancel ?? 'cancel').tr, style: AppTheme.btnTextPrimaryOutline)),
    );
  }

  static Future<ButtonActionType> showInputDialog(BuildContext context, {RxString obs}) {
    return Get.defaultDialog(
      title: 'rejectClaimTitle'.tr,
      content: context.myTextFormField(obs: obs, type: TextInputType.multiline, maxLines: 5),
      confirm: OutlinedButton(
          onPressed: () {
            // Get.dialog(Text('input dialog'))
            Get.back(result: ButtonActionType.OK);
          },
          style: NormalButtonStyle.primary, //AppTheme.btnPrimary,
          child: Text(('ok').tr, style: AppTheme.btnTextPrimary)),
      cancel: OutlinedButton(
          onPressed: () {
            // Get.dialog(Text('input dialog'))
            Get.back(result: ButtonActionType.Cancel);
          },
          style:NormalButtonStyle.primaryOutline, // AppTheme.btnPrimaryOutline,
          child: Text(('cancel').tr, style: AppTheme.btnTextPrimaryOutline)),
    );
  }

  static Future<List<Asset>> loadAssets(int numberImage,List<Asset> selectedAssets) async {
    return MultiImagePicker.pickImages(
      maxImages: numberImage,
      enableCamera: true,
      selectedAssets: selectedAssets,
      cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
      materialOptions: MaterialOptions(
        actionBarColor: "#abcdef",
        actionBarTitle: "Example App",
        allViewTitle: "All Photos",
        useDetailsView: false,
        selectCircleStrokeColor: "#000000",
      ),
    );
  }

  static Card card({Widget child, normal = true}) {
    return Card(
      color: Colors.transparent,
      shadowColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration (
          color: Colors.white,
          border: Border.all(
            width: 0,
            color: Colors.transparent
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        margin: AppTheme.cardMargin,
        child: child,
      ),
    );
  }


  static Card cardNoPaddingLTR({Widget child, normal = true} ){
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: normal?BorderSide.none: BorderSide(color: AppTheme.mainColor)
      ),
      child: Padding(
          padding: EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 10),
          child:child
      ),
    );
  }

  static Widget footer({List<Widget> children,Widget child}){

    return Container(
        // color: AppTheme.white,
        decoration: BoxDecoration(
          color: AppTheme.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
        ),
        padding: AppTheme.edgeAll,
        child: IntrinsicHeight(
            child: child!=null?child:Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: children,
        )));
  }

// static BlocProvider createProvider(BaseBloc bloc, UIDisplay display) {
//   return BlocProvider<BaseBloc>(
//       create: (context) => bloc,
//       child: BlocListener<BaseBloc, BaseBlocState>(
//         listener: (context, state) {
//           if (state is BaseBlocFailureState) {
//             ScaffoldMessenger.of(context).showSnackBar(SnackBar(
//               content: Text(state.error),
//             ));
//           }
//         },
//         child:
//             BlocBuilder<BaseBloc, BaseBlocState>(builder: (context, state) {
//           if (state is BaseBlocLoadingState) {
//             return WidgetCardLoading();
//           } else if (state is BaseBlocSuccessState) {
//             return display.getDisplay(state);
//           } else {
//             return new Container();
//           }
//         }),
//       ));
// }
//
// static BlocProvider createProvider1(GenericBloc bloc, UIDisplay display) {
//   return BlocProvider<GenericBloc>(
//       create: (context) => bloc,
//       child: BlocListener<GenericBloc, BaseBlocState>(
//         listener: (context, state) {
//           if (state is BaseBlocFailureState) {
//             ScaffoldMessenger.of(context).showSnackBar(SnackBar(
//               content: Text(state.error),
//             ));
//           }
//         },
//         child:
//         BlocBuilder<GenericBloc, BaseBlocState>(builder: (context, state) {
//           if (state is BaseBlocLoadingState) {
//             return WidgetCardLoading();
//           } else if (state is BaseBlocSuccessState) {
//             return display.getDisplay(state);
//           } else {
//             return new Container();
//           }
//         }),
//       ));
// }
}
