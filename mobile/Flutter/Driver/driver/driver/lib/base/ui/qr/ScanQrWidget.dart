import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:driver/base/ui/qr/ScanQr.dart';

class ScanQrWidget extends StatelessWidget {
  final Widget child;
  final bool Function(String qrCode) onData;
  final EdgeInsets padding;
  final bool showCode;
  final Widget Function(String qrCode) buildShowWidget;

  /*final ScanQRType scanType;
  final RxList<AbstractStatementDetail> statementDetails;*/

  ScanQrWidget(
      {Key key,
      @required this.onData,
      @required this.child,
      this.padding = EdgeInsets.zero, this.showCode, this.buildShowWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        var result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return Padding(
                padding: padding,
                child: QRViewExample(
                  onData,
                  showCode: showCode,
                  buildShowWidget: buildShowWidget,
                ));
          }),
        );
      },
      child: child,
    );
  }
}

