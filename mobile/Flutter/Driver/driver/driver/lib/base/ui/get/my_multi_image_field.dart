import 'package:flutter/material.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:driver/base/auth/api_auth_provider.dart';
import 'package:driver/base/ui/text/text_ui.dart';
import 'package:driver/theme/app_theme.dart';

import 'dart:async';

import 'package:multi_image_picker/multi_image_picker.dart';

import 'model/my_image_info.dart';

class MyMultiImageField extends StatefulWidget {
  RxList<MyImageInfo> images;
  final bool readOnly;

  MyMultiImageField({this.images, this.onChanged, this.readOnly});

  final ValueChanged<List<MyImageInfo>> onChanged;

  @override
  _MyMultiImageFieldState createState() => new _MyMultiImageFieldState();
}

class _MyMultiImageFieldState extends State<MyMultiImageField> {
  final double imgWidth = 300;
  final double imgHeight = 450;
  //List<MyImageInfo> images;
  // String _error = 'No Error Dectected';

  //_MyMultiImageFieldState(this.images);

  @override
  MyMultiImageField get widget => super.widget;

  @override
  void initState() {
    super.initState();
  }

  Widget buildGridView() {
    int plusOne = widget.readOnly ? 0 : 1;
    return GridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 5,
      mainAxisSpacing: 5,
      children: List.generate(widget.images.length + plusOne, (index) {
        if (index < widget.images.length) {
          return addItem(widget.images[index]);
        } else {
          return InkWell(
              child: Icon(Icons.add_a_photo_outlined), onTap: loadAssets);
        }
      }),
    );
  }

  Widget addItem(MyImageInfo imageInfo) {
    Widget image;
    if (imageInfo.asset != null) {
      image = AssetThumb(
        asset: imageInfo.asset,
        width: imgWidth.truncate(),
        height: imgHeight.truncate(),
      );
    } else {
      image = SizedBox(
          child: ApiAuthProvider().getImage(imageInfo.url),
          width: imgWidth.toDouble(),
          height: 140);
    }
    List<Widget> lst = [
      Container(
          decoration: new BoxDecoration(color: Colors.white),
          alignment: Alignment.center,
          padding: EdgeInsets.fromLTRB(0,0,0,40),
          height: imgHeight,
          child: image),
      Container(
          alignment: Alignment.bottomCenter,
          child: context.myTextFormField(
              text: imageInfo.description,
              onChange: (val)=> imageInfo.description = val),)
    ];
    if (!widget.readOnly) {
      lst.add(Align(
          alignment: Alignment.bottomRight,
          child: InkWell(
            child: Icon(
            Icons.delete_outline,
            color: AppTheme.removeColor,
          ),
          onTap: () => setState(() {
            widget.images.value.remove(imageInfo);
            if (widget.onChanged != null) {
              widget.onChanged(widget.images.value);
            }
          }))));
    }
    return Stack(children: lst);
  }

  List<Asset> getAssets() {
    List<Asset> lst = [];
    widget.images.value.forEach((element) {
      if (element.asset != null) lst.add(element.asset);
    });
    return lst;
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = [];
    // String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: getAssets(),
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      // error = e.toString();
      throw e;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      widget.images.assignAll(widget.images.value.where((element) => element.url != null).toList());
      widget.images.value.addAll(resultList.map((e) => MyImageInfo(null, e, null, null)).toList());
      if (widget.onChanged != null) {
        widget.onChanged(widget.images.value);
      }
      // _error = error;
    });
  }

  @override
  Widget build(BuildContext context) {
    return buildGridView();
  }
//
// Future<bool> checkAndRequestCameraPermissions() async {
//   PermissionStatus permission =
//   await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);
//   if (permission != PermissionStatus.granted) {
//     Map<PermissionGroup, PermissionStatus> permissions =
//     await PermissionHandler().requestPermissions([PermissionGroup.camera]);
//     return permissions[PermissionGroup.camera] == PermissionStatus.granted;
//   } else {
//     return true;
//   }
// }
}
