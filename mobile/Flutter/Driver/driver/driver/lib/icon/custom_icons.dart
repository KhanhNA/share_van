import 'package:flutter/material.dart';

class CustomIcons {
  static IconData fromName(String url){
    var list = url.split(".");
    var font = list[0];
    var codePoint = list[1];
    return IconData(int.parse(codePoint), fontFamily: font);
  }
}