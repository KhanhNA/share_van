import 'package:flutter/material.dart';

class DialogConfirm {
  String title = '';
  String content = '';
  bool isConfirm = false;
  Function onTapConfirm;

  DialogConfirm(this.title, this.content, this.isConfirm, this.onTapConfirm);

  showMaterialDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              title: Container(
                  height: 40,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        title,
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                            fontWeight: FontWeight.normal),
                      ),
                      IconButton(
                          icon: Icon(Icons.clear),
                          onPressed: () => {Navigator.of(context).pop()})
                    ],
                  )),
              content: Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Container(
                  height: 80,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        content,
                        style: TextStyle(
                            fontSize: 13,
                            color: Colors.black,
                            fontWeight: FontWeight.normal),
                        textAlign: TextAlign.center,
                      ),
                      isConfirm ? isConfirmWidget(context) : Container(),
                    ],
                  ),
                ),
              ),
            ));
  }
  Widget isConfirmWidget(BuildContext context) {
    return Container(
        child: ElevatedButton(
      onPressed: onTapConfirm,
      //         ()=>{
      //   Navigator.of(context).pop(),
      //   onTapConfirm
      // },
      child: Center(child: Text('Confirm')),
    ));
  }
}
