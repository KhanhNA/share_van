import 'package:driver/base/ui/list/list_ui.dart';
import 'package:driver/base/ui/common_ui.dart';
import 'package:driver/icon/custom_icons.dart';
import 'package:driver/model/notification/NotificationDto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:driver/base/ui/text/text_ui.dart';
import 'package:driver/theme/app_theme.dart';

import 'notification_routing_controller.dart';

class NotificationRoutingTapViewItem {
  final NotificationRoutingController controller1;

  NotificationRoutingTapViewItem(this.controller1);

  Widget buildItem(
      BuildContext context, NotificationDto notificationDto, int index) {
    return CommonUI.card(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Icon(
                Icons.calendar_today_outlined,
                color: AppTheme.grey,
              ),
              Text(
                notificationDto.create_date,
                style: TextStyle(color: Colors.black, fontSize: 10),
              )
            ],
          ),
          context.divider(),
          context.itemTitleView(notificationDto.title),
          context.itemView(notificationDto.content),
          context.divider(),
          context.listItemHeader(
            (''),
            context.textDrawableStart(
                "viewDetail".tr,
                CustomIcons.fromName('FontAwesome.0xf105'),
                Theme.of(context).primaryColor,
                isStart: false),
            onTap: () {
              controller1.details(notificationDto);
            },
          )
        ],
      ),
    );
  }
}
