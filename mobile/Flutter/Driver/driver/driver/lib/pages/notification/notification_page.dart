import 'package:driver/base/ui/get/base_get_view.dart';
import 'package:driver/pages/notification/notification_controller.dart';
import 'package:driver/pages/notification/routing_tap_view/Notification_routing_tap_view.dart';
import 'package:driver/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import 'system_tap_view/Notification_system_tap_view.dart';
import 'notification_vm.dart';

class NotificationPage extends BaseView<NotificationVM,NotificationController> {
  final List<Widget> listTab = <Widget>[];


  NotificationPage(){
    listTab.addAll([
      NotificationRoutingTapView(), NotificationSystemTapView()
    ]);
  }

  @override
  Widget body(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Column(
          children: <Widget>[
            Container(
              constraints: BoxConstraints(maxHeight: 150.0),
              child: Material(
                color: AppTheme.white,

                child: TabBar(
                  tabs: viewModel.notificationStatus.map((e) =>
                      Tab(text: ('Notification.' + e).tr)).toList(),
                ),
              ),
            ),
            Expanded(
              child: TabBarView(children: listTab),
            )
          ],
        ));
  }

  @override
  putController() {
    return NotificationController(NotificationVM());
  }


}