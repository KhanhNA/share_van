import 'package:driver/base/auth/api_auth_provider.dart';
import 'package:driver/model/list/PagedListData.dart';
import 'package:driver/model/notification/NotificationDto.dart';
import 'package:flutter/cupertino.dart';

class NotificationProvider {
  ApiAuthProvider api = ApiAuthProvider();

  Future<PagedListData<NotificationDto>> getNotifications(Map<String, dynamic> params) async {
      final response = await api.getListJsonParams<NotificationDto>(
          'notification',
          params,
              (js) => NotificationDto.fromJson(js));
      // when
      return response;
  }

  Future<PagedListData<NotificationDto>> seenNotification(Map<String, dynamic> params) async {
      final response = await api.getListJsonParams<NotificationDto>(
          'share_van_order/get_bill_routing_by_day',
          params,
              (js) => NotificationDto.fromJson(js));
      // when
      return response;
  }
}