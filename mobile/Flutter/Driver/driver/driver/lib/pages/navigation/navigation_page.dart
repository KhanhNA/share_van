import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';

import 'navigation_controller.dart';

class NavigationViewPage extends GetView<NavigationController> {
  final List<Widget> _widgetBody = <Widget>[
    Center(child: Text('Home')),
    Center(child: Text('Home')),
    Center(child: Text('Home')),
    Center(child: Text('Home')),
    Center(child: Text('Home')),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 20,
          backgroundColor: Colors.white,
          leading: Container(
              // 'assets/images/ic_dlp.png',
              // width: 48,
              // height: 48,
              ),
          // title: Row(
          //   mainAxisAlignment: MainAxisAlignment.end,
          //   children: <Widget>[
          //     Image.asset(
          //       'assets/icons/qr_code_96.png',
          //       width: 32,
          //       height: 32,
          //     ),
          //   ],
          // ),
        ),
        body: Padding(
            padding: EdgeInsets.only(top: 8),
            child: Obx(() => _getBody(controller.navInd.value))),
        // floatingActionButton: floatButton()
        bottomNavigationBar: _bottomNavigationBar());
  }

  Widget floatButton() {
    return SpeedDial(
        backgroundColor: Colors.green,
        child: Icon(Icons.add),
        children: [
          SpeedDialChild(
              backgroundColor: Colors.black12,
              child: Icon(Icons.create_new_folder_outlined),
              label: 'Create order',
              onTap: () => {print('Create order click')}),
          SpeedDialChild(
              backgroundColor: Colors.black12,
              child: Icon(Icons.qr_code_scanner_rounded),
              label: 'Scan qr Routing',
              onTap: () => {print('Create order click')}),
        ]);
  }

  Widget _getBody(int index) {
    return _widgetBody[index];
  }

  _bottomNavigationBar() {
    return Obx(() => BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            iconSize: 30,
            selectedFontSize: 13,
            onTap: _onItemTap,
            selectedItemColor: Colors.green,
            currentIndex: controller.navInd.value,
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.camera_alt),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Container(
                  decoration: BoxDecoration(shape: BoxShape.circle),
                  child: Icon(Icons.add),
                ),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.notifications_active),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.account_circle),
                label: '',
              ),
            ]));
  }

  _onItemTap(int index) {
    controller.navInd.value = index;
    // if(index==0){
    //   Get.to(()=>RoutingVanDayPage());
    // }
  }
}
