import 'package:driver/base/LocalizationService.dart';
import 'package:driver/base/auth/token.dart';
import 'package:driver/base/util/AppUtils.dart';
import 'package:driver/model/login/PartnerDto.dart';
import 'package:driver/model/login/UserAuthentication.dart';
import 'package:driver/model/login/login_body.dart';
import 'package:driver/model/session/OdooSessionDto.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'api/user_repository.dart';

class LoginController extends GetxController {
  final UserRepository repository = UserRepository();
  final box = GetStorage();
  LoginBody loginBody;
  final _username = 'afd'.obs;
  final _password = ''.obs;

  final _themeIsDark = false.obs;

  get themeIsDark => this._themeIsDark.value;

  set themeIsDark(value) => this._themeIsDark.value = value;

  @override
  onInit() {
    super.onInit();
    this.lang = LocalizationService.locale;
    this.username = 'minhngoc_manager'; //box.read('username');
    this.password = 'abc@123';
  }

  @override
  void onReady() {
    // changeTheme();

    super.onReady();
  }

  final _screen = 0.obs;

  get screen => this._screen.value;

  set screen(value) => this._screen.value = value;

  final _lang = 'en'.obs;

  String get lang => this._lang.value;

  set lang(value) => this._lang.value = value;

  changeLanguage(lang) {
    this.lang = lang;
    LocalizationService().changeLocale(lang);
    box.write('lang', lang);
  }

  Future<bool> login() async {
    final pass = _password.value ?? 'abc@123';
    LoginBody loginBody = LoginBody(_username.value, pass, 'DLP');

    OdooSessionDto odooSessionDto = await repository.login(loginBody);
    AppUtils.odooSessionDto = odooSessionDto;
    UserAuthentication me = await repository.getUserMe();
    PartnerDto partnerDto = await repository.getInfoCustomer();
    AppUtils.partnerDto = partnerDto;
    AppUtils.me = me;
    AppUtils.username = loginBody.login;
    await box.write(AppUtils.keyIsLogin, true);
    await box.write(AppUtils.keyUsername, loginBody.login);
    return true;
  }

  get password => _password;

  set password(value) {
    _password.value = value;
  }

  get username => _username;

  set username(value) {
    _username.value = value;
  }
}
