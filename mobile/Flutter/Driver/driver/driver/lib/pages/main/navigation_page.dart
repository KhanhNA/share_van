
import 'package:driver/base/controller/BaseZone.dart';
import 'package:driver/pages/login/login_page.dart';
import 'package:driver/pages/navigation/navigation_controller.dart';
import 'package:driver/pages/navigation/navigation_page.dart';
import 'package:driver/pages/navigation/navigation_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';



class NavigationPage extends GetView {
  // final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final NavigationController controller1 = Get.put(NavigationController(NavigationVM()));

  // String route = Routes.HOME;
  final List<Widget> screens = [
    LoginPage()
  ];
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: NavigationViewPage()
    );
  }

  NavigationPage(){
    // Get.put(HomeController(HomeVM()));
  }
}
