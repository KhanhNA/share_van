import 'package:json_annotation/json_annotation.dart';
import 'package:driver/model/base.dart';
part 'Menu.g.dart';
@JsonSerializable(explicitToJson: true)
class Menu extends _Menu {

    String error;
    Menu.withError(this.error);
    Menu();

    factory Menu.fromJson(Map<String, dynamic> js) =>Base().fromJs<Menu>(js, (js) => _$MenuFromJson(js));

    Map<String, dynamic> toJson() => _$MenuToJson(this);
}

class _Menu extends Base{
    int id;
    String clientId;
    String appType;
    String code;
    String url;
    bool hasChild;
    Menu parentMenu;
}
