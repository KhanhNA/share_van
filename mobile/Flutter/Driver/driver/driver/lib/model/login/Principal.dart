
import 'Menu.dart';
import 'Role.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:driver/model/base.dart';
part 'Principal.g.dart';
@JsonSerializable(explicitToJson: true)
class Principal extends _Principal {

    String error;
    Principal.withError(this.error);
    Principal();

    factory Principal.fromJson(Map<String, dynamic> js) =>Base().fromJs<Principal>(js, (js) => _$PrincipalFromJson(js));

    Map<String, dynamic> toJson() => _$PrincipalToJson(this);
}


class _Principal extends Base{
    String firstName;
    String lastName;
    bool mustChangePassword;
    List<Role> roles;
    List<Menu> menus;


}
