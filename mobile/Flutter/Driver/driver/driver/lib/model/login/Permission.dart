import 'package:json_annotation/json_annotation.dart';
import 'package:driver/model/base.dart';
part 'Permission.g.dart';
@JsonSerializable(explicitToJson: true)
class Permission extends _Permission {

    String error;
    Permission.withError(this.error);
    Permission();

    factory Permission.fromJson(Map<String, dynamic> js) =>Base().fromJs<Permission>(js, (js) => _$PermissionFromJson(js));

    Map<String, dynamic> toJson() => _$PermissionToJson(this);
}

class _Permission extends Base{
    int id;
    String clientId;
    String url;
    String description;
}
