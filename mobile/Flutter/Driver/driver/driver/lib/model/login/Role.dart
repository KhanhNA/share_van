
import 'package:json_annotation/json_annotation.dart';
import 'package:driver/model/base.dart';

import 'Menu.dart';
import 'Permission.dart';

part 'Role.g.dart';
@JsonSerializable(explicitToJson: true)
class Role extends _Role {

    String error;
    Role.withError(this.error);
    Role();

    factory Role.fromJson(Map<String, dynamic> js) =>Base().fromJs<Role>(js, (js) => _$RoleFromJson(js));

    Map<String, dynamic> toJson() => _$RoleToJson(this);
}


class _Role extends Base {
    int id;
    String roleName;
    String description;
    List<Menu> menus;
    List<Permission> permissions;


    List<Menu> getMobileMenus() {
        List<Menu> result = [];
        for (Menu menu in menus) {
            if ('MOBILE' == menu.appType) {
                result.add(menu);
            }
        }
        return result;
    }



}
