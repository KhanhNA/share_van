import 'package:json_annotation/json_annotation.dart';

import 'CountryDescription.dart';
import 'base.dart';

part 'Country.g.dart';
@JsonSerializable(explicitToJson: true)
class Country extends _Country {

  String error;
  Country.withError(this.error);
  Country();

  factory Country.fromJson(Map<String, dynamic> js) =>Base().fromJs<Country>(js, (js) => _$CountryFromJson(js));

  Map<String, dynamic> toJson() => _$CountryToJson(this);
}

class _Country extends Base {
  String isocode;
  bool support;
  List<CountryDescription> countryDescriptions;

  //_Country(this.isocode, this.support, this.countryDescriptions);

}
