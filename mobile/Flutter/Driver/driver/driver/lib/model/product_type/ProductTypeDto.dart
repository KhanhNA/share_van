import 'package:driver/base/json/datetime_converter.dart';
import 'package:driver/model/vendor/VendorDto.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
part 'ProductTypeDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class ProductTypeDto extends _ProductTypeDto{

  factory ProductTypeDto.fromJson(Map<String, dynamic> js) => Base().fromJs(js, (js) => _$ProductTypeDtoFromJson(js));

  Map<String, dynamic> toJson() => _$ProductTypeDtoToJson(this);

  ProductTypeDto();
}

class _ProductTypeDto extends Base{
   int id;
   String name_seq;
   String name;
   double net_weight;
   String description;
   String status;
   double extra_price;
}