// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PartnerDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PartnerDto _$PartnerDtoFromJson(Map<String, dynamic> json) {
  return PartnerDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..display_name = json['display_name'] as String
    ..date = json['date'] as bool
    ..title = json['title'] as String
    ..parent_id = json['parent_id'] as String
    ..parent_name = json['parent_name'] as String
    ..uri_path = json['uri_path'] as String
    ..depot_id = json['depot_id'] as int
    ..staff_type = json['staff_type'] as int
    ..staff_type_name = json['staff_type_name'] as String
    ..customer_type = json['customer_type'] as String
    ..ref = json['ref'] as String
    ..lang = json['lang'] as String
    ..active_lang_count = (json['active_lang_count'] as num)?.toDouble()
    ..tz = json['tz'] as String
    ..tz_offset = json['tz_offset'] as String
    ..vat = json['vat'] as String
    ..same_vat_partner_id = json['same_vat_partner_id'] as String
    ..website = json['website'] as String
    ..comment = json['comment'] as String
    ..credit_limit = (json['credit_limit'] as num)?.toDouble()
    ..active = json['active'] as bool
    ..employee = json['employee'] as String
    ..function = json['function'] as String
    ..type = json['type'] as String
    ..street = json['street'] as String
    ..street2 = json['street2'] as String
    ..zip = json['zip'] as String
    ..city = json['city'] as String
    ..partner_latitude = (json['partner_latitude'] as num)?.toDouble()
    ..partner_longitude = (json['partner_longitude'] as num)?.toDouble()
    ..email = json['email'] as String
    ..email_formatted = json['email_formatted'] as String
    ..phone = json['phone'] as String
    ..mobile = json['mobile'] as String
    ..is_company = json['is_company'] as String
    ..company_type = json['company_type'] as String
    ..company_id = json['company_id'] as int
    ..color = (json['color'] as num)?.toDouble()
    ..partner_share = json['partner_share'] as String
    ..contact_address = json['contact_address'] as String
    ..commercial_company_name = json['commercial_company_name'] as String
    ..company_name = json['company_name'] as String
    ..name_seq = json['name_seq'] as String
    ..im_status = json['im_status'] as String
    ..date_localization = json['date_localization'] as String
    ..activity_state = json['activity_state'] as String
    ..activity_user_id = json['activity_user_id'] as int
    ..activity_type_id = json['activity_type_id'] as int
    ..activity_date_deadline = json['activity_date_deadline'] as String
    ..activity_summary = json['activity_summary'] as String
    ..activity_exception_decoration =
        json['activity_exception_decoration'] as String
    ..activity_exception_icon = json['activity_exception_icon'] as String
    ..message_is_follower = json['message_is_follower'] as String
    ..message_unread = json['message_unread'] as String
    ..message_unread_counter =
        (json['message_unread_counter'] as num)?.toDouble()
    ..message_needaction = json['message_needaction'] as String
    ..message_needaction_counter =
        (json['message_needaction_counter'] as num)?.toDouble()
    ..message_has_error = json['message_has_error'] as String
    ..message_has_error_counter =
        (json['message_has_error_counter'] as num)?.toDouble()
    ..message_attachment_count =
        (json['message_attachment_count'] as num)?.toDouble()
    ..message_main_attachment_id = json['message_main_attachment_id'] as int
    ..email_normalized = json['email_normalized'] as String
    ..is_blacklisted = json['is_blacklisted'] as bool
    ..message_bounce = (json['message_bounce'] as num)?.toDouble()
    ..signup_token = json['signup_token'] as String
    ..signup_type = json['signup_type'] as String
    ..signup_expiration = json['signup_expiration'] as String
    ..signup_valid = json['signup_valid'] as String
    ..signup_url = json['signup_url'] as String
    ..partner_gid = json['partner_gid'] as int
    ..additional_info = json['additional_info'] as String
    ..phone_sanitized = json['phone_sanitized'] as String
    ..phone_blacklisted = json['phone_blacklisted'] as String
    ..message_has_sms_error = json['message_has_sms_error'] as String
    ..image_1920 = json['image_1920'] as String
    ..image_1024 = json['image_1024'] as String
    ..image_512 = json['image_512'] as String
    ..image_256 = json['image_256'] as String
    ..image_128 = json['image_128'] as String
    ..walletAcount = json['walletAcount'] as String
    ..walletId = json['walletId'] as int;
}

Map<String, dynamic> _$PartnerDtoToJson(PartnerDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'name': instance.name,
      'display_name': instance.display_name,
      'date': instance.date,
      'title': instance.title,
      'parent_id': instance.parent_id,
      'parent_name': instance.parent_name,
      'uri_path': instance.uri_path,
      'depot_id': instance.depot_id,
      'staff_type': instance.staff_type,
      'staff_type_name': instance.staff_type_name,
      'customer_type': instance.customer_type,
      'ref': instance.ref,
      'lang': instance.lang,
      'active_lang_count': instance.active_lang_count,
      'tz': instance.tz,
      'tz_offset': instance.tz_offset,
      'vat': instance.vat,
      'same_vat_partner_id': instance.same_vat_partner_id,
      'website': instance.website,
      'comment': instance.comment,
      'credit_limit': instance.credit_limit,
      'active': instance.active,
      'employee': instance.employee,
      'function': instance.function,
      'type': instance.type,
      'street': instance.street,
      'street2': instance.street2,
      'zip': instance.zip,
      'city': instance.city,
      'partner_latitude': instance.partner_latitude,
      'partner_longitude': instance.partner_longitude,
      'email': instance.email,
      'email_formatted': instance.email_formatted,
      'phone': instance.phone,
      'mobile': instance.mobile,
      'is_company': instance.is_company,
      'company_type': instance.company_type,
      'company_id': instance.company_id,
      'color': instance.color,
      'partner_share': instance.partner_share,
      'contact_address': instance.contact_address,
      'commercial_company_name': instance.commercial_company_name,
      'company_name': instance.company_name,
      'name_seq': instance.name_seq,
      'im_status': instance.im_status,
      'date_localization': instance.date_localization,
      'activity_state': instance.activity_state,
      'activity_user_id': instance.activity_user_id,
      'activity_type_id': instance.activity_type_id,
      'activity_date_deadline': instance.activity_date_deadline,
      'activity_summary': instance.activity_summary,
      'activity_exception_decoration': instance.activity_exception_decoration,
      'activity_exception_icon': instance.activity_exception_icon,
      'message_is_follower': instance.message_is_follower,
      'message_unread': instance.message_unread,
      'message_unread_counter': instance.message_unread_counter,
      'message_needaction': instance.message_needaction,
      'message_needaction_counter': instance.message_needaction_counter,
      'message_has_error': instance.message_has_error,
      'message_has_error_counter': instance.message_has_error_counter,
      'message_attachment_count': instance.message_attachment_count,
      'message_main_attachment_id': instance.message_main_attachment_id,
      'email_normalized': instance.email_normalized,
      'is_blacklisted': instance.is_blacklisted,
      'message_bounce': instance.message_bounce,
      'signup_token': instance.signup_token,
      'signup_type': instance.signup_type,
      'signup_expiration': instance.signup_expiration,
      'signup_valid': instance.signup_valid,
      'signup_url': instance.signup_url,
      'partner_gid': instance.partner_gid,
      'additional_info': instance.additional_info,
      'phone_sanitized': instance.phone_sanitized,
      'phone_blacklisted': instance.phone_blacklisted,
      'message_has_sms_error': instance.message_has_sms_error,
      'image_1920': instance.image_1920,
      'image_1024': instance.image_1024,
      'image_512': instance.image_512,
      'image_256': instance.image_256,
      'image_128': instance.image_128,
      'walletAcount': instance.walletAcount,
      'walletId': instance.walletId,
    };
