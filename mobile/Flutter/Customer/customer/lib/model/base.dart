import 'package:json_annotation/json_annotation.dart';

import 'base_model.dart';

// part 'base.g.dart';
// @JsonSerializable(explicitToJson: true)
typedef S ConvertFunc<S>(Map<String, dynamic> js);
abstract class DisplayItem{
  String get str;
}
class Base extends BaseModel {
  static const String UUID = 'uuId';
  static const String RFID = '@id';
  static const String RFEF = '@ref';
  static const String DATA = 'DATA';
  static const String OBJ = 'OBJ';
  @JsonKey(ignore: true)
  // static Map<String, List> referenceObj = {};
  // static Map<String, List> referenceNotExistObj = {};
  static Map<String, int> step = {};
  // static Map<String, Map> referenceData = {};
  static Map<String, dynamic> reference = {};

  reduce() {
    uuId = null;
    _fId = null;
    _refId = null;
  }

  int id;

  String uuId;

  String _fId;

  String _refId;
  DateTime createDate;
  String createUser;
  DateTime updateDate;
  String updateUser;

  @JsonKey(name: '@id')
  String get fId => _fId;

  String get display => toString();

  set fId(String value) {
    _fId = value;
    if (uuId == null) {
      return;
    }
    int step = Base.step[uuId];
    step ??= 0;
    if (step == 1) {
      if (value != null) {
        Map obj = reference[uuId + '-' + OBJ] ??= {};

        obj.putIfAbsent(uuId + '-' + (this.runtimeType).toString() + '-' + value, () => [this]);
      }
    }
  }

  @JsonKey(name: '@ref')
  String get refId => _refId;

  set refId(String value) {
    _refId = value;
  }

  T putMap<T>(Map<String, dynamic> js, ConvertFunc<T> convertFunc) {
    final uuId = js[UUID];
    js.forEach((key, value) {
      if (value is Map) {
        value[UUID] = uuId;
      } else if (value is List) {
        value.forEach((element) {
          if (element is Map) {
            element[UUID] = uuId;
          }
        });
      }
    });
    var rId = js['@id'];
    if (rId != null) {
      Map obj = reference[uuId + '-' + DATA] ??= {};
      final key = uuId + '-' + (T).toString() + '-' + rId;
      obj.putIfAbsent(key, () => js);
    }
    convertFunc(js);
    return null;
  }

  T getObj<T>(Map<String, dynamic> js, ConvertFunc<T> convertFunc) {
    var rId = js['@id'];
    var ref = js['@ref'];
    Map map = js;
    String uuId = js[UUID];
    if (ref != null) {
      Map obj = reference[uuId + '-' + OBJ] ??= {};
      List rObjs = obj[uuId + '-' + (T).toString() + '-' + ref];

      if (rObjs != null) {
        return rObjs.first;
      }
      obj = reference[uuId + '-' + DATA] ??= {};
      map = obj[uuId + '-' + (T).toString() + '-' + ref];
    } else if (rId != null) {
      Map obj = reference[uuId + '-' + OBJ] ??= {};
      List rObjs = obj[uuId + '-' + (T).toString() + '-' + rId];

      if (rObjs != null) {
        return rObjs.first;
      }
      // map = referenceData[uuId + '-' + (T).toString() + '-' + r];
    }
    return map != null ? convertFunc(map) : null;
  }

  T fromJs<T>(Map<String, dynamic> js, ConvertFunc<T> convertFunc) {
    String uuId = js[UUID];
    int step = Base.step[uuId];
    step ??= 0;
    // uuId??=Uuid().v4();
    if (step == 0) {
      return putMap(js, convertFunc);
    } else {
      return getObj(js, convertFunc);
    }
    //   js.forEach((key, value) {
    //     if (value is Map) {
    //       value[UUID] = uuId;
    //     }else if(value is List){
    //       value.forEach((element) {
    //         if(element is Map) {
    //           element[UUID] = uuId;
    //         }
    //       });
    //     }
    //   });
    // }
    // var rId = js['@id'];
    // var ref = js['@ref'];
    // T instance = convertFunc(js);
    // if(ref == null) {
    //
    //   if (rId != null) {
    //     List list = Base.referenceObj[uuId + '-' + rId];
    //     if (list == null) {
    //       list = [instance];
    //       Base.referenceObj[uuId + '-' + rId] = list;
    //     }
    //   }
    //   return instance;
    //
    // }
    // List list = Base.referenceObj[uuId + '-' + ref];
    // if(list == null){
    //   // Base.referenceNotExistObj.putIfAbsent(uuId + '-' + ref, () => )
    // }
    // return  list == null? instance:list.first as T;
  }
// int get fId => _fId;
//
// set fId(int value) {
//   _fId = value;
// }
//
// int get refId => _refId;
//
// set refId(int value) {
//   _refId = value;
// }
}
