import 'package:customer/base/json/datetime_converter.dart';
import 'package:customer/model/vendor/VendorDto.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
part 'InsuranceDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class InsuranceDto extends _InsuranceDto{

  factory InsuranceDto.fromJson(Map<String, dynamic> js) => _$InsuranceDtoFromJson(js);

  Map<String, dynamic> toJson() => _$InsuranceDtoToJson(this);

  InsuranceDto();
}

class _InsuranceDto extends Base{
   int id;
   String code;
   String name;
   VendorDto vendor;
   String status;
   double amount;
   String display_name;
   String description;
}