// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'OdooSessionDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OdooSessionDto _$OdooSessionDtoFromJson(Map<String, dynamic> json) {
  return OdooSessionDto()
    ..id = json['id'] as int
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..username = json['username'] as String
    ..db = json['db'] as String
    ..uid = json['uid'] as int
    ..is_system = json['is_system'] as bool
    ..is_admin = json['is_admin'] as bool
    ..name = json['name'] as String
    ..partner_display_name = json['partner_display_name'] as String
    ..company_id = json['company_id'] as int
    ..partner_id = json['partner_id'] as int
    ..show_effect = json['show_effect'] as String
    ..display_switch_company_menu = json['display_switch_company_menu'] as bool
    ..session_id = json['session_id'] as String
    ..access_token = json['access_token'] as String
    ..distance_notification_message =
        json['distance_notification_message'] as String
    ..time_mobile_notification_key =
        json['time_mobile_notification_key'] as String
    ..company_type = json['company_type'] as String
    ..save_log_duration = json['save_log_duration'] as String
    ..duration_request = json['duration_request'] as String
    ..distance_check_point = json['distance_check_point'] as String
    ..sharevan_sos = json['sharevan_sos'] as String
    ..biding_time_confirm = json['biding_time_confirm'] as String
    ..currency = json['currency'] as String
    ..google_api_key_geocode = json['google_api_key_geocode'] as String
    ..weight_constant_order = json['weight_constant_order'] as String
    ..rating_customer_duration_key =
        json['rating_customer_duration_key'] as String
    ..weight_unit = json['weight_unit'] as String
    ..volume_unit = json['volume_unit'] as String
    ..length_unit = json['length_unit'] as String
    ..parcel_unit = json['parcel_unit'] as String;
}

Map<String, dynamic> _$OdooSessionDtoToJson(OdooSessionDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'username': instance.username,
      'db': instance.db,
      'uid': instance.uid,
      'is_system': instance.is_system,
      'is_admin': instance.is_admin,
      'name': instance.name,
      'partner_display_name': instance.partner_display_name,
      'company_id': instance.company_id,
      'partner_id': instance.partner_id,
      'show_effect': instance.show_effect,
      'display_switch_company_menu': instance.display_switch_company_menu,
      'session_id': instance.session_id,
      'access_token': instance.access_token,
      'distance_notification_message': instance.distance_notification_message,
      'time_mobile_notification_key': instance.time_mobile_notification_key,
      'company_type': instance.company_type,
      'save_log_duration': instance.save_log_duration,
      'duration_request': instance.duration_request,
      'distance_check_point': instance.distance_check_point,
      'sharevan_sos': instance.sharevan_sos,
      'biding_time_confirm': instance.biding_time_confirm,
      'currency': instance.currency,
      'google_api_key_geocode': instance.google_api_key_geocode,
      'weight_constant_order': instance.weight_constant_order,
      'rating_customer_duration_key': instance.rating_customer_duration_key,
      'weight_unit': instance.weight_unit,
      'volume_unit': instance.volume_unit,
      'length_unit': instance.length_unit,
      'parcel_unit': instance.parcel_unit,
    };
