import 'package:customer/base/json/datetime_converter.dart';
import 'package:customer/base/util/AppUtils.dart';
import 'package:customer/model/insurance/SubscribeDto.dart';
import 'package:customer/model/product_type/ProductTypeDto.dart';
import 'package:json_annotation/json_annotation.dart';
import '../base.dart';
part 'BillPackageDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class BillPackageDto extends _BillPackageDto{

  factory BillPackageDto.fromJson(Map<String, dynamic> js) => _$BillPackageDtoFromJson(js);

  Map<String, dynamic> toJson() => _$BillPackageDtoToJson(this);

  BillPackageDto();
}
class _BillPackageDto extends Base{

   @JsonKey(ignore: true)
   int createOrderId;

   int id;
   String key_map;//map được bill package giữa kho xuất hàng và kho nhận hàng
   String warehouse_name;
   String address;
   String phone;

   int from_bill_package_id;
   int from_change_bill_package_id;
   int bill_package_id;

   int bill_lading_detail_id;
   String item_name;
   double net_weight;// weight sau khi tính toán
   double weight;//weight do người dùng nhập vào

   int quantity_package;
   double length;
   double width;
   double height;
   double totalVol;
   double capacity;
   double total_weight;
   int quantity_import;
   int quantity_export;

   String product_type_name;
   String status;

   ProductTypeDto productType;

   String qr_char;

   String insurance_name;
   String subscribe_name;
   // BillPackageDto origin_bill_package;


  //properties for ui
   int rest = 0; // số lượng còn lại
    bool isSelect;
    int splitQuantity = 0;//số lượng đã tách

   double calculateNetWeight() {
      if (length != null && width != null && height != null) {
         length *= 100;
         width *= 100;
         height *= 100;
//            từ đơn vị m * 100 -> đơn vị cm
         return (length * width * height) / double.parse(AppUtils.odooSessionDto.weight_constant_order);
      }
      return 0;
   }
   double getTotalVol() {
      return this.height * this.length * this.width * this.quantity_package;
   }

}