// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'BillServiceDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BillServiceDto _$BillServiceDtoFromJson(Map<String, dynamic> json) {
  return BillServiceDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..display_name = json['display_name'] as String
    ..service_code = json['service_code'] as String
    ..type = json['type'] as String
    ..price = (json['price'] as num)?.toDouble()
    ..description = json['description'] as String
    ..status = json['status'] as String
    ..partner_name = json['partner_name'] as String;
}

Map<String, dynamic> _$BillServiceDtoToJson(BillServiceDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'name': instance.name,
      'display_name': instance.display_name,
      'service_code': instance.service_code,
      'type': instance.type,
      'price': instance.price,
      'description': instance.description,
      'status': instance.status,
      'partner_name': instance.partner_name,
    };
