// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Distributor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Distributor _$DistributorFromJson(Map<String, dynamic> json) {
  return Distributor()
    ..id = json['id'] as int
    ..uuId = json['uuId'] as String
    ..createDate = json['createDate'] == null
        ? null
        : DateTime.parse(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate = json['updateDate'] == null
        ? null
        : DateTime.parse(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..aboutUs = json['aboutUs'] as String
    ..address = json['address'] as String
    ..code = json['code'] as String
    ..email = json['email'] as String
    ..enabled = json['enabled'] as bool
    ..name = json['name'] as String
    ..phoneNumber = json['phoneNumber'] as String;
}

Map<String, dynamic> _$DistributorToJson(Distributor instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uuId': instance.uuId,
      'createDate': instance.createDate?.toIso8601String(),
      'createUser': instance.createUser,
      'updateDate': instance.updateDate?.toIso8601String(),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'aboutUs': instance.aboutUs,
      'address': instance.address,
      'code': instance.code,
      'email': instance.email,
      'enabled': instance.enabled,
      'name': instance.name,
      'phoneNumber': instance.phoneNumber,
    };
