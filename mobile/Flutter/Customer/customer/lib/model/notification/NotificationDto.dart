import 'package:customer/base/json/datetime_converter.dart';
import 'package:customer/model/vendor/VendorDto.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
part 'NotificationDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class NotificationDto extends _NotificationDto{

  factory NotificationDto.fromJson(Map<String, dynamic> js) => _$NotificationDtoFromJson(js);

  Map<String, dynamic> toJson() => _$NotificationDtoToJson(this);

  NotificationDto();
}
class _NotificationDto extends Base{
   int id;
   int notification_id;
   bool is_read;
   int create_uid;
   String create_date;
   String sent_date;
   String title;
   String content;
   String type;
   String click_action;
   String message_type;
   String item_id;
   String object_status;
   String image_256;
   String description;
   int total_message_not_seen;
}