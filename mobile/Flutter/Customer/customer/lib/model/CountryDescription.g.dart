// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CountryDescription.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CountryDescription _$CountryDescriptionFromJson(Map<String, dynamic> json) {
  return CountryDescription()
    ..id = json['id'] as int
    ..uuId = json['uuId'] as String
    ..createDate = json['createDate'] == null
        ? null
        : DateTime.parse(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate = json['updateDate'] == null
        ? null
        : DateTime.parse(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..language = json['language'] == null
        ? null
        : Language.fromJson(json['language'] as Map<String, dynamic>)
    ..country = json['country'] == null
        ? null
        : Country.fromJson(json['country'] as Map<String, dynamic>)
    ..name = json['name'] as String
    ..title = json['title'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic> _$CountryDescriptionToJson(CountryDescription instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uuId': instance.uuId,
      'createDate': instance.createDate?.toIso8601String(),
      'createUser': instance.createUser,
      'updateDate': instance.updateDate?.toIso8601String(),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'language': instance.language?.toJson(),
      'country': instance.country?.toJson(),
      'name': instance.name,
      'title': instance.title,
      'description': instance.description,
    };
