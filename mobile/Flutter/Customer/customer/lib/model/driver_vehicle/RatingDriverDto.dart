import 'package:customer/model/base.dart';
import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

import 'RatingBadgesDto.dart';

part 'RatingDriverDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class RatingDriverDto extends _RatingDriverDto{

  factory RatingDriverDto.fromJson(Map<String, dynamic> js) => _$RatingDriverDtoFromJson(js);

  Map<String, dynamic> toJson() => _$RatingDriverDtoToJson(this);

  RatingDriverDto();
}
class _RatingDriverDto extends Base{
  int id;
  int num_rating;
  String note;
  List<RatingBadgesDto> badges_routings;
}