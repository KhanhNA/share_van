// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RatingBadgesDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RatingBadgesDto _$RatingBadgesDtoFromJson(Map<String, dynamic> json) {
  return RatingBadgesDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..rating_level = json['rating_level'] as String
    ..name = json['name'] as String
    ..description = json['description'] as String
    ..image = json['image'] as String
    ..check = json['check'] as bool;
}

Map<String, dynamic> _$RatingBadgesDtoToJson(RatingBadgesDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'rating_level': instance.rating_level,
      'name': instance.name,
      'description': instance.description,
      'image': instance.image,
      'check': instance.check,
    };
