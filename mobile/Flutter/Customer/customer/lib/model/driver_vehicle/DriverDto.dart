import '../base.dart';

import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'DriverDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class DriverDto extends _DriverDto{

  factory DriverDto.fromJson(Map<String, dynamic> js) => _$DriverDtoFromJson(js);

  Map<String, dynamic> toJson() => _$DriverDtoToJson(this);

  DriverDto();
}
class _DriverDto extends Base{
   int id;
   String driver_code;
   String name;

   String phone;
   String image_1920;
}