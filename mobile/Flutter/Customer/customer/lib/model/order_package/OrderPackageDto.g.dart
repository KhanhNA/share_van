// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'OrderPackageDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderPackageDto _$OrderPackageDtoFromJson(Map<String, dynamic> json) {
  return OrderPackageDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..type = json['type'] as String
    ..description = json['description'] as String;
}

Map<String, dynamic> _$OrderPackageDtoToJson(OrderPackageDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'name': instance.name,
      'type': instance.type,
      'description': instance.description,
    };
