
import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
part 'VendorDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class VendorDto extends _VendorDto{

  factory VendorDto.fromJson(Map<String, dynamic> js)  => _$VendorDtoFromJson(js);

  Map<String, dynamic> toJson() => _$VendorDtoToJson(this);

  VendorDto();
}

class _VendorDto extends Base{
   int id;
   String name;
   String phone;
   String email;
   String country_id;
   String city_name;
   String district;
}