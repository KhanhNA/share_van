// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AreaDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AreaDto _$AreaDtoFromJson(Map<String, dynamic> json) {
  return AreaDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..code = json['code'] as String
    ..province_name = json['province_name'] as String
    ..hubInfo = json['hubInfo'] == null
        ? null
        : HubDto.fromJson(json['hubInfo'] as Map<String, dynamic>)
    ..zoneInfo = json['zoneInfo'] == null
        ? null
        : ZoneDto.fromJson(json['zoneInfo'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AreaDtoToJson(AreaDto instance) => <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'name': instance.name,
      'code': instance.code,
      'province_name': instance.province_name,
      'hubInfo': instance.hubInfo?.toJson(),
      'zoneInfo': instance.zoneInfo?.toJson(),
    };
