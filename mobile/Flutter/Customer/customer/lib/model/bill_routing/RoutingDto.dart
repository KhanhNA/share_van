
import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
part 'RoutingDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class RoutingDto extends _RoutingDto{

  factory RoutingDto.fromJson(Map<String, dynamic> js) => _$RoutingDtoFromJson(js);

  Map<String, dynamic> toJson() => _$RoutingDtoToJson(this);

  RoutingDto();
}

class _RoutingDto extends Base{
   int id;
   String status;
   int driver_id;
   int vehicle_id;
   double latitude;
   double longitude;
   String warehouse_name;
   int from_routing_plan_day_id;
   String ship_type;
   String driver_name;
   String driver_phone;
   String license_plate;
   String vehicle_name;
   String accept_time;
   String type;// nhập hay xuất.
   String routing_plan_day_code;
}