import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:customer/translations/en_us.dart';
import 'package:customer/translations/vi_vn.dart';


class LocalizationService extends Translations {
  // Default locale
  static final locale = GetStorage().read('lang')??'en';//Locale('en', '');
  // static final locale = Locale('en', '');
  // // fallbackLocale saves the day when the locale gets in trouble
  // static final fallbackLocale = Locale('tr', 'TR');

  // Supported languages
  // Needs to be same order with locales
  static final langs = [
    'en',
    'vi',
  ];

  // Supported locales
  // Needs to be same order with langs
  static final locales = [
    Locale('en', ''),
    Locale('vi', ''),
  ];

  // Keys and their translations
  // Translations are separated maps in `lang` file
  @override
  Map<String, Map<String, String>> get keys => {
        'en': convert({}, null, enUS), // lang/en_us.dart
        'vi': convert({}, null, viVN), // lang/vi_vn.dart
      };

  Map<String, String > convert(Map<String, String> ret, String parentKey, Map<String, dynamic> data) {

    String key;
    data.forEach((k, v) {
      key = parentKey==null?k: parentKey + '.' + k;
      if (v is String) {
        ret.putIfAbsent(key, () => v);
      } else if (v is Map) {
        convert(ret, key, v);
      }
    });
    return ret;
  }

  // Gets locale from language, and updates the locale
  void changeLocale(String lang) {
    final locale = _getLocaleFromLanguage(lang);
    Get.updateLocale(locale);
  }

  // Finds language in `langs` list and returns it as Locale
  Locale _getLocaleFromLanguage(String lang) {
    for (int i = 0; i < langs.length; i++) {
      if (lang == langs[i]) return locales[i];
    }
    return Get.locale;
  }
}
