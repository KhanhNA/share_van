import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:customer/base/notify/notifies.dart';
import 'package:customer/base/ui/qr/ScanQr.dart';

class ScanQrWidget extends StatelessWidget {
  final Widget child;
  final bool Function(String qrCode) onData;
  final EdgeInsets padding;
  final bool showCode;
  final bool oneTime;
  final Widget Function(String qrCode) buildShowWidget;
  final bool enable;

  ScanQrWidget(
      {Key key,
        this.enable = true,
        @required this.onData,
        @required this.child,
        @required this.oneTime,
        this.padding = EdgeInsets.zero,
        this.showCode,
        this.buildShowWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: !enable
          ? () =>
          NotifyInfor('title'.tr, 'Scan qr code').showNotify() : () async {
        var result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return Padding(
                padding: padding,
                child: QRViewExample(
                  onData,
                  showCode: showCode,
                  oneTime: oneTime,
                  buildShowWidget: buildShowWidget,
                ));
          }),
        );
        if (result) {
          print('Qr_scan: '+result);
        }
      },
      child: child,
    );
  }
}
