import 'package:get/get.dart';
import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/model/list/PagedListData.dart';

abstract class BaseController<VM extends BaseVM> extends GetxController {
  final _bottomVisible = true.obs;
  final VM viewModel;
  RxString textSearch=''.obs;

  BaseController(this.viewModel);

  Future<void> refreshAll() async {
    // this.bottomVisible.value = !this.bottomVisible.value;
    update();
  }

  get bottomVisible => _bottomVisible;

  set bottomVisible(bool value) {
    _bottomVisible.value = value;
  }

  void removeNullOrBlankValue(Map params,{bool removeNull=true, bool removeBlank=true}){

    params.removeWhere((key, value) => (value== null && removeNull) || (GetUtils.isBlank(value) && removeBlank));
  }

// final Map<String, dynamic> params = {};
}


abstract class BaseGetController extends GetxController {
  final _bottomVisible = true.obs;
  // @override
  // initController(List list);
  Future<void> refreshAll() async {
    // this.bottomVisible.value = !this.bottomVisible.value;
    update();
  }

  get bottomVisible => _bottomVisible;

  set bottomVisible(bool value) {
    _bottomVisible.value = value;
  }

  // final Map<String, dynamic> params = {};
}


abstract class BaseController1<VM extends BaseVM> extends GetxController {
  final _bottomVisible = true.obs;

  VM viewModel;
  Future<void> reload();

  void init(VM vm);

  BaseController1(this.viewModel);
  // initController(List list);

  void refreshVM(void Function() updateVM){
    updateVM();
  }

  Future<void> refreshAll() async {
    // this.bottomVisible.value = !this.bottomVisible.value;
    update();
  }

  get bottomVisible => _bottomVisible;

  set bottomVisible(bool value) {
    _bottomVisible.value = value;
  }

  void removeNullOrBlankValue(Map params,{bool removeNull=true, bool removeBlank=true}){

    params.removeWhere((key, value) => (value== null && removeNull) || (GetUtils.isBlank(value) && removeBlank));
  }

// final Map<String, dynamic> params = {};
}


abstract class SearchController<T> extends BaseGetController {
  final Map<String, dynamic> params = {};
  // ListController<T> listController;
  //
  // SearchController(){
  //   listController = ListController(loadFunc: loadMore);
  // }

  void onSearchChanged(String paramName, value) {
    // searchInfo.text = text;
    if(GetUtils.isNullOrBlank(value)){
      params.remove(paramName);
    }else {
      params[paramName] = value;
    }
    // listController.refresh();
  }

  Future<PagedListData<T>> loadMore(int page);
}
