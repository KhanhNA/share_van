import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
class SwipperBanner extends StatelessWidget {
  final List<String> banners;
  SwipperBanner({this.banners});
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = 130;
    return Container(
      width: width,
      height: height,
      child: Swiper(
        itemBuilder: (BuildContext context, index) {
          return Image.network(
            banners[index],
            width: width,
            height: height,
          );
        },
        itemCount: banners.length,
        //viewportFraction: 0.9,
        pagination: new SwiperPagination(
            alignment: Alignment.bottomRight,
            builder: RectSwiperPaginationBuilder(
                color: Color(0xFF999999),
                activeColor: Colors.white,
                size: Size(5.0, 2),
                activeSize: Size(5, 5))),
        scrollDirection: Axis.horizontal,
        autoplay: true,
        onTap: (index) => print('点击了第$index个'),
      ),
    );
  }
}