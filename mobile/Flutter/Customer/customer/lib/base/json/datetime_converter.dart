import 'package:json_annotation/json_annotation.dart';

import 'package:customer/base/util/DateUtils.dart';

class CustomDateTimeConverter implements JsonConverter<DateTime, String> {
  const CustomDateTimeConverter();

  @override
  DateTime fromJson(String datetime) {


    return DateTimeUtils.convertStringToDate(datetime, DateTimeUtils.DATE_FORMAT);
  }

  @override
  String toJson(DateTime datetime) => datetime == null?null: DateTimeUtils.convertDateToString(datetime, DateTimeUtils.DATE_FORMAT);
}

class CustomConverter implements JsonConverter<num, String> {
  const CustomConverter();

  @override
  num fromJson(String data) {
    return data == null? null:(data.toLowerCase()=='false')?null:num.parse(data);
  }

  @override
  String toJson(num data) => data == null?null: data.toString();
}