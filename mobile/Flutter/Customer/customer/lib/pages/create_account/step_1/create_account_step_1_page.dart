import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/get/my_text_form_field.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/base/util/DateUtils.dart';
import 'package:customer/model/area/AddressDto.dart';
import 'package:customer/model/login/PartnerDto.dart';
import 'package:customer/pages/create_account/create_account_controller.dart';
import 'package:customer/pages/create_account/create_account_vm.dart';
import 'package:customer/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class CreateAccountTapView
    extends BaseView<CreateAccountVM, CreateAccountController> {
  CreateAccountTapView();

  // final _key=
  final _formFirstKey = GlobalKey<FormState>();
  final _formLastKey = GlobalKey<FormState>();
  DateTime selectedDate = DateTime.now();

  @override
  Widget body(BuildContext context) {
    return Container(
        child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * 0.6,
                child: Form(
                    key: _formFirstKey,
                    child: TextFormField(
                      decoration: InputDecoration(
                          labelText: 'Create_account.first_name'.tr),
                      onChanged: (text) => viewModel.firstName = text,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Not empty';
                        }
                        return null;
                      },
                    )),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.3,
                child: Form(
                    key: _formLastKey,
                    child: TextFormField(
                      decoration: InputDecoration(
                          labelText: 'Create_account.last_name'.tr),
                      onChanged: (text) => viewModel.lastName = text,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Not empty';
                        }
                        return null;
                      },
                    )),
              ),
            ],
          ),
        ),
        Row(
          children: <Widget>[
            Text('Create_account.day_of_birth'.tr + ' : ',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 14)),
            Container(
                height: 40,
                width: MediaQuery.of(context).size.width * 0.5,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.white,
                    border: Border.all(color: Colors.black12)),
                child: Obx(() => Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.25,
                          child: Text(
                            viewModel.dayOfBirth.value,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.normal,
                                fontSize: 14),
                          ),
                        ),
                        Container(
                          child: IconButton(
                              icon: Icon(Icons.calendar_today_outlined),
                              onPressed: () => {_selectDate(context)}),
                        )
                      ],
                    )))
          ],
        ),
        Container(
          child: Obx(() => Column(
                children: <Widget>[
                  RadioListTile(
                    title: Text('Create_account.male'.tr),
                    value: viewModel.gender[0],
                    groupValue: viewModel.genderObs.value,
                    onChanged: (val) {
                      viewModel.selectedPeriod = val;
                    },
                  ),
                  RadioListTile(
                    title: Text('Create_account.female'.tr),
                    value: viewModel.gender[1],
                    groupValue: viewModel.genderObs.value,
                    onChanged: (val) {
                      viewModel.genderObs.value = val;
                    },
                  ),
                ],
              )),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            InkWell(
                onTap: () => {
                      viewModel.areaType = 'province',
                      _selectArea(context, 'province')
                    },
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white,
                      border: Border.all(color: Colors.black12)),
                  child: Obx(() => Center(
                        child: Text(
                          viewModel?.province?.value?.name ?? 'Select province',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                              fontSize: 14),
                        ),
                      )),
                )),
            InkWell(
                onTap: () => {
                      viewModel.areaType = 'district',
                      _selectArea(context, 'district')
                    },
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.65,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white,
                      border: Border.all(color: Colors.black12)),
                  child: Obx(() => Center(
                        child: Text(
                          viewModel?.district?.value?.name ?? 'Select district',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                              fontSize: 14),
                        ),
                      )),
                )),
          ],
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
          child: InkWell(
            onTap: () => _selectCareer(context),
            child: Container(
              height: 40,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.white,
                  border: Border.all(color: Colors.black12)),
              width: MediaQuery.of(context).size.width * 0.95,
              child: Obx(() => Center(
                    child: Text(
                      viewModel?.career?.value?.name ?? 'Select career',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontSize: 14),
                    ),
                  )),
            ),
          ),
        ),
        ElevatedButton(
          child: Text(
            'Create_account.next_step'.tr,
          ),
          style: ButtonStyle(
            minimumSize: MaterialStateProperty.all(Size(20, 40)),
          ),
          onPressed: () async {
            _nextStep(context);
          },
        ),
      ],
    ));
  }

  _selectArea(BuildContext context, String value) {
    if (value == 'district') {
      viewModel.districts.refresh();
    }
    context.showBottomSheet<AddressDto>(
        context.listItemView(('Create_account.' + value).tr, "",
            styleLb: AppTheme.title,
            padding: AppTheme.edgeAll), (context, item, index) {
      return CommonUI.card(
          child: InkWell(
              onTap: () => {
                    if (value == 'district')
                      {viewModel.district.value = item}
                    else
                      {
                        viewModel.province.value = item,
                      },
                    Navigator.pop(context)
                  },
              child: Container(
                height: 40,
                width: double.infinity,
                child: Center(
                    child: Text(
                  item.name,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.normal),
                )),
              )));
    }, value == 'province' ? viewModel.provinces : viewModel.districts);
  }

  _selectCareer(BuildContext context) {
    context.showBottomSheet<AddressDto>(
        context.listItemView('Create_account.career'.tr, "",
            styleLb: AppTheme.title,
            padding: AppTheme.edgeAll), (context, item, index) {
      return CommonUI.card(
        child: InkWell(
            onTap: () =>
                {viewModel.career.value = item, Navigator.pop(context)},
            child: Container(
              height: 40,
              width: double.infinity,
              child: Center(
                  child: Text(
                item.name,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.normal),
              )),
            )),
      );
    }, viewModel.careers);
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null) {
      selectedDate = picked;
    }
    viewModel.dayOfBirth.value = DateTimeUtils.convertDateToString(
        selectedDate, DateTimeUtils.DATE_SHOW_FORMAT);
  }

  void _nextStep(BuildContext context) {
    if (_formFirstKey.currentState.validate() &&_formLastKey.currentState.validate()) {
      DefaultTabController.of(context).animateTo(1);
    }
  }

  @override
  CreateAccountController putController() {
    return CreateAccountController(CreateAccountVM());
  }
}
