import 'package:customer/model/area/AddressDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/pages/create_account/api/create_account_provider.dart';
import 'package:dio/dio.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class CreateAccountRepository {
  final CreateAccountProvider _api = CreateAccountProvider();

  Future<PagedListData<AddressDto>> getCareer(Map<String, dynamic> params) =>
      _api.getCareer(params);

  Future<PagedListData<AddressDto>> getArea(Map<String, dynamic> params) =>
      _api.getArea(params);
  Future<ResponseBody> createAccount(Map<String, dynamic> params,List<Asset> listAsset) =>
      _api.createAccount(params,listAsset);
}