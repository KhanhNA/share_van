import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/model/login/PartnerDto.dart';
import 'package:customer/model/login/UserAuthentication.dart';
import 'package:customer/base/auth/api_auth_provider.dart';
import 'package:customer/base/auth/token.dart';
import 'package:customer/model/login/login_body.dart';
import 'package:customer/model/session/OdooSessionDto.dart';

class UserProvider {
  ApiAuthProvider api = ApiAuthProvider();

  Future<OdooSessionDto> loginUser(LoginBody loginBody) async {
    final response = await api.loginUser(loginBody);
    return response;
  }

  Future<UserAuthentication> getUserMe() async {
    final response = await api.getUserMe();
    return response;
  }

  Future<PartnerDto> getInfoCustomer() async {
    Map<String,dynamic> params={

    };
    final response = await api.getListJsonParams<PartnerDto>(
        'share_van_order/get_customer_information',
        params,
        (js) => PartnerDto.fromJson(js));
    PartnerDto partnerDto =response.content[0];
    return partnerDto;
  }
}
