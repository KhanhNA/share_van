import 'package:customer/base/auth/api_auth_provider.dart';
import 'package:customer/model/import_export/RoutingDayDto.dart';

class ImportExportProvider{
  final String GET_IMPORT_EXPORT_DETAIL ='share_van_order/routing_plan_day/detail';
  ApiAuthProvider api = ApiAuthProvider();

  Future<RoutingDayDto> getRoutingDayDetailDto(Map<String, dynamic> params) async {
    try {
      final response = await api.getListJsonParams(GET_IMPORT_EXPORT_DETAIL, params, (js) =>
          RoutingDayDto.fromJson(js));
      return response.content[0];
    } catch (error, stacktrace) {
      _printError(error, stacktrace);
    }

  }
  void _printError(error, StackTrace stacktrace) {
    print('error: $error & stacktrace: $stacktrace');
  }


}