import 'package:customer/model/import_export/RoutingDayDto.dart';
import 'package:customer/pages/import_export_warehouse/api/import_export_provider.dart';

class ImportExportRepository{
  ImportExportProvider _api = ImportExportProvider();
  Future<RoutingDayDto> getRoutingDayDetailDto(Map<String, dynamic> params) => _api.getRoutingDayDetailDto(params);
}