import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/model/import_export/RoutingDayDto.dart';
import 'package:customer/pages/import_export_warehouse/api/import_export_repository.dart';
import 'package:get/get.dart';

class ImportExportVM extends BaseVM{
    ImportExportRepository repository = ImportExportRepository();
    Rx<RoutingDayDto> routingDayObs= Rx(RoutingDayDto());
    String routingDayCode;
}