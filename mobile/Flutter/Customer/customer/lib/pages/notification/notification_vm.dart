import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/model/notification/NotificationDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:get/get.dart';

import 'api/notification_repository.dart';

class NotificationVM extends BaseVM {
  RxInt totalElementsNotificationRouting = 0.obs;
  RxInt totalElementsNotificationSystem= 0.obs;
  final notificationStatus = ['Routing', 'System'];
  final notificationDto = NotificationDto();
  final repository = NotificationRepository();
  String notificationType;

  NotificationVM();

  ListController<NotificationDto> listNotificationSystemController;
  ListController<NotificationDto> listNotificationRoutingController;
}
