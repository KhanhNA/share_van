import 'package:customer/base/controller/BaseZone.dart';
import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/pages/notification/notification_detail_page.dart';
import 'package:get/get.dart';
import 'notification_vm.dart';


class NotificationController extends BaseController<NotificationVM>{
  NotificationController(NotificationVM viewModel) : super(viewModel);
  final inDex = 0.obs;
}