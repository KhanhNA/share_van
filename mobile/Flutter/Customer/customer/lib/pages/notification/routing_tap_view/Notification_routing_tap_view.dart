import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:flutter/widgets.dart';

import '../notification_vm.dart';
import 'notification_routing_controller.dart';
import 'notification_routing_tap_view_item.dart';

class NotificationRoutingTapView extends BaseView<NotificationVM,NotificationRoutingController>{

  NotificationRoutingTapView();

  @override
  Widget body(BuildContext context) {
    return SliverView(
      // floatBar: SearchImportStock(controller),
      listController: viewModel.listNotificationRoutingController,
      itemBuilder: NotificationRoutingTapViewItem(controller).buildItem,
      // widgetBefore: [context.sliverTotalRows(viewModel.totalElementsNotificationRouting)],
    );
  }

  @override
  putController() {
    return NotificationRoutingController(NotificationVM());
  }

}