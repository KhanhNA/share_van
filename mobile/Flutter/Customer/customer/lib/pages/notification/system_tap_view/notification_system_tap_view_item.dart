import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/icon/custom_icons.dart';
import 'package:customer/model/notification/NotificationDto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:customer/base/ui/text/text_ui.dart';
import 'package:customer/theme/app_theme.dart';

import 'notification_system_controller.dart';

class NotificationSystemTapViewItem {
  final NotificationSystemController controller1;

  NotificationSystemTapViewItem(this.controller1);

  Widget buildItem(
      BuildContext context, NotificationDto notificationDto, int index) {
    return CommonUI.card(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Icon(
                Icons.adb,
                color: AppTheme.grey,
              ),
              Text(
                notificationDto.create_date,
                style: TextStyle(color: Colors.black, fontSize: 10),
              )
            ],
          ),
          context.divider(),
          Container(
            child : (notificationDto?.image_256 !=null )? Image.network(
              notificationDto.image_256,
              width: double.infinity,
              height: 150,
            ) : Image.asset("assets/icons/no_image.png"),
          ),
          context.divider(),
          context.itemTitleView(notificationDto.title),
          context.itemView(notificationDto.content),
          context.divider(),
          context.listItemHeader(
            (''),
            context.textDrawableStart(
                "viewDetail".tr,
                CustomIcons.fromName('FontAwesome.0xf105'),
                Theme.of(context).primaryColor,
                isStart: false),
            onTap: () {
              controller1.details(notificationDto);
            },
          )
        ],
      ),
    );
  }
}
