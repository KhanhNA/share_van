import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import 'routing_day_controller.dart';
import 'routing_day_item/routing_day_item.dart';
import 'routing_day_vm.dart';

class RoutingDayPage
    extends BaseView<RoutingDayVM, RoutingDayController> {
  @override
  AppBar appBar(BuildContext context) {
    return  AppBar(
      title: Container(
        child: Text('menu.Routing_day'.tr,style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.normal),),
      ),
    );
  }
  @override
  Widget body(BuildContext context) {
    // MediaQuery.of(context).size;
    return SliverView(
      toolbarHeight: 200,
      // floatBar: _floatBar(context),
      listController: viewModel.listController,
      itemBuilder: (c,item,i)=>RoutingDayItem(c,item,i,controller),
    );
  }

  Widget _floatBar(BuildContext context) {
    return Container(
            padding: AppTheme.edgeLeftRight,
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.08),
                spreadRadius: 0,
                blurRadius: 16,
                offset: Offset(0, 8),
              )
            ]),
            child: _sumInfo(context, 160));
  }

  @override
  RoutingDayController putController() {
    return RoutingDayController(RoutingDayVM());
  }

  Widget _sumInfo(BuildContext context, double height) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
          height: height,
          decoration: new BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8.0),
          ),
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  child: Row(children: [
                Expanded(child: _widgetWallet('Tích điểm', '120', 'Điểm')),
                Expanded(child: _widgetWallet('Tài khoản', '1.820.000', 'VNĐ')),
              ])),
              Expanded(
                  child: Row(children: [
                Expanded(
                  child: _widgetOftenUsed('Đơn hàng'),
                ),
                Expanded(
                  child: _widgetOftenUsed('Lộ trình'),
                ),
                Expanded(
                  child: _widgetOftenUsed('Kho hàng'),
                ),
                Expanded(
                  child: _widgetOftenUsed('Chi nhánh'),
                ),
              ]))
            ],
          )),
    );
  }

  Widget _widgetOftenUsed(String lbl) {
    return InkWell(
      onTap: () {
        print("Container clicked: " + lbl);
      },
      child: Center(
        child: Container(
          decoration: new BoxDecoration(
              border: Border(
                  right: true
                      ? BorderSide(color: AppTheme.divider)
                      : BorderSide.none)),
          child: Center(
            child: Column(
              children: <Widget>[
                Icon(Icons.ac_unit),
                Text(
                  lbl,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.normal),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _widgetWallet(String lbl, String data, String sub) {
    return InkWell(
      onTap: () {
        print("Container clicked: " + lbl);
      },
      child: Center(
        child: Container(
          decoration: new BoxDecoration(
              border: Border(
                  bottom: true
                      ? BorderSide(color: AppTheme.divider)
                      : BorderSide.none,
                  right: true
                      ? BorderSide(color: AppTheme.divider)
                      : BorderSide.none)),
          child: Column(
            children: <Widget>[
              Text(
                lbl,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.normal),
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.ac_unit),
                  Text(
                    data + ' ' + sub,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
