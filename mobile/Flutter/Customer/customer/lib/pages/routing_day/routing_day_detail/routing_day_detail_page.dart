import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/model/bill_routing/BillRoutingDto.dart';
import 'package:customer/pages/import_export_warehouse/import_export_page.dart';
import 'package:customer/pages/list_order/bill_lading_detail/bill_lading_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'routing_day_detail_controller.dart';
import 'routing_day_detail_vm.dart';

class RoutingDayDetailPage
    extends BaseView<RoutingDayDetailVM, RoutingDayDetailController> {
  BillRoutingDto billRoutingDto;
  RoutingDayDetailPage(this.billRoutingDto) {
    viewModel.billRoutingId = billRoutingDto.id;
  }


initData() async {
  viewModel.billRoutingDto.value = await controller.getBillRoutingDetail();
}


  @override
  AppBar appBar(BuildContext context) {
    return AppBar(
        title: Text(
      "Routing van day detail page",
      style: TextStyle(
          color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
    ));
  }

  @override
  Widget body(BuildContext context) {
    initData();
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Container(
                  height: 40,
                  width: MediaQuery.of(context).size.width * 0.9,
                  // color: Colors.white,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: Colors.black12),
                  child: context.textOnTap('Routing day detail',
                      onTap: () => controller.gotoPage(billRoutingDto))),
            ),
          ),
          Obx(() => Wrap(
                children: viewModel?.billRoutingDto?.value?.arrBillLadingDetail
                        ?.map((e) {
                      return CommonUI.card(
                          child: Column(children: <Widget>[
                        context.listItemViewV2('Address', e.address ?? 'No address'),
                            context.textOnTap('go to import export',onTap: ()=> Get.to(() => ImportExportPage(e?.routing[0]?.routing_plan_day_code??'')))
                      ]));
                    })?.toList() ??
                    <Widget>[
                      Container(),
                    ],
              ))
        ],
      ),
    );
  }

  @override
  RoutingDayDetailController putController() {
    return RoutingDayDetailController(RoutingDayDetailVM());
  }
}
