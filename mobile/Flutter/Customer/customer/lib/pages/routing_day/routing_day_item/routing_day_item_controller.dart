import 'package:customer/base/controller/BaseZone.dart';
import 'package:customer/model/bill_routing/BillRoutingDetailDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:customer/pages/routing_day/routing_day_map/routing_day_map_page.dart';
import 'package:get/get.dart';

class RoutingDayItemController extends GetxController{
  ListController<BillRoutingDetailDto> listController;
  showMap(billRoutingDetailDto){
    BaseZone.run(() => Get.to(()=>RoutingDayMapPage(billRoutingDetailDto)));
  }
}