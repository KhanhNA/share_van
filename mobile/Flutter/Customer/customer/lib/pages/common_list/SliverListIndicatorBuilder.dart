

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_more_list/loading_more_list.dart';
import 'package:customer/base/ui/widget/EmptyData.dart';

class SliverListIndicatorBuilder {

  ///whether it need sliver as container
  final bool isSliver;
  final Function() tryAgain;

  SliverListIndicatorBuilder({this.isSliver = true, this.tryAgain});


  Widget build(BuildContext context, IndicatorStatus status) {
    Widget widget;
    print('Build: status $status');
    switch (status) {
      case IndicatorStatus.none:
        widget = Container(height: 0.0);
        break;
      case IndicatorStatus.loadingMoreBusying:
        loadMoreBusyingWidget(context);
        break;
      case IndicatorStatus.fullScreenBusying:
        widget = buildFullScreenBusying(context);
        break;
      case IndicatorStatus.error:
        widget = buildLoadError(context);
        break;
      case IndicatorStatus.fullScreenError:
        widget = buildFullScreenLoadError(context);
        break;
      case IndicatorStatus.noMoreLoad:
        widget = Text('noMoreItem'.tr);
        widget = _setbackground(false, widget, 35.0);
        break;
      case IndicatorStatus.empty:
        widget = EmptyData();
        widget = _setbackground(true, widget, double.infinity);
        if (isSliver) {
          widget = SliverFillRemaining(
            child: widget,
          );
        } else {
          widget = CustomScrollView(
            slivers: <Widget>[
              SliverFillRemaining(
                child: widget,
              )
            ],
          );
        }
        break;
    }
    return widget;
  }

  Widget _setbackground(bool full, Widget widget, double height) {
    widget = Container(
        width: double.infinity,
        height: height,
        child: widget,
        // color: backgroundColor ?? Colors.grey[200],
        alignment: Alignment.center);
    return widget;
  }

  Widget getIndicator(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return theme.platform == TargetPlatform.iOS
        ? const CupertinoActivityIndicator(
      animating: true,
      radius: 16.0,
    )
        : CircularProgressIndicator(
      strokeWidth: 2.0,
      valueColor: AlwaysStoppedAnimation<Color>(theme.primaryColor),
    );
  }

  void loadMoreBusyingWidget(BuildContext context) {
    Widget widget = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(right: 5.0),
          height: 15.0,
          width: 15.0,
          child: getIndicator(context),
        ),
        Text('loading'.tr)
      ],
    );
    widget = _setbackground(false, widget, 35.0);
  }

  Widget buildFullScreenBusying(BuildContext context) {
    Widget widget = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(right: 0.0),
          height: 30.0,
          width: 30.0,
          child: getIndicator(context),
        ),
        Text('loading'.tr)
      ],
    );
    widget = _setbackground(true, widget, double.infinity);
    if (isSliver) {
      widget = SliverFillRemaining(
        child: widget,
      );
    } else {
      widget = CustomScrollView(
        slivers: <Widget>[
          SliverFillRemaining(
            child: widget,
          )
        ],
      );
    }
    return widget;
  }

  Widget buildLoadError(BuildContext context) {
    Widget widget = Text(
      'tryLoadAgain'.tr,
    );
    _setbackground(false, widget, 35.0);
    if (tryAgain != null) {
      widget = GestureDetector(
        onTap: () {
          tryAgain();
        },
        child: widget,
      );
    }
    return widget;
  }

  Widget buildFullScreenLoadError(BuildContext context) {
    Widget widget = Text(
      'tryLoadAgain'.tr,
    );
    widget = _setbackground(true, widget, double.infinity);
    if (tryAgain != null) {
      widget = GestureDetector(
        onTap: () {
          tryAgain();
        },
        child: widget,
      );
    }
    if (isSliver) {
      widget = SliverFillRemaining(
        child: widget,
      );
    } else {
      widget = CustomScrollView(
        slivers: <Widget>[
          SliverFillRemaining(
            child: widget,
          )
        ],
      );
    }
    return widget;
  }
}