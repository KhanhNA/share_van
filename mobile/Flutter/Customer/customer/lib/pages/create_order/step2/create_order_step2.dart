import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/base/ui/text/text_ui.dart';
import 'package:customer/model/bill_package/BillPackageDto.dart';
import 'package:customer/pages/create_order/create_order_controller.dart';
import 'package:customer/pages/create_order/create_order_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'create_order_step2_add_warehouse.dart';

class CreateOrderStep2 extends BaseView<CreateOrderVM, CreateOrderController> {
  @override
  Widget body(BuildContext context) {
    return Obx(() => viewModel.billLadingDto.value.arrBillLadingDetail.length > 1
        ? widgetListBillLadingDetailCreated(context)
        : _notProduct(context));
  }

  Widget widgetListBillLadingDetailCreated(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            onTap: () => Get.to(()=>SelectImportWarehouse()),
            child: Padding(
                padding: const EdgeInsets.only(top:8.0,left:16),
                child: Container(
                  height: 40,
                  width: MediaQuery.of(context).size.width*0.5,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8), color: Colors.green),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Add import warehouse',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: 13),
                        ),
                        Icon(Icons.add_chart,size: 24,color: Colors.white,),
                      ]),
                ),
              ),
          ),
          Expanded(
              child: ListView.builder(
                  itemCount: viewModel.billLadingDto.value.arrBillLadingDetail.length - 1,
                  itemBuilder: (_, index) {
                    return CommonUI.card(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              context.listItemViewV2('Warehouse name', viewModel.billLadingDto?.value?.arrBillLadingDetail[index+1].warehouse.name),
                              context.listItemViewV2('Warehouse address', viewModel.billLadingDto?.value?.arrBillLadingDetail[index+1].warehouse.address),
                              context.listItemViewV2('Warehouse phone', viewModel.billLadingDto?.value?.arrBillLadingDetail[index+1].warehouse.phone),
                              listPackageWidget(context,viewModel.billLadingDto?.value?.arrBillLadingDetail[index+1].billPackages)
                            ],
                          ),
                        ));
                  })),
          ElevatedButton(
              onPressed: () async {
                // await controller.calculatorPrice();
                viewModel.orderPackageListController.refresh();
                  DefaultTabController.of(context).animateTo(2);
              },
              child: Container(child: Center(child: Text('Next step'))))
        ],
      ),
    );
  }

  Widget listPackageWidget(BuildContext context,List<BillPackageDto> listData){
    List<Widget> listWidgets= listData.map((e){
      return Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: Container(
            height: 30,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black12),
                borderRadius: BorderRadius.circular(8)),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Center(child: context.normalTextOneLine(e.quantity_package.toString()+' - '+e.item_name)),
            )),
      );
    }).toList();
    return Container(
      height: 30,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: listWidgets,
      )
    );
  }


  Widget _notProduct(BuildContext context) {
    return CommonUI.card(
      child: Container(
        height: 200,
        width: MediaQuery.of(context).size.width * 0.9,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/images/warehouse.png',
                height: 60, width: 60, fit: BoxFit.fill),
            ElevatedButton(
              onPressed: () => Get.to(()=>SelectImportWarehouse()),
              child: context.normalText('Select warehouse import'),
            )
          ],
        ),
      ),
    );
  }

  @override
  CreateOrderController putController() {
    return CreateOrderController(CreateOrderVM());
  }
}
