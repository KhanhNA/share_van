import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/model/notification/NotificationDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:customer/pages/home/api/home_repository.dart';
import 'package:customer/pages/notification/api/notification_repository.dart';
import 'package:get/get.dart';

class HomeVM extends BaseVM{
    String textSearch='';
    RxString searchKeyword=''.obs;
    ListController<NotificationDto> listPromotion;
    final NotificationRepository repository= NotificationRepository();
}