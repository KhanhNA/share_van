import 'package:customer/base/controller/BaseZone.dart';
import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/base/util/AppUtils.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:customer/pages/list_order/bill_lading_detail/bill_lading_detail_page.dart';
import 'package:customer/pages/list_order/list_order_tap_view/List_order_tap_view_vm.dart';
import 'package:get/get.dart';

class ListOrderTapViewController extends BaseController<ListOrderTapViewVM>{

  ListOrderTapViewController(ListOrderTapViewVM viewModel) : super(viewModel){
    viewModel.initData();
  }
  Future<PagedListData<BillLadingDto>> getBillLadings(int page) async {
    String bill_state = "running";
    String status;
    status = viewModel.status.value;
    if (BillLadingStatus.DELETED == viewModel.status) {
      bill_state = null;
    }
     else if (BillLadingStatus.FINISHED == viewModel.status) {
      bill_state = 'finished';
      status = 'running';
    }
    if (viewModel.searchKeyword == '') {
      viewModel.searchKeyword = null;
    }
    Map<String, dynamic> params = {
      'limit': 10,
      'company_id': AppUtils.odooSessionDto.company_id,
      'offset': (page - 1) * 10,
      'status': status,
      'bill_state': bill_state,
      "text_search": viewModel.searchKeyword,
      'start_date': viewModel.fromDate.value,
      'end_date': viewModel.toDate.value,
    };
    params.removeNullOrBlankValue();
    final data = await viewModel.repository.getBillLadings(params);
    return data;
  }
  void changeStatus(BillLadingStatus s){
    viewModel.status = s;
    if(s!=null && !viewModel.listControllers.containsKey(s)){
      // if( viewModel.listControllers[viewModel.status]!=null){
      //   viewModel.searchKeyword='';
      // }
      viewModel.listControllers[viewModel.status] = ListController(loadFunc: this.getBillLadings);
      viewModel.listControllers[viewModel.status].refresh();
    }

  }
  details(billLadingId) {
    Get.to(()=>BillLadingDetailPage(billLadingId));
  }

}