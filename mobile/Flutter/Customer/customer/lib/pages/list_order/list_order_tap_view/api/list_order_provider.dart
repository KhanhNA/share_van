
import 'package:customer/base/auth/api_auth_provider.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/model/list/PagedListData.dart';

class ListOrderTapViewProvider {
  final String URI_GET_BILL_LADING = 'share_van_order/bill_lading_history_ad';
  ApiAuthProvider api = ApiAuthProvider();

  Future<PagedListData<BillLadingDto>> getBillLadings(
      Map<String, dynamic> params) async {
    final response = await api.getListJsonParams<BillLadingDto>(
        URI_GET_BILL_LADING, params, (js) => BillLadingDto.fromJson(js));
    return response;
  }
}
