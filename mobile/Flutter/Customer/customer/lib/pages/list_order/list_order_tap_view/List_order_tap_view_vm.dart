import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:customer/pages/list_order/list_order_tap_view/api/list_order_repository.dart';
import 'package:get/get.dart';

class ListOrderTapViewVM extends BaseVM {
  RxString fromDate="".obs;
  RxString toDate="".obs;
  BillLadingStatus status;
  String _orderCode='';
  String get searchKeyword => this._orderCode;
  set searchKeyword(String value) {
    _orderCode = value;
    listControllers[status].refresh();
  }
  final Map<BillLadingStatus,ListController<BillLadingDto>> listControllers= <BillLadingStatus,ListController<BillLadingDto>>{};
  final repository= ListOrderTapViewRepository();
  final billLadingDto = BillLadingDto();

  final allStatus =[BillLadingStatus.RUNNING, BillLadingStatus.FINISHED, BillLadingStatus.DELETED];


  initData(){
    toDate.value='2021-03-13';
    fromDate.value='2021-02-01';
    _orderCode='';
  }
}
