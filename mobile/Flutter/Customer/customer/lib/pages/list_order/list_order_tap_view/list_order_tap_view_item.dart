import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/base/ui/text/text_ui.dart';
import 'package:customer/icon/custom_icons.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/pages/list_order/list_order_tap_view/list_order_tap_view_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter/src/widgets/framework.dart';

class ListOrderTapViewItem{
  final ListOrderTapViewController controller1;

  ListOrderTapViewItem(this.controller1);

  Widget buildItem(
      BuildContext context, BillLadingDto billLadingDto, int index) {
    return CommonUI.card(
      child: Column(
        children: [
          context.listItemHeader(
            (''),
            context.textDrawableStart(
                "viewDetail".tr,
                CustomIcons.fromName('FontAwesome.0xf105'),
                Theme.of(context).primaryColor,
                isStart: false),
            onTap: () {
              controller1.details(billLadingDto.id);
            },
          ),
          context.divider(),
          context.listItemView('Bill_lading.bill_lading_code'.tr, billLadingDto.name),
          context.listItemView('Bill_lading.total_amount'.tr, billLadingDto.total_amount.toString()),
          context.listItemView('Bill_lading.subscription'.tr, billLadingDto.subscribe_name),
          context.listItemView('Bill_lading.insurance'.tr, billLadingDto.insurance_name==null?'Not_have'.tr: billLadingDto.insurance_name),
          context.listItemView('Bill_lading.order_package'.tr, billLadingDto.order_package.name),
          context.listItemView('Bill_lading.create_order'.tr, billLadingDto.create_date),
        ],
      ),
    );
  }
}