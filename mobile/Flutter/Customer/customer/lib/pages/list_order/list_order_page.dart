import 'package:customer/base/ui/common_enum.dart';
import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/pages/list_order/list_order_tap_view/List_order_tap_view_vm.dart';
import 'package:customer/pages/list_order/list_order_tap_view/list_order_tap_view.dart';
import 'package:customer/pages/list_order/list_order_tap_view/list_order_tap_view_controller.dart';
import 'package:customer/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListOrderPage extends BaseView<ListOrderTapViewVM, ListOrderTapViewController> {

  @override
  AppBar appBar(BuildContext context) {
    return AppBar(
      title: Text('Info_event.List_order'.tr,style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
      actions: <Widget>[
        // IconButton(
        //   icon: Icon(Icons.search),
        //   onPressed: () {
        //     // _ClaimBloc.add(BaseEvent());
        //   },
        // ),
      ],
    );
  }

  @override
  Widget body(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Column(
          children: <Widget>[
            Container(
              constraints: BoxConstraints(maxHeight: 150.0),
              child: Material(
                color: AppTheme.white,
                child: TabBar(
                  tabs: viewModel.allStatus
                      .map((e) => Tab(text: ('Bill_lading_status.' + CommonEnum.name(e)).tr))
                      .toList(),
                ),
              ),
            ),
            Expanded(
              child: TabBarView(children: List.generate(viewModel.allStatus.length, (index) => ListOrderTapView(viewModel.allStatus[index]))),
            )
          ],
        ));
  }

  _makeTabView(BuildContext context) {}

  @override
  ListOrderTapViewController putController() {
    return ListOrderTapViewController(ListOrderTapViewVM());
  }
}
