import 'package:customer/base/auth/api_auth_provider.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/model/list/PagedListData.dart';

class BillLadingDetailProvider {
  final String URI_GET_BILL_LADING_DETAIL ='share_van_order/get_bill_lading_details';
  final ApiAuthProvider _api = ApiAuthProvider();

  Future<BillLadingDto> getBillLadings(
      Map<String, dynamic> params) async {
    final response = await _api.getListJsonParams<BillLadingDto>(
        URI_GET_BILL_LADING_DETAIL, params, (js) => BillLadingDto.fromJson(js));

    if(response.content.length>0){
      final billLadingDto= response.content[0];
      return billLadingDto;
    }
    return null;

  }
}
