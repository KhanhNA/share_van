import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:customer/pages/warehouse_manager/warehouse_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WarehouseItemBuilder {
  WarehouseController controller;

  WarehouseItemBuilder(this.controller);

  Widget itemBuilder(BuildContext context, WarehouseDto dto, int index) {
    return InkWell(
      onTap: () => {controller.gotoUpdateWarehouse(dto)},
      child: Padding(
        padding: const EdgeInsets.only(left: 16, top: 8, right: 16),
        child: Container(
          child: Column(
            children: [
              Row(
                children: <Widget>[
                  Image.asset(
                    'assets/images/warehouse.png',
                    height: 23,
                    width: 23,
                    fit: BoxFit.fill,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        context.titleText(dto.name),
                        context.normalText(dto.phone),
                        context.normalText(dto.address),
                      ],
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: context.divider(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
