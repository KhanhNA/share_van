import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:loading_more_list/loading_more_list.dart';

class AppTheme {

  static const double DEFAULT_TEXT_HEIGHT = 40;
  static const double searchItemHeight = 40;
  static const double qrScanSize = 35;
  static const double qrCodeSize = 120;


  AppTheme._();

  static const Color mainColor = Color(0xFF1CA275);
  static const Color warningColor = Color(0xFFFFCED2);
  static const Color darkColor = Color(0xFFF4F4F4);
  static const Color textFieldNomalBorder = Color(0xFFE9E9EA);
  static const Color greyBgColor = Color(0xFFF2F4F8);
  static const Color textBold = Color(0xFF24282C);
  static const Color removeColor = Color(0xFFFF6D79);
  static const Color backgroundApp = Color(0xE5E5E5);


  static const Color statePending = Color(0xFFFFA903);
  static const Color stateRejected = Color(0xFFFF645C);
  static const Color stateApproved = Color(0xFF007DDD);
  static const Color stateOngoing = Color(0xFF02AC45);
  static const Color stateClosed = Color(0xFFA7A7A7);
  static const Color stateCancelled = Color(0xFFFF645C);
  static const Color stateDelivering = Color(0xFF007DDD);
  static const Color stateDelivered = Color(0xFF4CD964);
  static const Color stateConfirmed = Color(0xFF7B61FF);
  static const Color stateReturned = Color(0xFFA7A7A7);
  static const Color stateWaiting = Color(0xFFF27C0F);
  static const Color stateActive = Color(0xFF02AC45);
  static const Color stateExpired = Color(0xFFA7A7A7);
  static const Color stateInactive = Color(0xFF6AC7B7);

  // @JsonValue(0) NOT_ENOUGH_GOODS, @JsonValue(1) ENOUGH_GOODS, @JsonValue(2) RETURN_SUCCESS,
  // @JsonValue(3) REJECTED, @JsonValue(4) BE_RETURNED,



  static const Map<String, Color> dashboardOptionStatus={
    'poWaitForImport': Color(0xFF02AC45),
    'soWaitForExport': Color(0xFFA7A7A7),
    'stoWaitForExport': Color(0xFFFF645C),
    'stoDelivery': Color(0xFFFF645C),
    'stoWaitForImport': Color(0xFFFF645C),
  };



  static const Color alertSuccess = Color(0xFFDBF7E0);
  static const Color alertSuccessBorder= Color(0xFFCDF4D4);
  static const Color alertWarning = Color(0xFFFFF2D9);
  static const Color alertWarningBorder = Color(0xFFFFEDCA);
  static const Color alertError = Color(0xFFF5F0F5);
  static const Color alertErrorBorder = Color(0xFFF5F0F5);
  static const Color alertInfo = Color(0xFFCCE5F8);
  static const Color alertInfoBorder = Color(0xFFB8DBF5);
  static const Color alertText = Color(0xFF3A3E41);


  static const Color divider = Color(0xFFE9E9EA);
  static const Color notWhite = Color(0xFFEDF0F2);
  static const Color nearlyWhite = Color(0xFFFEFEFE);
  static const Color white = Color(0xFFFFFFFF);
  static const Color nearlyBlack = Color(0xFF213333);
  static const Color grey = Color(0xFF3A5160);
  static const Color dark_grey = Color(0xFF313A44);
  static const Color darkText = Color(0xFF5B5E61);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF4A6572);
  static const Color deactivatedText = Color(0xFF767676);
  static const Color dismissibleBackground = Color(0xFF364A54);
  static const Color chipBackground = Color(0xFFEEF1F3);
  static const Color spacer = Color(0xFFF2F2F2);
  static const Color background = Color(0xFFE5E5E5);
  static const Color backgroundStrokeTextFile = Color(0xFFE9E9EA);
  static const Color backgroundTextFile = Color(0xFFFBFBFB);
  static const Color colorView = Color(0xFFFF4F4F4);
  static const Color colorDash = Color(0xFFC8C9CA);
  static const Color colorBlue = Color(0xFF3A8CFF);
  static const Color colorItem1 = Color(0xFFF6AA2F);
  static const Color colorItem3 = Color(0xFF4CD964);
  static const Color colorItem4 = Color(0xFFFF645C);
  static const Color colorTextWarning= Color(0xFFFF0C20);

  static const Color txtHintColor = Color(0xFF919395);
  static const Color txtNormalColor = Color(0xFF3A3E41);


  static const String fontName = 'WorkSans';

  static const TextTheme textTheme = TextTheme(
    headline4: display1,
    headline5: headline,
    headline6: title,
    subtitle2: subtitle,
    bodyText2: body2,
    bodyText1: body1,
    caption: caption,
  );

  static const TextStyle dashboard = TextStyle(
    // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 22,
    letterSpacing: 0.27,
    color: darkerText,
  );

  static const TextStyle btnTextPrimary = TextStyle(
    // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.normal,
    fontSize: 16,
    letterSpacing: 0.27,
    color: AppTheme.white,

  );

  static const TextStyle textChildBlue = TextStyle(
    // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.normal,
    fontSize: 12.0,
    letterSpacing: 0.27,
    color: AppTheme.colorBlue,
  );

  static const TextStyle btnTextPrimaryOutline = TextStyle(
    // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.normal,
    fontSize: 16,
    letterSpacing: 0.27,
    color: AppTheme.mainColor,

  );

  static const TextStyle txtStyleToastSuccess = TextStyle(
    // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 13.0,
    letterSpacing: 0.27,
    color: Colors.white,
  );

  static Theme themeCalender(Widget child) {
    return Theme(
        data: ThemeData.light().copyWith(
          primaryColor: AppTheme.mainColor,
          accentColor: AppTheme.mainColor,
          colorScheme: ColorScheme.light(primary: AppTheme.mainColor),
          buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
        ),
        child: child);
  }

  static const TextStyle display1 = TextStyle(
    // h4 -> display1
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 36,
    letterSpacing: 0.4,
    height: 0.9,
    color: darkerText,
  );

  static const TextStyle headline = TextStyle(
    // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 24,
    letterSpacing: 0.27,
    color: darkerText,
  );

  static const TextStyle title = TextStyle(
    // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: 0.18,
    color: darkerText,
  );

  static const TextStyle normal = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: darkText,
  );
  static const TextStyle normalWhite = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: white,
  );
  static const TextStyle normalBold = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
    fontSize: 14,
    letterSpacing: -0.04,
    color: textBold,
  );

  static const TextStyle subtitle = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: darkText,
  );

  static const TextStyle subtitle1 = TextStyle(
    // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 14,
    color: textBold,
    letterSpacing: -0.04,
  );

  static const TextStyle body2 = TextStyle(
    // body1 -> body2
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: 0.2,
    color: darkText,
  );

  static const TextStyle body1 = TextStyle(
    // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: -0.05,
    color: darkText,
  );

  static const TextStyle body3 = TextStyle(
    // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: -0.05,
    color: txtHintColor,
  );

  static const TextStyle caption = TextStyle(
    // Caption -> caption
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    letterSpacing: 0.2,
    color: lightText, // was lightText
  );

  static const TextStyle textDetail = TextStyle(
    // Caption -> caption
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
    fontSize: 14,
    letterSpacing: 0.2,
    color: mainColor, // was lightText
  );

  static const EdgeInsets edgeLeft = EdgeInsets.fromLTRB(16, 0, 0, 0);
  static const EdgeInsets edgeRight = EdgeInsets.fromLTRB(0.0, 0, 16, 0);
  static const EdgeInsets edgeTop = EdgeInsets.fromLTRB(0.0, 16, 0, 0);
  static const EdgeInsets edgeButtom = EdgeInsets.fromLTRB(0, 0, 0, 16);
  static const EdgeInsets edgeAll = EdgeInsets.fromLTRB(16, 16, 16, 16);
  static const EdgeInsets edgeLRB = EdgeInsets.fromLTRB(16, 0, 16, 16);
  static const EdgeInsets edgeLeftRight = EdgeInsets.fromLTRB(16, 0, 16, 0);
  static const EdgeInsets edgeAll5 = EdgeInsets.fromLTRB(5, 5, 5, 5);
  static const EdgeInsets edgeContent = EdgeInsets.fromLTRB(16, 8, 16, 0);
  static const EdgeInsets edgeContentEnd = EdgeInsets.fromLTRB(16, 8, 16, 8);
  static const EdgeInsets edgeHomeDash = EdgeInsets.fromLTRB(12, 20, 8, 20);
  static const EdgeInsets edgeHomeDash1 = EdgeInsets.fromLTRB(16, 12, 16, 0);

  static const EdgeInsets contentPadding = EdgeInsets.symmetric(horizontal: 15);
  static const EdgeInsets contentMargin = EdgeInsets.only(bottom: 15);
  static const EdgeInsets cardMargin = EdgeInsets.only(top: 5, right: 15, bottom: 5, left: 15);
  static const EdgeInsets cardMarginBottom = EdgeInsets.only(bottom: 10);
  static const EdgeInsets cardPadding = EdgeInsets.all(15);
  static const EdgeInsets cardHeaderPadding = EdgeInsets.only(top: 10, right: 15, bottom: 10, left: 15);
  static const EdgeInsets cardBodyPadding = EdgeInsets.only(top: 10, right: 15, bottom: 0, left: 15);
  static const EdgeInsets cardFieldPadding = EdgeInsets.only(top: 0, right: 15, bottom: 10, left: 15);
  static const EdgeInsets cardDividerPadding = EdgeInsets.only(top: 0, right: 0, bottom: 10, left: 0);

  static const EdgeInsets edgeLeftSmall = EdgeInsets.fromLTRB(8, 0, 0, 0);

  static const BorderRadius radius8 = BorderRadius.all(
    const Radius.circular(8.0),
  );

  static const BorderRadius radius12 = BorderRadius.all(
    const Radius.circular(12.0),
  );

  static const OutlineInputBorder textFieldBorder = OutlineInputBorder(
    borderSide: BorderSide(
        color: textFieldNomalBorder, width: 1, style: BorderStyle.solid),
    borderRadius: radius8,
  );

  static const TextStyle textFieldHintStyle = TextStyle(color: txtHintColor, fontSize: 15);

  static const TextStyle textFieldDropDownStyle = TextStyle(fontSize: 15);

  static const roundedBorderDecoration = BoxDecoration(
      border: Border.fromBorderSide(BorderSide(color: textFieldNomalBorder)),
      borderRadius: radius8);

  static const roundedBorderDecoration12dp = BoxDecoration(
      border: Border.fromBorderSide(BorderSide(color: textFieldNomalBorder)),
      borderRadius: radius12
  );


  static const Color primary = Color(0xFF1CA275);
  static const Color secondary = Color(0xFFFFA400);
  static const Color danger = Color(0xFFFC5555);
  static const Color info = Color(0xFF3366FF);
  static const Color gray = Color(0xFF7A7A7A);
}

// Theme 1
final ThemeData appThemeData =  ThemeData(
  // colorScheme: ColorScheme (
  //   primary: AppTheme.primary,
  //   primaryVariant: AppTheme.primary,
  //   onPrimary: Colors.white,
  //   secondary: AppTheme.secondary,
  //   secondaryVariant: AppTheme.secondary,
  //   onSecondary: Colors.white,
  //   error: AppTheme.danger,
  //   onError: Colors.white,
  //   surface: AppTheme.info,
  //   onSurface: Colors.black,
  //   background: Colors.white,
  //   onBackground: Colors.white,
  //   brightness: Brightness.light,
  // ),

  primaryColor: AppTheme.primary,
 // primarySwatch: AppTheme.danger,
  visualDensity: VisualDensity.adaptivePlatformDensity,
  accentColor: Color(0xFF222222),
  backgroundColor: Colors.white,
  unselectedWidgetColor: Color(0xFFC8C9CA),

  appBarTheme: AppBarTheme(
    backgroundColor: AppTheme.primary,

    textTheme: TextTheme (
      headline6: TextStyle (
        fontSize: 16,
        fontWeight: FontWeight.bold,
        color: Color(0xFFFFFFFF),
      )
    )
  ),
  dividerColor: Color(0xFFF4F4F4),

  textTheme: TextTheme (
    headline1: TextStyle (
      fontSize: 40,
      fontWeight: FontWeight.bold,
    ),
    headline2: TextStyle (
      fontSize: 32,
      fontWeight: FontWeight.bold,
    ),
    headline3: TextStyle (
      fontSize: 22,
      fontWeight: FontWeight.bold,
    ),
    headline4: TextStyle (
      fontSize: 20,
      fontWeight: FontWeight.bold,
    ),
    headline5: TextStyle (
      fontSize: 18,
      fontWeight: FontWeight.bold,
    ),
    headline6: TextStyle (
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: Color(0xFF3A3E41),
    ),
    bodyText1: TextStyle (
      fontSize: 14,
      fontWeight: FontWeight.normal,
      color: Color(0xFF3A3E41),
    ),
    bodyText2: TextStyle (
      fontSize: 14,
      fontWeight: FontWeight.w600,
      color: Color(0xFF24282C),
    ),
    caption: TextStyle (
      fontSize: 14,
      color: Color(0xFF5B5E61),
    ),
  ),

  primaryTextTheme: TextTheme (
    headline6: TextStyle (
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: Color(0xFFFFFFFF),
    ),
    bodyText1: TextStyle (
      fontSize: 16,
      color: AppTheme.primary
    ),
    bodyText2: TextStyle (
      fontSize: 16,
      color: Colors.white
    ),
  ),

  accentTextTheme: TextTheme (
    bodyText1: TextStyle (
      fontSize: 14,
      color: AppTheme.info
    ),

  ),

  textSelectionTheme: TextSelectionThemeData (

  ),

  iconTheme: IconThemeData (
    color: Color(0xFF919395),
  ),

  buttonTheme: ButtonThemeData (
    colorScheme: ColorScheme (
      primary: AppTheme.primary,
      primaryVariant: AppTheme.primary,
      onPrimary: Colors.black,
      secondary: AppTheme.secondary,
      secondaryVariant: AppTheme.primary,
      onSecondary: Colors.white,
      error: AppTheme.danger,
      onError: Colors.white,
      surface: AppTheme.info,
      onSurface: Colors.white,
      background: Colors.white,
      onBackground: Colors.white,
      brightness: Brightness.light,
    ),
  ),

  elevatedButtonTheme: ElevatedButtonThemeData (
    style: ButtonStyle (
      backgroundColor: MaterialStateProperty.all(AppTheme.primary),
      minimumSize: MaterialStateProperty.all(Size(40, NormalButtonStyle.DEFAULT_BUTTON_HEIGHT)),
      textStyle: MaterialStateProperty.all(
        TextStyle (
          fontFamily: AppTheme.fontName,
          fontSize: 14,
        )
      ),
    ),
  ),

  outlinedButtonTheme: OutlinedButtonThemeData (
    style: OutlinedButton.styleFrom (
      minimumSize: Size(40, NormalButtonStyle.DEFAULT_BUTTON_HEIGHT),
      textStyle: TextStyle(
        fontFamily: AppTheme.fontName,
        fontSize: 14,
      ),
    ),
  ),

  textButtonTheme: TextButtonThemeData (
    style: TextButton.styleFrom(
      minimumSize: Size(40, NormalButtonStyle.DEFAULT_BUTTON_HEIGHT),
      textStyle: TextStyle(
        fontFamily: AppTheme.fontName,
        fontSize: 14,
      ),
      backgroundColor: Colors.transparent,
      side: BorderSide(
        color: Colors.transparent,
        width: 0,
      )
    )
  ),

  inputDecorationTheme: InputDecorationTheme (
    filled: true,
    fillColor: Color(0xFFFFFFFF),
    enabledBorder: OutlineInputBorder (
      borderSide: BorderSide (
        color: Color(0xFFE9E9EA),
        width: 1,
      ),
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
    focusedBorder: OutlineInputBorder (
      borderSide: BorderSide (
        color: AppTheme.primary,
        width: 1,
      ),
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
    disabledBorder: OutlineInputBorder (
      borderSide: BorderSide (
        color: Color(0xFFE9E9EA),
        width: 1,
      ),
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
    errorBorder: OutlineInputBorder (
      borderSide: BorderSide (
        color: AppTheme.danger,
        width: 1,
      ),
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
  ),

  tabBarTheme: TabBarTheme (
    labelColor: AppTheme.primary,
    labelStyle: TextStyle (
      fontSize: 14,
      color: AppTheme.primary
    ),
    unselectedLabelColor: Color(0xFF5B5E61),
    unselectedLabelStyle: TextStyle (
      fontSize: 14,
      color: Color(0xFF5B5E61)
    ),

    indicator: BoxDecoration (
      color: Colors.white,
      border: Border (
        bottom: BorderSide (
          color: AppTheme.primary,
          width: 2
        )
      )
    ),
  ),

  checkboxTheme: CheckboxThemeData (
    fillColor: MaterialStateProperty.all(Color(0xFF00A359)),
    checkColor: MaterialStateProperty.all(Colors.white),
  ),

);

class AppTheme2 {
  static const Color primary = Color(0xFF000000);
  static const Color secondary = Color(0xFF4c4f54);
  static const Color danger = Color(0xFFd16767);
  static const Color info = Color(0xFF4799eb);
  static const Color gray = Color(0xFF7A7A7A);

  static const String fontName = 'Arial';
}

// Theme 2
final ThemeData secondaryTheme =  ThemeData(
  // colorScheme: ColorScheme (
  //   primary: AppTheme2.primary,
  //   primaryVariant: AppTheme2.primary,
  //   onPrimary: Colors.white,
  //   secondary: AppTheme2.secondary,
  //   secondaryVariant: AppTheme2.secondary,
  //   onSecondary: Colors.white,
  //   error: AppTheme2.danger,
  //   onError: Colors.white,
  //   surface: AppTheme2.info,
  //   onSurface: Colors.black,
  //   background: Colors.white,
  //   onBackground: Colors.white,
  //   brightness: Brightness.light,
  // ),

  primaryColor: AppTheme2.primary,
  // primarySwatch: AppTheme2.danger,
  visualDensity: VisualDensity.adaptivePlatformDensity,
  accentColor: Color(0xFF222222),
  backgroundColor: Colors.black,
  unselectedWidgetColor: Color(0xFFC8C9CA),

  appBarTheme: AppBarTheme(
      backgroundColor: AppTheme2.primary,
      textTheme: TextTheme (
          headline6: TextStyle (
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Color(0xFFFFFFFF),
          )
      )
  ),
  dividerColor: Color(0xFFF4F4F4),

  textTheme: TextTheme (
    headline1: TextStyle (
      fontSize: 40,
      fontWeight: FontWeight.bold,
    ),
    headline2: TextStyle (
      fontSize: 32,
      fontWeight: FontWeight.bold,
    ),
    headline3: TextStyle (
      fontSize: 22,
      fontWeight: FontWeight.bold,
    ),
    headline4: TextStyle (
      fontSize: 20,
      fontWeight: FontWeight.bold,
    ),
    headline5: TextStyle (
      fontSize: 18,
      fontWeight: FontWeight.bold,
    ),
    headline6: TextStyle (
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: Color(0xFF3A3E41),
    ),
    bodyText1: TextStyle (
      fontSize: 14,
      fontWeight: FontWeight.normal,
      color: Color(0xFF3A3E41),
    ),
    bodyText2: TextStyle (
      fontSize: 14,
      fontWeight: FontWeight.w600,
      color: Color(0xFF24282C),
    ),
    caption: TextStyle (
      fontSize: 14,
      color: Color(0xFF5B5E61),
    ),
  ),

  primaryTextTheme: TextTheme (
    headline6: TextStyle (
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: Color(0xFFFFFFFF),
    ),
    bodyText1: TextStyle (
        fontSize: 16,
        color: AppTheme2.primary
    ),
    bodyText2: TextStyle (
        fontSize: 16,
        color: Colors.white
    ),
  ),

  accentTextTheme: TextTheme (
    bodyText1: TextStyle (
        fontSize: 14,
        color: AppTheme2.info
    ),

  ),

  textSelectionTheme: TextSelectionThemeData (

  ),

  iconTheme: IconThemeData (
    color: Color(0xFF919395),
  ),

  buttonTheme: ButtonThemeData (
    colorScheme: ColorScheme (
      primary: AppTheme2.primary,
      primaryVariant: AppTheme2.primary,
      onPrimary: Colors.black,
      secondary: AppTheme2.secondary,
      secondaryVariant: AppTheme2.primary,
      onSecondary: Colors.white,
      error: AppTheme2.danger,
      onError: Colors.white,
      surface: AppTheme2.info,
      onSurface: Colors.white,
      background: Colors.white,
      onBackground: Colors.white,
      brightness: Brightness.light,
    ),
  ),

  elevatedButtonTheme: ElevatedButtonThemeData (
    style: ButtonStyle (
      backgroundColor: MaterialStateProperty.all(AppTheme2.primary),
      minimumSize: MaterialStateProperty.all(Size(40, 20)),
      textStyle: MaterialStateProperty.all(
          TextStyle (
            fontFamily: AppTheme2.fontName,
            fontSize: 14,
          )
      ),
    ),
  ),

  outlinedButtonTheme: OutlinedButtonThemeData (
    style: OutlinedButton.styleFrom (
      minimumSize: Size(40, 20),
      textStyle: TextStyle(
        fontFamily: AppTheme2.fontName,
        fontSize: 14,
      ),
    ),
  ),

  textButtonTheme: TextButtonThemeData (
      style: TextButton.styleFrom(
          minimumSize: Size(40, 20),
          textStyle: TextStyle(
            fontFamily: AppTheme2.fontName,
            fontSize: 14,
          ),
          backgroundColor: Colors.transparent,
          side: BorderSide(
            color: Colors.transparent,
            width: 0,
          )
      )
  ),

  inputDecorationTheme: InputDecorationTheme (
    filled: true,
    fillColor: Color(0xFFFFFFFF),
    enabledBorder: OutlineInputBorder (
      borderSide: BorderSide (
        color: Color(0xFFE9E9EA),
        width: 1,
      ),
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
    focusedBorder: OutlineInputBorder (
      borderSide: BorderSide (
        color: AppTheme2.primary,
        width: 1,
      ),
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
    disabledBorder: OutlineInputBorder (
      borderSide: BorderSide (
        color: Color(0xFFE9E9EA),
        width: 1,
      ),
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
    errorBorder: OutlineInputBorder (
      borderSide: BorderSide (
        color: AppTheme2.danger,
        width: 1,
      ),
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
  ),

  tabBarTheme: TabBarTheme (
    labelColor: AppTheme2.primary,
    labelStyle: TextStyle (
        fontSize: 14,
        color: AppTheme2.primary
    ),
    unselectedLabelColor: Color(0xFF5B5E61),
    unselectedLabelStyle: TextStyle (
        fontSize: 14,
        color: Color(0xFF5B5E61)
    ),

    indicator: BoxDecoration (
        color: Colors.white,
        border: Border (
            bottom: BorderSide (
                color: AppTheme2.primary,
                width: 2
            )
        )
    ),
  ),

  checkboxTheme: CheckboxThemeData (
    fillColor: MaterialStateProperty.all(Color(0xFF00A359)),
    checkColor: MaterialStateProperty.all(Colors.white),
  ),

);

class NormalButtonStyle{
  static const double DEFAULT_BUTTON_HEIGHT = 40;
  static const double DEFAULT_BUTTON_WIDTH = 20;

  static ButtonStyle primary = OutlinedButton.styleFrom(
    minimumSize: Size(DEFAULT_BUTTON_WIDTH, DEFAULT_BUTTON_HEIGHT),
    backgroundColor: AppTheme.primary,
    textStyle: AppTheme.normalWhite
  );

  static ButtonStyle reject = OutlinedButton.styleFrom(
    minimumSize: Size(DEFAULT_BUTTON_WIDTH, DEFAULT_BUTTON_HEIGHT),
    backgroundColor: AppTheme.removeColor,
    textStyle: AppTheme.normal
  );

  static ButtonStyle primaryOutline = OutlinedButton.styleFrom(
    side: BorderSide(width: .5, color: AppTheme.mainColor),
    backgroundColor: AppTheme.white,
    //alignment: Alignment.center,
    textStyle: AppTheme.normal)
  ;

  static ButtonStyle appBar = OutlinedButton.styleFrom(
    // h5 -> headline
    //   side: BorderSide.none,
    //   side: BorderSide(width: .5, color: AppTheme.white),
    // backgroundColor: Colors.transparent,
      textStyle: AppTheme.btnTextPrimary,
      primary: AppTheme.white
  );

  static ButtonStyle transparent = TextButton.styleFrom(
      side: BorderSide.none,
      // minimumSize: Size(1.0, 1.0),
      minimumSize: Size(DEFAULT_BUTTON_WIDTH, DEFAULT_BUTTON_HEIGHT),
      padding: EdgeInsets.zero,
      textStyle: AppTheme.normal,
      tapTargetSize: MaterialTapTargetSize.shrinkWrap
  );
}

class MinButtonStyle{
  static const double DEFAULT_BUTTON_HEIGHT = 1;
  static const double DEFAULT_BUTTON_WIDTH = 1;
  static final ButtonStyle primary = OutlinedButton.styleFrom(
    // h5 -> headline
    //   padding: edgeAll,
    //   padding: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
      minimumSize: Size(DEFAULT_BUTTON_WIDTH, DEFAULT_BUTTON_HEIGHT),
      backgroundColor: AppTheme.mainColor,
      textStyle: AppTheme.normalWhite);

  static ButtonStyle reject = OutlinedButton.styleFrom(
    // h5 -> headline
    //   padding: edgeAll,
      minimumSize: Size(DEFAULT_BUTTON_WIDTH, DEFAULT_BUTTON_HEIGHT),
      backgroundColor: AppTheme.removeColor,
      textStyle: AppTheme.normal
  );

  static ButtonStyle primaryOutline = OutlinedButton.styleFrom(
    // h5 -> headline
      side: BorderSide(width: .5, color: AppTheme.mainColor),
      backgroundColor: AppTheme.white,
      //alignment: Alignment.center,
      textStyle: AppTheme.normal)
  ;

  static ButtonStyle appBar = OutlinedButton.styleFrom(
    // h5 -> headline
    //   side: BorderSide.none,
    //   side: BorderSide(width: .5, color: AppTheme.white),
    // backgroundColor: Colors.transparent,
    textStyle: AppTheme.btnTextPrimary,
    primary: AppTheme.white
  );

  static ButtonStyle transparent = TextButton.styleFrom(
    side: BorderSide.none,
    minimumSize: Size(DEFAULT_BUTTON_WIDTH, DEFAULT_BUTTON_HEIGHT),
    padding: EdgeInsets.zero,
    textStyle: AppTheme.normal,
    tapTargetSize: MaterialTapTargetSize.shrinkWrap
  );
}